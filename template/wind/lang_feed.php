<?php
!function_exists('readover') && exit('Forbidden');

$lang['feed'] = array(
	'post'				=> '發表了主題 [url=$GLOBALS[db_bbsurl]/read.php?tid=$L[tid]]$L[subject][/url]',
	'honor'				=> '更改了簽名 $L[honor]',
	'friend'			=> '和 [url=$GLOBALS[db_bbsurl]/{$GLOBALS[db_userurl]}$L[uid]]$L[friend][/url] 成為了好友!',
	'colony_create'		=> '創建了新群組[url={$L[link]}][b]{$L[cname]}[/b][/url],一起去體驗吧',
	'colony_pass'		=> '加入了群組[url={$L[link]}][b]{$L[cname]}[/b][/url],快點去看看吧',
	'share_view'		=> ' $L[type_name] \n $L[share_code][url=$L[link]]$L[title][/url]\n $L[abstract]\n $L[imgs]\n $L[descrip]',
	'o_write'			=> '發表了一條記錄：$L[text]',
	'photo'				=> ' 上傳了{$L[num]}張照片到 {$L[text]}',
	'post_board'		=> ' 在[url={$GLOBALS[db_bbsurl]}/{$GLOBALS[db_userurl]}{$L[touid]}]{$L[tousername]}[/url]的留言板上留了言',
	'diary_data'		=> '發表了一篇日誌 [url={$GLOBALS[db_bbsurl]}/{#APPS_BASEURL#}q=diary&a=detail&uid={$L[uid]}&did={$L[did]}]{$L[subject]}[/url]\n $L[content]',
	'diary_copy'		=> '轉載了一篇日誌 [url={$GLOBALS[db_bbsurl]}/{#APPS_BASEURL#}&q=diary&a=detail&uid={$L[uid]}&did={$L[did]}]{$L[subject]}[/url]\n $L[content]',
	'colony_post'		=> '在群組[url={$GLOBALS[db_bbsurl]}/apps.php?q=group&cyid={$L[cyid]}]{$L[colony_name]}[/url]中發表了一個討論[url={$GLOBALS[db_bbsurl]}/apps.php?q=group&a=read&cyid={$L[cyid]}&tid={$L[tid]}]{$L[title]}[/url]',
	'colony_photo'		=> '在群組[url={$GLOBALS[db_bbsurl]}/apps.php?q=group&cyid={$L[cyid]}]{$L[colony_name]}[/url]中上傳了{$L[num]}張照片\n{$L[text]}',
);
?>