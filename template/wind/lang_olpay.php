<?php
!function_exists('readover') && exit('Forbidden');

$lang['olpay'] = array(
	
	'olpay_0_title'		=> '積分充值(訂單號：$L[order_no])',
	'olpay_0_content'	=> '購買論壇{$GLOBALS[creditName]}(論壇UID：$GLOBALS[winduid])',

	'olpay_1_title'		=> '道具購買(訂單號：$L[order_no])',
	'olpay_1_content'	=> '購買論壇道具{$GLOBALS[toolinfo][name]}(論壇UID：$GLOBALS[winduid])',

	'olpay_2_title'		=> '版塊訪問權限購買(訂單號：$L[order_no])',
	'olpay_2_content'	=> '購買版塊（{$GLOBALS[fname]}）的訪問權限(論壇UID：$GLOBALS[winduid])',

	'olpay_3_title'		=> '特殊組購買(訂單號：$L[order_no])',
	'olpay_3_content'	=> '購買論壇特殊用戶組（{$GLOBALS[grouptitle]}）身份(論壇UID：$GLOBALS[winduid])',

	'olpay_4_title'		=> '註冊碼購買(訂單號：$L[order_no])',
	'olpay_4_content'	=> '購買論壇註冊碼',
	
	// 孔明燈 by chenyun 2011-07-8
	'olpay_5_title'		=> '孔明燈購買(訂單號：$L[order_no])',
	'olpay_5_content'	=> '購買論壇孔明燈',
);
?>