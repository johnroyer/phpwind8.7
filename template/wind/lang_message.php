<?php
!function_exists('readover') && exit('Forbidden');
$lang['message'] = array (

'colony_joinlimit'			=> '加入群組失敗，你允許加入的群組個數已滿。',
'colony_memberlimit'		=> '加入群組失敗，該群組人數已滿。',
'colony_joinrefuse'			=> '該群組拒絕新成員加入。',
'colony_joinfail'			=> '你的{$GLOBALS[moneyname]}不足，加入群組需要 {$GLOBALS[o_joinmoney]} {$GLOBALS[moneyname]}',
'colony_passfail'			=> '用戶 <b>{$GLOBALS[rt][username]}</b> {$GLOBALS[moneyname]}不足，不能通過審核。',
'colony_alreadyjoin'		=> '你已經加入了該群組。',
'colony_joinsuccess'		=> '加入群組成功!',
'colony_joinsuccess_check'	=> '你的加入群組申請已提交，請等待群主審核',
'colony_joinsuccess_check2'	=> '你的加入群組申請已提交，請等待群主審核',
'colony_add_ignore'			=> '你已經忽略了對方的群組邀請',
'colony_request_agree'		=> '你已經同意了對方的群組邀請'	,
'colony_check_success'		=> '你已經同意了TA加入群組的請求'	,
'colony_check_fail'			=> '你已不具備審核資格'	,
'colony_check_ignore'		=> '你已經忽略了對方加入群組的請求'	,
'colony_check_agree'		=> '你已經同意了對方加入群組的請求'	,

'friend_add_success'		=> '從現在開始，你們倆就是好友了！<a href="{$GLOBALS[db_userurl]}{$GLOBALS[fid]}">馬上去看TA</a>',
'friend_add_fail'			=> '添加好友失敗',
'friend_add_ignore'			=> '你已經忽略了對方的請求',
'friend_request_agree'		=> '你已經同意了TA的好友請求'	,

'app_add_success'			=> '你同意安裝了此應用',
'app_add_fail'				=> '應用安裝失敗',
'app_add_ignore'			=> '你已經忽略了對方的請求',
'app_request_agree'			=> '你已經同意了TA的應用請求'	,

'request_friend'			=> '好友請求',
'request_group'				=> '群組請求',
'request_app'				=> '應用請求',
'request_ignore'			=> '你已經忽略了對方的請求',
)

?>