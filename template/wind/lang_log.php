<?php
!function_exists('readover') && exit('Forbidden');

$lang['log'] = array (

'bk_save_descrip_1'		=> '[b]{$L[username1]}[/b]使用活期存款功能，存入金額：{$L[field1]}{$GLOBALS[db_moneyname]}',
'bk_save_descrip_2'		=> '[b]{$L[username1]}[/b]使用定期存款功能，存入金額：{$L[field1]}{$GLOBALS[db_moneyname]}',
'bk_draw_descrip_1'		=> '[b]{$L[username1]}[/b]使用活期取款功能，取出金額：{$L[field1]}{$GLOBALS[db_moneyname]}',
'bk_draw_descrip_2'		=> '[b]{$L[username1]}[/b]使用定期取款功能，取出金額：{$L[field1]}{$GLOBALS[db_moneyname]}',
'bk_vire_descrip'		=> '[b]{$L[username1]}[/b]使用轉帳功能，轉帳給[b]{$L[username2]}[/b]'
							.'金額：{$L[field1]}{$GLOBALS[db_moneyname]}，轉帳附言：{$GLOBALS[memo]}',
'topped_descrip'		=> '帖子：[url=$GLOBALS[db_bbsurl]/read.php?tid=$L[tid]]$L[subject][/url]\n'
							.'操作：將文章設為置頂{$L[topped]}\n原因：{$L[reason]}',
'untopped_descrip'		=> '帖子：[url=$GLOBALS[db_bbsurl]/read.php?tid=$L[tid]]$L[subject][/url]\n'
							.'操作：解除文章置頂\n原因：{$L[reason]}',
'digest_descrip'		=> '帖子：[url=$GLOBALS[db_bbsurl]/read.php?tid=$L[tid]]$L[subject][/url]\n'
							.'操作：將文章設為精華{$L[digest]}\n原因：{$L[reason]}\n影響：{$L[affect]}',
'undigest_descrip'		=> '帖子：[url=$GLOBALS[db_bbsurl]/read.php?tid=$L[tid]]$L[subject][/url]\n'
							.'操作：取消文章精華\n原因：{$L[reason]}\n影響：{$L[affect]}',
'highlight_descrip'		=> '帖子：[url=$GLOBALS[db_bbsurl]/read.php?tid=$L[tid]]$L[subject][/url]\n'
							.'操作：將文章標題加亮\n原因：{$L[reason]}',
'unhighlight_descrip'	=> '帖子：[url=$GLOBALS[db_bbsurl]/read.php?tid=$L[tid]]$L[subject][/url]\n'
							.'操作：將文章標題取消加亮\n原因：{$L[reason]}',
'push_descrip'			=> '帖子：[url=$GLOBALS[db_bbsurl]/read.php?tid=$L[tid]]$L[subject][/url]\n'
							.'操作：將文章提前\n原因：{$L[reason]}',
'lock_descrip'			=> '帖子：[url=$GLOBALS[db_bbsurl]/read.php?tid=$L[tid]]$L[subject][/url]\n'
							.'操作：將文章鎖定\n原因：{$L[reason]}',
'unlock_descrip'		=> '帖子：[url=$GLOBALS[db_bbsurl]/read.php?tid=$L[tid]]$L[subject][/url]\n'
							.'操作：將文章解除鎖定\n原因：{$L[reason]}',
'delrp_descrip'			=> '帖子：[url=$GLOBALS[db_bbsurl]/read.php?tid=$L[tid]]$L[subject][/url]\n'
							.'操作：刪除回復\n原因：{$L[reason]}\n影響：{$L[affect]}',
'deltpc_descrip'		=> '帖子：[url=$GLOBALS[db_bbsurl]/read.php?tid=$L[tid]]$L[subject][/url]\n'
							.'操作：刪除主題\n原因：{$L[reason]}\n影響：{$L[affect]}',
'del_descrip'			=> '帖子：[url=$GLOBALS[db_bbsurl]/read.php?tid=$L[tid]]$L[subject][/url]\n'
							.'操作：將文章刪除\n原因：{$L[reason]}\n影響：{$L[affect]}',
'move_descrip'			=> '帖子：[url=$GLOBALS[db_bbsurl]/read.php?tid=$L[tid]]$L[subject][/url]\n'
							.'操作：將文章移動到新版塊([url=$GLOBALS[db_bbsurl]/thread.php?fid={$L[tofid]}][b]$L[toforum][/b][/url])\n'
							.'原因：{$L[reason]}',
'copy_descrip'			=> '帖子：[url=$GLOBALS[db_bbsurl]/read.php?tid=$L[tid]]$L[subject][/url]\n'
							.'操作：將文章複製到新版塊([url=$GLOBALS[db_bbsurl]/thread.php?fid={$L[tofid]}][b]$L[toforum][/b][/url])\n'
							.'原因：{$L[reason]}',
'edit_descrip'			=> '帖子：[url=$GLOBALS[db_bbsurl]/read.php?tid=$L[tid]]$L[subject][/url]\n'
							.'操作：編輯文章',
'credit_descrip'		=> '帖子：[url=$GLOBALS[db_bbsurl]/read.php?tid=$L[tid]]$L[subject][/url]\n'
							.'操作：文章被評分\n原因：{$L[reason]}\n影響：{$L[affect]}',
'creditdel_descrip'		=> '帖子：[url=$GLOBALS[db_bbsurl]/read.php?tid=$L[tid]]$L[subject][/url]\n'
							.'操作：文章評分被取消\n原因：{$L[reason]}\n影響：{$L[affect]}',
'banuser_descrip'		=> '用戶 [b]{$L[username1]}[/b] 被禁言\n操作：用戶禁言\n原因：{$L[reason]}',
'deluser_descrip'		=> '用戶 [b]{$L[username1]}[/b] 被刪除\n操作：批量刪除用戶',
'join_descrip'			=> '[b]{$L[username1]}[/b] 加入{$GLOBALS[cn_name]}[b]{$L[cname]}[/b]，花費{$GLOBALS[moneyname]}：{$L[field1]}。',
'donate_descrip'		=> '[b]{$L[username1]}[/b] 使用捐獻給所在{$GLOBALS[cn_name]}($L[cname])捐獻{$GLOBALS[moneyname]}：{$L[field1]}。',
'cy_vire_descrip'		=> '[b]{$L[username2]}[/b] 使用{$GLOBALS[cn_name]}{$GLOBALS[moneyname]}管理功能，'
							.'給用戶[b]{$L[username1]}[/b]轉帳 {$L[field1]}{$GLOBALS[moneyname]}，系統收取手續費：{$L[tax]}。',
'shield_descrip'		=> '帖子：[url=$GLOBALS[db_bbsurl]/read.php?tid=$L[tid]]$L[subject][/url]\n'
							.'操作：屏蔽主題\n原因：{$L[reason]}',
'banuserip_descrip'		=> '帖子：[url=$GLOBALS[db_bbsurl]/read.php?tid=$L[tid]]$L[subject][/url]\n'
							.'操作：禁止IP\n原因：{$L[reason]}',
'signature_descrip'	=> '帖子：[url=$GLOBALS[db_bbsurl]/read.php?tid=$L[tid]]$L[subject][/url]\n'
							.'操作：禁止簽名\n原因：{$L[reason]}',
'unite_descrip'			=> '帖子：[url=$GLOBALS[db_bbsurl]/read.php?tid=$L[tid]]$L[subject][/url]\n'
							.'操作：主題合併\n原因：{$L[reason]}',
'remind_descrip'		=> '帖子：[url=$GLOBALS[db_bbsurl]/read.php?tid=$L[tid]]$L[subject][/url]\n'
							.'操作：管理提示\n原因：{$L[reason]}',
'down_descrip'			=> '帖子：[url=$GLOBALS[db_bbsurl]/read.php?tid=$L[tid]]$L[subject][/url]\n'
							.'操作：將文章壓帖\n原因：{$L[reason]}',
'recycle_topic_delete'	=> '版塊:$L[forum]\n操作：將文章從主題回收站中徹底刪除',
'recycle_topic_restore'	=> '版塊:$L[forum]\n操作：將文章從主題回收站中還原',
'recycle_topic_empty'	=> '版塊:$L[forum]\n操作：將主題回收站清空',
'recycle_reply_delete'	=> '版塊:$L[forum]\n操作：將文章從回復回收站中徹底刪除',
'recycle_reply_restore'	=> '版塊:$L[forum]\n操作：將文章從回復回收站中還原',
'recycle_reply_empty'	=> '版塊:$L[forum]\n操作：將回復回收站清空',
'pushto_descrip'		=> '帖子：[url=$GLOBALS[db_bbsurl]/read.php?tid=$L[tid]]$L[subject][/url]\n'
							.'操作：將文章推送\n原因：{$L[reason]}'
);
?>