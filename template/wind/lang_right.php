<?php
!function_exists('readover') && exit('Forbidden');

$lang['right_title'] = array(
	'basic'		=> '基本權限',
	'read'		=> '帖子權限',
	'att'		=> '附件權限',
	'group'		=> '群組權限',
	'message'	=> '消息權限',
	'special'	=> '用戶組購買',
	'system'	=> '管理權限'
);
$lang['right'] = array (
	'message' => array(
		'allowmessege'	=> array(
			'title'	=> '發送消息',
			'desc'  => '開啟後，此用戶組的用戶可以發送消息',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[allowmessege]" $allowmessege_Y />開啟</li><li><input type="radio" value="0" name="group[allowmessege]" $allowmessege_N />關閉</li></ul>'
		),
		'maxmsg'	=> array(
			'title'	=> '可存儲的最大消息數目',
			'desc'	=> '只統計短消息和多人對話',
			'html'	=> '<input class="input input_wa" value="$maxmsg" name="group[maxmsg]" />'
		),
		'maxsendmsg'	=> array(
			'title'	=> '每日最大發送消息數目',
			'html'	=> '<input class="input input_wa" value="$maxsendmsg" name="group[maxsendmsg]" />'
		),
		'messagecontentsize' => array(
			'title'   => '每條消息內容最多字節數',
			'html'    => '<input class="input input_wa" name="group[messagecontentsize]" value="$messagecontentsize" />'
		),
		'msggroup'	=> array(
			'title'	=> '只接收特定用戶組的消息',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[msggroup]" $msggroup_Y />開啟</li><li><input type="radio" value="0" name="group[msggroup]" $msggroup_N />關閉</li></ul>'
		),
		'multiopen' => array(
			'title'	  => '發送多人消息',
			'desc'	  => '開啟後，此用戶組的用戶可以發送多人消息',
			'html'    => '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[multiopen]" $multiopen_Y />開啟</li><li><input type="radio" value="0" name="group[multiopen]" $multiopen_N />關閉</li></ul>'
		)
	),
	'basic' => array(
		'allowvisit' => array(
			'title'	=> '站點訪問',
			'desc'	=> '關閉後，用戶將不能訪問站點的任何頁面',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[allowvisit]" $allowvisit_Y />開啟</li><li><input type="radio" value="0" name="group[allowvisit]" $allowvisit_N />關閉</li></ul>'
		),
		'allowhide' => array(
			'title'	=> '隱身登錄',
			'desc'	=> '開啟後，用戶可以隱身登錄站點',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[allowhide]" $allowhide_Y />開啟</li><li><input type="radio" value="0" name="group[allowhide]" $allowhide_N />關閉</li></ul>'
		),
		'userbinding' => array(
			'title'	=> '多賬號綁定',
			'desc'	=> '俗稱馬甲綁定',
			'desc'	=> '開啟後，用戶可以進行多賬號綁定',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[userbinding]" $userbinding_Y />開啟</li><li><input type="radio" value="0" name="group[userbinding]" $userbinding_N />關閉</li></ul>'
		),
		'allowread'	=> array(
			'title'	=> '瀏覽帖子',
			'desc'	=> '開啟後，用戶可以瀏覽帖子',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[allowread]" $allowread_Y />開啟</li><li><input type="radio" value="0" name="group[allowread]" $allowread_N />關閉</li></ul>'
		),
		'allowsearch'	=> array(
			'title'	=> '搜索控制',
			'desc'	=> '修改用戶的搜索權限',
			'html'	=> '<ul class="list_A"><li><input type="radio" value="0" name="group[allowsearch]" $allowsearch_0 />不允許</li>
			<li><input type="radio" value="1" name="group[allowsearch]" $allowsearch_1 />允許搜索主題標題</li>
			<li><input type="radio" value="2" name="group[allowsearch]" $allowsearch_2 />允許搜索主題標題、內容</li>
			<li><input type="radio" value="3" name="group[allowsearch]" $allowsearch_3 />允許搜索全部內容(包含回復內容)</li></ul>'
		),
		'allowmember'	=> array(
			'title'	=> '查看會員列表',
			'desc'	=> '開啟後，用戶可以查看會員列表',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[allowmember]" $allowmember_Y />開啟</li><li><input type="radio" value="0" name="group[allowmember]" $allowmember_N />關閉</li></ul>'
		),
		'allowviewonlineread'	=> array(
			'title'	=> '查看在線會員所在頁面',
			'desc'	=> '允許此用戶組用戶查看在線會員當前所在的頁面',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[allowviewonlineread]" $allowviewonlineread_Y />開啟</li><li><input type="radio" value="0" name="group[allowviewonlineread]" $allowviewonlineread_N />關閉</li></ul>'
		),
		/*
		'allowprofile'	=> array(
			'title'	=> '查看會員資料',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[allowprofile]" $allowprofile_Y />開啟</li><li><input type="radio" value="0" name="group[allowprofile]" $allowprofile_N />關閉</li></ul>'
		),
		*/
		'atclog' => array(
			'title'	=> '查看帖子操作記錄',
			'desc'	=> '允許用戶查看自己帖子的被操作情況',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[atclog]" $atclog_Y />開啟</li><li><input type="radio" value="0" name="group[atclog]" $atclog_N />關閉</li></ul>'
		),
		//去掉展區功能@modify panjl@2010-11-2
		/*
		'show' => array(
			'title'	=> '使用展區',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[show]" $show_Y />開啟</li><li><input type="radio" value="0" name="group[show]" $show_N />關閉</li></ul>'
		),
		*/
		'allowreport' => array(
			'title'	=> '使用舉報',
			'desc'	=> '開啟後，此用戶組的用戶可以使用舉報功能',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[allowreport]" $allowreport_Y />開啟</li><li><input type="radio" value="0" name="group[allowreport]" $allowreport_N />關閉</li></ul>'
		),
		'upload' => array(
			'title'	=> '頭像上傳',
			'desc'	=> "此設置需先在 <a href=\"$admin_file?adminjob=member\" onclick=\"parent.PW.Dialog({id:'member',url:'$admin_file?adminjob=member',name:'會員相關'});return false;\">全局->會員相關->頭像設置</a> 中開啟頭像上傳功能才有效",
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[upload]" $upload_Y />開啟</li><li><input type="radio" value="0" name="group[upload]" $upload_N />關閉</li></ul>'
		),
		'allowportait'	=> array(
			'title'	=> '頭像鏈接',
			'desc'	=> '開啟後，此用戶組的用戶可以用外部網站圖片鏈接地址作為自己的頭像',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[allowportait]" $allowportait_Y />開啟</li><li><input type="radio" value="0" name="group[allowportait]" $allowportait_N />關閉</li></ul>'
		),
		'allowhonor'	=> array(
			'title'	=> '個性簽名',
			'desc'	=> '開啟後，此用戶組的用戶可以使用個性簽名功能',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[allowhonor]" $allowhonor_Y />開啟</li><li><input type="radio" value="0" name="group[allowhonor]" $allowhonor_N />關閉</li></ul>'
		),
		/*'allowmessege'	=> array(
			'title'	=> '發送消息',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[allowmessege]" $allowmessege_Y />開啟</li><li><input type="radio" value="0" name="group[allowmessege]" $allowmessege_N />關閉</li></ul>'
		),*/
		'allowsort'	=> array(
			'title'	=> '查看統計排行',
			'desc'	=> '開啟後，此用戶組的用戶可以查看統計排行',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[allowsort]" $allowsort_Y />開啟</li><li><input type="radio" value="0" name="group[allowsort]" $allowsort_N />關閉</li></ul>'
		),
		'alloworder'=> array(
			'title'	=> '主題排序',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[alloworder]" $alloworder_Y />開啟</li><li><input type="radio" value="0" name="group[alloworder]" $alloworder_N />關閉</li></ul>'
		),
		'viewipfrom'	=> array(
			'title'	=> '查看ip來源',
			'desc'	=> "如果論壇模式下界面設置中<a href=\"$admin_file?adminjob=interfacesettings&adminitem=read\">帖子閱讀頁設置</a>關閉此功能，則此項設置無效",
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[viewipfrom]" $viewipfrom_Y />開啟</li><li><input type="radio" value="0" name="group[viewipfrom]" $viewipfrom_N />關閉</li></ul>'
		),
		'searchtime'	=> array(
			'title'	=> '兩次搜索時間間隔[秒]',
			'html'	=> '<input class="input input_wa" name="group[searchtime]" value="$searchtime" />'
		),
		/*'schtime' => array(
			'title'	=> '搜索發表時間範圍',
			'html'	=> '<select name="group[schtime]" class="select_wa">
				<option value="all" $schtime_all>所有主題</option>
				<option value="86400" $schtime_86400>1天內的主題</option>
				<option value="172800" $schtime_172800>2天內的主題</option>
				<option value="604800" $schtime_604800>1星期內的主題</option>
				<option value="2592000" $schtime_2592000>1個月內的主題</option>
				<option value="5184000" $schtime_5184000>2個月內的主題</option>
				<option value="7776000" $schtime_7776000>3個月內的主題</option>
				<option value="15552000" $schtime_15552000>6個月內的主題</option>
				<option value="31536000" $schtime_31536000>1年內的主題</option>
			</select>'
		),*/
		'signnum' => array(
			'title'	=> '帖子簽名最大字節數',
			'desc'	=> '為0則不限制',
			'html'	=> '<input class="input input_wa" name="group[signnum]" value="$signnum" />'
		),
		'imgwidth' => array(
			'title'	=> '簽名中的圖片最大寬度',
			'desc'	=> "留空使用<a href=\"$admin_file?adminjob=member\"  onclick=\"parent.PW.Dialog({id:'member',url:'$admin_file?adminjob=member',name:'會員相關'});return false;\">全局->會員相關->簽名設置</a>裡的設置",
			'html'	=> '<input class="input input_wa" name="group[imgwidth]" value="$imgwidth" />'
		),
		'imgheight' => array(
			'title'	=> '簽名中的圖片最大高度',
			'desc'	=> "留空使用<a href=\"$admin_file?adminjob=member\"  onclick=\"parent.PW.Dialog({id:'member',url:'$admin_file?adminjob=member',name:'會員相關'});return false;\">全局->會員相關->簽名設置</a>裡的設置",
			'html'	=> '<input class="input input_wa" name="group[imgheight]" value="$imgheight" />'
		),
		'fontsize'	=> array(
			'title'	=> '簽名中[size]標籤最大值',
			'desc'	=> "留空使用<a href=\"$admin_file?adminjob=member\"  onclick=\"parent.PW.Dialog({id:'member',url:'$admin_file?adminjob=member',name:'會員相關'});return false;\">全局->會員相關->簽名設置</a>裡的設置",
			'html'	=> '<input class="input input_wa" name="group[fontsize]" value="$fontsize" />'
		),
		/*'maxmsg'	=> array(
			'title'	=> '最大短消息數目',
			'desc'	=> '最大消息數為個人消息，不包括群發消息和系統消息',
			'html'	=> '<input class="input input_wa" value="$maxmsg" name="group[maxmsg]" />'
		),*/
		/*'maxsendmsg'	=> array(
			'title'	=> '每日最大發送短消息數目',
			'html'	=> '<input class="input input_wa" value="$maxsendmsg" name="group[maxsendmsg]" />'
		),*/
		/*'msggroup'	=> array(
			'title'	=> '只接收特定用戶組的消息',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[msggroup]" $msggroup_Y />開啟</li><li><input type="radio" value="0" name="group[msggroup]" $msggroup_N />關閉</li></ul>'
		),*/
		/*'ifmemo'	=> array(
			'title'	=> '能使用便箋的功能',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[ifmemo]" $ifmemo_Y />開啟</li><li><input type="radio" value="0" name="group[ifmemo]" $ifmemo_N />關閉</li></ul>'
		),*/
		'pergroup' =>	array(
			'title'	=> '查看用戶組權限',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="checkbox" name="group[pergroup][]" value="member" $pergroup_sel[member] />會員組</li><li><input type="checkbox" name="group[pergroup][]" value="system" $pergroup_sel[system] />系統組</li><li><input type="checkbox" name="group[pergroup][]" value="special" $pergroup_sel[special] />特殊組</li></ul>'
		),
		'maxfavor'	=> array(
			'title'	=> '收藏夾容量',
			'desc'  => '設置收藏夾可以收藏的信息條數',
			'html'	=> '<input class="input input_wa" value="$maxfavor" name="group[maxfavor]" />'
		),
		'maxgraft'	=> array(
			'title'	=> '草稿箱容量',
			'desc'  => '設置草稿箱可以容納的信息條數',
			'html'	=> '<input class="input input_wa" value="$maxgraft" name="group[maxgraft]" />'
		),
		'pwdlimitime'	=> array(
			'title'	=> '強制用戶組密碼變更[天]',
			'desc'	=> '0或留空表示不限制',
			'html'	=> '<input class="input input_wa" value="$pwdlimitime" name="group[pwdlimitime]" />'
		),
		/*'maxcstyles'	=> array(
			'title'	=> '自定義風格數量',
			'desc'	=> '(設置0或留空，則不允許使用自定義風格)',
			'html'	=> '<input class="input input_wa" value="$maxcstyles" name="group[maxcstyles]" />'
		)*/
		'allowat'	=> array(
			'title'	=> '允許@其他人',
			'desc'	=> '開啟後，此用戶組的用戶可以@提到他人',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[allowat]" $allowat_Y />開啟</li><li><input type="radio" value="0" name="group[allowat]" $allowat_N />關閉</li></ul>'
		),
		'atnum'	=> array(
			'title'	=> '可@用戶數',
			'desc'	=> '可@用戶數上限，0或留空表示不限制',
			'html'	=> '<input class="input input_wa" value="$atnum" name="group[atnum]" />'
		),
		'allowvideo' => array(
			'title'	=> '允許發視頻帖',
			'desc'	=> '允許此用戶組用戶發視頻帖',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[allowvideo]" $allowvideo_Y />開啟</li><li><input type="radio" value="0" name="group[allowvideo]" $allowvideo_N />關閉</li></ul>'
		),
		'allowmusic' => array(
			'title'	=> '允許發音樂帖',
			'desc'	=> '允許此用戶組用戶發音樂帖',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[allowmusic]" $allowmusic_Y />開啟</li><li><input type="radio" value="0" name="group[allowmusic]" $allowmusic_N />關閉</li></ul>'
		),
	),
	'read'	=> array(
		'allowpost'	=> array(
			'title'	=> '發表主題',
			'desc'	=> '開啟後，此用戶組的用戶可以發表主題',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[allowpost]" $allowpost_Y />開啟</li><li><input type="radio" value="0" name="group[allowpost]" $allowpost_N />關閉</li></ul>'
		),
		'specialtopics' => array(
			'title'	=> '允許發表特殊主題',
			'desc'	=> '允許修改投票選項：允許用戶修改自己已發起的投票的選項<br/>勾選此用戶組用戶可發表的特殊主題，此權限同時受版塊權限中的可發表的帖子類型控制',
			'html'	=> '
				<input type="checkbox" name="group[allownewvote]" value="1" $allownewvote_Y /> 投票帖
				&nbsp; <select class="elect_wa" name="group[modifyvote]">
					<option value="1" $modifyvote_Y>允許修改投票選項</option>
					<option value="0" $modifyvote_N>不允許修改投票選項</option>
				</select>
				<ul class="list_120 cc">
					<li><input type="checkbox" name="group[allowreward]" value="1" $allowreward_Y /> 懸賞帖</li>
					<li><input type="checkbox" name="group[allowgoods]" value="1" $allowgoods_Y /> 商品帖</li>
					<li><input type="checkbox" name="group[allowdebate]" value="1" $allowdebate_Y /> 辯論帖</li>
					<li><input type="checkbox" name="group[allowmodelid]" value="1" $allowmodelid_Y /> 分類信息帖</li>
					<li><input type="checkbox" name="group[allowpcid]" value="1" $allowpcid_Y /> 團購帖</li>
					<li><input type="checkbox" name="group[allowactivity]" value="1" $allowactivity_Y /> 活動帖</li>
				</ul>
				<div style="height:1px;background:#dde9f5;overflow:hidden;margin:5px 0;"></div>
				<ul class="list_120 cc">
					<li><input type="checkbox" name="group[robbuild]" value="1" $robbuild_Y /> 搶樓帖</li>
					<li><input type="checkbox" name="group[htmlcode]" value="1" $htmlcode_Y /> html帖</li>
					<li><input type="checkbox" name="group[anonymous]" value="1" $anonymous_Y /> 匿名帖</li>
					<li><input type="checkbox" name="group[allowhidden]" value="1" $allowhidden_Y /> 隱藏帖</li>
					<li><input type="checkbox" name="group[allowsell]" value="1" $allowsell_Y /> 出售帖</li>
					<li><input type="checkbox" name="group[allowencode]" value="1" $allowencode_Y /> 加密帖</li>
				</ul>'
		),
		'allowrp'	=> array(
			'title'	=> '回復主題',
			'desc'	=> '開啟後，此用戶組的用戶可以回復主題',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[allowrp]" $allowrp_Y />開啟</li><li><input type="radio" value="0" name="group[allowrp]" $allowrp_N />關閉</li></ul>'
		),
		/*'allownewvote'	=> array(
			'title'	=> '發起投票',
			'desc'	=> '開啟後，此用戶組的用戶可以發起投票',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[allownewvote]" $allownewvote_Y />開啟</li><li><input type="radio" value="0" name="group[allownewvote]" $allownewvote_N />關閉</li></ul>'
		),
		'modifyvote'	=> array(
			'title'	=> '修改發起的投票選項',
			'desc'	=> '開啟後，此用戶組的用戶有權限修改發起的投票選項',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[modifyvote]" $modifyvote_Y />開啟</li><li><input type="radio" value="0" name="group[modifyvote]" $modifyvote_N />關閉</li></ul>'
		),*/
		'allowvote'	=> array(
			'title' => '參與投票',
			'desc'	=> '開啟後，此用戶組的用戶可以參與投票',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[allowvote]" $allowvote_Y />開啟</li><li><input type="radio" value="0" name="group[allowvote]" $allowvote_N />關閉</li></ul>'
		),
		'viewvote'	=> array(
			'title'	=> '查看投票用戶',
			'desc'	=> '開啟後，此用戶組的用戶可以查看投票用戶',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[viewvote]" $viewvote_Y />開啟</li><li><input type="radio" value="0" name="group[viewvote]" $viewvote_N />關閉</li></ul>'
		),
		'leaveword'	=>	array(
			'title'	=> '樓主留言',
			'desc'	=> '開啟後，此用戶組的用戶可使用樓主留言功能',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[leaveword]" $leaveword_Y />開啟</li><li><input type="radio" value="0" name="group[leaveword]" $leaveword_N />關閉</li></ul>'
		),
		'dig'	=> array(
			'title'	=> '推薦帖子',
			'desc'	=> '開啟後，此用戶組的用戶可以推薦帖子',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[dig]" $dig_Y />開啟</li><li><input type="radio" value="0" name="group[dig]" $dig_N />關閉</li></ul>'
		),
		/*'allowactive'	=> array(
			'title'	=> '發活動帖',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[allowactive]" $allowactive_Y />開啟</li><li><input type="radio" value="0" name="group[allowactive]" $allowactive_N />關閉</li></ul>'
		),*/
		/*'allowreward'	=> array(
			'title'	=> '懸賞帖',
			'desc'	=> '開啟後，此用戶組的用戶可以發表懸賞帖',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[allowreward]" $allowreward_Y />開啟</li><li><input type="radio" value="0" name="group[allowreward]" $allowreward_N />關閉</li></ul>'
		),
		'allowgoods'	=> array(
			'title'	=> '商品帖',
			'desc'	=> '開啟後，此用戶組的用戶可以發表商品帖',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[allowgoods]" $allowgoods_Y />開啟</li><li><input type="radio" value="0" name="group[allowgoods]" $allowgoods_N />關閉</li></ul>'
		),
		'allowdebate'	=> array(
			'title'	=> '辯論帖',
			'desc'	=> '開啟後，此用戶組的用戶可以發表辯論帖',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[allowdebate]" $allowdebate_Y />開啟</li><li><input type="radio" value="0" name="group[allowdebate]" $allowdebate_N />關閉</li></ul>'
		),
		'allowmodelid'	=> array(
			'title'	=> '分類信息帖',
			'desc'	=> '開啟後，此用戶組的用戶可以發表分類信息帖',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[allowmodelid]" $allowmodelid_Y />開啟</li><li><input type="radio" value="0" name="group[allowmodelid]" $allowmodelid_N />關閉</li></ul>'
		),
		'allowpcid'	=> array(
			'title'	=> '團購帖',
			'desc'	=> '開啟後，此用戶組的用戶可以發表團購帖',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" name="group[allowpcid]" value="1" $allowpcid_Y/>開啟</li><li><input type="radio" name="group[allowpcid]" value="0" $allowpcid_N />關閉</li>'
			//<li><input type="checkbox" name="group[allowpcid][]" value="2" $allowpcid_sel[2] />活動</li></ul>'
		),
		'allowactivity'	=> array(
			'title'	=> '活動帖',
			'desc'	=> '開啟後，此用戶組的用戶可以發表活動帖',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[allowactivity]" $allowactivity_Y />開啟</li><li><input type="radio" value="0" name="group[allowactivity]" $allowactivity_N />關閉</li></ul>'
		),
		'robbuild'	=> array(
			'title'	=> '搶樓帖',
			'desc'	=> '開啟後，此用戶組的用戶可以發表搶樓帖',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[robbuild]" $robbuild_Y />開啟</li><li><input type="radio" value="0" name="group[robbuild]" $robbuild_N />關閉</li></ul>'
		),
		'htmlcode'	=> array(
			'title'	=> '發表html帖',
			'desc'	=> '這將使用戶擁有直接編輯 html 源代碼的權利!',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[htmlcode]" $htmlcode_Y />開啟</li><li><input type="radio" value="0" name="group[htmlcode]" $htmlcode_N />關閉</li></ul>'
		),
		'allowhidden'	=> array(
			'title'	=> '隱藏帖',
			'desc'	=> '註：此設置同時受版塊權限限制，開啟版塊權限中的發表隱藏帖功能方有效',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[allowhidden]" $allowhidden_Y />開啟</li><li><input type="radio" value="0" name="group[allowhidden]" $allowhidden_N />關閉</li></ul>'
		),
		'allowsell'	=> array(
			'title'	=> '出售帖',
			'desc'	=> '註：此設置同時受版塊權限限制，開啟版塊權限中的發表出售帖功能方有效',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[allowsell]" $allowsell_Y />開啟</li><li><input type="radio" value="0" name="group[allowsell]" $allowsell_N />關閉</li></ul>'
		),
		'allowencode'	=> array(
			'title'	=> '加密帖',
			'desc'	=> '註：此設置同時受版塊權限限制，開啟版塊權限中的發表加密帖功能方有效',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[allowencode]" $allowencode_Y />開啟</li><li><input type="radio" value="0" name="group[allowencode]" $allowencode_N />關閉</li></ul>'
		),
		'anonymous'	=> array(
			'title'	=> '匿名帖',
			'desc'	=> '註：此設置同時受版塊權限限制，開啟版塊權限中的發表匿名帖功能方有效',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[anonymous]" $anonymous_Y />開啟</li><li><input type="radio" value="0" name="group[anonymous]" $anonymous_N />關閉</li></ul>'
		),*/
		
		'allowdelatc'	=> array(
			'title'	=> '刪除自己的帖子',
			'desc'	=> '開啟後，此用戶組的用戶可以刪除自己的帖子',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[allowdelatc]" $allowdelatc_Y />開啟</li><li><input type="radio" value="0" name="group[allowdelatc]" $allowdelatc_N />關閉</li></ul>'
		),
		'atccheck'	=> array(
			'title'	=> '帖子需審核',
			'desc'	=> '開啟版塊帖子審核時,發表的帖子是否需要管理員審核，此項只有在開啟版塊帖子審核時有效',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[atccheck]" $atccheck_Y />開啟</li><li><input type="radio" value="0" name="group[atccheck]" $atccheck_N />關閉</li></ul>'
		),
		'allowreplyreward' => array(
			'title'	=> '允許設置回帖獎勵',
			'desc'	=> '允許此用戶組用戶在發帖時給回復者一定的積分獎勵',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[allowreplyreward]" $allowreplyreward_Y />開啟</li><li><input type="radio" value="0" name="group[allowreplyreward]" $allowreplyreward_N />關閉</li></ul>'
		),
		'allowremotepic' => array(
			'title'	=> '允許下載遠程圖片',
			'desc'	=> '允許此用戶組用戶在發帖時將遠程圖片本地化',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[allowremotepic]" $allowremotepic_Y />開啟</li><li><input type="radio" value="0" name="group[allowremotepic]" $allowremotepic_N />關閉</li></ul>'
		),
		'postlimit'	=> array(
			'title'	=> '每日最多發帖數',
			'desc'	=> '0或留空表示不限制',
			'html'	=> '<input class="input input_wa" value="$postlimit" name="group[postlimit]" />'
		),
		'allowbuykmd' => array(
			'title'	=> '允許購買孔明燈',
			'desc'	=> '',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[allowbuykmd]" $allowbuykmd_Y />開啟</li><li><input type="radio" value="0" name="group[allowbuykmd]" $allowbuykmd_N />關閉</li></ul>'
		),
		'postpertime'	=> array(
			'title'	=> '連續發帖時間控制（秒）',
			'desc'	=> '設定的時間間隔內用戶不可連續發帖，0或留空表示不限制，此功能原名為：灌水預防',
			'html'	=> '<input class="input input_wa" value="$postpertime" name="group[postpertime]" />'
		),
		'edittime'	=> array(
			'title'	=> '可編輯時間控制（分鐘）',
			'desc'	=> '用戶發帖成功後，可以在設定的時間段內重新編輯帖子，0或留空表示不限制，此功能原名為：編輯時間約束[分鐘]',
			'html'	=> '<input class="input input_wa" value="$edittime" name="group[edittime]" />'
		),
		//增加鏈接帖發帖限制@modify panjl@2010-11-2
		'posturlnum'	=> array(
			'title'	=> '鏈接帖發帖數控制',
			'desc'	=> '發帖數達到設定之後才可以發表帶鏈接的帖子，0或留空表示不限制，此設置可防止註冊機註冊後發佈帶鏈接的廣告帖',
			'html'	=> '<input class="input input_wa" value="$posturlnum" name="group[posturlnum]" />'
		),
		'media'	=> array(
			'title'	=> '多媒體自動展開',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="checkbox" name="group[media][]" value="flash" $media_sel[flash] />flash</li><li>
			<input type="checkbox" name="group[media][]" value="wmv" $media_sel[wmv] />wmv</li><li>
			<input type="checkbox" name="group[media][]" value="rm" $media_sel[rm] />rm</li><li>
			<input type="checkbox" name="group[media][]" value="mp3" $media_sel[mp3] />mp3</li></ul>'
		),
		'markable'	=> array(
			'title'	=> '帖子評分權限',
			'desc'	=> '版主在所管理版塊始終有評分權限',
			'html'	=> '<ul class="list_A cc td2_wp"><li><input type="radio" value="0" name="group[markable]" $markable_0 />無</li><li>
			<input type="radio" value="1" name="group[markable]" $markable_1 />允許評分</li><li>
			<input type="radio" value="2" name="group[markable]" $markable_2 />允許重複評分</li></ul>'
		),
		/*'maxcredit'	=> array(
			'title' => '評分上限<font color=blue> 說明：</font>每天所有積分總和允許的最大評分點數',
			'html'	=> '<input type="text" class="input input_wa" value="$maxcredit" name="group[maxcredit]" />'
		),
		'marklimit' => array(
			'title'	=> '評分限制<font color=blue> 說明：</font>每次評分的最大和最小值',
			'html'	=> '最小 <input type=text size="3" class="input" value="$minper" name="group[marklimit][0]" /> 最大 <input type=text size="3" class="input" value="$maxper" name="group[marklimit][1]" />'
		),*/
		'markset'	=> array(
			'title'	=> '帖子評分設置',
			'desc'	=> '復選框不選中或者評分上限、評分限制任何一項留空/設為0，前台將無法使用該評分類型',
			'html'	=> $credit_type
		)
		/*'markdt'	=> array(
			'title'	=> '評分是否需要扣除自身相應的積分',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[markdt]" $markdt_Y />開啟</li><li><input type="radio" value="0" name="group[markdt]" $markdt_N />關閉</li></ul>'
		),
		'postpertime'	=> array(
			'title'	=> '灌水預防',
			'desc'	=> '多少秒間隔內不能發帖，0或留空表示不限制',
			'html'	=> '<input class="input input_wa" value="$postpertime" name="group[postpertime]" />'
		),
		'edittime'	=> array(
			'title'	=> '編輯時間約束[分鐘]',
			'desc'	=> '超過設定時間後拒絕用戶編輯。0或留空表示不限制',
			'html'	=> '<input class="input input_wa" value="$edittime" name="group[edittime]" />'
		),
		//增加鏈接帖發帖限制@modify panjl@2010-11-2
		'posturlnum'	=> array(
			'title'	=> '鏈接帖發帖限制',
			'desc'	=> '當會員的發表的帖子數量達到設定值以後就可以發表帶鏈接的帖子。此設置為防止註冊機註冊後髮帶有鏈接地址的廣告。0或留空表示不限制',
			'html'	=> '<input class="input input_wa" value="$posturlnum" name="group[posturlnum]" />'
		)*/
	),
	'group' =>array(
		'allowcreate'=>array(
			'title'=>'允許創建群組個數',
			'desc' =>"0 或 留空 表示沒限制，需要在群組基礎設置中開啟<a href=\"$admin_file?adminjob=apps&admintype=groups_set\">允許創建新群組</a>才有效",
			'html'=>'<input size="35" class="input" value="$allowcreate" name="group[allowcreate]" />'
		),
		'allowjoin'=>array(
			'title'=>'允許加入群組個數',
			'desc' =>'0 或 留空 表示沒限制',
			'html'=>'<input size="35" class="input" value="$allowjoin" name="group[allowjoin]" />'
		)
	),
	'att'	=> array(
		'allowupload'	=> array(
			'title'	=> '上傳附件權限',
			'desc'	=> '可在版塊設置處設置上傳附件「獎勵或扣除」積分',
			'html'	=> '<ul class="list_A"><li><input type="radio" value="0" name="group[allowupload]" $allowupload_0 />不允許上傳附件</li><li><input type="radio" value="1" name="group[allowupload]" $allowupload_1 />允許上傳附件，按照版塊設置獎勵或扣除積分</li><li><input type="radio" value="2" name="group[allowupload]" $allowupload_2 />允許上傳附件，不獎勵或扣除積分</li></ul>'
		),
		'allowdownload'	=> array(
			'title'	=> '下載附件權限',
			'desc'	=> '可在版塊設置處設置下載附件「獎勵或扣除」積分',
			'html'	=> '<ul class="list_A"><li><input type="radio" value="0" name="group[allowdownload]" $allowdownload_0 />不允許下載附件</li><li><input type="radio" value="1" name="group[allowdownload]" $allowdownload_1 />允許下載附件，按照版塊設置獎勵或扣除積分</li><li><input type="radio" value="2" name="group[allowdownload]" $allowdownload_2 />允許下載附件，不獎勵或扣除積分</li></ul>'
		),
		'allownum'	=> array(
			'title'	=> '一天最多上傳附件個數',
			'html'	=> '<input class="input input_wa" value="$allownum" name="group[allownum]" />'
		),
		'uploadtype'	=> array(
			'title'	=> '附件上傳的後綴和尺寸',
			'desc'	=> "<font color=\"red\">系統限制上傳附件最大尺寸為{$maxuploadsize},</font>留空則使用站點全局中的設置",
			'html'	=> '<div class="admin_table_b"><table cellpadding="0" cellspacing="0">
				<tbody id="mode" style="display:none"><tr>
					<td><input class="input input_wc" name="filetype[]" value=""></td>
					<td><input class="input input_wc" name="maxsize[]" value=""></td><td><a href="javascript:;" onclick="removecols(this);">[刪除]</a></td>
				</tr></tbody>
				<tr>
					<td>後綴名(<b>小寫</b>)</td>
					<td>最大尺寸(KB)</td>
					<td><a href="javascript:;" class="s3" onclick="addcols(\'mode\',\'ft\');">[添加]</a></td>
				</tr>
				{$upload_type}
				<tbody id="ft"></tbody>
			</table></div>
			<script type="text/javascript">
			addcols(\'mode\',\'ft\');
			</script>'
		)
	),
	'special' => array(
		'allowbuy'	=> array(
			'title'	=> '允許購買',
			'desc'	=> "開啟該功能後，需同時到<a href=\"$admin_file?adminjob=plantodo\"><font color=\"blue\">計劃任務</font></a>開啟「限期頭銜自動回收」功能.",
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[allowbuy]" $allowbuy_Y />開啟</li><li><input type="radio" value="0" name="group[allowbuy]" $allowbuy_N />關閉</li></ul>'
		),
		'selltype'	=> array(
			'title'	=> '特殊組購買幣種',
			'html'	=> '<select name="group[selltype]" class="select_wa">$special_type</select>'
		),
		'sellprice'	=> array(
			'title'	=> '每日價格[積分]',
			'html'	=> '<input type="text" class="input input_wa" name="group[sellprice]" value="$sellprice" />'
		),
		'rmbprice'	=> array(
			'title'	=> '每日價格[現金]',
			'desc'	=> '設置此價格，將允許通過網上支付來購買',
			'html'	=> '<input type="text" class="input input_wa" name="group[rmbprice]" value="$rmbprice" />'
		),
		'selllimit'	=> array(
			'title'	=> '購買特殊組天數限制',
			'html'	=> '<input type="text" class="input input_wa" name="group[selllimit]" value="$selllimit" />'
		),
		'sellinfo'	=> array(
			'title'	=> '特殊組描述',
			'desc'	=> '可以填寫購買說明和該用戶組擁有的特殊權限',
			'html'	=> '<textarea name="group[sellinfo]" class="textarea">$sellinfo</textarea>'
		)
	),
	'system'	=> array(
		'superright' => array(
			'title'	=> '超級管理權限',
			'desc'	=> "<font color=\"red\">是</font>：表明以下針對版塊的權限設置對所有版塊生效（例如：管理員）<br /><font color=\"red\">否</font>：表明以下針對版塊的權限設置對所有版塊無效，此時如果要配置單個版塊的管理權限，需要到<a href=\"$admin_file?adminjob=singleright\">版塊用戶權限</a>裡進行配置<br />（註：當編輯版主權限時，開啟則版主在所有版塊都擁有權限，關閉則版主只在本版塊擁有權限）",
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[superright]" $superright_Y />開啟</li><li><input type="radio" value="0" name="group[superright]" $superright_N />關閉</li></ul>'
		),
		'enterreason' => array(
			'title' => '強制輸入操作原因',
			'desc' => '開啟後，前台管理的所有操作都必須輸入操作原因。可避免由於管理操作上的不透明而引起站點會員糾紛',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[enterreason]" $enterreason_Y />開啟</li><li><input type="radio" value="0" name="group[enterreason]" $enterreason_N />關閉</li></ul>'
		),
		'colonyright' => array(
			'title'	=> '群組管理權限',
			'desc'	=> "開啟了此權限，此類用戶將具備任意群組管理員的管理權限",
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[colonyright]" $colonyright_Y />開啟</li><li><input type="radio" value="0" name="group[colonyright]" $colonyright_N />關閉</li></ul>'
		),
		'forumcolonyright' => array(
			'title'	=> '關聯版塊群組管理權限',
			'desc'	=> "開啟了此權限，此類用戶將具備關聯版塊中群組的管理權限",
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[forumcolonyright]" $forumcolonyright_Y />開啟</li><li><input type="radio" value="0" name="group[forumcolonyright]" $forumcolonyright_N />關閉</li></ul>'
		)
	),
	'systemforum' => array(
		'posthide'	=> array(
			'title'	=> '查看隱藏帖',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[posthide]" $posthide_Y />開啟</li><li><input type="radio" value="0" name="group[posthide]" $posthide_N />關閉</li></ul>'
		),
		'sellhide'	=> array(
			'title'	=> '查看出售帖',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[sellhide]" $sellhide_Y />開啟</li><li><input type="radio" value="0" name="group[sellhide]" $sellhide_N />關閉</li></ul>'
		),
		'encodehide'	=> array(
			'title'	=> '查看加密帖',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[encodehide]" $encodehide_Y />開啟</li><li><input type="radio" value="0" name="group[encodehide]" $encodehide_N />關閉</li></ul>'
		),
		'anonyhide'	=> array(
			'title'	=> '查看匿名帖',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[anonyhide]" $anonyhide_Y />開啟</li><li><input type="radio" value="0" name="group[anonyhide]" $anonyhide_N />關閉</li></ul>'
		),
		'activitylist'=> array(
			'title'	=> '管理活動報名列表',
			'desc'	=> '開啟之後有查看報名列表、導出列表、群發短信等操作權限',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[activitylist]" $activitylist_Y />開啟</li><li><input type="radio" value="0" name="group[activitylist]" $activitylist_N />關閉</li></ul>'
		),
		'postpers'	=> array(
			'title'	=> '灌水',
			'desc'	=> '不受灌水時間限制',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[postpers]" $postpers_Y />開啟</li><li><input type="radio" value="0" name="group[postpers]" $postpers_N />關閉</li></ul>'
		),
		'replylock'	=> array(
			'title'	=> '回復鎖定帖',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type=radio value=1 $replylock_Y name=group[replylock]>開啟</li><li><input type=radio value=0 $replylock_N name=group[replylock]>關閉</li></ul>'
		),
		'viewip'	=> array(
			'title'	=> '查看IP',
			'desc'	=> '瀏覽帖子時顯示,管理員將不受該功能限制',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[viewip]" $viewip_Y />開啟</li><li><input type="radio" value="0" name="group[viewip]" $viewip_N />關閉</li></ul>'
		),
		'topped'	=> array(
			'title'	=> '置頂權限',
			'html'	=> '<ul class="list_A"><li><input type="radio" value="0" name="group[topped]" $topped_0 />無</li><li>
			<input type="radio" value="1" name="group[topped]" $topped_1 />版塊置頂</li><li>
			<input type="radio" value="2" name="group[topped]" $topped_2 />版塊置頂,分類置頂</li><li>
			<input type="radio" value="3" name="group[topped]" $topped_3 />版塊置頂,分類置頂,總置頂</li><li>
			<input type="radio" value="4" name="group[topped]" $topped_4 />任意版塊置頂</li></ul>'
		),
		'replayorder' => array(
			'title'	  => '帖子回復顯示順序',
			'desc'    => '開啟後，在編輯帖子時，用戶能夠設置帖子回復的顯示順序',
			'html'	  => '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[replayorder]" $replayorder_Y />開啟</li><li><input type="radio" value="0" name="group[replayorder]" $replayorder_N />關閉</li></ul>',
		),
		'digestadmin'	=> array(
			'title'	=> '前台精華',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[digestadmin]" $digestadmin_Y />開啟</li><li><input type="radio" value="0" name="group[digestadmin]" $digestadmin_N />關閉</li></ul>'
		),
		'lockadmin'	=> array(
			'title'	=> '前台鎖定',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[lockadmin]" $lockadmin_Y />開啟</li><li><input type="radio" value="0" name="group[lockadmin]" $lockadmin_N />關閉</li></ul>'
		),
		'pushadmin'	=> array(
			'title'	=> '前台提前',
			'html'	=> '
			<ul class="list_A list_80 cc fl mr20"><li><input type="radio" value="1" name="group[pushadmin]" $pushadmin_Y />開啟</li><li><input type="radio" value="0" name="group[pushadmin]" $pushadmin_N />關閉</li></ul>'
		),
		'pushtime'	=> array(
			'title'	=> '提前時間上限[小時]',
			'desc'	=> '留空或0表示不限制',
			'html'	=> '<input type="text" value="$pushtime" name="group[pushtime]" class="input input_wa" />'
		),
		'coloradmin'	=> array(
			'title'	=> '前台加亮',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[coloradmin]" $coloradmin_Y />開啟</li><li><input type="radio" value="0" name="group[coloradmin]" $coloradmin_N />關閉</li></ul>'
		),
		'downadmin'	=> array(
			'title'	=> '前台壓帖',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[downadmin]" $downadmin_Y />開啟</li><li><input type="radio" value="0" name="group[downadmin]" $downadmin_N />關閉</li></ul>'
		),
		'replaytopped' => array(
			'title'    => '前台帖內置頂',
			'html'	   => '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[replaytopped]" $replaytopped_Y />開啟</li><li><input type="radio" value="0" name="group[replaytopped]" $replaytopped_N />關閉</li></ul>',
		),
		'tpctype'	=> array(
			'title'	=> '主題分類管理',
			'desc'	=> '<font color=blue> 說明：</font>主題分類批量管理權限',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[tpctype]" $tpctype_Y />開啟</li><li><input type="radio" value="0" name="group[tpctype]" $tpctype_N />關閉</li></ul>'
		),
		'tpccheck'	=> array(
			'title'	=> '主題驗證管理',
			'desc'	=> '<font color=blue> 說明：</font>前台主題驗證管理權限',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[tpccheck]" $tpccheck_Y />開啟</li><li><input type="radio" value="0" name="group[tpccheck]" $tpccheck_N />關閉</li></ul>'
		),
		'delatc'	=> array(
			'title'	=> '批量刪除主題',
			'desc'	=> '<font color=blue> 說明：</font>前台帖子管理權限',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[delatc]" $delatc_Y />開啟</li><li><input type="radio" value="0" name="group[delatc]" $delatc_N />關閉</li></ul>'
		),
		'moveatc'	=> array(
			'title'	=> '批量移動帖子',
			'desc'	=> '<font color=blue> 說明：</font>前台帖子管理權限',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[moveatc]" $moveatc_Y />開啟</li><li><input type="radio" value="0" name="group[moveatc]" $moveatc_N />關閉</li></ul>'
		),
		'copyatc'	=> array(
			'title'	=> '批量複製帖子',
			'desc'	=> '<font color=blue> 說明：</font>前台帖子管理權限',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[copyatc]" $copyatc_Y />開啟</li><li><input type="radio" value="0" name="group[copyatc]" $copyatc_N />關閉</li></ul>'
		),
		'modother'	=> array(
			'title'	=> '刪除單一帖子[包括回復]',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[modother]" $modother_Y />開啟</li><li><input type="radio" value="0" name="group[modother]" $modother_N />關閉</li></ul>'
		),
		'deltpcs'	=> array(
			'title'	=> '編輯用戶帖子',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[deltpcs]" $deltpcs_Y />開啟</li><li><input type="radio" value="0" name="group[deltpcs]" $deltpcs_N />關閉</li></ul>'
		),
		'viewcheck'	=> array(
			'title'	=> '查看需要驗證的帖子',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[viewcheck]" $viewcheck_Y />開啟</li><li><input type="radio" value="0" name="group[viewcheck]" $viewcheck_N />關閉</li></ul>'
		),
		'viewclose'	=> array(
			'title'	=> '查看關閉帖子',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[viewclose]" $viewclose_Y />開啟</li><li><input type="radio" value="0" name="group[viewclose]" $viewclose_N />關閉</li></ul>'
		),
		'delattach'	=> array(
			'title'	=> '刪除附件',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[delattach]" $delattach_Y />開啟</li><li><input type="radio" value="0" name="group[delattach]" $delattach_N />關閉</li></ul>'
		),
		'shield'	=> array(
			'title'	=> '屏蔽單一主題',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[shield]" $shield_Y />開啟</li><li><input type="radio" value="0" name="group[shield]" $shield_N />關閉</li></ul>'
		),
		'unite'	=> array(
			'title'	=> '合併主題',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[unite]" $unite_Y />開啟</li><li><input type="radio" value="0" name="group[unite]" $unite_N />關閉</li></ul>'
		),
		'split'	=> array(
			'title'	=> '拆分帖子',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[split]" $split_Y />開啟</li><li><input type="radio" value="0" name="group[split]" $split_N />關閉</li></ul>'
		),
		'remind'	=> array(
			'title'	=> '帖子管理提醒',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[remind]" $remind_Y />開啟</li><li><input type="radio" value="0" name="group[remind]" $remind_N />關閉</li></ul>'
		),
		'pingcp'	=> array(
			'title'	=> '管理評分記錄',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[pingcp]" $pingcp_Y />開啟</li><li><input type="radio" value="0" name="group[pingcp]" $pingcp_N />關閉</li></ul>'
		),
		'inspect'	=> array(
			'title'	=> '版主標記已閱讀',
			'desc'	=> '<font color=blue> 說明：</font>需在版塊基本權限裡開啟後方可用',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[inspect]" $inspect_Y />開啟</li><li><input type="radio" value="0" name="group[inspect]" $inspect_N />關閉</li></ul>'
		),
		'allowtime'	=> array(
			'title'	=> '不受版塊發帖時間域限制',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[allowtime]" $allowtime_Y />開啟</li><li><input type="radio" value="0" name="group[allowtime]" $allowtime_N />關閉</li></ul>'
		),
		'banuser'	=> array(
			'title'	=> '禁言用戶的權限',
			'desc'	=> '<font color="blue">說明:</font><br />
			<font color="red">無禁言權限:</font>該用戶組無權限對會員進行禁言操作<br />
			<font color="red">所有版塊:</font>(有禁言權限)並且被禁言會員在所有版塊中都沒權限發言<br />
			<font color="red">單一版塊</font>(有禁言權限)並且被禁言會員在帖子所在版塊沒權限發言,而在其他版塊中可以發言',
			'html'	=> '<ul class="list_A"><li><input type="radio" value="0" name="group[banuser]" $banuser_0 />無禁言權限</li><li><input type="radio" value="1" name="group[banuser]" $banuser_1 />單一版塊</li><li><input type="radio" value="2" name="group[banuser]" $banuser_2 />所有版塊</li></ul>'
		),
		'bantype'	=> array(
			'title'	=> '永久禁言用戶',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[bantype]" $bantype_Y />開啟</li><li><input type="radio" value="0" name="group[bantype]" $bantype_N />關閉</li></ul>'
		),
		'banmax'	=> array(
			'title'	=> '禁言時間限制',
			'desc'	=> '<font color=blue> 說明：</font>禁言會員的最大天數',
			'html'	=> '<input type=text class="input input_wa" value="$banmax" name="group[banmax]" />'
		),
		'banuserip' => array(
			'title' => '禁止ip',
			'desc'  => '開啟後，擁有禁止ip權限',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[banuserip]" $banuserip_Y />開啟</li><li><input type="radio" value="0" name="group[banuserip]" $banuserip_N />關閉</li></ul>'
		),
		'banadmin'	=> array(
			'title'	=> '可禁言管理組',
			'desc'	=> '<font color=blue> 說明：</font>開啟後，可以禁言所有用戶組，包括系統組和特殊用戶組',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[banadmin]" $banadmin_Y />開啟</li><li><input type="radio" value="0" name="group[banadmin]" $banadmin_N />關閉</li></ul>'
		),
		'bansignature' => array(
			'title' => '禁止帖子簽名',
			'desc'  => '開啟後，擁有禁止帖子簽名權限',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[bansignature]" $bansignature_Y />開啟</li><li><input type="radio" value="0" name="group[bansignature]" $bansignature_N />關閉</li></ul>'
		),
		'areapush'	=> array(
			'title'	=> '門戶推送',
			'desc'	=> '<font color=blue> 說明：</font>是否有向門戶模式的首頁和頻道頁推送帖子的權限',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[areapush]" $areapush_Y />開啟</li><li><input type="radio" value="0" name="group[areapush]" $areapush_N />關閉</li></ul>'
		),
		'overprint'	=> array(
			'title'	=> '帖子印戳',
			'desc'	=> '<font color=blue> 說明：</font>是否有給帖子加印戳效果的權限',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[overprint]" $overprint_Y />開啟</li><li><input type="radio" value="0" name="group[overprint]" $overprint_N />關閉</li></ul>'
		),
		'tcanedit'	=> array(
			'title'	=> '可編輯管理組帖子',
			'desc'	=> '<font color=blue> 說明：</font>可編輯管理組的帖子',
			'html'	=> '<ul class="list_A list_80 cc">
			<li><label><input type="checkbox" name="group[tcanedit][]" value="3" $tcanedit_sel[3]/>' . $ltitle[3] . '</label></li>
			<li><label><input type="checkbox" name="group[tcanedit][]" value="4" $tcanedit_sel[4]/>' . $ltitle[4] . '</label></li>
			<li><label><input type="checkbox" name="group[tcanedit][]" value="5" $tcanedit_sel[5]/>' . $ltitle[5] . '</label></li></ul>'
		),
		'deldiary'	=> array(
			'title'	=> '日誌刪除權限',
			'desc'	=> '<font color=blue> 說明：</font>開啟後可刪除其他用戶日誌',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[deldiary]" $deldiary_Y />開啟</li><li><input type="radio" value="0" name="group[deldiary]" $deldiary_N />關閉</li></ul>'
		),
		'delalbum'	=> array(
			'title'	=> '相冊刪除權限',
			'desc'	=> '<font color=blue> 說明：</font>開啟後可刪除其他用戶相冊和照片',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[delalbum]" $delalbum_Y />開啟</li><li><input type="radio" value="0" name="group[delalbum]" $delalbum_N />關閉</li></ul>'
		),
		'delweibo'	=> array(
			'title'	=> '新鮮事刪除權限',
			'desc'	=> '<font color=blue> 說明：</font>開啟後可刪除其他用戶新鮮事',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[delweibo]" $delweibo_Y />開啟</li><li><input type="radio" value="0" name="group[delweibo]" $delweibo_N />關閉</li></ul>'
		),
		'delactive'	=> array(
			'title'	=> '活動刪除權限',
			'desc'	=> '<font color=blue> 說明：</font>開啟後可刪除個人中心活動',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[delactive]" $delactive_Y />開啟</li><li><input type="radio" value="0" name="group[delactive]" $delactive_N />關閉</li></ul>'
		),
		'recommendactive'	=> array(
			'title'	=> '活動推薦權限',
			'desc'	=> '<font color=blue> 說明：</font>開啟後可操作個人中心活動推薦',
			'html'	=> '<ul class="list_A list_80 cc"><li><input type="radio" value="1" name="group[recommendactive]" $recommendactive_Y />開啟</li><li><input type="radio" value="0" name="group[recommendactive]" $recommendactive_N />關閉</li></ul>'
		)
	)
);
?>