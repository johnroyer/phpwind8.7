<?php
!function_exists('readover') && exit('Forbidden');

$lang['fielderror'] = array (
	'start_time_later_than_end_time'					=> '結束時間必須大於開始時間',
	'signup_end_time_later_than_activity_start_time'	=> '報名結束時間必須早於活動開始時間',
	'invalid_money'										=> '不是有效金額',
	'invalid_calendar'									=> '不是有效時間',
	'invalid_fees_format'								=> '費用格式錯誤',
	'no_fees_defined'									=> '未定義費用',
	'invalid_fees_detail_format'						=> '經費明細格式錯誤',
	'invalid_telephone_format'							=> '不是有效的電話號碼',
	'invalid_participant_number'						=> '人數限制不是有效數值',
	'minimum_larger_than_maximum'						=> '最小人數不能大於最大人數',
	'max_less_than_people_already_signup'				=> '最大人數不能小於已報名人數',
	'invalid_input'										=> '內容有誤',
	'invalid_contact_format'							=> '請輸入聯繫人',
	'invalid_feescondition'								=> '請定義活動費用',
);
