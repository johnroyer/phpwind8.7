<?php
!function_exists('readover') && exit('Forbidden');

$lang['creditpop'] = array (

	/*
	* 論壇相關操作
	*/
	'main_buygroup'		=> '使用用戶組身份購買功能，花費積分 ',
	'main_showsign'		=> '使用簽名展示功能，花費積分 ',
	'main_forumsell'	=> '購買版塊訪問權限，花費積分 ',
	'main_changereduce'	=> '使用積分轉換功能，轉出積分 ',
	'main_changeadd'	=> '使用積分轉換功能，轉進積分 ',
	'main_virefrom'		=> '給其他用戶轉帳，轉出積分 ',
	'main_vireto'		=> '收到用戶轉帳，轉進積分 ',
	'main_olpay'		=> '使用在線充值功能，充值積分 ',

	/*
	* 註冊
	*/
	'reg_register'		=> '註冊成功，獎勵積分 ',

	/*
	* 主題相關操作
	*/
	'topic_upload'		=> '上傳附件，獎勵積分 ',
	'topic_download'	=> '下載附件，消耗積分 ',
	'topic_Post'		=> '發表主題，獎勵積分 ',
	'topic_Reply'		=> '回復帖子，獎勵積分 ',
	'topic_Digest'		=> '被設置精華，獎勵積分 ',
	'topic_Delete'		=> '主題被刪除，扣除積分 ',
	'topic_Deleterp'	=> '回復被刪除，扣除積分 ',
	'topic_Undigest'	=> '主題被取消精華，扣除積分 ',
	'topic_buy'			=> '購買帖子閱讀權限，消耗積分 ',
	'topic_sell'		=> '出售帖子閱讀權限成功，獲得積分 ',
	'topic_attbuy'		=> '購買附件下載權限，消耗積分 ',
	'topic_attsell'		=> '出售附件下載權限，獲得積分 ',
	'reply_reward'		=> '回帖獎勵，獎勵積分 ',


	/*
	* 評分相關操作
	*/
	'credit_showping'	=> '發表的帖子評分，獎勵積分 ',
	'credit_delping'	=> '發表的帖子被取消評分，扣除積分 ',

	/*
	* 懸賞相關操作
	*/
	'reward_new'		=> '發佈懸賞，消耗積分 ',
	'reward_modify'		=> '追加懸賞積分，消耗積分 ',
	'reward_answer'		=> '獲得最佳答案獎勵，獎勵積分 ',
	'reward_active'		=> '獲得熱心助人獎勵，獎勵積分 ',
	'reward_return'		=> '懸賞帖已經結束，系統返回積分 ',

	/*
	* 插件相關操作
	*/
	'hack_banksave1'	=> '在銀行活期存款，存入積分 ',
	'hack_banksave2'	=> '在銀行定期存款，存入積分 ',
	'hack_bankdraw1'	=> '在銀行活期取款，取出積分 ',
	'hack_bankdraw2'	=> '在銀行定期取款，取出積分 ',
	'hack_cytransfer'	=> '朋友圈轉讓，消耗積分 ',
	'hack_cycreate'		=> '創建朋友圈，消耗積分 ',
	'hack_cyjoin'		=> '加入群組，消耗積分 ',
	'hack_cydonate'		=> '給群帳戶充值積分，消耗積分 ',
	'hack_cyvire'		=> '獲得來自群的積分轉帳，轉帳積分 ',
	'hack_cyalbum'		=> '創建了一個相冊，消耗積分 ',
	'hack_dbpost'		=> '發表辯論主題，獎勵積分 ',
	'hack_dbreply'		=> '發表辯論觀點，獎勵積分 ',
	'hack_dbdelt'		=> '發表的辯論主題被刪除，扣除積分 ',
	'hack_toolubuy'		=> '向用戶購買道具，消耗積分 ',
	'hack_toolbuy'		=> '購買道具，消耗積分 ',
	'hack_toolsell'		=> '向用戶出售道具，獲得積分 ',
	'hack_teampay'		=> '獲得月份考核獎勵，獲得積分 ',
	'hack_invcodebuy'	=> '購買邀請碼',
	/* phpwind.net */
	'hack_creditget'	=> '領取積分兌換功能贈送的積分，領取積分 ',
	'hack_creditaward'	=> '使用積分兌換禮品，兌換積分 ',

	//運氣卡
	'hack_creditluckadd' =>'[b]{$L[username]}[/b] 使用運氣卡獲得積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'hack_creditluckdel' =>'[b]{$L[username]}[/b] 使用運氣卡減少積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',

	/*
	* 分享相關
	*/
	'share_Delete'		=> '刪除分享，扣除積分',
	'share_Post'		=> '發表分享，增加積分',

	/*
	* 日誌相關
	*/
	'diary_Delete'		=> '刪除日誌，扣除積分',
	'diary_Post'		=> '發表日誌，增加積分',

	/*
	* 群組相關
	*/
	'groups_Uploadphoto'=> '上傳照片，增加積分',
	'groups_Deletephoto'=> '刪除照片，扣除積分',
	'groups_Createalbum'=> '創建相冊，扣除積分',
	'groups_Postarticle'=> '發佈文章，增加積分',
	'groups_Deletearticle'=> '刪除文章，扣除積分',
	'groups_Joingroup'  => '加入群組，扣除積分',
	'groups_Creategroup'=> '創建群組，扣除積分',

	/*
	* 相冊相關
	*/
	'photos_Deletephoto'=> '刪除照片，扣除積分',
	'photos_Uploadphoto'=> '上傳照片，增加積分',
	'photos_Createalbum'=> '創建相冊，扣除積分',

	/*
	* 記錄相關
	*/
	'write_Post'        => '發表記錄，增加積分',
	'write_Delete'      => '刪除記錄，扣除積分',

	/*
	* 其他
	*/
	'other_present'		=> '獲得操作節日送禮贈送的積分，贈送積分 ',
	'other_propaganda' 	=> '宣傳獎勵',

);
?>