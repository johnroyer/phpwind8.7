<?php
!function_exists('readover') && exit('Forbidden');

$lang['creditlog'] = array (

	/*
	* 論壇相關操作
	*/
	'main_buygroup'		=> '[b]{$L[username]}[/b] 使用用戶組身份購買功能，購買用戶組($L[gptitle])身份{$L[days]}天。\n花費積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'main_showsign'		=> '[b]{$L[username]}[/b] 使用簽名展示功能。\n花費積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'main_forumsell'	=> '[b]{$L[username]}[/b] 購買[b]{$L[fname]}[/b]版塊訪問權限{$L[days]}天，\n花費積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'main_changereduce'	=> '[b]{$L[username]}[/b] 使用 [b]{$L[cname]}[/b] -> [b]{$L[tocname]}[/b] 積分轉換功能。\n轉出積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'main_changeadd'	=> '[b]{$L[username]}[/b] 使用 [b]{$L[fromcname]}[/b] -> [b]{$L[cname]}[/b] 積分轉換功能。\n轉進積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'main_virefrom'		=> '[b]{$L[username]}[/b] 給用戶 [b]{$L[toname]}[/b] 轉帳{$L[cname]}。\n轉出積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'main_vireto'		=> '[b]{$L[username]}[/b] 收到用戶 [b]{$L[fromname]}[/b] 轉帳的{$L[cname]}。\n轉進積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'main_olpay'		=> '[b]{$L[username]}[/b] 使用在線充值功能，充值金額：{$L[number]}。\n充值積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',

	/*
	* 註冊
	*/
	'reg_register'		=> '[b]{$L[username]}[/b] 註冊成功。\n獎勵積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',

	/*
	* 主題相關操作
	*/
	'topic_upload'		=> '[b]{$L[username]}[/b] 在版塊 {$L[fname]} 上傳附件。\n獎勵積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'topic_download'	=> '[b]{$L[username]}[/b] 在版塊 {$L[fname]} 下載附件。\n消耗積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'topic_Post'		=> '[b]{$L[username]}[/b] 在版塊 {$L[fname]} 發表主題。\n獎勵積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'topic_Reply'		=> '[b]{$L[username]}[/b] 在版塊 {$L[fname]} 回復帖子。\n獎勵積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'topic_Digest'		=> '[b]{$L[username]}[/b] 在版塊 {$L[fname]} 的主題被 {$L[operator]} 設置精華。\n獎勵積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'topic_Delete'		=> '[b]{$L[username]}[/b] 在版塊 {$L[fname]} 的主題被 {$L[operator]} 刪除。\n扣除積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'topic_Deleterp'	=> '[b]{$L[username]}[/b] 在版塊 {$L[fname]} 的回復被 {$L[operator]} 刪除。\n扣除積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'topic_Undigest'	=> '[b]{$L[username]}[/b] 在版塊 {$L[fname]} 的主題被 {$L[operator]} 取消精華。\n扣除積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'topic_buy'			=> '[b]{$L[username]}[/b] 購買 [b][url={$GLOBALS[db_bbsurl]}/read.php?tid=$L[tid]]{$L[subject]}[/url][/b] 帖子閱讀權限。\n消耗積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'topic_sell'		=> '[b]{$L[username]}[/b] 向 {$L[buyer]} 出售 [b][url={$GLOBALS[db_bbsurl]}/read.php?tid=$L[tid]]{$L[subject]}[/url][/b] 帖子閱讀權限成功。\n獲得積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'topic_attbuy'		=> '[b]{$L[username]}[/b] 購買附件下載權限。\n消耗積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'topic_attsell'		=> '[b]{$L[username]}[/b] 向 {$L[buyer]} 出售附件下載權限。\n獲得積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',


	/*
	* 評分相關操作
	*/
	'credit_showping'	=> '[b]{$L[username]}[/b] 發表的帖子：[url={$GLOBALS[db_bbsurl]}/read.php?tid={$L[tid]}]{$L[subject]}[/url] 被 [b]{$L[operator]}[/b] 評分。\n原因：{$L[reason]}\n獎勵積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'credit_delping'	=> '[b]{$L[username]}[/b] 發表的帖子：[url={$GLOBALS[db_bbsurl]}/read.php?tid={$L[tid]}]{$L[subject]}[/url] 被 [b]{$L[operator]}[/b] 取消評分。\n原因：{$L[reason]}\n扣除積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',

	/*
	* 懸賞相關操作
	*/
	'reward_new'		=> '[b]{$L[username]}[/b] 在版塊 {$L[fname]} 發佈懸賞。\n最佳答案：{$L[cbval]} {$L[cbtype]}，熱心助人：{$L[caval]} {$L[catype]}\n消耗積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'reward_modify'		=> '[b]{$L[username]}[/b] 在版塊 {$L[fname]} 追加懸賞積分。\n最佳答案：+{$L[cbval]} {$L[cbtype]}，熱心助人：+{$L[caval]} {$L[catype]}\n消耗積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'reward_answer'		=> '[b]{$L[username]}[/b] 在版塊 {$L[fname]} 獲得最佳答案獎勵。\n獎勵積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'reward_active'		=> '[b]{$L[username]}[/b] 在版塊 {$L[fname]} 獲得熱心助人獎勵。\n獎勵積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'reward_return'		=> '[b]{$L[username]}[/b] 在版塊 {$L[fname]} 的懸賞帖已經結束。\n系統返回積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',

	/*
	* 插件相關操作
	*/
	'hack_banksave1'	=> '[b]{$L[username]}[/b] 在銀行活期存款。\n存入積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'hack_banksave2'	=> '[b]{$L[username]}[/b] 在銀行定期存款。\n存入積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'hack_bankdraw1'	=> '[b]{$L[username]}[/b] 在銀行活期取款。\n取出積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'hack_bankdraw2'	=> '[b]{$L[username]}[/b] 在銀行定期取款。\n取出積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'hack_cytransfer'	=> '[b]{$L[username]}[/b] 將朋友圈 [b]{$L[cnname]}[/b] 轉讓給 {$L[toname]}。\n消耗積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'hack_cycreate'		=> '[b]{$L[username]}[/b] 創建朋友圈 [b]{$L[cnname]}[/b]。\n消耗積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'hack_cyjoin'		=> '[b]{$L[username]}[/b] 加入群組 [b]{$L[cnname]}[/b]。\n消耗積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'hack_cydonate'		=> '[b]{$L[username]}[/b] 給群 [b]{$L[cnname]}[/b] 帳戶充值積分。\n消耗積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'hack_cyvire'		=> '[b]{$L[username]}[/b] 獲得來自群 [b]{$L[cnname]}[/b] 的積分轉帳\n操作者：{$L[operator]}。\n轉帳積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'hack_cyalbum'		=> '[b]{$L[username]}[/b] 創建了一個相冊 {[b]{$L[aname]}[/b]}。\n消耗積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'hack_dbpost'		=> '[b]{$L[username]}[/b] 發表辯論主題。\n獎勵積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'hack_dbreply'		=> '[b]{$L[username]}[/b] 發表辯論觀點。\n獎勵積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'hack_dbdelt'		=> '[b]{$L[username]}[/b] 發表的辯論主題被 {$L[operator]} 刪除。\n扣除積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'hack_toolubuy'		=> '[b]{$L[username]}[/b] 向用戶 [b]{$L[seller]}[/b] 購買{$L[nums]}個 [b]{$L[toolname]}[/b] 道具。\n消耗積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'hack_toolbuy'		=> '[b]{$L[username]}[/b] 購買{$L[nums]}個 [b]{$L[toolname]}[/b] 道具。\n消耗積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'hack_toolsell'		=> '[b]{$L[username]}[/b] 向用戶 [b]{$L[buyer]}[/b] 出售道具 [b]{$L[toolname]}[/b]。\n獲得積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'hack_teampay'		=> '[b]{$L[username]}[/b] 獲得 {$L[datef]} 月份考核獎勵。\n獲得積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'hack_invcodebuy'	=> '[b]{$L[username]}[/b] 購買 {$L[invnum]} 個邀請碼。\n消耗積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	/* phpwind.net */
	'hack_creditget'	=> '[b]{$L[username]}[/b] 領取積分兌換功能贈送的積分。\n領取積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'hack_creditaward'	=> '[b]{$L[username]}[/b] 使用積分兌換禮品 [b]{$L[subject]}[/b] {$L[num]}件。\n兌換積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',


	/*
	* 分享相關
	*/
	'share_Delete'		=> '[b]{$L[username]}[/b] 刪除分享扣除積分。\n扣除積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'share_Post'		=> '[b]{$L[username]}[/b] 發表分享增加積分。\n增加積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',

	/*
	* 日誌相關
	*/
	'diary_Delete'		=> '[b]{$L[username]}[/b] 刪除日誌扣除積分。\n扣除積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'diary_Post'		=> '[b]{$L[username]}[/b] 發表日誌增加積分。\n增加積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',

	/*
	* 群組相關
	*/
	'groups_Uploadphoto'=> '[b]{$L[username]}[/b] 上傳照片增加積分。\n增加積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'groups_Deletephoto'=> '[b]{$L[username]}[/b] 刪除照片扣除積分。\n扣除積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'groups_Createalbum'=> '[b]{$L[username]}[/b] 創建相冊扣除積分。\n扣除積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'groups_Postarticle'=> '[b]{$L[username]}[/b] 發佈文章增加積分。\n增加積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'groups_Deletearticle'=> '[b]{$L[username]}[/b] 刪除文章扣除積分。\n扣除積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'groups_Joingroup'  => '[b]{$L[username]}[/b] 加入群組扣除積分。\n扣除積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'groups_Creategroup'=> '[b]{$L[username]}[/b] 創建群組扣除積分。\n扣除積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',

	/*
	* 相冊相關
	*/
	'photos_Deletephoto'=> '[b]{$L[username]}[/b] 刪除照片扣除積分。\n扣除積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'photos_Uploadphoto'=> '[b]{$L[username]}[/b] 上傳照片增加積分。\n增加積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'photos_Createalbum'=> '[b]{$L[username]}[/b] 創建相冊扣除積分。\n扣除積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',

	/*
	* 新鮮事相關
	*/
	'weibo_Post'=> '[b]{$L[username]}[/b] 發表新鮮事增加積分。\n增加積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'weibo_Delete'=> '[b]{$L[username]}[/b] 刪除新鮮事扣除積分。\n扣除積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',


	/*
	* 其他
	*/
	'other_finishjob'   => '[b]{$L[username]}[/b] 完成 [b]{$GLOBALS[job]}[/b] 任務,獲得系統 贈送的積分。\n獎勵積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'other_finishpunch'   => '[b]{$L[username]}[/b] 完成 [b]每日打卡[/b] 任務,獲得系統 贈送的積分。\n獎勵積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'other_present'		=> '[b]{$L[username]}[/b] 獲得由 [b]{$L[admin]}[/b] 操作節日送禮贈送的積分。\n贈送積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',
	'other_propaganda'  =>'[b]{$L[username]}[/b] 由於發送宣傳鏈接獲得贈送積分。\n贈送積分：[b]{$L[cname]}[/b]，影響：{$L[affect]}。',


);
?>