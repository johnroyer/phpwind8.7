<?php
!function_exists('readover') && exit('Forbidden');

$lang['toollog'] = array (

	'sell'				=> '轉讓',
	'sell_descrip'		=> '[b]{$L[username]}[/b] 轉讓道具： [b]{$L[toolname]}[/b] ,數量：{$L[nums]}。',

	'buy'				=> '購買',
	'buy_descrip'		=> '[b]{$L[username]}[/b] 向系統購買道具： [b]{$L[toolname]}[/b] ,數量：{$L[nums]},花費：{$L[money]}。',

	'buyuser_descrip'	=> '[b]{$L[username]}[/b] 向[b]{$L[from]}[/b]購買道具： [b]{$L[toolname]}[/b] ,數量：{$L[nums]},花費：{$L[money]}。',
	'buyself_descrip'	=> '[b]{$L[username]}[/b] 購回自己轉讓的道具： [b]{$L[toolname]}[/b] ,數量：{$L[nums]}',

	'change'			=> '轉換',
	'change_descrip_1'	=> '[b]{$L[username]}[/b] 使用[b]交易幣轉換[/b]功能，獲得 {$L[creditinfo]}，總計花費交易幣數：{$L[currency]}',
	'change_descrip_2'	=> '[b]{$L[username]}[/b] 使用[b]交易幣轉換[/b]功能，轉換 $L[creditinfo] 為交易幣，總共獲得交易幣：$L[currency]',
	'use'				=> '使用',

	'tool_1_descrip'	=> '[b]{$L[username]}[/b] 使用道具 [b]{$L[toolname]}[/b] ,將負威望轉為 0。',

	'tool_2_descrip'	=> '[b]{$L[username]}[/b] 使用道具 [b]{$L[toolname]}[/b] ,將所有負積分轉為 0。',

	'tool_3_descrip'	=> '[b]{$L[username]}[/b] 使用道具 [b]{$L[toolname]}[/b] ,將帖子 [url=${$GLOBALS[db_bbsurl]}/read.php?tid=$L[tid]][b]$L[subject][/b][/url] 加亮顯示。',

	'tool_4_descrip'	=> '[b]{$L[username]}[/b] 使用道具 [b]{$L[toolname]}[/b] ,將帖子 [url={$GLOBALS[db_bbsurl]}/read.php?tid=$L[tid]][b]$L[subject][/b][/url] 在版塊中置頂。',

	'tool_5_descrip'	=> '[b]{$L[username]}[/b] 使用道具 [b]{$L[toolname]}[/b] ,將帖子 [url=${$GLOBALS[db_bbsurl]}/read.php?tid=$L[tid]][b]$L[subject][/b][/url] 在所在分類中置頂。',

	'tool_6_descrip'	=> '[b]{$L[username]}[/b] 使用道具 [b]{$L[toolname]}[/b] ,將帖子 [url=${$GLOBALS[db_bbsurl]}/read.php?tid=$L[tid]][b]$L[subject][/b][/url] 在整個論壇中置頂。',

	'tool_7_descrip'	=> '[b]{$L[username]}[/b] 使用道具 [b]{$L[toolname]}[/b] ,將帖子 [url=${$GLOBALS[db_bbsurl]}/read.php?tid=$L[tid]][b]$L[subject][/b][/url] 提前到該版塊首頁。',

	'tool_8_descrip'	=> '[b]{$L[username]}[/b] 使用道具 [b]{$L[toolname]}[/b] ,將自己在論壇的用戶名改為[b]{$L[newname]}[/b]。',

	'tool_9_descrip'	=> '[b]{$L[username]}[/b] 使用道具 [b]{$L[toolname]}[/b] ,將帖子 [url=${$GLOBALS[db_bbsurl]}/read.php?tid=$L[tid]][b]$L[subject][/b][/url] 加為精華I。',

	'tool_10_descrip'	=> '[b]{$L[username]}[/b] 使用道具 [b]{$L[toolname]}[/b] ,將帖子 [url=${$GLOBALS[db_bbsurl]}/read.php?tid=$L[tid]][b]$L[subject][/b][/url] 加為精華II。',

	'tool_11_descrip'	=> '[b]{$L[username]}[/b] 使用道具 [b]{$L[toolname]}[/b] ,將帖子 [url=${$GLOBALS[db_bbsurl]}/read.php?tid=$L[tid]][b]$L[subject][/b][/url] 鎖定。',

	'tool_12_descrip'	=> '[b]{$L[username]}[/b] 使用道具 [b]{$L[toolname]}[/b] ,將帖子 [url=${$GLOBALS[db_bbsurl]}/read.php?tid=$L[tid]][b]$L[subject][/b][/url] 解除鎖定。',

	'tool_13_descrip'	=> '[b]{$L[username]}[/b] 使用道具 [b]{$L[toolname]}[/b] ,將帖子 [url=${$GLOBALS[db_bbsurl]}/read.php?tid=$L[tid]][b]$L[subject][/b][/url] 支持度加1。',

	'tool_14_descrip'	=> '[b]{$L[username]}[/b] 使用道具 [b]{$L[toolname]}[/b] ,將帖子 [url=${$GLOBALS[db_bbsurl]}/read.php?tid=$L[tid]][b]$L[subject][/b][/url] 反對度加1。',

	'tool_15_descrip'	=> '[b]{$L[username]}[/b] 使用道具 [b]{$L[toolname]}[/b] ,使自己獲得[b]{$L[newcurrency]}[/b]交易幣。',

	'tool_16_descrip'	=> '[b]{$L[username]}[/b] 使用道具 [b]{$L[toolname]}[/b] ,為對方送去生日祝福。',

	'tool_17_descrip'	=> '[b]{$L[username]}[/b] 使用道具 [b]{$L[toolname]}[/b] ,將帖子 [url=${$GLOBALS[db_bbsurl]}/read.php?tid=$L[tid]][b]$L[subject][/b][/url] 停止在12小時以前。',

	'tool_18_descrip'	=> '[b]{$L[username]}[/b] 使用道具 [b]{$L[toolname]}[/b] ,將對方頭像變成豬頭。',
	'tool_19_descrip'	=> '[b]{$L[username]}[/b] 使用道具 [b]{$L[toolname]}[/b] ,還原豬頭頭像。',
	'tool_20_descrip'	=> '[b]{$L[username]}[/b] 使用道具 [b]{$L[toolname]}[/b] ,查看IP。',

	'tool_21_descrip'	=> '[b]{$L[username]}[/b] 使用道具 [b]{$L[toolname]}[/b] ,對特定用戶使用了護身符。',

	'tool_22_descrip'	=> '[b]{$L[username]}[/b] 使用道具 [b]{$L[toolname]}[/b] ,將帖子 [url={$GLOBALS[db_bbsurl]}/read.php?tid=$L[tid]][b]$L[subject][/b][/url] 停止在12小時以後。',

	'olpay'				=> '充值',
	'olpay_descrip'		=> '[b]{$L[username]}[/b] 使用交易幣充值功能，充值金額：{$L[number]}，獲得交易幣：{$L[currency]}',
	'colony'			=> '建群',
	'colony_descrip'	=> '[b]{$L[username]}[/b] 使用創建群功能創建群[b]{$L[cname]}[/b]，總共花費交易幣：{$L[currency]}。',
	'join'				=> '加群',
	'join_descrip'		=> '[b]{$L[username]}[/b] 加入群[b]{$L[cname]}[/b]，花費交易幣：{$L[currency]}。',
	'sign'				=>	'簽名',
	'sign_descrip'		=> '[b]{$L[username]}[/b] 使用簽名展示功能，花費{$L[ctype]}：{$L[currency]}{$L[cunit]}。',
	'vire'				=>	'轉帳',
	'vire_descrip'		=> '[b]{$L[username]}[/b] 使用交易幣轉帳功能，給用戶[b]{$L[toname]}[/b]轉帳 {$L[currency]}交易幣，'
							. '系統收取手續費：{$L[tax]}。',
	'donate'			=> '提升',
	'donate_descrip'	=> '[b]{$L[username]}[/b] 使用提升我的榮譽點功能，消耗交易幣：{$L[currency]}。',
	'cyvire_descrip'	=> '[b]{$L[username]}[/b] 使用群交易幣管理功能，給用戶[b]{$L[toname]}[/b]轉帳 {$L[currency]}交易幣，'
							. '系統收取手續費：{$L[tax]}。',
	'group'				=> '用戶組',
	'group_descrip'		=> '[b]{$L[username]}[/b] 使用用戶組身份購買功能，購買用戶組($L[gptitle])身份{$L[days]}天，'
							. '總共花費{$L[curtype]}數：{$L[currency]}。',
	);
?>