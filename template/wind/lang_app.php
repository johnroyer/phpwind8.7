<?php
!function_exists('readover') && exit('Forbidden');

$lang['app'] = array (

'group'			=> '群組',
'active'		=> '活動',
'groupactive'	=> '群組活動',
'defaultalbum'	=> '默認相冊',
'photo_belong'	=> '屬於：',
'video'			=> '視頻',
'music'			=> '音樂',
'web'			=> '網頁',
'flash'			=> 'flash',
'user'			=> '用戶',
'photo'			=> '照片',
'album'			=> '相冊',
'reply'			=> '回復',
'house'			=> '樓盤',
'sale'          => '出售房源',
'hire'          => '出租房源',

'diary'			=> '日誌',
'topic'			=> '帖子',

'video_recommend'	=> '推薦了一個視頻',
'music_recommend'	=> '推薦了一個音樂',
'web_recommend'		=> '推薦了一個網頁',
'flash_recommend'	=> '收藏了一個flash',

'diary_recommend'		=> '推薦了一篇日誌',
'photo_recommend'		=> '推薦了一張照片',
'album_recommend'		=> '推薦了一個相冊',
'group_recommend'		=> '推薦了一個群組',
'groupactive_recommend'	=> '推薦了一個群組活動',
'topic_recommend'		=> '推薦了一篇帖子',
'reply_recommend'		=> '推薦了一篇回復',
'cms_recommend'			=> '推薦了一篇文章',
'house_recommend'		=> '推薦了一個樓盤',

'weibo'			=> '新鮮事',
'multimedia'	=> '多媒體',
'cms'			=> '文章',
'postfavor'			=> '帖子',
'tucool'			=> '圖酷',
'collection_type_name'	=> '[{$L[type]}] 收藏於: {$L[postdate]}',
'collection_postfavor_name'	=> '[{$L[type]}] 更新於: {$L[postdate]}',
'ajax_sendweibo_info' => '我在用戶[url={$L[db_bbsurl]}/{$GLOBALS[db_userurl]}{$L[uid]}]{$L[username]}[/url]的個人空間發現這個信息，認為很有價值，特別推薦。\\n\\n主要信息:\\n{$L[title]}\\n\\n描述:\\n{$L[descrip]}\\n\\n希望你能喜歡。',
'ajax_sendweibo_groupinfo' => '我在群組「{$L[cname]}」發現這個信息，認為很有價值，特別推薦。\\n\\n主要信息:\\n{$L[title]}\\n\\n描述:\\n{$L[descrip]}\\n\\n希望你能喜歡。',
'ajax_sendweibo_cmsinfo' => '我發現了一篇文章，認為很有價值，特別推薦。\\n\\n主要信息:\\n{$L[title]}\\n\\n描述:\\n{$L[descrip]}\\n\\n希望你能喜歡。',
'ajax_sendweibo_houseinfo' => '我發現一個樓盤：{$L[title]} &nbsp;認為很有價值，特別推薦給你。\\n樓盤位置: {$L[postion]} \\n希望你能喜歡。',
);
?>