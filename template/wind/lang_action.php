<?php
!function_exists('readover') && exit('Forbidden');

$lang['action'] = array (
	//-------兼容PW < v7.3.2----------------
	'hm'				=>'社區首頁',
	'mb'				=>'會員列表',
	'sc'				=>'站內搜索',
	'rg'				=>'註冊中...',
	'vt'				=>'今日到訪會員',
	'hp'				=>'查看幫助',
	'nt'				=>'公告中心',
	'st'				=>'統計信息',
	//--------------------------------------
	'index'				=>'社區首頁',
	'member'			=>'會員列表',
	'search'			=>'站內搜索',
	'register'			=>'註冊中...',
	'viewtody'			=>'今日到訪會員',
	'faq'				=>'查看幫助',
	'notice'			=>'公告中心',
	'sort'				=>'統計信息',
	'survey'			=>'社區大調查',
	'bank'				=>'社區銀行',
	'medal'				=>'勳章中心',
	'u'					=>'用戶中心',
	'userpay'			=>'積分管理',
	'show'				=>'社區展區',
	'rss'				=>'RSS聚合',
	'mode'				=>'社區模式',
	'toolcenter'		=>'道具中心',
	'invite'			=>'邀請註冊',
	'read'				=>'閱讀帖子',
	'thread'			=>'查看帖子',
	'post'				=>'發表帖子',
	'other'				=>'閒逛中...',
);
?>