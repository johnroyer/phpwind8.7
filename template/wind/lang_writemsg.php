<?php
!function_exists('readover') && exit('Forbidden');

$lang['_othermsg'] = '\n\n[b]帖子：[/b][url=$GLOBALS[db_bbsurl]/read.php?tid={$L[tid]}]{$L[subject]}[/url]\n'
					. '[b]發表日期：[/b]{$L[postdate]}\n'
					. '[b]所在版塊：[/b][url=$GLOBALS[db_bbsurl]/thread.php?fid={$L[fid]}]{$L[forum]}[/url]\n'
					. '[b]操作時間：[/b]{$L[admindate]}\n'
					. '[b]操作理由：[/b]{$L[reason]}\n\n'
					. '論壇管理操作通知短消息，對本次管理操作有任何異議，請與我取得聯繫。';
$lang['_othermsg1'] = '\n\n[b]帖子：[/b][url=$GLOBALS[db_bbsurl]/job.php?action=topost&tid={$L[tid]}&pid={$L[pid]}]{$L[subject]}[/url]\n'
					. '[b]發表日期：[/b]{$L[postdate]}\n'
					. '[b]所在版塊：[/b][url=$GLOBALS[db_bbsurl]/thread.php?fid={$L[fid]}]{$L[forum]}[/url]\n'
					. '[b]操作時間：[/b]{$L[admindate]}\n'
					. '[b]操作理由：[/b]{$L[reason]}\n\n'
					. '論壇管理操作通知短消息，對本次管理操作有任何異議，請與我取得聯繫。';
$lang['_othermsg_colony'] = '\n\n[b]帖子：[/b][url=$GLOBALS[db_bbsurl]/job.php?action=topost&tid={$L[tid]}&pid={$L[pid]}]{$L[subject]}[/url]\n'
							. '[b]發表日期：[/b]{$L[postdate]}\n'
							. '[b]所在版塊：[/b][url=$GLOBALS[db_bbsurl]/thread.php?fid={$L[fid]}]{$L[forum]}[/url]\n'
							. '[b]操作時間：[/b]{$L[admindate]}\n'
							. '[b]操作理由：[/b]{$L[reason]}\n\n'
							. '論壇管理操作通知短消息，對本次管理操作有任何異議，請與我取得聯繫。';

$lang['writemsg'] = array (
	'olpay_title'			=> '積分充值支付成功.',
	'olpay_content'			=> '交易幣充值支付成功，您需要登錄支付寶使用「[color=red]確認收貨[/color]」功能完成本次交易。\n'
								. '確認收貨後系統會自動對您的交易幣帳戶進行充值。',
	'olpay_content_2'		=> '積分充值成功，本次充值金額：{$L[number]}RMB，總共獲得{$L[cname]}個數：{$L[currency]}。\n謝謝使用！',

	'toolbuy_title'			=> '道具購買支付成功!',
	'toolbuy_content'		=> '道具購買成功，本次支付金額：{$L[fee]}RMB，共購得[b]{$L[toolname]}[/b]道具 [b]{$L[number]}[/b] 個!',

	'forumbuy_title'		=> '版塊訪問權限購買支付成功!',
	'forumbuy_content'		=> '版塊訪問權限購買成功，本次支付金額：{$L[fee]}RMB，共購得版塊 [b]{$L[fname]}[/b] 訪問期限 [b]{$L[number]}[/b] 天!',

	'groupbuy_title'		=> '特殊組購買支付成功!',
	'groupbuy_content'		=> '特殊組身份購買成功，本次支付金額：{$L[fee]}RMB，共購得用戶組 [b]{$L[gname]}[/b] 身份 [b]{$L[number]}[/b] 天!',

	'virement_title'		=> '銀行匯款通知!!',
	'virement_content'		=> '用戶{$L[windid]}通過銀行給你轉帳{$L[to_money]}元錢，'
								. '系統自動把以前的利息加到你的存款中，你的利息將從現在重新開始計算\n轉帳附言：{$L[memo]}',
	'metal_add'				=> '授予勳章通知',
	'metal_post_title'	    => '您的勳章申請已提交',
	'metal_post_content'    => '您的勳章申請已提交，正在審核中\n\n勳章名稱：{$L[mname]}\n操作：{$L[windid]}\n理由：{$L[reason]}',
	'metal_add_content'		=> '您被授予勳章\n\n勳章名稱：{$L[mname]}\n操作：{$L[windid]}\n理由：{$L[reason]}',
	'metal_cancel'			=> '收回勳章通知',
	'metal_cancel_content'	=> '您的勳章被收回\n\n勳章名稱：{$L[mname]}\n操作：{$L[windid]}\n理由：{$L[reason]}',
	'metal_cancel_text'		=> '您的勳章被收回\n\n勳章名稱：{$L[medalname]}\n操作：SYSTEM\n理由：過期',
	'metal_refuse'			=> '勳章申請未通過',
	'metal_refuse_content'	=> '您的勳章申請未通過審核\n\n勳章名稱：{$L[mname]}\n操作：{$L[windid]}\n理由：{$L[reason]}',
	'medal_apply_title'		=> '勳章申請通知!',
	'medal_apply_content'	=> '用戶 {$L[username]} 於 {$L[time]} 申請了 {$L[medal]} 勳章，請求您審核。',
	'vire_title'			=> '交易幣轉帳通知',
	'vire_content'			=> '用戶 [b]{$L[windid]}[/b] 使用積分轉帳功能，給您轉帳 {$L[paynum]} {$L[cname]}，請注意查收。',
	'cyvire_title'			=> '{$L[cn_name]}{$L[moneyname]}轉帳通知',
	'cyvire_content'		=> '{$L[cn_name]}([url=$GLOBALS[db_bbsurl]/hack.php?'
								. 'H_name=colony&cyid={$L[cyid]}&job=view&id={$L[cyid]}]'
								. '{$L[all_cname]}[/url])管理員使用{$L[moneyname]}管理功能，'
								. '給你轉帳 {$L[currency]} {$L[moneyname]}，請注意查收。',
	'donate_title'			=> '{$L[cn_name]}捐獻通知消息',
	'donate_content'		=> '用戶{$L[windid]}通過捐獻功能，給{$L[cn_name]}({$L[allcname]})'
								. '捐獻{$L[moneyname]}：{$L[sendmoney]}。',

	'top_title'				=> '您的帖子被置頂.',
	'untop_title'			=> '您的帖子被解除置頂.',
	'top_content'			=> '您發表的帖子被 [b]{$L[manager]}[/b] 執行 [b]置頂[/b] 操作'.$lang['_othermsg'],
	'untop_content'			=> '您的帖子被 [b]{$L[manager]}[/b] 執行 [b]解除置頂[/b] 操作'.$lang['_othermsg'],
	'digest_title'			=> '您的帖子被設為精華帖',
	'digest_content'		=> '您發表的帖子被 [b]{$L[manager]}[/b] 執行 [b]精華[/b] 操作\n\n'
								. '對您的影響：{$L[affect]}'.$lang['_othermsg'],
	'undigest_title'		=> '您的帖子被取消精華',
	'undigest_content'		=> '您發表的帖子被 [b]{$L[manager]}[/b] 執行 [b]取消精華[/b] 操作\n\n'
								. '對您的影響：{$L[affect]}'.$lang['_othermsg'],
	'lock_title'			=> '您的帖子被鎖定',
	'lock_content'			=> '您發表的帖子被 [b]{$L[manager]}[/b] 執行 [b]鎖定[/b] 操作'.$lang['_othermsg'],
	'lock_title_2'			=> '您的帖子被關閉',
	'lock_content_2'		=> '您發表的帖子被 [b]{$L[manager]}[/b] 執行 [b]關閉[/b] 操作'.$lang['_othermsg'],
	'unlock_title'			=> '您的帖子被解除鎖定',
	'unlock_content'		=> '您發表的帖子被 [b]{$L[manager]}[/b] 執行 [b]解除鎖定[/b] 操作'.$lang['_othermsg'],
	'push_title'			=> '您的帖子被提前',
	'push_content'          => '您發表的帖子被 [b]{$L[manager]}[/b] [b]提前了 {$L[timelimit]} 小時[/b]'.$lang['_othermsg'],
	'recommend_title'		=> '您的帖子被推薦',
	'recommend_content'		=> '您發表的帖子被 [b]{$L[manager]}[/b] 執行 [b]推薦[/b] 操作'.$lang['_othermsg'],
	'pushto_title'			=> '您的帖子被推送',
	'pushto_content'		=> '您發表的帖子被 [b]{$L[manager]}[/b] 執行 [b]推送[/b] 操作'.$lang['_othermsg'],
	'unhighlight_title'		=> '您的帖子標題被取消加亮顯示',
	'unhighlight_content'	=> '您發表的帖子被 [b]{$L[manager]}[/b] 執行 [b]標題取消加亮[/b] 操作'.$lang['_othermsg'],
	'highlight_title'		=> '您的帖子標題被加亮顯示',
	'highlight_content'		=> '您發表的帖子被 [b]{$L[manager]}[/b] 執行 [b]標題加亮[/b] 操作'.$lang['_othermsg'],
	'del_title'				=> '您的帖子被刪除',
	'del_content'			=> '您發表的帖子被 [b]{$L[manager]}[/b] 執行 [b]刪除[/b] 操作\n\n'
								. '對您的影響：{$L[affect]}'.$lang['_othermsg'],
	'move_title'			=> '您的帖子被移動',
	'move_content'			=> '您發表的帖子被 [b]{$L[manager]}[/b] 執行 [b]移動[/b] 操作\n\n'
								. '[b]目的版塊：[/b][url=$GLOBALS[db_bbsurl]/thread.php?fid={$L[tofid]}]{$L[toforum]}[/url]'.$lang['_othermsg'],
	'copy_title'			=> '您的帖子被複製到新版塊',
	'copy_content'			=> '您發表的帖子被 [b]{$L[manager]}[/b] 執行 [b]複製[/b] 操作\n\n'
								. '[b]目的版塊：[/b][url=$GLOBALS[db_bbsurl]/thread.php?fid={$L[tofid]}]{$L[toforum]}[/url]'.$lang['_othermsg'],
	'ping_title'			=> '"{$L[sender]}"給"{$L[receiver]}"的帖子評分',
	'ping_content'			=> '"{$L[sender]}"給[b]"{$L[receiver]}"[/b]的帖子  執行 [b]評分[/b] 操作\n\n'
								. '影響：{$L[affect]}'.$lang['_othermsg1'],
	'delping_title'			=> '"{$L[receiver]}"的帖子被"{$L[sender]}"取消評分',
	'delping_content'		=> '"{$L[receiver]}"的帖子被[b]"{$L[sender]}"[/b] 執行 [b]取消評分[/b] 操作\n\n'
								. '影響：{$L[affect]}'.$lang['_othermsg1'],
	'deltpc_title'			=> '您的帖子被刪除',
	'deltpc_content'		=> '您發表的帖子被 [b]{$L[manager]}[/b] 執行 [b]刪除[/b] 操作\n\n'
								. '影響：{$L[affect]}'.$lang['_othermsg'],
	'delrp_title'			=> '您的回復被刪除',
	'delrp_content'			=> '您發表的回復被 [b]{$L[manager]}[/b] 執行 [b]刪除[/b] 操作\n\n'
								. '對您的影響：{$L[affect]}'.$lang['_othermsg'],
	'reward_title_1'		=> '您的回復被設為最佳答案!',
	'reward_content_1'		=> '您的回復被設為最佳答案!\n\n對您的影響：{$L[affect]}'.$lang['_othermsg'],
	'reward_title_2'		=> '您的回復獲得熱心助人獎勵!',
	'reward_content_2'		=> '您的回復獲得熱心助人獎勵!\n\n對您的影響：{$L[affect]}'.$lang['_othermsg'],
	'endreward_title_1'		=> '您的懸賞被取消!',
	'endreward_title_2'		=> '您的懸賞被強制結案!',
	'endreward_content_1'	=> '由於沒有合適的答案，您的懸賞被管理員 [b]{$L[manager]}[/b] 執行 [b]取消[/b] 操作!\n\n'
								. '系統返回您:{$L[affect]}'.$lang['_othermsg'],
	'endreward_content_2'	=> '由於您長時間未對懸賞帖進行結案,且已經有合適的答案，所以被[b]{$L[manager]}[/b] 執行 [b]強制結案[/b] 操作\n\n'
								. '系統不返回所有積分'.$lang['_othermsg'],
	'rewardmsg_title'		=> '懸賞帖(編號:{$L[tid]})請求處理!',
	'rewardmsg_content'		=> '尊敬的版主:\n\t\t您好!\n\t\t由於該次懸賞帖沒有產生合適答案，現請求您結案,'
								. '希望您仔細查證後，作出公平處理!'.$lang['_othermsg'],
	'rewardmsg_notice_title'	=> '懸賞帖到期通知!',
	'rewardmsg_notice_content'	=> '您的懸賞帖已經到期，系統提醒您盡快作出處理,否則版主有權強行結案!\n\n'
								. '[b]帖子：[/b][url=$GLOBALS[db_bbsurl]/read.php?tid={$L[tid]}]{$L[subject]}[/url]\n'
								. '[b]發表日期：[/b]{$L[postdate]}\n'
								. '[b]所在版塊：[/b][url=$GLOBALS[db_bbsurl]/thread.php?fid={$L[fid]}]{$L[name]}[/url]\n\n'
								. '論壇管理操作通知短消息，對本次管理操作有任何異議，請與我取得聯繫。',
	'shield_title_2'		=> '您的帖子主題被刪除',
	'shield_content_2'		=> '您發表的帖子主題被 [b]{$L[manager]}[/b] [b]刪除[/b]'.$lang['_othermsg'],
	'shield_title_1'		=> '您的帖子被屏蔽',
	'shield_content_1'		=> '您發表的帖子被 [b]{$L[manager]}[/b] [b]屏蔽[/b]'.$lang['_othermsg'],
	'shield_title_0'		=> '您的帖子被取消屏蔽',
	'shield_content_0'		=> '您發表的帖子被 [b]{$L[manager]}[/b] 執行 [b]取消屏蔽[/b] 操作'.$lang['_othermsg'],
	'bansignature_title_0'	=> '您的論壇簽名被解除屏蔽',
	'bansignature_title_1'	=> '您的論壇簽名被禁止',
	'bansignature_content_1'=> '您的論壇簽名被 [b]{$L[manager]}[/b] 於{$L[admindate]}執行 [b]禁止[/b] 操作\n'.
							   '[b]操作理由:[/b]{$L[reason]}',
	'bansignature_content_0'=> '您的論壇簽名被 [b]{$L[manager]}[/b] 於{$L[admindate]}執行 [b]解除屏蔽[/b] 操作',
	'remind_title'			=> '您的帖子被提醒管理',
	'remind_content'		=> '您發表的帖子被 [b]{$L[manager]}[/b] 執行 [b]提醒管理[/b] 操作'.$lang['_othermsg'],
	'unite_title'			=> '您的帖子被{$L[manager]}合併',
	'unite_content'			=> '您發表的帖子被 [b]{$L[manager]}[/b] 執行 [b]合併[/b] 操作'.$lang['_othermsg'],
	'unite_owner_content'	=> '您發表的帖子被 [b]{$L[manager]}[/b] 執行 [b]合併[/b] 操作',
	'leaveword_title'		=> '您的帖子被留言',
	'leaveword_content'		=> '[b]{$L[author]}[/b] 在您發表的帖子上留言了。'.$lang['_othermsg'],
	'birth_title'			=> '$L[userName],祝您生日快樂',
	'birth_content'			=> '[img]$GLOBALS[db_bbsurl]/u/images/birthday.gif[/img]\r\n'
								. '祈願您的生日，為您帶來一個最瑰麗最金碧輝煌的一生。\r\n'
								. '只希望你的每一天都快樂、健康、美麗，生命需要奮鬥、創造、把握！\r\n'
								. '生日的燭光中搖曳一季繁花，每一支都是我的祝願：生日快樂！\r\n\r\n'
								. '--------------------------------------- {$L[fromUsername]} 送上最真摯的祝福！\r\n\r\n',
	'down_title'			=> '您的帖子被執行壓帖操作',
	'down_content'			=> '您發表的帖子被 [b]{$L[manager]}[/b] [b]壓後了 {$L[timelimit]} 小時[/b]'.$lang['_othermsg'],
	'change_type_title'		=> '您的帖子被修改了主題分類',
	'change_type_content'	=> '您發表的帖子主題被 [b]{$L[manager]}[/b] [b]修改了主題分類為：{$L[type]}[/b]'.$lang['_othermsg'],
	'check_title'			=> '您的帖子已通過審核',
	'check_content'			=> '您發表的帖子主題被 [b]{$L[manager]}[/b] [b]通過審核[/b]'.$lang['_othermsg'],

	'post_pass_title'		=> '您發表的回復已經通過審核!',
	'post_pass_content'		=> '您的回復，已經通過審核。[url=$GLOBALS[db_bbsurl]/read.php?tid={$L[tid]}]點擊查看[/url]\n\n',
	'subject_reply_title'	=> '「{$L[windid]}」回復了「{$L[author]}」發表的主題[{$L[title]}]',
	'subject_replytouser_title' => '「{$L[windid]}」在主題[{$L[title]}]中回復了你',
	'subject_reply_content' => '{$L[windid]}說：$L[content]\n\n[url=$GLOBALS[db_bbsurl]/job.php?action=topost&tid={$L[tid]}&pid={$L[pid]}]查看回復[/url] [url=$GLOBALS[db_bbsurl]/read.php?tid={$L[tid]}]查看主題[/url]\n\n',
	'advert_buy_title'		=> '您的廣告位申請已通過審核',
	'advert_buy_content'	=> '本廣告位的價格為：{$L[creditnum]} {$L[creditypename]} 每天\n\n'
								. '你購買的天數為：{$L[days]}',
	'advert_apply_title'	=> '廣告出租位申請通知!',
	'advert_apply_content'	=> '用戶 {$L[username]} 於 {$L[time]} 申請了 {$L[days]} 天的廣告展示，請求您審核。',

	'friend_add_title_1'	=> '好友系統通知：{$L[username]}將您列入他（她）的好友名單',
	'friend_add_content_1'	=> '[url=$GLOBALS[db_bbsurl]/{$GLOBALS[db_userurl]}{$L[uid]}]{$L[username]}[/url] 將您列入他（她）的好友名單。',
	'friend_add_title_2'	=> '好友系統通知：{$L[username]} 請求加您為好友',
	'friend_add_content_2'	=> '[url={$GLOBALS[db_bbsurl]}/{$GLOBALS[db_userurl]}{$L[uid]}]{$L[username]}[/url] 請求加您為好友。是否同意？\n\n$L[msg]\n\n',
	'friend_delete_title'	=> '好友系統通知：{$L[username]} 解除與您的好友關係',
	'friend_delete_content'	=> '[url={$GLOBALS[db_bbsurl]}/{$GLOBALS[db_userurl]}{$L[uid]}]{$L[username]}[/url] 解除與您的好友關係。',
	'friend_accept_title'	=> '好友系統通知：{$L[username]} 通過了您的好友請求',
	'friend_accept_content' => '[url={$GLOBALS[db_bbsurl]}/{$GLOBALS[db_userurl]}{$L[uid]}]{$L[username]}[/url] 通過了您的好友請求。',
	'friend_acceptadd_title'=> '好友系統通知：{$L[username]} 通過了您的好友請求,並加您為好友',
	'friend_acceptadd_content'	=> '[url={$GLOBALS[db_bbsurl]}/{$GLOBALS[db_userurl]}{$L[uid]}]{$L[username]}[/url] 通過了您的好友請求,並加您為好友。',

	'friend_refuse_title'	=> '好友系統通知：{$L[username]} 拒絕了您的好友請求',
	'friend_refuse_content'	=> '[url={$GLOBALS[db_bbsurl]}/{$GLOBALS[db_userurl]}{$L[uid]}]{$L[username]}[/url]'
								. '拒絕了您的好友請求\r\n\r\n[b]拒絕理由：[/b]{$L[msg]}\r\n\r\n',
	'friend_agree_title'	=> '好友系統通知：{$L[username]} 通過了您的好友請求',
	'friend_agree_content'	=> '[url={$GLOBALS[db_bbsurl]}/{$GLOBALS[db_userurl]}{$L[uid]}]{$L[username]}[/url]'
								. '通過了您的好友請求\r\n\r\n',
	'user_update_title'		=> '系統通知',
	'user_update_content'	=> '親愛的{$L[username]}，我們非常高興的告訴你，你剛剛升到了{$L[membername]}級別。你離下一級{$L[upmembername]}級別還有{$L[userneed]}分，要繼續努力哦，<a href="profile.php?action=permission" target="_blank">查看會員組權限</a>',
	'banuser_title'			=> '系統禁言通知',
	'banuser_content_1'		=> '你已經被管理員{$L[manager]} 禁言,禁言時間為{$L[limit]} 天\n\n{$L[reason]}',
	'banuser_content_2'		=> '你已經被管理員{$L[manager]} 禁言\n\n{$L[reason]}',
	'banuser_content_3'		=> '你已經被管理員{$L[manager]} 禁言\n\n{$L[reason]}',

	'banuser_free_title'	=> '解除禁言通知',
	'banuser_free_content'	=> '你已經被管理員{$L[manager]} 解除禁言\n\n{$L[reason]}',

	'onlinepay_logistics'	=> '物流公司：{$L[logistics]}\r\n物流單號：{$L[orderid]}',
	'goods_pay_title'		=> '買家付款通知!',
	'goods_pay_content'		=> '買家 [b]{$L[buyer]}[/b] 於 {$L[buydate]} 下單的商品 [b][url={$GLOBALS[db_bbsurl]}/read.php?tid={$L[tid]}]{$L[goodsname]}[/url][/b] 已經付款，付款信息如下：\r\n\r\n{$L[descrip]}\r\n\r\n請確認後，盡快發貨!',
	'goods_send_title'		=> '賣家發貨通知!',
	'goods_send_content'	=> '您於 {$L[buydate]} 購買的商品[b][url={$GLOBALS[db_bbsurl]}/read.php?tid={$L[tid]}]{$L[goodsname]}[/url][/b]，賣家 [b]{$L[seller]}[/b] 已經發貨，發貨信息為：\r\n\r\n{$L[descrip]}',

	'sharelink_apply_title'		=> '自助友情鏈接申請通知!',
	'sharelink_apply_content'	=> '用戶 {$L[username]} 於 {$L[time]} 申請了友情鏈接展示，請求您審核。',

	'sharelink_pass_title'		=> '自助友情鏈接申請通過通知!',
	'sharelink_pass_content'	=> '您提交的自助友情鏈接申請已通過審核。',

	'o_addadmin_title'		=> '群組通知[{$L[cname]}]：您被升為管理員了!',
	'o_addadmin_content'	=> '您加入的群組[url={$L[curl]}]{$L[cname]}[/url]，已經將您升為管理員了，趕快去看看!',
	'o_deladmin_title'		=> '群組通知[{$L[cname]}]：您被取消管理員身份了!',
	'o_deladmin_content'	=> '您加入的群組[url={$L[curl]}]{$L[cname]}[/url]，已經將您取消管理員身份了，趕快去看看!',
	'o_check_title'			=> '群組通知[{$L[cname]}]：您已正式加入群組了!',
	'o_check_content'		=> '您日前申請加入的群組[url={$L[curl]}]{$L[cname]}[/url]，已經正式批准您加入了，[url={$L[curl]}]趕快去看看[/url]!',

	'o_friend_success_title'	=> '好友系統通知：您和{$L[username]}成為了好友',
	'o_friend_success_cotent'	=> '通過邀請好友：您和[url={$L[myurl]}]{$L[username]}[/url]成為了好友',

	'o_board_success_title'		=> '"{$L[sender]}"給"{$L[receiver]}"留了言',
	'o_board_success_cotent'	=> '{$L[content]} \n[url={$GLOBALS[db_bbsurl]}/u.php?a=board&uid={$L[touid]}]查看更多留言[/url]',

	'o_share_success_title'		=> '"{$L[sender]}"評論了"{$L[receiver]}"的分享',
	'o_share_success_cotent'	=> '{$L[title]}\n\n[url={$GLOBALS[db_bbsurl]}/apps.php?q=share]去我的分享頁面[/url]',
	'o_write_success_title'		=> '"{$L[sender]}"評論了"{$L[receiver]}"的記錄',
	'o_write_success_cotent'	=> '{$L[title]}\n\n[url={$GLOBALS[db_bbsurl]}/apps.php?q=write]去我的記錄頁面[/url]',
	'o_photo_success_title'		=> '"{$L[sender]}"評論了"{$L[receiver]}"的照片',
	'o_photo_success_cotent'	=> '{$L[title]}\n\n[url={$GLOBALS[db_bbsurl]}/apps.php?username={$L[receiver]}&q=photos&a=view&pid={$L[id]}]去此照片頁面[/url]',
	'o_diary_success_title'		=> '"{$L[sender]}"評論了"{$L[receiver]}"的日誌',
	'o_diary_success_cotent'	=> '{$L[title]}\n\n[url={$GLOBALS[db_bbsurl]}/apps.php?username={$L[receiver]}&q=diary&a=detail&did={$L[id]}]查看詳細日誌[/url]',

	'inspect_title'				=> '你的主題已被版主閱讀',
	'inspect_content'			=> '您發表的帖子被 [b]{$L[manager]}[/b] 執行 [b]已閱[/b] 操作\r\n[b]帖子標題：[/b]<a target="_blank" href="read.php?tid={$L[tid]}">{$L[subject]}</a>\r\n[b]操作日期：[/b]{$L[postdate]}\r\n[b]操作理由：[/b]{$L[reason]}',
	
	
	'report_title'				=> '有會員舉報不良信息，請及時處理',
	'report_content'		=> '你舉報的內容已被管理員 [b]{$L[manager]}[/b]處理\r\n [b]類型：[/b]{$L[type]}\r\n[b]操作日期：[/b]{$L[admindate]}\r\n[b]您的舉報理由：[/b]{$L[reason]}\r\n[b]鏈接地址：[/b][url={$L[url]}]進入[/url]',
	'report_content_1_1'	=> '該帖很優秀,建議加為精華帖!'.$lang['_othermsg'],
	'report_content_1_0'	=> '該帖很優秀,建議加為精華帖!'.$lang['_othermsg1'],
	'report_content_0_0'	=> '有會員舉報不良信息，請及時處理!'
							. '\n\n[b]類型：$L[type]\n'
							. '[b]操作時間：[/b]{$L[admindate]}\n'
							. '[b]舉報理由：[/b]{$L[reason]}\n\n'
							. '[b]鏈接地址：[/b][url={$L[url]}]進入[/url]\n\n',
							
	'report_deal_title'			=> '您舉報的內容被已被管理員處理',
	'report_deal_content'		=> '你舉報的內容已被管理員 [b]{$L[manager]}[/b]處理\r\n [b]類型：[/b]{$L[type]}\r\n[b]操作日期：[/b]{$L[admindate]}\r\n[b]您的舉報理由：[/b]{$L[reason]}\r\n[b]鏈接地址：[/b][url={$L[url]}]進入[/url]',
	
	
	
	
	/*'birth_title'				=> '{$L[userName]},祝你生日快樂！',
	'birth_content'				=> '生日快樂，稍上我的祝福，祝你開心每一天！',*/

	'group_attorn_title'		=> '轉讓群組通知',
	'group_attorn_content'		=> '[b]{$L[username]}[/b]將群組「[url={$GLOBALS[db_bbsurl]}/apps.php?q=group&cyid={$L[cyid]}]{$L[cname]}[/url]」轉讓給了您，以下是關於此群組的介紹：{$L[descrip]}',

	'group_invite_title'		=> '轉讓群組通知',
	'email_groupactive_invite_subject' => '{$GLOBALS[windid]}邀請您加入活動{$GLOBALS[objectName]}，並成為他的好友',
	'email_groupactive_invite_content' => '我是{$GLOBALS[windid]}，我在{$GLOBALS[db_bbsname]}上發現了活動{$GLOBALS[objectName]}，下面是關於它的介紹，趕快加入吧！<br />活動{$GLOBALS[objectName]}簡介：<br />{$GLOBALS[objectDescrip]}<br /><div id="customdes">{$GLOBALS[customdes]}</div>請點擊以下鏈接，接受好友邀請<br /><a href="{$GLOBALS[invite_url]}">{$GLOBALS[invite_url]}</a>',
	'email_group_invite_subject' => '{$GLOBALS[windid]}邀請您加入群組{$GLOBALS[objectName]}，並成為他的好友',
	'email_group_invite_content' => '我是{$GLOBALS[windid]}，我在{$GLOBALS[db_bbsname]}上發現了群組{$GLOBALS[objectName]}，下面是關於它的介紹，趕快加入吧！。<br />群組{$GLOBALS[objectName]}簡介：<br />{$GLOBALS[objectDescrip]} [<a href="{$GLOBALS[db_bbsurl]}/apps.php?q=group&cyid={$GLOBALS[id]}">查看群組</a>]  [<a href="{$GLOBALS[db_bbsurl]}/apps.php?q=group&cyid={$GLOBALS[id]}&a=join&invite_flag=1&step=1" onclick="return ajaxurl(this)">加入群組</a>]<br /><div id="customdes">{$GLOBALS[customdes]}</div>請點擊以下鏈接，接受好友邀請<br /><a href="{$GLOBALS[invite_url]}">{$GLOBALS[invite_url]}</a>',
	'message_group_invite_content' => '{$GLOBALS[windid]}邀請你群組<a href="{$GLOBALS[db_bbsurl]}/apps.php?q=group&cyid={$GLOBALS[id]}">{$GLOBALS[objectName]}</a><br />群組{$GLOBALS[objectName]}介紹：{$GLOBALS[objectDescrip]} ',
	'message_groupactive_invite_subject' => '{$GLOBALS[windid]}邀請您加入活動{$GLOBALS[objectName]}',
	'message_groupactive_invite_content' => '{$GLOBALS[windid]}邀請你加入群組<a href="{$GLOBALS[db_bbsurl]}/apps.php?q=group&cyid={$GLOBALS[cyid]}">{$GLOBALS[colonyName]}</a>的活動「{$GLOBALS[objectName]}」<br />活動介紹：{$GLOBALS[objectDescrip]}<br /><a href="{$GLOBALS[db_bbsurl]}/apps.php?q=group&a=active&cyid={$GLOBALS[cyid]}&job=view&id={$GLOBALS[id]}">快去看看</a>吧！ ',
	'filtermsg_thread_pass_title'	=> '【敏感詞】您發表的帖子已經通過審核!',
	'filtermsg_thread_pass_content'	=> '您發佈的帖子：{$L[subject]}，已經通過審核。\n\n',
	'filtermsg_thread_del_title'	=> '【敏感詞】您發表的帖子因包含敏感內容被管理員刪除!',
	'filtermsg_thread_del_content'	=> '您發佈的帖子：{$L[subject]}，因包含敏感內容被管理員刪除。\n\n',
	'filtermsg_post_pass_title'		=> '【敏感詞】您發表的回復已經通過審核!',
	'filtermsg_post_pass_content'	=> '您發表於：{$L[subject]}的回復，已經通過審核。\n\n',
	'filtermsg_post_del_title'		=> '【敏感詞】您發表的回復因包含敏感內容被管理員刪除!',
	'filtermsg_post_del_content'	=> '您發表於：{$L[subject]}的回復，因包含敏感內容被管理員刪除。\n\n',
	'colony_join_title_check'		=> '群組請求[{$L[cname]}]：{$GLOBALS[windid]}申請加入，請審核',
	'colony_join_content_check'		=> '<a href="{$GLOBALS[db_userurl]}{$GLOBALS[winduid]}" target="_blank">{$GLOBALS[windid]}</a>申請加入群組{$L[cname]}，請審核。<a href="{$L[colonyurl]}">查看詳情</a>！',
	'colony_join_title'				=> '群組通知[{$L[cname]}]:{$GLOBALS[windid]}已成功加入',
	'colony_join_content'		=> '用戶<a href="{$GLOBALS[db_userurl]}{$GLOBALS[winduid]}" target="_blank">{$GLOBALS[windid]}</a>加入了群組[{$L[cname]}] <a href="{$L[colonyurl]}">快去看看吧</a>',
	'o_del_title'				=> '群組通知[{$L[cname]}]：已將你移出該群!',
	'o_del_content'			=> '<a href="{$GLOBALS[db_userurl]}{$GLOBALS[winduid]}" target="_blank">{$GLOBALS[windid]}</a>已經將您移出 [url=$L[curl]]{$L[cname]}[/url] 群了!',

	//團購
	'activity_pcjoin_new_title'		=> '{$L[username]}報名參加了您的團購活動',
	'activity_pcjoin_new_content'	=> '[url=$GLOBALS[db_bbsurl]/{$GLOBALS[db_userurl]}{$L[uid]}]{$L[username]}[/url]報名參加了您發起的團購活動[url=$GLOBALS[db_bbsurl]/read.php?tid={$L[tid]}]「{$L[subject]}」[/url]\r\n\r\n' . '發表日期：{$L[createtime]}'.'\r\n'.'所在版塊：[url=$GLOBALS[db_bbsurl]/thread.php?fid={$L[fid]}]{$L[fname]}[/url]',
	
	//活動

	//報名
	'activity_signup_new_title'		=> '{$L[username]}報名參加了您的活動',
	'activity_signup_new_content'	=> '[url=$GLOBALS[db_bbsurl]/{$GLOBALS[db_userurl]}{$L[uid]}]{$L[username]}[/url]報名參加了您發起的活動[url=$GLOBALS[db_bbsurl]/read.php?tid={$L[tid]}]「{$L[subject]}」[/url]\r\n\r\n' . '發表日期：{$L[createtime]}'.'\r\n'.'所在版塊：[url=$GLOBALS[db_bbsurl]/thread.php?fid={$L[fid]}]{$L[fname]}[/url]',
	

	//刪除
	'activity_signup_close_title'		=> '您從活動中關閉了{$L[username]}',
	'activity_signup_close_content'		=> '您從活動「{$L[subject]}「中關閉了報名者[url=$GLOBALS[db_bbsurl]/{$GLOBALS[db_userurl]}{$L[uid]}]{$L[username]}[/url]\r\n\r\n' . '[url=$GLOBALS[db_bbsurl]/read.php?tid={$L[tid]}]查看活動詳情[/url]',
	'activity_signuper_close_title'		=> '您的報名已被{$L[username]}關閉',
	'activity_signuper_close_content'	=> '[url=$GLOBALS[db_bbsurl]/{$GLOBALS[db_userurl]}{$L[uid]}]{$L[username]}[/url]已關閉您在「{$L[subject]}「活動中的報名信息\r\n\r\n' . '[url=$GLOBALS[db_bbsurl]/read.php?tid={$L[tid]}]查看活動詳情[/url]',

	//關閉
	'activity_close_pay_title'				=> '您關閉了{$L[username]}的費用',
	'activity_close_pay_content'			=> '您已關閉了[url=$GLOBALS[db_bbsurl]/{$GLOBALS[db_userurl]}{$L[uid]}]{$L[username]}[/url]在活動「{$L[subject]}「中的追加費用{$L[totalcash]}元\r\n\r\n' . '[url=$GLOBALS[db_bbsurl]/read.php?tid={$L[tid]}]查看活動詳情[/url]',
	'activity_close_signuper_pay_title'		=> '{$L[username]}關閉了您的費用',
	'activity_close_signuper_pay_content'	=> '[url=$GLOBALS[db_bbsurl]/{$GLOBALS[db_userurl]}{$L[uid]}]{$L[username]}[/url]已關閉了您在活動「{$L[subject]}「中的追加費用{$L[totalcash]}元\r\n\r\n' . '[url=$GLOBALS[db_bbsurl]/read.php?tid={$L[tid]}]查看活動詳情[/url]',

	//支付
	'activity_payed_title'				=> '{$L[username]}支付了活動費用',
	'activity_payed_content'			=> '[url=$GLOBALS[db_bbsurl]/{$GLOBALS[db_userurl]}{$L[uid]}]{$L[username]}[/url]通過支付寶支付了「{$L[subject]}「的活動費用{$L[totalcash]}元\r\n\r\n' . '[url=$GLOBALS[db_bbsurl]/read.php?tid={$L[tid]}]查看活動詳情[/url]',
	'activity_payed2_content'			=> '[url=$GLOBALS[db_bbsurl]/{$GLOBALS[db_userurl]}{$L[uid]}]{$L[username]}[/url]通過支付寶支付了「{$L[subject]}「的追加費用{$L[totalcash]}元\r\n\r\n' . '[url=$GLOBALS[db_bbsurl]/read.php?tid={$L[tid]}]查看活動詳情[/url]',
	'activity_payed_signuper_title'		=> '您支付了活動費用',
	'activity_payed_signuper_content'	=> '您已成功支付了「{$L[subject]}「的活動費用{$L[totalcash]}元\r\n\r\n' . '[url=$GLOBALS[db_bbsurl]/read.php?tid={$L[tid]}]查看活動詳情[/url]',
	'activity_payed2_signuper_content'	=> '您已成功支付了「{$L[subject]}「的追加費用{$L[totalcash]}元\r\n\r\n' . '[url=$GLOBALS[db_bbsurl]/read.php?tid={$L[tid]}]查看活動詳情[/url]',

	//代付
	'activity_payed_from_title'			=> '您支付了{$L[username]}的活動費用',
	'activity_payed_from_content'		=> '您已成功幫[url=$GLOBALS[db_bbsurl]/{$GLOBALS[db_userurl]}{$L[uid]}]{$L[username]}[/url]支付了「{$L[subject]}「的活動費用{$L[totalcash]}元\r\n\r\n' . '[url=$GLOBALS[db_bbsurl]/read.php?tid={$L[tid]}]查看活動詳情[/url]',
	'activity_payed2_from_content'		=> '您已成功幫[url=$GLOBALS[db_bbsurl]/{$GLOBALS[db_userurl]}{$L[uid]}]{$L[username]}[/url]支付了「{$L[subject]}「的追加費用{$L[totalcash]}元\r\n\r\n' . '[url=$GLOBALS[db_bbsurl]/read.php?tid={$L[tid]}]查看活動詳情[/url]',

	//被代付
	'activity_payed_signuper_from_title'	=> '{$L[username]}支付了您的活動費用',
	'activity_payed_signuper_from_content'	=> '[url=$GLOBALS[db_bbsurl]/{$GLOBALS[db_userurl]}{$L[uid]}]{$L[username]}[/url]幫您支付了「{$L[subject]}「的活動費用{$L[totalcash]}元\r\n\r\n' . '[url=$GLOBALS[db_bbsurl]/read.php?tid={$L[tid]}]查看活動詳情[/url]',
	'activity_payed2_signuper_from_content'	=> '[url=$GLOBALS[db_bbsurl]/{$GLOBALS[db_userurl]}{$L[uid]}]{$L[username]}[/url]幫您支付了「{$L[subject]}「的追加費用{$L[totalcash]}元\r\n\r\n' . '[url=$GLOBALS[db_bbsurl]/read.php?tid={$L[tid]}]查看活動詳情[/url]',

	//確認支付
	'activity_confirmpay_title'				=> '您修改了{$L[username]}的支付狀態',
	'activity_confirmpay_content'			=> '您已將[url=$GLOBALS[db_bbsurl]/{$GLOBALS[db_userurl]}{$L[uid]}]{$L[username]}[/url]在活動「{$L[subject]}「中{$L[totalcash]}元費用的支付狀態改為「已支付」\r\n\r\n' . '[url=$GLOBALS[db_bbsurl]/read.php?tid={$L[tid]}]查看活動詳情[/url]',
	'activity_confirmpay2_content'	=> '您已將[url=$GLOBALS[db_bbsurl]/{$GLOBALS[db_userurl]}{$L[uid]}]{$L[username]}[/url]在活動「{$L[subject]}「中{$L[totalcash]}元追加費用的支付狀態改為「已支付」\r\n\r\n' . '[url=$GLOBALS[db_bbsurl]/read.php?tid={$L[tid]}]查看活動詳情[/url]',
	'activity_confirmpay_signuper_title'	=> '{$L[username]}修改了您的支付狀態',
	'activity_confirmpay_signuper_content'	=> '[url=$GLOBALS[db_bbsurl]/{$GLOBALS[db_userurl]}{$L[uid]}]{$L[username]}[/url]已將您在活動「{$L[subject]}「中{$L[totalcash]}元費用的支付狀態改為「已支付」\r\n\r\n' . '[url=$GLOBALS[db_bbsurl]/read.php?tid={$L[tid]}]查看活動詳情[/url]',
	'activity_confirmpay2_signuper_content'	=> '[url=$GLOBALS[db_bbsurl]/{$GLOBALS[db_userurl]}{$L[uid]}]{$L[username]}[/url]已將您在活動「{$L[subject]}「中{$L[totalcash]}元追加費用的支付狀態改為「已支付」\r\n\r\n' . '[url=$GLOBALS[db_bbsurl]/read.php?tid={$L[tid]}]查看活動詳情[/url]',

	//退款
	'activity_refund_title'				=> '您退回了{$L[username]}的費用',
	'activity_refund_content'			=> '您已成功退回[url=$GLOBALS[db_bbsurl]/{$GLOBALS[db_userurl]}{$L[uid]}]{$L[username]}[/url]在活動「{$L[subject]}「中的費用{$L[totalcash]}元\r\n\r\n' . '[url=$GLOBALS[db_bbsurl]/read.php?tid={$L[tid]}]查看活動詳情[/url]',
	'activity_refund2_content'			=> '您已成功退回[url=$GLOBALS[db_bbsurl]/{$GLOBALS[db_userurl]}{$L[uid]}]{$L[username]}[/url]在活動「{$L[subject]}「中的追加費用{$L[totalcash]}元\r\n\r\n' . '[url=$GLOBALS[db_bbsurl]/read.php?tid={$L[tid]}]查看活動詳情[/url]',
	'activity_refund_signuper_title'	=> '{$L[username]}退回了您的費用',
	'activity_refund_signuper_content'	=> '[url=$GLOBALS[db_bbsurl]/{$GLOBALS[db_userurl]}{$L[uid]}]{$L[username]}[/url]已成功退回您在活動「{$L[subject]}「中的費用{$L[totalcash]}元\r\n\r\n' . '[url=$GLOBALS[db_bbsurl]/read.php?tid={$L[tid]}]查看活動詳情[/url]',
	'activity_refund2_signuper_content'	=> '[url=$GLOBALS[db_bbsurl]/{$GLOBALS[db_userurl]}{$L[uid]}]{$L[username]}[/url]已成功退回您在活動「{$L[subject]}「中的追加費用{$L[totalcash]}元\r\n\r\n' . '[url=$GLOBALS[db_bbsurl]/read.php?tid={$L[tid]}]查看活動詳情[/url]',

	//追加活動費用
	'activity_additional_title'			=> '{$L[username]}追加了你的活動費用',
	'activity_additional_content'		=> '[url=$GLOBALS[db_bbsurl]/{$GLOBALS[db_userurl]}{$L[uid]}]{$L[username]}[/url]追加了「{$L[subject]}「的活動費用{$L[totalcash]}元\r\n\r\n' . '[url=$GLOBALS[db_bbsurl]/read.php?tid={$L[tid]}]查看活動詳情[/url]',

	//取消活動
	'activity_cancel_title'				=> '「{$L[subject]}「活動已取消',
	'activity_cancel_content'			=> '「{$L[subject]}「活動未達到最低人數，活動自動取消，請及時退款已繳納的報名費用\r\n\r\n' . '[url=$GLOBALS[db_bbsurl]/read.php?tid={$L[tid]}]查看活動詳情[/url]',
	'activity_cancel_signuper_title'	=> '「{$L[subject]}「活動已取消',
	'activity_cancel_signuper_content'	=> '「{$L[subject]}「活動未達到最低人數，活動自動取消，請聯繫活動發起者退回報名費用\r\n\r\n' . '[url=$GLOBALS[db_bbsurl]/read.php?tid={$L[tid]}]查看活動詳情[/url]',

	//活動被刪除（帖子被刪除）
	'activity_delete_title'				=> '「{$L[subject]}「活動被刪除',
	'activity_delete_content'			=> '「{$L[subject]}「活動被管理員刪除，請及時退款已繳納的報名費用\n\n'
					. '論壇管理操作通知短消息，對本次管理操作有任何異議，請與我取得聯繫。',
	'activity_delete_signuper_content'	=> '「{$L[subject]}「活動被管理員刪除，請聯繫活動發起者退回報名費用\n\n'
					. '[url=$GLOBALS[db_bbsurl]/mode.php?m=o&q=activity&see=feeslog]查看我的活動費用流通日誌[/url]',
					
	'split_title'			=> '您的帖子被拆分',
	'split_content'		    => '您發表的帖子{$L[spiltInfo]}被 [b]{$L[manager]}[/b] 執行 [b]拆分[/b] 操作\n\n'
								. '操作原因：{$L[msg]}\n\n'
								. '論壇管理操作通知短消息，對本次管理操作有任何異議，請與我取得聯繫。',
								
	//孔明燈
	'kmd_manage_pass_title'	=> '您的孔明燈申請已通過審核',
	'kmd_manage_pass_content'	=> '您好，您於[color=blue]{$L[creadtime]}[/color]申請的孔明燈帖[color=blue]{$L[subject]}[/color]已通過審核，推廣到期時間為：[color=blue]{$L[endtime]}[/color]。如有問題，請聯繫管理員',	
	'kmd_manage_repulse_title'	=> '您的孔明燈申請未通過審核',
	'kmd_manage_repulse_content'	=> '您好，您於[color=blue]{$L[creadtime]}[/color] 申請的孔明燈帖 [color=blue]{$L[subject]}[/color]未通過審核。如有問題，請聯繫管理員',					
    'kmd_manage_pay_back_title'  => '您的孔明燈申請已退款成功!',
	'kmd_manage_pay_back_content'  => ' 您好，您於 [color=blue]{$L[creadtime]}[/color]  申請的孔明燈帖  [color=blue]{$L[subject]}[/color]，費用為 [color=blue]{$L[rmb]}[/color]元， 已退款成功，請注意查收。如有問題，請聯繫管理員。  ',		
	'kmd_manage_pay_title'  =>  '您的孔明燈申請已支付成功!',
	'kmd_manage_pay_content'  =>	'您好，您於 [color=blue]{$L[creadtime]}[/color]  申請的孔明燈帖  [color=blue]{$L[subject]}[/color] ，費用為 [color=blue]{$L[rmb]}[/color]元，已成功支付，請等待審核，審核通過後即可正常顯示。如有問題，請聯繫管理員。',
	'kmd_review_title'	=> '【審核】$L[username]已提交孔明燈申請，請確認是否已支付',
	'kmd_review_content'	=> '[url=$GLOBALS[db_bbsurl]/u.php?username=$L[username]]$L[username][/url]提交了孔明燈申請，推廣版塊：[url=$GLOBALS[db_bbsurl]/thread.php?fid=$L[fid]]$L[forumname][/url]，推廣費用：[color=orange]$L[money]元[/color]，請查看後進入後台確認支付狀態。',
	'kmd_review_user_title' => '您的孔明燈申請提交成功，請等待審核',
	'kmd_review_user_content' => '您申請的孔明燈，推廣版塊：[url=$GLOBALS[db_bbsurl]/thread.php?fid=$L[fid]]$L[forumname][/url]，推廣費用：[color=orange]$L[money]元[/color]，已成功提交至後台，支付後請聯繫管理員審核開通。',
	'kmd_review_thread_change_title' => '【審核】$L[username]已提交孔明燈帖子更換申請',
	'kmd_review_thread_add_title' => '【審核】$L[username]已提交孔明燈帖子添加申請',
	'kmd_review_thread_content' => '[url=$GLOBALS[db_bbsurl]/u.php?username=$L[username]]$L[username][/url]提交了孔明燈推廣申請，推廣帖子：[url=$GLOBALS[db_bbsurl]/read.php?tid=$L[tid]]$L[threadtitle][/url]，請進入後台進行審核狀態。',
	'kmd_review_user_thread_title' => '您的孔明燈推廣申請提交成功，請等待審核',
	'kmd_review_user_thread_content' => '您申請的孔明燈推廣帖子：[url=$GLOBALS[db_bbsurl]/read.php?tid=$L[tid]]$L[threadtitle][/url]，已成功提交至後台，請等待管理員審核開通。',

	'kmd_admin_paylog_checked_title' => '您的孔明燈購買申請已開通成功 ',
	'kmd_admin_paylog_checked_content' => '您申請的孔明燈，推廣版塊：[url=$GLOBALS[db_bbsurl]/thread.php?fid=$L[fid]]$L[forumname][/url]，推廣費用：$L[price]元，已成功開通，現在可以在[url=$GLOBALS[db_bbsurl]/apps.php?q=kmd]個人中心-孔明燈[/url] 添加推廣帖子了。',
	'kmd_admin_paylog_reject_title' => '您的孔明燈購買申請未被批准',
	'kmd_admin_paylog_reject_content' => '您申請的孔明燈，推廣版塊：[url=$GLOBALS[db_bbsurl]/thread.php?fid=$L[fid]]$L[forumname][/url]，推廣費用：$L[price]元，未被批准。',
	'kmd_admin_thread_checked_title' => '您的孔明燈推廣帖子已被通過',
	'kmd_admin_thread_checked_content' => '您申請的孔明燈推廣帖子：[url=$GLOBALS[db_bbsurl]/read.php?tid=$L[tid]]$L[subject][/url]，已被通過，請至相應版塊查看效果。',
	'kmd_admin_thread_reject_title' => '您的孔明燈推廣帖子已被拒絕',
	'kmd_admin_thread_reject_content' => '您申請的孔明燈推廣帖子：您申請的孔明燈推廣帖子：[url=$GLOBALS[db_bbsurl]/read.php?tid=$L[tid]]$L[subject][/url]，已被拒絕，請換個帖子試試。，已被拒絕，請換個帖子試試。',
	'kmd_admin_kmd_canceled_title' => '因特殊原因，您的孔明燈已被管理員撤消',
	'kmd_admin_kmd_canceled_content' => '對不起，您的孔明燈涉及不良操作已被管理員撤消，如有疑問請聯繫站方溝通。',
);
?>