<?php
!function_exists('readover') && exit('Forbidden');

$lang['email'] = array (

'email_check_subject'	=> '激活您在 {$GLOBALS[db_bbsname]} 會員帳號的必要步驟!',
'email_check_content'	=> '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>激活帳號</title></head><body><div align="center"><table cellpadding="0" cellspacing="1" style="border:3px solid #d9e9f1;background:#7fbddd; text-align:left;"><tr><td style="padding:0;"><table cellpadding="30" cellspacing="0" style="border:1px solid #ffffff;background:#f7f7f7;width:500px;"><tr><td style="line-height:2;font-size:12px;"><div style="font-size:14px;margin-bottom:10px;font-weight:700;">Hi, {$GLOBALS[regname]}</div>
						<p style="color:#ff6600;margin:0;">請點擊下面的鏈接完成激活：</p>
						<div style="padding:8px 10px;margin:10px 0 5px;background:#ffffff;border:1px solid #cbcbcb;word-break:break-all;word-wrap:break-word;line-height:1.5;font-size:14px;"><a href="{$GLOBALS[db_bbsurl]}/{$GLOBALS[db_registerfile]}?step=finish&vip=activating&r_uid={$GLOBALS[winduid]}&pwd={$GLOBALS[rgyz]}&toemail={$GLOBALS[regemail]}" style="color:#3366cc;">{$GLOBALS[db_bbsurl]}/{$GLOBALS[db_registerfile]}?step=finish&vip=activating&r_uid={$GLOBALS[winduid]}&pwd={$GLOBALS[rgyz]}</a></div>
						<div style="color:#999999;margin-bottom:5px;">如果不能點擊鏈接，請複製地址並粘貼到瀏覽器的地址輸入框</div>
						激活後盡快刪除此郵件，以免帳號信息洩漏<div style="border-top:1px solid #e2e2e2;background:#ffffff;overflow:hidden;height:1px;*height:2px;margin:10px 0;"></div>歡迎您加入{$GLOBALS[db_bbsname]}，請妥善保管好您的帳號信息<p style="margin:0;"><span style="padding-right:5em;">用戶名：{$GLOBALS[regname]}</span>密碼：{$GLOBALS[sRegpwd]}</p>
						<div style="border-top:1px solid #e2e2e2;background:#ffffff;overflow:hidden;height:1px;*height:2px;margin:10px 0;"></div>
						如果忘記密碼，可以到社區找回密碼，也可以寫信請管理員重新設定。<br />
						社區地址：<a href="{$GLOBALS[db_bbsurl]}" style="color:#3366cc;">{$GLOBALS[db_bbsurl]}</a></td></tr></table></td></tr></table></div></body></html>',
'email_check_content_resend'	=> '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>激活帳號</title></head><body><div align="center"><table cellpadding="0" cellspacing="1" style="border:3px solid #d9e9f1;background:#7fbddd; text-align:left;"><tr><td style="padding:0;"><table cellpadding="30" cellspacing="0" style="border:1px solid #ffffff;background:#f7f7f7;width:500px;"><tr><td style="line-height:2;font-size:12px;"><div style="font-size:14px;margin-bottom:10px;font-weight:700;">Hi, {$GLOBALS[regname]}</div>
						<p style="color:#ff6600;margin:0;">請點擊下面的鏈接完成激活：</p>
						<div style="padding:8px 10px;margin:10px 0 5px;background:#ffffff;border:1px solid #cbcbcb;word-break:break-all;word-wrap:break-word;line-height:1.5;font-size:14px;"><a href="{$GLOBALS[db_bbsurl]}/{$GLOBALS[db_registerfile]}?step=finish&vip=activating&r_uid={$GLOBALS[winduid]}&pwd={$GLOBALS[rgyz]}&toemail={$GLOBALS[regemail]}" style="color:#3366cc;">{$GLOBALS[db_bbsurl]}/{$GLOBALS[db_registerfile]}?step=finish&vip=activating&r_uid={$GLOBALS[winduid]}&pwd={$GLOBALS[rgyz]}</a></div>
						<div style="color:#999999;margin-bottom:5px;">如果不能點擊鏈接，請複製地址並粘貼到瀏覽器的地址輸入框</div>
						激活後盡快刪除此郵件，以免帳號信息洩漏<div style="border-top:1px solid #e2e2e2;background:#ffffff;overflow:hidden;height:1px;*height:2px;margin:10px 0;"></div>
						<div style="border-top:1px solid #e2e2e2;background:#ffffff;overflow:hidden;height:1px;*height:2px;margin:10px 0;"></div>
						如果忘記密碼，可以到社區找回密碼，也可以寫信請管理員重新設定。<br />
						社區地址：<a href="{$GLOBALS[db_bbsurl]}" style="color:#3366cc;">{$GLOBALS[db_bbsurl]}</a></td></tr></table></td></tr></table></div></body></html>',

'email_additional'		=> 'Reply-To:{$GLOBALS[fromemail]}\r\nX-Mailer: phpwind電子郵件快遞',

'email_welcome_subject'	=> '{$GLOBALS[regname]},您好,感謝您註冊{$GLOBALS[db_bbsname]}',
'email_welcome_content'	=> '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>感謝您註冊</title></head><body><div align="center"><table cellpadding="0" cellspacing="1" style="border:3px solid #d9e9f1;background:#7fbddd; text-align:left;"><tr><td style="padding:0;"><table cellpadding="30" cellspacing="0" style="border:1px solid #ffffff;background:#f7f7f7;width:500px;"><tr><td style="line-height:2;font-size:12px;"><div style="font-size:14px;margin-bottom:10px;font-weight:700;">Hi, {$GLOBALS[regname]}</div>{$GLOBALS[db_bbsname]}歡迎您的加入！<p style="margin:0;"><span style="padding-right:5em;">您的註冊名為： {$GLOBALS[regname]}</span>您的密碼為： {$GLOBALS[sRegpwd]}</p><div style="border-top:1px solid #e2e2e2;background:#ffffff;overflow:hidden;height:1px;*height:2px;margin:10px 0;"></div>請盡快刪除此郵件，以免帳號信息洩漏<br />如果忘記密碼，可以到社區找回密碼，也可以寫信請管理員重新設定。<br />社區地址：<a href="{$GLOBALS[db_bbsurl]}">{$GLOBALS[db_bbsurl]}</a></td></tr></table></td></tr></table></div></body></html>',

'email_sendpwd_subject'	=> '{$GLOBALS[db_bbsname]} 密碼重發',
'email_sendpwd_content'	=> '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>密碼重發</title></head><body><div align="center"><table cellpadding="0" cellspacing="1" style="border:3px solid #d9e9f1;background:#7fbddd; text-align:left;"><tr><td style="padding:0;"><table cellpadding="30" cellspacing="0" style="border:1px solid #ffffff;background:#f7f7f7;width:500px;"><tr><td style="line-height:2;font-size:12px;">請到下面的網址修改密碼：<div style="padding:8px 10px;margin:10px 0 5px;background:#ffffff;border:1px solid #cbcbcb;word-break:break-all;word-wrap:break-word;line-height:1.5;font-size:14px;"><a href="{$GLOBALS[db_bbsurl]}/sendpwd.php?action=getback&pwuser={$GLOBALS[pwuser]}&submit={$GLOBALS[submit]}&st={$GLOBALS[timestamp]}">{$GLOBALS[db_bbsurl]}/sendpwd.php?action=getback&pwuser={$GLOBALS[pwuser]}&submit={$GLOBALS[submit]}&st={$GLOBALS[timestamp]}</a></div>修改後請牢記您的密碼<br />歡迎來到 {$GLOBALS[db_bbsname]} 我們的網址是:<a href="{$GLOBALS[db_bbsurl]}">{$GLOBALS[db_bbsurl]}</a></td></tr></table></td></tr></table></div></body></html>',

'email_reply_subject'	=> '您在{$GLOBALS[db_bbsname]}的帖子有回復',
'email_reply_content'	=> '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>帖子有回復</title></head><body><div align="center"><table cellpadding="0" cellspacing="1" style="border:3px solid #d9e9f1;background:#7fbddd; text-align:left;"><tr><td style="padding:0;"><table cellpadding="30" cellspacing="0" style="border:1px solid #ffffff;background:#f7f7f7;width:500px;"><tr><td style="line-height:2;font-size:12px;"><div style="font-size:14px;margin-bottom:10px;font-weight:700;">Hi</div>我是{$GLOBALS[db_bbsname]}郵件大使<br />您在{$GLOBALS[db_bbsname]}發表的帖子: {$GLOBALS[old_title]}<br />現在有人回復.快來關注一下吧<br /><a href="{$GLOBALS[db_bbsurl]}/read.php?fid={$GLOBALS[fid]}&tid={$GLOBALS[tid]}">{$GLOBALS[db_bbsurl]}/read.php?fid={$GLOBALS[fid]}&tid={$GLOBALS[tid]}</a><br />下次再有人參與主題時,我將不來打擾了</td></tr></table></td></tr></table></div></body></html>',

'email_invite_subject'	=> '您的朋友{$GLOBALS[windid]}邀請您加入{$GLOBALS[db_bbsname]}',
'email_invite_content'	=> '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>購買註冊碼</title></head><body><div align="center"><table cellpadding="0" cellspacing="1" style="border:3px solid #d9e9f1;background:#7fbddd; text-align:left;"><tr><td style="padding:0;"><table cellpadding="30" cellspacing="0" style="border:1px solid #ffffff;background:#f7f7f7;width:500px;"><tr><td style="line-height:2;font-size:12px;">您在{$GLOBALS[db_bbsname]}論壇購買的邀請碼如下：
<div style="padding:8px 10px;margin:10px 0;background:#ffffff;border:1px solid #cbcbcb;word-break:break-all;word-wrap:break-word;line-height:1.5;font-size:14px;">{$GLOBALS[invlink]}</div>註冊地址：<a href="{$GLOBALS[db_bbsurl]}/{$GLOBALS[db_registerfile]}">{$GLOBALS[db_bbsurl]}/{$GLOBALS[db_registerfile]}</a></td></tr></table></td></tr></table></div></body></html>',
'email_invite_content_new' => '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>購買註冊碼
</title></head><body><div align="center">
<table cellpadding="0" cellspacing="1" style="border:3px solid #d9e9f1;background:#7fbddd; text-align:left;">
<tr>
<td style="padding:0;">
<table cellpadding="30" cellspacing="0" style="border:1px solid #ffffff;background:#f7f7f7;width:500px;">
<tr><td>{$GLOBALS[extranote]}</td></tr>
<tr><td style="line-height:2;font-size:12px;"><div style="padding:8px 10px;margin:10px 0;background:#ffffff;border:1px solid 

#cbcbcb;word-break:break-all;word-wrap:break-word;line-height:1.5;font-size:14px;">{$GLOBALS[invlink]}</div></td></tr>
</table>
</td></tr></table></div></body></html>',

'emailcheck_subject'	=> 'PHPwind電子郵件發送檢測',
'emailcheck_content'	=> 'PHPwind電子郵件發送檢測成功!',
'email_mode_o_title'	=> '您的朋友{$GLOBALS[windid]}邀請您加入{$GLOBALS[db_bbsname]}',
'email_mode_o_content'	=> '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>購買註冊碼</title></head><body><div align="center"><table cellpadding="0" cellspacing="1" style="border:3px solid #d9e9f1;background:#7fbddd; text-align:left;"><tr><td style="padding:0;"><table cellpadding="30" cellspacing="0" style="border:1px solid #ffffff;background:#f7f7f7;width:500px;"><tr><td style="line-height:2;font-size:12px;">我是{$GLOBALS[windid]}，我在{$GLOBALS[db_bbsname]}上建立了個人主頁，請你也加入並成為我的好友。<br />{$GLOBALS[extranote]}\n\n請點擊以下鏈接，接受好友邀請：<br /><a href="{$GLOBALS[invite_url]}">{$GLOBALS[invite_url]}</a><br />{$GLOBALS[db_bbsname]} (<a href="{$GLOBALS[db_bbsurl]}">{$GLOBALS[db_bbsurl]}</a>)</td></tr></table></td></tr></table></div></body></html>',
'email_groupactive_invite_subject' => '{$GLOBALS[windid]}邀請您加入活動{$GLOBALS[objectName]}',
'email_groupactive_invite_content' => '我是{$GLOBALS[windid]}，我在{$GLOBALS[db_bbsname]}上發現了活動{$GLOBALS[objectName]}，請你也加入並成為我的好友。<br />活動{$GLOBALS[objectName]}介紹：<br />{$GLOBALS[objectDescrip]}<br /><div id="customdes">{$GLOBALS[customdes]}</div>請點擊以下鏈接，接受好友邀請<br /><a href="{$GLOBALS[invite_url]}">{$GLOBALS[invite_url]}</a>',
'email_group_invite_subject' => '{$GLOBALS[windid]}邀請您加入群組{$GLOBALS[objectName]}',
'email_group_invite_content' => '我是{$GLOBALS[windid]}，我在{$GLOBALS[db_bbsname]}上發現了群組{$GLOBALS[objectName]}，請你也加入並成為我的好友。<br />群組{$GLOBALS[objectName]}介紹：<br />{$GLOBALS[objectDescrip]} [<a href="{$GLOBALS[db_bbsurl]}/apps.php?q=group&cyid={$GLOBALS[cyid]}">查看群組</a>]<br /><div id="customdes">{$GLOBALS[customdes]}</div>請點擊以下鏈接，接受好友邀請<br /><a href="{$GLOBALS[invite_url]}">{$GLOBALS[invite_url]}</a>',

);
?>