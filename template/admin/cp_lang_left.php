<?php
!function_exists('readover') && exit('Forbidden');
require GetLang('purview');
$nav_manager = array(
	'name'		=> '創始人',
	'items'		=> array(
		'manager',
		'rightset',
		'optimize',
		'diyoption',
		'advanced',
		'usercenter'=> array(
			'name'		=> '用戶中心',
			'items'		=> array(
				'ucset',
				'ucapp',
				'uccredit',
				'ucnotify',
			),
		),
	),
);

$nav_left = array(
	'config' => array(
		'name' => '全局',
		'items' => array(
			'basic',
			'customnav',
			'regset'	=> array(
				'name'	=> '註冊設置',
				'items'	=> array(
					'reg',
					'customfield',
					'invite',
					'propagateset'
				),
			),
			'credit',
			'member',
			'editer',
			/*
			'creditset'	=> array(
				'name'	=> '積分設置',
				'items'	=> array(
					'credit',
					'creditdiy',
					'creditchange',
				)
			),
			*/
			'attftp'	=> array(
				'name'	=>'附件設置',
				'items'	=>array(
					'att',
					'attachment',
					'attachrenew',
					'attachstats',
				)
			),
			'safe',
			'seoset',
			'messageset',
			'searcher',
			'email',
			'userpay',
			'help',
			//'wap',
			'sethtm',
			//'pcache',
		),
	),
	'consumer' => array(
		'name'	=> '用戶',
		'items'	=> array(
			'level',
			'upgrade',
			'usermanage',
			'banuser',
			'bansignature',
			'usercheck',
			'userstats',
			//'customcredit',
			'uptime',
		),
	),
	'contentforums'	=> array(
		'name'	=> '內容',
		'items'	=> array(
			/*'contentmanage' => array(
				'name'	=> '內容管理',
				'items'=> array(*/
					'article',
					'photos_manage',
					'diary_manage',
					'groups_manage',
					//'app_share',
					'weibo_manage',
					'o_comments',
					'message',
					'reportmanage' => array(
						'name' => '舉報管理',
						'items' => array(
							'reportcontent',
							'reportremind'
						)
					),
					'draftset',
					'recycle',
				/*),
			),*/
			'contentcheck'	=> array(
				'name'	=> '內容審核',
				'items'	=> array(
					'tpccheck',
					'setbwd',
					'urlcheck',
				),
			),
			'rulelist'	=> array(
				'name'=>'內容過濾設置',
				'items'=>array(
				)
			),
			'tagset',
			//'pwcode',
			//'setform',
			//'overprint',
		),
	),
	'datacache'	=> array(
		'name'	=> '數據',
		'items'	=> array(
			'bakup',
			'aboutcache',
			'record',
			'filecheck',
			'ipban',
			'viewtoday',
			'postindex',
			'datastate',
			'ystats',
			'creditlog',
			'tucool',
		),
	),
	'applicationcenter'	=> array(
		'name'	=> '應用',
		'items'	=> array(
		'onlineapplication' => array(
				'name'	=> '應用中心',
				'items' => array(
					'appset',
					'onlineapp',
					'i9p',
					'blooming',
					'taolianjie',
					'sinaweibo',
					'yunstatistics',
					'cnzz'
				),
			),
			'admincollege' => array(
				'name'	=> '站長魔盒',
				'items' => array(
				//	'products',    beta版先隱藏目錄
					'hacksource',
					'stylesource',
				),
			),
			'hackcenter',
			'hookcenter',
			//'appslist',
			'photos_set',
			'diary_set',
			'groups_set',
			//'app_share',
			'hot',
			'weibo_set',
			'medal_manage',
			'postcate',
			'activity',
			'topiccate',
			/*
			'basicapp' => array(
				'name' => '基礎應用',
				'items' => array(
					
				),
			),
			*/
		),
	),
	'markoperation'=> array(
		'name'	=> '運營',
		'items'	=> array(
			'setadvert',
			'stopic',
			'job',
			'announcement',
			'sendmail',
			'sendmsg',
			'present',
			'share',
			'plantodo',
			'team',
			'kmd_set',
//			'setads',
			//'sitemap',
		),
	),
	'cloudcomputing' => array(
		'name'  => '雲工具',
		'items'  => array(
			'yunbasic',
			'yuncheckserver',
			'yunsearch',
			'yundefend',
			'cloudcaptcha',
			'authentication',
		),
	),
	
	'modelist' => array(
		'name'	=> '模式設置',
		'items'=> array(
			'modeset',
			//'modestamp',
			//'modepush',
		),
	),
	'bbs' => array(
		'name'	=> '論壇模式',
		'items'=> array(
			'bbssetting',
			'setforum',
			'interfacesettings',
			/*
			'forums' => array(
				'name'	=> '版塊管理',
				'items'=> array(
					'setforum',
					'uniteforum',
					'forumsell',
					'creathtm',
				),
			),
			*/
			'singleright',
			'setstyles',
			'rebang',
		),
	),
	'o' => array(
		'name'	=> '個人中心',
		'items'=> array(
			'o_global',
			'o_tags',
			'o_skin',
			'o_commend',
		)
	),
	'wap'		=> array(
			'name'  => 'WAP設置', 
			'items' => array(
				'wapconfig',
				'wapsettings',
				'wapadvert',
			)
		),
);

/*動態加載模式菜單*/
if (isset($db_modes)) {
	foreach ($db_modes as $key => $value) {
		if ($value['ifopen'] && file_exists(R_P.'mode/'.$key.'/config/cp_lang_left.php')) {
			include R_P.'mode/'.$key.'/config/cp_lang_left.php';
		}
	}
}

/*動態加載擴展菜單*/
$extentPath = R_P.'require/extents/menu';
if ($fp = opendir($extentPath)) {
	while (($filename = readdir($fp))) {
		if($filename=='..' || $filename=='.') continue;
		$leftFile = $extentPath.'/'.$filename.'/cp_lang_left.php';
		if (file_exists($leftFile)) include $leftFile;
	}
}

?>