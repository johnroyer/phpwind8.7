<?php
!function_exists('readover') && exit('Forbidden');

$lang['all'] = array (

'reg_member'		=> '普通會員',

'operate'			=> '操作:',
'reason'			=> '原因:',

'record_rvrc'		=> '威望',
'record_money'		=> '金錢',
'record_credit'		=> '支持度',

'forum_cate'		=> '◆-',
'forum_cate1'		=> '■-',
'forum_cate2'		=> '●-',
'forum_cate3'		=> '▲-',

'send_welcome'		=> '您好，歡迎光臨 $GLOBALS[db_bbsurl].',
'send_feast'		=> '您好，$windid，今天是節日，本論壇全體版主祝您節日愉快！我們特地贈送您財富：$money，威望：$rvrc 作為節日禮物，請笑納。祝您玩得愉快~',
'whole_notice'		=> '全局公告',
'cms_notice'		=> '文章系統',
'all_notice'		=> '所有公告',
'add_notice'		=> '添加公告',
'read_ischeck'		=> '該帖還未通過驗證',
'read_deleted'		=> '該帖已經被刪除',

'sqlinfo'			=> '* 數據庫相關信息設置\r\n* 具體數值，請聯繫您的主機商,詢問具體的數據庫相關信息',
'dbhost'			=> '// 數據庫 主機名 或 IP 地址，如數據庫端口不是3306，請在 主機名 或 IP 地址後添加「:具體端口」，'
						. '如您的主機是localhost，端口是3307，則更改為「localhost:3307」',
'dbuser'			=> '// 數據庫用戶名和密碼，連接和訪問 MySQL 數據庫時所需的用戶名和密碼，不推薦使用空的數據庫密碼。',
'dbname'			=> '// 數據庫名，論壇程序所使用的數據庫名。',
'database'			=> '// 數據庫類型，有效選項有 mysql 和 mysqli，自pwforums v6.3.2起，'
						. '引入了mysqli的支持，兼容性更好，效率性能更穩定，與mysql連接更穩定\r\n\t'
						. '// 若服務器的配置是 PHP5.1.0或更高版本 和 MySQL4.1.3或更高版本，可以嘗試使用 mysqli。',
'PW'				=> '// 表區分符，用於區分每一套程序的符號',
'pconnect'			=> '// 是否持久連接，暫不支持mysqli',
'charset'			=> '* Mysql編碼設置(常用編碼：gbk、big5、utf8、latin1)\r\n'
						. '* 如果您的論壇出現亂碼現象，需要設置此項來修復\r\n'
						. '* 請不要隨意更改此項，否則將可能導致論壇出現亂碼現象',
'managerinfo'		=> '* 創始人將擁有論壇的所有權限，自pwforums v6.3起支持多重創始人，用戶名密碼更改方法：\r\n'
						. '* 方法1、將./data/sql_config.php的文件屬性設置為777（非NT服務器）或取消只讀並將用戶權限設置為Full Control（完全控制，NT服務器），\r\n'
						. '* 然後用原始創始人帳號登錄後台，在更改論壇創始人處進行相關添加修改操作，\r\n'
						. '* 操作完畢後，再將./data/sql_config.php的文件屬性設置為644（非NT服務器）或只讀（NT服務器）。（推薦）\r\n'
						. '* 方法2、用記事本打開./data/sql_config.php文件，在「創始人用戶名數組」中加入新的用戶名，\r\n'
						. '* 如「\$manager = array(\'admin\');」更改為「\$manager = array(\'admin\',\'phpwind\');」，在「創始人密碼數組」中加入新的密碼，\r\n'
						. '* 如「\$manager_pwd = array(\'21232f297a57a5a743894a0e4a801fc3\');」\r\n'
						. '* 更改為「\$manager_pwd = array(\'21232f297a57a5a743894a0e4a801fc3\',\'21232f297a57a5a743894a0e4a801fc3\');」，\r\n'
						. '* 其中「21232f297a57a5a743894a0e4a801fc3」是密碼為admin的md5的加密串，您可以創建一個新的文件在根目錄（test.php），\r\n'
						. '* 文件內容為 "<?php echo md5(\'您的密碼\');?>" ，在地址欄輸入http://你的論壇/test.php獲得md5加密後的密碼，用完記得刪除文件test.php。',
'managername'		=> '// 創始人用戶名數組',
'managerpwd'		=> '// 創始人密碼數組',
'hostweb'			=> '* 鏡像站點設置，默認為1，代表是主站點',
'attach_url'		=> '* 附件url地址，以http:// 開頭的絕對地址，為空使用默認',
'slaveConfig'		=> '* 這裡填寫了代表您已經配置好了主從數據庫，程序會進行讀寫分離處理，前面的配置作主數據庫，下面的配置作從數據庫。您可以根據您的實際情況配置多台從數據庫，配置說明請參考上面的主數據庫',				
'week_1'			=> '星期一',
'week_2'			=> '星期二',
'week_3'			=> '星期三',
'week_4'			=> '星期四',
'week_5'			=> '星期五',
'week_6'			=> '星期六',
'week_0'			=> '星期日',

'mode_bbs_mname'	=> '論壇模式',
'mode_bbs_title'	=> '論壇',
'mode_o_mname'		=> '個人中心',
'mode_o_title'		=> '家園',

'nav_index'			=> '首頁',
'mode_o_nav_home'	=> '我的首頁',
'mode_o_nav_user'	=> '個人空間',
'mode_o_nav_friend'	=> '朋友',
'mode_o_nav_browse'	=> '隨便看看',

'mode_bbs_nav'		=> "會員應用,app,,root\n"
						. "最新帖子,lastpost,searcher.php?sch_time=newatc,root\n"
						. "精華區,digest,searcher.php?digest=1,root\n"
						. "社區服務,hack,,root\n"
						. "會員列表,member,member.php,root\n"
						. "統計排行,sort,sort.php,root\n"
						. "基本信息,sort_basic,sort.php,sort\n"
						. "到訪IP統計,sort_ipstate,sort.php?action=ipstate,sort\n"
						. "管理團隊,sort_team,sort.php?action=team,sort\n"
						. "管理操作,sort_admin,sort.php?action=admin,sort\n"
						. "在線會員,sort_online,sort.php?action=online,sort\n"
						. "會員排行,sort_member,sort.php?action=member,sort\n"
						. "版塊排行,sort_forum,sort.php?action=forum,sort\n"
						. "帖子排行,sort_article,sort.php?action=article,sort\n"
						. "標籤排行,sort_taglist,link.php?action=taglist,sort\n"

);
?>