<?php
/**
 * phpwind 新浪微博數據統計
 * 
 * @author phpwind team
 * @version 1.0
 * @package api
 */
!defined('P_W') && exit('Forbidden');
require_once(R_P . 'api/class_Statistics.php');

class Statistics_Sinaweibo extends Statistics {
	
	/**
	 * 每天綁定用戶總數
	 * @param string $day 'Y-m-d'
	 * @return int 
	 */

	function getBindOfDay($day = null) {
		$platformApiClient = $this->_getPlatformApiClient();
		$response = $platformApiClient->get('weibo.stat.site', array('time' => $day, 'type' => 'bound'));
		return new ApiResponse($response);
	}
	
	/**
	 * 站點每日到新浪的微博數量
	 * @param string $day 'Y-m-d'
	 * @return int 
	 */

	function getSiteToSinaWeiboOfDay($day = null) {
		$platformApiClient = $this->_getPlatformApiClient();
		$response = $platformApiClient->get('weibo.stat.site', array('time' => $day, 'type' => 'siteToSinaWeibo'));
		return new ApiResponse($response);
	}

	/**
	 * 站點每日到新浪的評論數量
	 * @param string $day 'Y-m-d'
	 * @return int 
	 */

	function getSiteToSinaCommentOfDay($day = null) {
		$platformApiClient = $this->_getPlatformApiClient();
		$response = $platformApiClient->get('weibo.stat.site', array('time' => $day, 'type' => 'siteToSinaComment'));
		return new ApiResponse($response);
	}

	/**
	 * 站點每日到新浪的轉發數量
	 * @param string $day 'Y-m-d'
	 * @return int 
	 */

	function getSiteToSinaForwardOfDay($day = null) {
		$platformApiClient = $this->_getPlatformApiClient();
		$response = $platformApiClient->get('weibo.stat.site', array('time' => $day, 'type' => 'siteToSinaForward'));
		return new ApiResponse($response);
	}

	/**
	 * 新浪每日到站點的微博數量
	 * @param string $day 'Y-m-d'
	 * @return int 
	 */

	function getSinaToSiteWeiboOfDay($day = null) {
		$platformApiClient = $this->_getPlatformApiClient();
		$response = $platformApiClient->get('weibo.stat.site', array('time' => $day, 'type' => 'sinaToSiteWeibo'));
		return new ApiResponse($response);
	}

	/**
	 * 新浪每日到站點的評論數量
	 * @param string $day 'Y-m-d'
	 * @return int 
	 */

	function getSinaToSiteCommentOfDay($day = null) {
		$platformApiClient = $this->_getPlatformApiClient();
		$response = $platformApiClient->get('weibo.stat.site', array('time' => $day, 'type' => 'sinaToSiteComment'));
		return new ApiResponse($response);
	}

	/**
	 * 新浪每日到站點的轉發數量
	 * @param string $day 'Y-m-d'
	 * @return int 
	 */

	function getSinaToSiteForwardOfDay($day = null) {
		$platformApiClient = $this->_getPlatformApiClient();
		$response = $platformApiClient->get('weibo.stat.site', array('time' => $day, 'type' => 'sinaToSiteForward'));
		return new ApiResponse($response);
	}
	
	/**
	 * (30天)站點轉發新浪的用戶top10（數據量）
	 * @param int $days
	 * @param int $num
	 * @return array
	 */
	function getTopUsersForSiteToSinaWeibo($days = 30, $num = 10) {
		return $this->_getTopUsers('siteToSinaWeibo', $days, $num);
	}
	
	/**
	 * (30天)站點轉發新浪的評論用戶top10（數據量）
	 * @param int $days
	 * @param int $num
	 * @return array
	 */
	function getTopUsersForSiteToSinaComment($days = 30, $num = 10) {
		return $this->_getTopUsers('siteToSinaComment', $days, $num);
	}
	
	/**
	 * (30天)站點轉發新浪的轉發用戶top10（數據量）
	 * @param int $days
	 * @param int $num
	 * @return array
	 */
	function getTopUsersForSiteToSinaForward($days = 30, $num = 10) {
		return $this->_getTopUsers('siteToSinaForward', $days, $num);
	}
	
	/**
	 * (30天)新浪轉發站點的用戶top10（數據量）
	 * @param int $days
	 * @param int $num
	 * @return array
	 */
	function getTopUsersForSinaToSiteWeibo($days = 30, $num = 10) {
		return $this->_getTopUsers('sinaToSiteWeibo', $days, $num);
	}
	
	/**
	 * (30天)新浪轉發站點的評論用戶top10（數據量）
	 * @param int $days
	 * @param int $num
	 * @return array
	 */
	function getTopUsersForSinaToSiteComment($days = 30, $num = 10) {
		return $this->_getTopUsers('sinaToSiteComment', $days, $num);
	}
	
	/**
	 * (30天)新浪轉發站點的轉發用戶top10（數據量）
	 * @param int $days
	 * @param int $num
	 * @return array
	 */
	function getTopUsersForSinaToSiteForward($days = 30, $num = 10) {
		return $this->_getTopUsers('sinaToSiteForward', $days, $num);
	}
	
	/**
	 * 新浪微博安裝情況
	 */
	function getInstallInfo() {
		$platformApiClient = $this->_getPlatformApiClient();
		$response = $this->_jsonDecode($platformApiClient->get('weibo.stat.siteinfo'));
		return new ApiResponse($response);
	}
	
	/**
	 * (30天)新浪與站點的用戶top10的封裝（數據量）
	 * @param string $type 統計類型
	 * @param int $days
	 * @param int $num
	 * @return array
	 */
	function _getTopUsers($type, $days = 30, $num = 10) {
		$platformApiClient = $this->_getPlatformApiClient();
		$topUsers = $this->_jsonDecode($platformApiClient->get('weibo.stat.user', array('type' => $type, 'day' => $days, 'offset' => $num)));
		if (!$topUsers || !is_array($topUsers)) return new ApiResponse(array());
		
		$userService = L::loadClass('userService', 'user'); /* @var $userService PW_UserService */
		$uids = $uidToStats = $userSort =  array();
		$i = 0;
		foreach ($topUsers as $topUser) {
			 $uids[] = $topUser['uid'];
			 $uidToStats[$topUser['uid']] = array($topUser['total']);
		}

		require_once R_P.'require/showimg.php';
		foreach ($userService->getByUserIds($uids) as $rt) {
			$uidToStats[$rt['uid']] = array_merge($uidToStats[$rt['uid']], array(
				$rt['uid'],
				$rt['username'],
				showfacedesign($rt['icon']),
			));
		}
		return new ApiResponse($uidToStats);
	}
	/**
	 * @return PlatformApiClient
	 */
	function _getPlatformApiClient() {
		static $client = null;
		if (null === $client) {
			global $db_sitehash, $db_siteownerid;
			L::loadClass('client', 'utility/platformapisdk', false);
			$client = new PlatformApiClient($db_sitehash, $db_siteownerid);
		}
		return $client;
	}
	
	function _jsonDecode ($response) {
		require_once(R_P . 'api/class_json.php');
		$json = new Services_JSON(true);
		return $json->decode($response);
	}
}