<?php
!function_exists('adminmsg') && exit('Forbidden');
$siteBindInfoService = L::loadClass('WeiboSiteBindInfoService', 'sns/weibotoplatform/service'); /* @var $siteBindInfoService PW_WeiboSiteBindInfoService */

$templatesConfig = array(
	'article' => array(
		'title' => '帖子內容',
		'description' => '{title}為帖子標題 ; {content}為帖子內容摘要 ; {url}為帖子地址',
	),
	'diary' => array(
		'title' => '日誌內容',
		'description' => '{title}為日誌標題;  {content}為日誌內容摘要;  {url}為日誌地址',
	),
	'group_active' => array(
		'title' => '群組活動',
		'description' => '{title}為群組活動標題; {content}為群組活動內容摘要; {url}為群組活動地址',
	),
	'cms' => array(
		'title' => '文章內容',
		'description' => '{title}為文章標題; {content}為文章內容摘要;  {url}為文章地址',
	),
	'photos' => array(
		'title' => '相冊',
		'description' => '{photo_count}為照片張數;  {url}為相冊地址',
	),
	'group_photos' => array(
		'title' => '群組相冊',
		'description' => '{photo_count}為照片張數;  {url}為群組相冊地址',
	),
);

InitGP(array('step', 'templates'));
if ($step == 'edit' && !empty($templates)) {
	$warningMessage = '';
	foreach ($templatesConfig as $key => $value) {
		if (!isset($templates[$key]) || '' == $templates[$key]) $warningMessage = '所有微博模版不能為空';
	}
	if (!$warningMessage) {
		$siteBindInfoService->saveWeiboTemplates($templates);
		$warningMessage = '恭喜, 設置成功了';
	}
}

$templatesSet = $siteBindInfoService->getWeiboTemplates();

include PrintTemplate('custom_weibo_template');
exit();
?>