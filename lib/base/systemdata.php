<?php
!defined('P_W') && exit('Forbidden');
class SystemData/*abstruct*/ {
	var $_lang=array();
	/**
	 * 獲取數據對外接口
	 * @param array $config
	 * @param int $num
	 * return array
	 */
	function getSourceData($config,$num) {
	}
	/**
	 * 獲取該數據源相關數據類型
	 * return string
	 */
	function getRelateType() {
	}
	/**
	 * 獲取本數據源配置信息
	 */
	function getSourceConfig() {
	}
	/**
	 * 獲取數據源的各個數據的名稱
	 * @param string $key
	 * return string
	 */
	function getSourceLang($key) {
		$lang = $this->_lang;
		return isset($lang[$key]) ? $lang[$key] : '';
	}

}