<?php
!defined('P_W') && exit('Forbidden');

/**
 * 數據庫操作基類
 * 
 * @package DB
 */
class BaseDB {
	/**
	 * 數據庫連接對像
	 * 
	 * @var DB
	 */
	var $_db = null;
	/**
	 * @var primary ID
	 */
	var $_primaryKey = '';
	var $_tableName = '';
	
	/**
	 * 構造器
	 */
	function BaseDB() {
		if (!$GLOBALS['db']) PwNewDB();
		$this->_db = $GLOBALS['db'];
	}
	
	function _getConnection() {
		return $GLOBALS['db']; //global
	}
	
	/**
	 * 構造更新的sql
	 * 
	 * @see S::sqlSingle
	 * @access protected
	 * @param array $arr 更新數據數組
	 * @return string
	 */
	function _getUpdateSqlString($arr) {
		return S::sqlSingle($arr);
	}
	
	/**
	 * 獲取查詢結果
	 * 
	 * @access protected
	 * @param resource $query 數據庫結果集資源符
	 * @param string|null 數據結果數組的索引key，為null則為自增key
	 * @return array
	 */
	function _getAllResultFromQuery($query, $resultIndexKey = null) {
		$result = array();
		
		if ($resultIndexKey) {
			while ($rt = $this->_db->fetch_array($query)) {
				$result[$rt[$resultIndexKey]] = $rt;
			}
		} else {
			while ($rt = $this->_db->fetch_array($query)) {
				$result[] = $rt;
			}
		}
		return $result;
	}
	
	/**
	 * 檢查數組key是否為合法字段
	 * 
	 * @access protected
	 * @param array $fieldData 數據數組
	 * @param array $allowFields 允許的字段
	 * @return array 過濾後的數據
	 */
	function _checkAllowField($fieldData, $allowFields) {
		foreach ($fieldData as $key => $value) {
			if (!in_array($key, $allowFields)) {
				unset($fieldData[$key]);
			}
		}
		return $fieldData;
	}
	
	/**
	 * 反斜槓過濾
	 * 
	 * @see S::sqlEscape
	 * @access protected
	 * @param mixed $var 數據
	 * @return mixed 過濾後的數據
	 */
	function _addSlashes($var) {
		return S::sqlEscape($var);
	}
	
	/**
	 * implode組裝數組為sql
	 * 
	 * @see S::sqlImplode
	 * @access protected
	 * @param $arr 數據數組
	 * @param bool $strip 是否經過stripslashes處理
	 */
	function _getImplodeString($arr, $strip = true) {
		return S::sqlImplode($arr, $strip);
	}
	
	/**
	 * 序列化數據
	 * 
	 * @access protected
	 * @param mixed $value
	 * @return string
	 */
	function _serialize($value) {
		if (is_array($value)) {
			return serialize($value);
		}
		if (is_string($value) && is_array(unserialize($value))) {
			return $value;
		}
		return '';
	}
	
	/**
	 * 反序列化數據
	 * 
	 * @access protected
	 * @param string $value
	 * @return mixed
	 */
	function _unserialize($value) {
		if ($value && is_array($tmpValue = unserialize($value))) {
			$value = $tmpValue;
		}
		return $value;
	}
	/**
	 * 基礎增加數據查詢語句
	 * @param $fieldData
	 * @return unknown_type
	 */
	function _insert($fieldData) {
		if (!$this->_check() || !$fieldData) return false;
		//* $this->_db->update("INSERT INTO " . $this->_tableName . " SET " . $this->_getUpdateSqlString($fieldData));
		return pwQuery::insert($this->_tableName, $fieldData);
	}
	/**
	 * 基礎更新數據查詢語句
	 * @param $fieldData
	 * @param $id
	 * @return unknown_type
	 */
	function _update($fieldData, $id) {
		if (!$this->_check() || !$fieldData || $id < 1) return false;
		//* $this->_db->update("UPDATE " . $this->_tableName . " SET " . $this->_getUpdateSqlString($fieldData) . " WHERE " . $this->_primaryKey . "=" . $this->_addSlashes($id) . " LIMIT 1");
		return pwQuery::update($this->_tableName, "{$this->_primaryKey}=:{$this->_primaryKey}", array($id), $fieldData);
	}
	/**
	 * 基礎刪除一條數據查詢語句
	 * @param $id
	 * @return unknown_type
	 */
	function _delete($id) {
		if (!$this->_check() || $id < 1) return false;
		//* $this->_db->update("DELETE FROM " . $this->_tableName . " WHERE " . $this->_primaryKey . "=" . $this->_addSlashes($id) . " LIMIT 1");
		return pwQuery::delete($this->_tableName, "{$this->_primaryKey}=:{$this->_primaryKey}", array($id));
	}
	/**
	 * 基礎獲取一條數據查詢語句
	 * @param $id
	 * @param $fields 返回的字段名，多個用','隔開，全部為'*'
	 * @return unknown_type
	 */
	function _get($id, $fields = '*') {
		if (!$this->_check() || $id < 1) return false;
		return $this->_db->get_one("SELECT $fields FROM " . $this->_tableName . " WHERE " . $this->_primaryKey . "=" . $this->_addSlashes($id) . " LIMIT 1");
	}
	/**
	 * 基礎統計全部數據查詢語句
	 * @return unknown_type
	 */
	function _count() {
		if (!$this->_check()) return false;
		$result = $this->_db->get_one("SELECT COUNT(*) as total FROM " . $this->_tableName);
		return $result['total'];
	}
	/**
	 * 私用檢查表名稱與主鍵是否定義函數
	 * @return unknown_type
	 */
	function _check() {
		return (!$this->_tableName || !$this->_primaryKey) ? false : true;
	}
	
	/**
	 * SQL查詢中,構造LIMIT語句
	 *
	 * @param int $start 開始記錄位置
	 * @param int $num 讀取記錄數目
	 * @return string SQL語句
	 */
	function _Limit($start, $num = false){
		return S::sqlLimit($start, $num);
	}
}

