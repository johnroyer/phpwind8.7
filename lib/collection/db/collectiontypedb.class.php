<?php
!defined('P_W') && exit('Forbidden');

/**
 * 收藏分類數據層
 * 
 * @package PW_CollectionTypeDB
 * @author	panjl
 * @abstract
 */

class PW_CollectionTypeDB extends BaseDB {
	var $_tableName 	= 	"pw_collectiontype";
	var $_primaryKey 	= 	'ctid';

	/**
	 * 添加
	 * 
	 * @param array $fieldsData 數據數組，以數據庫字段為key
	 * @return int 新增id
	 */
	function insert($fieldsData) {
		if (S::isArray($fieldsData)) {
			return $this->_insert($fieldsData);
		}
	}

	/**
	 * 編輯
	 * 
	 * @param array $fieldsData 數據數組，以數據庫字段為key
	 * @param int $ctid 分類ID
	 * @return int 最新編輯的分類ID
	 */
	function update($fieldsData,$ctid) {
		$ctid = intval($ctid);
		if ($ctid < 1) return null;
		if (S::isArray($fieldsData)) {
			return $this->_update($fieldsData,$ctid);
		}
	}

	/**
	 * 刪除
	 * 
	 * @param int $ctid  分類ID
	 * @return int 刪除行數
	 */
	function delete($ctid) {
		$ctid = intval($ctid);
		if ($ctid < 1) return null;
		return $this->_delete($ctid);
	}

	/**
	 * 根據分類ID取分類信息
	 * 
	 * @param int $ctid  分類ID
	 * @return array 
	 */
	function getTypeByCtid($ctid) {
		$ctid = intval($ctid);
		if ( $ctid < 1 ) return array();
		return $this->_db->get_one ("SELECT * FROM ".$this->_tableName." WHERE ctid = ". S::sqlEscape($ctid));
	}

	/**
	 * 根據用戶uid取分類信息
	 * 
	 * @param int $uid  用戶uid
	 * @return array 
	 */
	function getTypesByUid($uid) {
		$uid = intval($uid);
		if ( $uid < 1 ) return array();
		$query = $this->_db->query ( "SELECT * FROM ".$this->_tableName." WHERE uid = ". S::sqlEscape($uid));
		return $this->_getAllResultFromQuery ($query);
	}

	/**
	 * 按uid檢測分類是否存在
	 * 
	 * @param int $userId 用戶uid
	 * @param string $typeName
	 * @return int ctid
	 */
	function getCtidByUidAndName($uid, $typeName) {
		$uid = intval($uid);
		if ( $uid < 1 || !$typeName ) return 0;
		return $this->_db->get_value( "SELECT ctid FROM ".$this->_tableName." WHERE uid = ". S::sqlEscape($uid) . " AND name = " . S::sqlEscape($typeName));
	}

}