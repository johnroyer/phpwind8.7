<?php
!defined('P_W') && exit('Forbidden');

/**
 * 收藏分類service
 * 
 * @package PW_CollectionTypeService
 * @author	panjl
 * @abstract
 */

class PW_CollectionTypeService {
	
	/**
	 * 添加
	 * 
	 * @param array $fieldsData 數據數組，以數據庫字段為key
	 * @return int 新增id
	 */
	function insert ($fieldsData) {
		if (S::isArray($fieldsData)) {
			$typeDb = $this->_getCollectionTypeDB();
			return $typeDb->insert($fieldsData);
		}
	}

	/**
	 * 編輯
	 * 
	 * @param array $fieldsData 數據數組，以數據庫字段為key
	 * @param int $ctid 分類ID
	 * @return int 最新編輯的分類ID
	 */
	function update ($fieldsData, $ctid) {
		$ctid = intval($ctid);
		if ($ctid < 1) return NULL;
		if (S::isArray($fieldsData)) {
			$typeDb = $this->_getCollectionTypeDB();
			return $typeDb->update($fieldsData, $ctid);
		}
	}

	/**
	 * 刪除
	 * 
	 * @param int $ctid  分類ID
	 * @return int 刪除行數
	 */
	function delete ($ctid) {
		$ctid = intval($ctid);
		if ($ctid != '-1' && $ctid < 1) return null;
		$typeDb = $this->_getCollectionTypeDB();
		return $typeDb->delete($ctid);
	}

	/**
	 * 按用戶ID和分類名查ctid
	 *
	 * @param int $uid 用戶uid
	 * @param string $typeName 分類名
	 * @return int 分類ID
	 */
	function getCtidByUidAndName($uid, $typeName) {
		$uid = (int)$uid;
		if ( $uid < 1 || !$typeName ) return 0;
		$typeDb = $this->_getCollectionTypeDB();
		return $typeDb->getCtidByUidAndName($uid, $typeName);
	}

	/**
	 * 按uid檢測分類是否存在
	 *
	 * @param int $uid 用戶uid
	 * @param string $typeName 分類名
	 * @param int ctid 分類ID
	 * @return boolen 
	 */
	function checkTypeExist($uid, $typeName, $ctid=null) {
		global $winduid;
		$uid = (int)$uid;
		$ctid   = (int)$ctid;
		if ( !$typeName || ($uid != $winduid) ) return false;
		if ( !$ctid ) {
			$isExistType = $this->getCtidByUidAndName($uid,$typeName);
			if ($isExistType > 0) return false;
		} else {
			$typeDb = $this->getTypeByCtid($ctid);
			if ($typeDb['name'] != $typeName) {
				$isExistType = $this->getCtidByUidAndName($uid,$typeName);
				if ($isExistType > 0) return false;
			}
		}
		 return true;
	}

	/**
	 * 根據用戶id 查找收藏分類
	 *
	 * @param int $uid 用戶uid
	 * @return array 收藏分類
	 */
	function getTypesByUid($uid) {
		$uid = (int)$uid;
		if ( $uid < 1 ) return array();
		$typeDb = $this->_getCollectionTypeDB();
		return $typeDb->getTypesByUid($uid);
	}

	/**
	 * 根據分類ID 查找收藏分類
	 *
	 * @param int $ctid 分類ID
	 * @return array
	 */
	function getTypeByCtid($ctid) {
		$ctid = (int)$ctid;
		if ( $ctid < 1 ) return array();
		$typeDb = $this->_getCollectionTypeDB();
		return $typeDb->getTypeByCtid($ctid);
	}

	/**
	 * get PW_CollectionTypeDB
	 * 
	 * @access protected
	 * @return PW_CollectionTypeDB
	 */
	function _getCollectionTypeDB() {
		return L::loadDB('CollectionType', 'collection');
	}
}

