<?php
!defined('P_W') && exit('Forbidden');
include_once (R_P . 'lib/datanalyse/datanalyse.base.php');

/**
 * 用戶排行
 * @author yishuo
 */
class PW_Useranalyse extends PW_Datanalyse {
	var $pk = 'tid';
	/* 用戶在線排行，分享排行，積分排行，好友排行 */
	var $actions = array('memberOnLine', 'memberThread', 'memberShare', 'memberCredit', 'memberFriend');

	function PW_Useranalyse() {
		$this->__construct();
	}

	function __construct() {
		parent::__construct();
	}

	/**
	 * 根據日誌ID數組獲得日誌信息
	 * @return array
	 */
	function _getDataByTags() {
		if (empty($this->tags)) return array();
		
		$userService = L::loadClass('UserService', 'user'); /* @var $userService PW_UserService */
		return $userService->getUsersWithMemberDataByUserIds($this->tags);
	}
}
?>