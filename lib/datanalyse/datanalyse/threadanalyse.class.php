<?php
!defined('P_W') && exit('Forbidden');
include_once (R_P . 'lib/datanalyse/datanalyse.base.php');

/**
 * 帖子排行
 * @author yishuo
 */
class PW_Threadanalyse extends PW_Datanalyse {
	var $pk = 'tid';
	/* 回復排行，評價排行，收藏排行，分享排行 */
	var $actions = array('threadPost', 'threadFav', 'threadShare', 'threadRate');

	function PW_Threadanalyse() {
		$this->__construct();
	}

	function __construct() {
		parent::__construct();
	}

	/**
	 * 獲得類別評價類型
	 * 	1 帖子評價
	 *  2 日誌評價
	 *  3 相片評價
	 * @return array
	 */
	function _getExtendActions() {
		global $db_ratepower;
		$rateSets = unserialize($db_ratepower);
		if ($rateSets[1]) {
			$rate = L::loadClass('rate', 'rate');
			$_tmp = $rate->getRatePictureHotTypes();
		}
		return array_keys($_tmp);
	}

	/**
	 * 根據日誌ID數組獲得日誌信息
	 * @return array
	 */
	function _getDataByTags() {
		if (empty($this->tags)) return array();
		$threadsDB = L::loadDB('threads', 'forum');
		$result = $threadsDB->getsBythreadIds($this->tags);
		return $result;
	}

}
?>