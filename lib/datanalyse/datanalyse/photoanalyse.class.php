<?php
!defined('P_W') && exit('Forbidden');
include_once (R_P . 'lib/datanalyse/datanalyse.base.php');

/**
 * 相冊排行
 * @author yishuo
 */
class PW_Photoanalyse extends PW_Datanalyse {
	var $pk = 'pid';
	var $actions = array('picNew', 'picComment', 'picFav', 'picShare', 'picRate');

	function PW_Photoanalyse() {
		$this->__construct();
	}

	function __construct() {
		parent::__construct();
	}

	/**
	 * 獲得根據類別評價類型
	 *  1 帖子評價
	 *  2 日誌評價
	 *  3 相片評價
	 * @return array
	 */
	function _getExtendActions() {
		global $db_ratepower;
		$rateSets = unserialize($db_ratepower);
		$_tmp = array();
		if ($rateSets[3]) {
			$rate = L::loadClass('rate', 'rate');
			$_tmp = $rate->getRatePictureHotTypes();
		}
		return array_keys($_tmp);
	}

	/**
	 * 根據日誌ID數組獲得日誌信息
	 * @return array
	 */
	function _getDataByTags() {
		if (empty($this->tags)) return array();
		$cnphotoDB = L::loadDB('cnphoto', 'colony');
		$result = $cnphotoDB->getDataByPids($this->tags);
		return $result;
	}

}
?>