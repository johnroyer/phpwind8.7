<?php
!defined('P_W') && exit('Forbidden');

/**
 * 道具中心全局調用類
 * @2010-4-19 zhudong
 */

class PW_ToolCenter {

	var $_db = null;
	var $_timestamp = 0;

	function PW_ToolCenter(){
		global $db,$timestamp;
		$this->_db = $db;
		$this->_timestamp = $timestamp;		
	}

	/**
	 * 隨機調取指定個數的道具信息
	 * @param $num 調取的個數
	 * @return array 
	*/

	function getToolsByRandom($num){
		$tools = array();
		$query = $this->_db->query("SELECT * FROM pw_tools WHERE state=1 ORDER BY RAND() LIMIT ".intval($num));
		while ($rt = $this->_db->fetch_array($query)) {
			if(empty($rt['logo'])) {
				$rt['logo'] = $GLOBALS['imgpath'] . '/nopic.gif';
			} else {
				$rt['logo'] = "u/images/toolcenter/tool/$rt[id].gif";
			}
			$rt['subdescrip'] = substrs($rt['descrip'],20);
			$tools[] = $rt;
		}
		return $tools;
	}

	/**
	 * 調取指定個數和指定用戶的的道具信息
	 * @param $uid 調取的用戶
	 * @param $num 調取的個數
	 * @return array 
	*/

	function getToolsByUidAndNum($uid,$num){
		$tools = array();
		$query = $this->_db->query("SELECT u.*,t.name,t.price,t.creditype,t.stock,t.descrip,t.type,t.logo FROM pw_usertool u LEFT JOIN pw_tools t ON t.id=u.toolid WHERE u.uid=".S::sqlEscape($uid)." AND u.nums>0 LIMIT ".intval($num));
		while ($rt = $this->_db->fetch_array($query)) {
			if(empty($rt['logo'])) {
				$rt['logo'] = $GLOBALS['imgpath'] . '/nopic.gif';
			} else {
				$rt['logo'] = "u/images/toolcenter/tool/$rt[toolid].gif";
			}
			$rt['subdescrip'] = substrs($rt['descrip'],20);
			$tools[] = $rt;
		}
		return $tools;
	}
}