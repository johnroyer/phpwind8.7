<?php
!function_exists('readover') && exit('Forbidden');
/**
 * 消息中心服務層
 * @2010-4-6 liuhui
 */
class PW_Message {
	/**
	 * 公共發送站內信函數  單人對話/多人對話
	 * @param int $userId  發送用戶UID
	 * @param array $usernames 接收用戶名數組
	 * @param array $messageinfo array('create_uid','create_username','title','content','expand') 消息體數組
	 * @param string $typeId 站內信類型，默認為短消息，可選評論/留言/評價
	 * @return int 發送成功的messageId
	 */
	function sendMessage($userId, $usernames, $messageInfo, $typeId = null ,$isSuper = false) {
		$service = $this->_serviceFactory("message");
		return $service->sendMessage($userId, $usernames, $messageInfo, $typeId ,$isSuper);
	}
	/**
	 * 公共消息中心全局回復函數
	 * @param int $parentId 父消息ID
	 * @param int $relationId 關係ID
	 * @param int $userId 發送用戶UID
	 * @param array $messageinfo 回復體數組
	 * @return int 發送成功的回復id
	 */
	function sendReply($userId, $relationId, $parentId, $messageInfo) {
		$service = $this->_serviceFactory("message");
		return $service->sendReply($userId, $relationId, $parentId, $messageInfo);
	}
	/**
	 * 獲取某個人所有站內信
	 * @param int $page
	 * @param int $userId 用戶UID
	 * @param int $perpage
	 * @return array array(array(mid,rid,uid,title,content,typeid,categoryid,status,isown,created_time,modified_time,expand,create_uid,create_username)) 二維數組結構 關係表和信息表的字段數組
	 */
	function getAllMessages($userId, $page, $perpage) {
		$service = $this->_serviceFactory("message");
		return $service->getAllMessages($userId, $page, $perpage);
	}
	/**
	 * 獲取某個人末讀站內信
	 * @param int $userId 用戶UID
	 * @param int $page
	 * @param int $perpage
	 * @return array array(array(mid,rid,uid,title,content,typeid,categoryid,status,isown,created_time,modified_time,expand,create_uid,create_username)) 二維數組結構 關係表和信息表的字段數組
	 */
	function getMessagesNotRead($userId, $page, $perpage) {
		$service = $this->_serviceFactory("message");
		return $service->getMessagesNotRead($userId, $page, $perpage);
	}
	/**
	 * 獲取某類型的站內信
	 * @param int $userId 用戶UID
	 * @param int $typeId
	 * @param int $page
	 * @param int $perpage
	 * @return array
	 */
	function getMessages($userId, $typeId, $page, $perpage) {
		$service = $this->_serviceFactory("message");
		return $service->getMessages($userId, $typeId, $page, $perpage);
	}
	/**
	 * 獲取全部對話內容
	 * @param int $userId 用戶UID
	 * @param int $messageId
	 * @param int $relationId
	 * @return array array(array(mid,rid,uid,title,content,typeid,categoryid,status,isown,created_time,modified_time,expand,create_uid,create_username))
	 */
	function getReplies($userId, $messageId, $relationId) {
		$service = $this->_serviceFactory("message");
		return $service->getReplies($userId, $messageId, $relationId);
	}
	/**
	 * 獲取消息的上一條
	 * @param int $relationId
	 * @return array array(mid,rid,uid,title,content,typeid,categoryid,status,isown,created_time,modified_time,expand,create_uid,create_username)
	 */
	function getUpMessage($userId, $relationId, $typeId = null) {
		$service = $this->_serviceFactory("message");
		return $service->getUpMessage($userId, $relationId, $typeId);
	}
	
	function getUpInfoByType($userId, $relationId, $isown, $typeId = null) {
		$service = $this->_serviceFactory("message");
		return $service->getUpInfoByType($userId, $relationId, $isown, $typeId);
	}
	
	function getDownInfoByType($userId, $relationId, $isown, $typeId = null) {
		$service = $this->_serviceFactory("message");
		return $service->getDownInfoByType($userId, $relationId, $isown, $typeId);
	}
	/**
	 * 獲取消息的下一條
	 * @param int $relationId
	 * @return array array(mid,rid,uid,title,content,typeid,categoryid,status,isown,created_time,modified_time,expand,create_uid,create_username)
	 */
	function getDownMessage($userId, $relationId, $typeId = null) {
		$service = $this->_serviceFactory("message");
		return $service->getDownMessage($userId, $relationId, $typeId);
	}
	/**
	 * 獲取我發送的站內信
	 * @param int $page
	 * @param int $perpage
	 * @return array array(array(mid,rid,uid,title,content,typeid,categoryid,status,isown,created_time,modified_time,expand,create_uid,create_username))
	 */
	function getMessagesBySelf($userId, $typeId, $page, $perpage) {
		$service = $this->_serviceFactory("message");
		return $service->getMessagesBySelf($userId, $typeId, $page, $perpage);
	}
	/**
	 * 獲取我接收的站內信
	 * @param int $page
	 * @param int $perpage
	 * @return array array(array(mid,rid,uid,title,content,typeid,categoryid,status,isown,created_time,modified_time,expand,create_uid,create_username))
	 */
	function getMessagesByOther($userId, $typeId, $page, $perpage) {
		$service = $this->_serviceFactory("message");
		return $service->getMessagesByOther($userId, $typeId, $page, $perpage);
	}
	/**
	 * 獲取我發送的站全部內信
	 * @param int $page
	 * @param int $perpage
	 * @return array array(array(mid,rid,uid,title,content,typeid,categoryid,status,isown,created_time,modified_time,expand,create_uid,create_username))
	 */
	function getAllMessagesBySelf($userId, $typeId, $page, $perpage) {
		$service = $this->_serviceFactory("message");
		return $service->getAllMessagesBySelf($userId, $typeId, $page, $perpage);
	}
	/**
	 * 獲取我接收的全部站內信
	 * @param int $page
	 * @param int $perpage
	 * @return array array(array(mid,rid,uid,title,content,typeid,categoryid,status,isown,created_time,modified_time,expand,create_uid,create_username))
	 */
	function getAllMessagesByOther($userId, $typeId, $page, $perpage) {
		$service = $this->_serviceFactory("message");
		return $service->getAllMessagesByOther($userId, $typeId, $page, $perpage);
	}
	/**
	 * 統計所有站內信
	 * @return int
	 */
	function countAllMessage($userId) {
		$service = $this->_serviceFactory("message");
		return $service->countAllMessage($userId);
	}
	/**
	 * 統計所有末讀站內信
	 * @return int
	 */
	function countMessagesNotRead($userId) {
		$service = $this->_serviceFactory("message");
		return $service->countMessagesNotRead($userId);
	}
	/**
	 * 統計所有我發送站內信
	 * @return int
	 */
	function countMessagesBySelf($userId, $typeId) {
		$service = $this->_serviceFactory("message");
		return $service->countMessagesBySelf($userId, $typeId);
	}
	/**
	 * 統計所有我收到站內信
	 * @return int
	 */
	function countMessagesByOther($userId, $typeId) {
		$service = $this->_serviceFactory("message");
		return $service->countMessagesByOther($userId, $typeId);
	}
	/**
	 * 統計某類型的站內信
	 * @param $typeid
	 * @return int
	 */
	function countMessage($userId, $typeId) {
		$service = $this->_serviceFactory("message");
		return $service->countMessage($userId, $typeId);
	}
	/**
	 * 刪除一條關係體
	 * @param $relationId
	 * @return int
	 */
	function deleteMessage($userId, $relationId) {
		$service = $this->_serviceFactory("message");
		return $service->deleteMessage($userId, $relationId);
	}
	/**
	 * 刪除多條關係體
	 * @param array $relationIds
	 * @return int
	 */
	function deleteMessages($userId, $relationIds) {
		$service = $this->_serviceFactory("message");
		return $service->deleteMessages($userId, $relationIds);
	}
	/**
	 * 更新一條關係體
	 * @param array $fieldData
	 * @param $relationId
	 * @return int
	 */
	function updateMessage($fieldData, $userId, $relationId) {
		$service = $this->_serviceFactory("message");
		return $service->updateMessage($fieldData, $userId, $relationId);
	}
	/**
	 * 標記一條關係體
	 * @param $relationId
	 * @return bool
	 */
	function markMessage($userId, $relationId) {
		$service = $this->_serviceFactory("message");
		return $service->markMessage($userId, $relationId);
	}
	/**
	 * 標記多條關係體
	 * @param array $relationIds
	 * @return bool
	 */
	function markMessages($userId, $relationIds) {
		$service = $this->_serviceFactory("message");
		return $service->markMessages($userId, $relationIds);
	}
	/**
	 * 根椐消息ID獲取消息內容
	 * @param $messageId
	 * @return unknown_type
	 */
	function getMessage($messageId) {
		$service = $this->_serviceFactory("message");
		return $service->getMessage($messageId);
	}
	/**
	 * 根椐消息ID更新關係狀態為已讀
	 * @param $userId
	 * @param $messageId
	 * @return unknown_type
	 */
	function readMessages($userId, $messageId) {
		$service = $this->_serviceFactory("message");
		return $service->readMessages($userId, $messageId);
	}
	/**
	 * 根據大類清空消息
	 * @param $categorys 數組/大類keys數據 如 array('groupsms','sms','notice');
	 * @return unknown_type
	 */
	function clearMessages($userId, $categorys) {
		$service = $this->_serviceFactory("message");
		return $service->clearMessages($userId, $categorys);
	}
	/**
	 * 統計用戶所有消息數
	 * @param $userId
	 * @return unknown_type
	 */
	function statisticsMessage($userId) {
		$service = $this->_serviceFactory("message");
		return $service->statisticsMessage($userId);
	}
	/**
	 * 獲取互動消息
	 * @param $userId
	 * @param $page
	 * @param $perpage
	 * @return array('總數','消息數據')
	 */
	function interactiveMessages($userId, $page, $perpage) {
		$service = $this->_serviceFactory("message");
		return $service->interactiveMessages($userId, $page, $perpage);
	}
	/**
	 * 檢查用戶發送消息權限
	 * @param $category
	 * @param $number
	 * @return unknown_type
	 */
	function checkUserMessageLevle($category, $number) {
		$service = $this->_serviceFactory("message");
		return $service->checkUserLevle($category, $number);
	}
	/**
	 * 檢查接收者的信息
	 * @param $usernames
	 * @return unknown_type
	 */
	function checkReceiver($usernames) {
		$service = $this->_serviceFactory("message");
		return $service->checkReceiver($usernames);
	}
	/**
	 * 根椐關係體刪除關係
	 * @param $relationIds
	 * @return unknown_type
	 */
	function deleteRelationsByRelationIds($relationIds) {
		$service = $this->_serviceFactory("message");
		return $service->deleteRelationsByRelationIds($relationIds);
	}
	/**
	 * 獲取所有末讀消息列表
	 * @param $userId
	 * @param $page
	 * @param $perpage
	 * @return unknown_type
	 */
	function getAllNotRead($userId, $page, $perpage) {
		$service = $this->_serviceFactory("message");
		return $service->getAllNotRead($userId, $page, $perpage);
	}
	/**
	 * 批量統計用戶消息數
	 * @param array $userIds
	 * @return unknown_type
	 */
	function statisticUsersNumbers($userIds) {
		$service = $this->_serviceFactory("message");
		return $service->countUserNumbers($userIds);
	}
	/**
	 * 根椐用戶名和關係ID獲取關係
	 * @param $userId
	 * @param $relationId
	 * @return unknown_type
	 */
	function getRelation($userId, $relationId) {
		$service = $this->_serviceFactory("message");
		return $service->getRelation($userId, $relationId);
	}
	
	function getInBox($userId, $page, $perpage){
		$service = $this->_serviceFactory("message");
		return $service->getInBox($userId, $page, $perpage);
	}
	
	function countInBox($userId){
		$service = $this->_serviceFactory("message");
		return $service->countInBox($userId);
	}
	
	function getOutBox($userId, $page, $perpage){
		$service = $this->_serviceFactory("message");
		return $service->getOutBox($userId, $page, $perpage);
	}
	
	function countOutBox($userId){
		$service = $this->_serviceFactory("message");
		return $service->countOutBox($userId);
	}
	
	function getMessageByTypeIdWithBoxName($userId, $typeId, $page, $perpage, $boxName = 'outbox'){
		$service = $this->_serviceFactory("message");
		return $service->getMessageByTypeIdWithBoxName($userId, $typeId, $page, $perpage, $boxName);
	}
	
	function countMessageByTypeIdWithBoxName($userId, $typeId, $boxName = 'outbox'){
		$service = $this->_serviceFactory("message");
		return $service->countMessageByTypeIdWithBoxName($userId, $typeId, $boxName);
	}
	
	/**************************************************************/
	/**
	 * 發送一個通知
	 * @param $userId
	 * @param array $usernames
	 * @param array $messageinfo array('create_uid','create_username','title','content','expand') 消息體數組
	 * @param int $typeId
	 * @return $messageId 返回增加成功的消息ID
	 */
	function sendNotice($userId, $usernames, $messageInfo, $typeId = null) {
		$service = $this->_serviceFactory("notice");
		return $service->sendNotice($userId, $usernames, $messageInfo, $typeId);
	}
	/**
	 * 獲取某個用戶的所有通知
	 * @param $userId
	 * @param $page
	 * @param $perpage
	 * @return unknown_type
	 */
	function getAllNotices($userId, $page, $perpage) {
		$service = $this->_serviceFactory("notice");
		return $service->getAllNotices($userId, $page, $perpage);
	}
	/**
	 * 獲取某個用戶的所有末讀通知
	 * @param $userId
	 * @param $page
	 * @param $perpage
	 * @return unknown_type
	 */
	function getNoticesNotRead($userId, $page, $perpage) {
		$service = $this->_serviceFactory("notice");
		return $service->getNoticesNotRead($userId, $page, $perpage);
	}
	/**
	 * 獲取某個用戶的某類型通知
	 * @param $userId
	 * @param $typeId
	 * @param $page
	 * @param $perpage
	 * @return unknown_type
	 */
	function getNotices($userId, $typeId, $page, $perpage) {
		$service = $this->_serviceFactory("notice");
		return $service->getNotices($userId, $typeId, $page, $perpage);
	}
	/**
	 * 獲取上一條通知
	 * @param $userId
	 * @param $relationId
	 * @return unknown_type
	 */
	function getUpNotice($userId, $relationId, $typeId) {
		$service = $this->_serviceFactory("notice");
		return $service->getUpNotice($userId, $relationId, $typeId);
	}
	/**
	 * 獲取下一條通知
	 * @param $userId
	 * @param $relationId
	 * @return unknown_type
	 */
	function getDownNotice($userId, $relationId, $typeId) {
		$service = $this->_serviceFactory("notice");
		return $service->getDownNotice($userId, $relationId, $typeId);
	}
	/**
	 * 統計所有通知
	 * @param $userId
	 * @return unknown_type
	 */
	function countAllNotice($userId) {
		$service = $this->_serviceFactory("notice");
		return $service->countAllNotice($userId);
	}
	/**
	 * 統計所有末讀通知
	 * @param $userId
	 * @return unknown_type
	 */
	function countNoticesNotRead($userId) {
		$service = $this->_serviceFactory("notice");
		return $service->countNoticesNotRead($userId);
	}
	/**
	 * 統計某類型通知
	 * @param $userId
	 * @param $typeId
	 * @return unknown_type
	 */
	function countNotice($userId, $typeId) {
		$service = $this->_serviceFactory("notice");
		return $service->countNotice($userId, $typeId);
	}
	/**
	 * 刪除一條通知
	 * @param $userId
	 * @param $relationId
	 * @return unknown_type
	 */
	function deleteNotice($userId, $relationId) {
		$service = $this->_serviceFactory("notice");
		return $service->deleteNotice($userId, $relationId);
	}
	/**
	 * 刪除多條通知
	 * @param $userId
	 * @param $relationIds
	 * @return unknown_type
	 */
	function deleteNotices($userId, $relationIds) {
		$service = $this->_serviceFactory("notice");
		return $service->deleteNotices($userId, $relationIds);
	}
	/**
	 * 更新一條通知
	 * @param $fieldData
	 * @param $userId
	 * @param $relationId
	 * @return unknown_type
	 */
	function updateNotice($fieldData, $userId, $relationId) {
		$service = $this->_serviceFactory("notice");
		return $service->updateNotice($fieldData, $userId, $relationId);
	}
	/**
	 * 標記一條通知已讀
	 * @param $userId
	 * @param $relationId
	 * @return unknown_type
	 */
	function markNotice($userId, $relationId) {
		$service = $this->_serviceFactory("notice");
		return $service->markNotice($userId, $relationId);
	}
	/**
	 * 標記多條通知已讀
	 * @param $userId
	 * @param $relationIds
	 * @return unknown_type
	 */
	function markNotices($userId, $relationIds) {
		$service = $this->_serviceFactory("notice");
		return $service->markNotices($userId, $relationIds);
	}
	/**
	 * 獲取一條通知
	 * @param $messageId
	 * @return unknown_type
	 */
	function getNotice($messageId) {
		$service = $this->_serviceFactory("notice");
		return $service->getNotice($messageId);
	}
	/**
	 * 讀取一條通知
	 * @param $userId
	 * @param $messageId
	 * @return unknown_type
	 */
	function readNotices($userId, $messageId) {
		$service = $this->_serviceFactory("notice");
		return $service->readNotices($userId, $messageId);
	}
	/**************************************************************/
	/**
	 * 發送消息附件
	 * @param array $fieldDatas 二維數組 array(array('uid','aid','mid','status')......);
	 * @return last_insert_id
	 */
	function sendAttachs($fieldDatas) {
		$service = $this->_serviceFactory("attach");
		return $service->addAttachs($fieldDatas);
	}
	/**
	 * 展示消息附件
	 * @param $userId
	 * @param $messageId
	 * @return array
	 */
	function showAttachs($userId, $messageId) {
		$service = $this->_serviceFactory("attach");
		return $service->getAttachs($userId, $messageId);
	}
	/**
	 * 移除消息附件
	 * @param $userId
	 * @param $id
	 * @return bool
	 */
	function removeAttach($userId, $id) {
		$service = $this->_serviceFactory("attach");
		return $service->removeAttach($userId, $id);
	}
	/**
	 * 獲取所有消息附件
	 * @return unknown_type
	 */
	function getAllAttachs($page, $perpage) {
		$service = $this->_serviceFactory("attach");
		return $service->getAllAttachs($page, $perpage);
	}
	/**
	 * 統計所有消息附件
	 * @return unknown_type
	 */
	function countAllAttachs() {
		$service = $this->_serviceFactory("attach");
		return $service->countAllAttachs();
	}
	/**
	 * 根椐消息數組刪除附件
	 * @param $messageIds
	 * @return unknown_type
	 */
	function deleteAttachsByMessageIds($messageIds) {
		$service = $this->_serviceFactory("search");
		return $service->deleteAttachsByMessageIds($messageIds);
	}
	/*************************************************/
	/**
	 * 發送請求
	 * @param int $userId
	 * @param array $usernames
	 * @param array array('create_uid','create_username','title','content','expand') 消息體數組
	 * @param int $typeId
	 * @return $messageId 返回增加成功的消息ID
	 */
	function sendRequest($userId, $usernames, $messageInfo, $typeId) {
		$service = $this->_serviceFactory("request");
		return $service->sendRequest($userId, $usernames, $messageInfo, $typeId);
	}
	/**
	 * 獲取所有請求
	 * @param int $userId
	 * @param int $page
	 * @param int $perpage
	 * @return array
	 */
	function getAllRequests($userId, $page, $perpage) {
		$service = $this->_serviceFactory("request");
		return $service->getAllRequests($userId, $page, $perpage);
	}
	/**
	 * 獲取所有末讀請求
	 * @param int $userId
	 * @param int $page
	 * @param int $perpage
	 * @return array 
	 */
	function getRequestsNotRead($userId, $page, $perpage) {
		$service = $this->_serviceFactory("request");
		return $service->getRequestsNotRead($userId, $page, $perpage);
	}
	/**
	 * 根椐類型獲取請求
	 * @param int $userId
	 * @param int $typeId
	 * @param int $page
	 * @param int $perpage
	 * @return array 
	 */
	function getRequests($userId, $typeId, $page, $perpage) {
		$service = $this->_serviceFactory("request");
		return $service->getRequests($userId, $typeId, $page, $perpage);
	}
	/**
	 * 獲取上一條請求
	 * @param int $userId
	 * @param int $relationId
	 * @param int $typeId
	 * @return array 
	 */
	function getUpRequest($userId, $relationId, $typeId) {
		$service = $this->_serviceFactory("request");
		return $service->getUpRequest($userId, $relationId, $typeId);
	}
	/**
	 * 獲取下一條請求
	 * @param int $userId
	 * @param int $relationId
	 * @param int $typeId
	 * @return array 
	 */
	function getDownRequest($userId, $relationId, $typeId) {
		$service = $this->_serviceFactory("request");
		return $service->getDownRequest($userId, $relationId, $typeId);
	}
	/**
	 * 統計所有請求
	 * @param int $userId
	 * @return int
	 */
	function countAllRequest($userId) {
		$service = $this->_serviceFactory("request");
		return $service->countAllRequest($userId);
	}
	/**
	 * 統計末讀請求
	 * @param int $userId
	 * @return int
	 */
	function countRequestsNotRead($userId) {
		$service = $this->_serviceFactory("request");
		return $service->countRequestsNotRead($userId);
	}
	/**
	 * 統計某類請求
	 * @param int $userId
	 * @param int $typeId
	 * @return int
	 */
	function countRequest($userId, $typeId) {
		$service = $this->_serviceFactory("request");
		return $service->countRequest($userId, $typeId);
	}
	/**
	 * 刪除一條請求
	 * @param int $userId
	 * @param int $relationId
	 * @return int
	 */
	function deleteRequest($userId, $relationId) {
		$service = $this->_serviceFactory("request");
		return $service->deleteRequest($userId, $relationId);
	}
	/**
	 * 刪除多條請求
	 * @param int $userId
	 * @param int $relationIds
	 * @return int
	 */
	function deleteRequests($userId, $relationIds) {
		$service = $this->_serviceFactory("request");
		return $service->deleteRequests($userId, $relationIds);
	}
	/**
	 * 更新一條請求
	 * @param array $fieldData
	 * @param int $userId
	 * @param int $relationId
	 * @return int
	 */
	function updateRequest($fieldData, $userId, $relationId) {
		$service = $this->_serviceFactory("request");
		return $service->updateRequest($fieldData, $userId, $relationId);
	}
	/**
	 * 標記一條請求
	 * @param int $userId
	 * @param int $relationId
	 * @return bool
	 */
	function markRequest($userId, $relationId) {
		$service = $this->_serviceFactory("request");
		return $service->markRequest($userId, $relationId);
	}
	/**
	 * 標記多條請求
	 * @param int $userId
	 * @param int $relationIds
	 * @return bool
	 */
	function markRequests($userId, $relationIds) {
		$service = $this->_serviceFactory("request");
		return $service->markRequests($userId, $relationIds);
	}
	/**
	 * 獲取一條消息體
	 * @param $messageId
	 * @return bool
	 */
	function getRequest($messageId) {
		$service = $this->_serviceFactory("request");
		return $service->getRequest($messageId);
	}
	/**
	 * 讀取一條消息
	 * @param $userId
	 * @param $messageId
	 * @return array 
	 */
	function readRequests($userId, $messageId) {
		$service = $this->_serviceFactory("request");
		return $service->readRequests($userId, $messageId);
	}
	/**
	 * 忽略請求
	 * @param int $userId
	 * @param array $relationIds
	 * @return bool
	 */
	function overlookRequests($userId, $relationIds) {
		$service = $this->_serviceFactory("request");
		return $service->overlookRequest($userId, $relationIds);
	}
	/**
	 * 同意請求
	 * @param $userId
	 * @param $relationIds
	 * @return unknown_type
	 */
	function agreeRequests($userId, $relationIds) {
		$service = $this->_serviceFactory("request");
		return $service->agreeRequests($userId, $relationIds);
	}
	/*************************************************/
	/**
	 * 發送群組/用戶組消息
	 * @param int $userId
	 * @param int $groupId 群組/用戶組ID
	 * @param array array('create_uid','create_username','title','content','expand') 消息體數組
	 * @param int $type  colony/usergroup
	 * @param array $userNames 指定發送用戶`
	 * @return $messageId 返回增加成功的消息ID
	 */
	function sendGroupMessage($userId, $groupId, $messageInfo, $type = null, $userNames = array()) {
		$service = $this->_serviceFactory("groupsms");
		return $service->sendGroupMessage($userId, $groupId, $messageInfo, $type, $userNames);
	}
	/**
	 * 獲取所有群組/多人對話消息
	 * @param int $userId
	 * @param int $page
	 * @param int $perpage
	 * @return array
	 */
	function getAllGroupMessages($userId, $page, $perpage) {
		$service = $this->_serviceFactory("groupsms");
		return $service->getAllGroupMessages($userId, $page, $perpage);
	}
	/**
	 * 所有末讀群組/多人對話消息
	 * @param $userId
	 * @param $page
	 * @param $perpage
	 * @return unknown_type
	 */
	function getGroupMessagesNotRead($userId, $page, $perpage) {
		$service = $this->_serviceFactory("groupsms");
		return $service->getGroupMessagesNotRead($userId, $page, $perpage);
	}
	/**
	 * 獲取群組消息
	 * @param $userId
	 * @param $typeId
	 * @param $page
	 * @param $perpage
	 * @return unknown_type
	 */
	function getGroupMessages($userId, $typeId, $page, $perpage) {
		$service = $this->_serviceFactory("groupsms");
		return $service->getGroupMessages($userId, $typeId, $page, $perpage);
	}
	/**
	 * 獲取群組/多人對話
	 * @param $userId
	 * @param $messageId
	 * @param $relationId
	 * @return unknown_type
	 */
	function getGroupReplies($userId, $messageId, $relationId) {
		$service = $this->_serviceFactory("groupsms");
		return $service->getGroupReplies($userId, $messageId, $relationId);
	}
	/**
	 * 獲取群組/多人上一條
	 * @param $userId
	 * @param $relationId
	 * @param $typeId
	 * @return unknown_type
	 */
	function getGroupUpMessage($userId, $relationId, $typeId = null) {
		$service = $this->_serviceFactory("groupsms");
		return $service->getGroupUpMessage($userId, $relationId, $typeId);
	}
	/**
	 * 獲取群組/多人下一條
	 * @param $userId
	 * @param $relationId
	 * @param $typeId
	 * @return unknown_type
	 */
	function getGroupDownMessage($userId, $relationId, $typeId = null) {
		$service = $this->_serviceFactory("groupsms");
		return $service->getGroupDownMessage($userId, $relationId, $typeId);
	}
	/**
	 * 獲取我發送的群組/多人消息
	 * @param $userId
	 * @param $typeId
	 * @param $page
	 * @param $perpage
	 * @return unknown_type
	 */
	function getGroupMessagesBySelf($userId, $page, $perpage, $typeId = null) {
		$service = $this->_serviceFactory("groupsms");
		return $service->getGroupMessagesBySelf($userId, $typeId, $page, $perpage);
	}
	/**
	 * 獲取我收到的群組/多人消息
	 * @param $userId
	 * @param $typeId
	 * @param $page
	 * @param $perpage
	 * @return unknown_type
	 */
	function getGroupMessagesByOther($userId, $page, $perpage, $typeId = null) {
		$service = $this->_serviceFactory("groupsms");
		return $service->getGroupMessagesByOther($userId, $typeId, $page, $perpage);
	}
	/**
	 * 統計所有群組/多人消息
	 * @param $userId
	 * @return unknown_type
	 */
	function countAllGroupMessage($userId) {
		$service = $this->_serviceFactory("groupsms");
		return $service->countAllGroupMessage($userId);
	}
	/**
	 * 統計群組/多人末讀消息
	 * @param $userId
	 * @return unknown_type
	 */
	function countGroupMessagesNotRead($userId) {
		$service = $this->_serviceFactory("groupsms");
		return $service->countGroupMessagesNotRead($userId);
	}
	/**
	 * 統計群組/多人我發送的消息
	 * @param $userId
	 * @param $typeId
	 * @return unknown_type
	 */
	function countGroupMessagesBySelf($userId, $typeId = null) {
		$service = $this->_serviceFactory("groupsms");
		return $service->countGroupMessagesBySelf($userId, $typeId);
	}
	/**
	 * 統計群組/多人我接收的消息
	 * @param $userId
	 * @param $typeId
	 * @return unknown_type
	 */
	function countGroupMessagesByOther($userId, $typeId = null) {
		$service = $this->_serviceFactory("groupsms");
		return $service->countGroupMessagesByOther($userId, $typeId);
	}
	/**
	 * 統計群組/多人消息
	 * @param $userId
	 * @param $typeId
	 * @return unknown_type
	 */
	function countGroupMessage($userId, $typeId) {
		$service = $this->_serviceFactory("groupsms");
		return $service->countGroupMessage($userId, $typeId);
	}
	/**
	 * 刪除一條群組/多人消息
	 * @param $userId
	 * @param $relationId
	 * @return unknown_type
	 */
	function deleteGroupMessage($userId, $relationId) {
		$service = $this->_serviceFactory("groupsms");
		return $service->deleteGroupMessage($userId, $relationId);
	}
	/**
	 * 刪除多條群組/多人消息
	 * @param $userId
	 * @param $relationIds
	 * @return unknown_type
	 */
	function deleteGroupMessages($userId, $relationIds) {
		$service = $this->_serviceFactory("groupsms");
		return $service->deleteGroupMessages($userId, $relationIds);
	}
	/**
	 * 更新一條群組/多人消息
	 * @param $fieldData
	 * @param $userId
	 * @param $relationId
	 * @return unknown_type
	 */
	function updateGroupMessage($fieldData, $userId, $relationId) {
		$service = $this->_serviceFactory("groupsms");
		return $service->updateGroupMessage($fieldData, $userId, $relationId);
	}
	/**
	 * 標記群組/多人消息
	 * @param $userId
	 * @param $relationId
	 * @return unknown_type
	 */
	function markGroupMessage($userId, $relationId) {
		$service = $this->_serviceFactory("groupsms");
		return $service->markGroupMessage($userId, $relationId);
	}
	/**
	 * 標記多條群組/多人消息
	 * @param $userId
	 * @param $relationIds
	 * @return unknown_type
	 */
	function markGroupMessages($userId, $relationIds) {
		$service = $this->_serviceFactory("groupsms");
		return $service->markGroupMessages($userId, $relationIds);
	}
	/**
	 * 獲取一條群組/多人消息
	 * @param $messageId
	 * @return unknown_type
	 */
	function getGroupMessage($messageId) {
		$service = $this->_serviceFactory("groupsms");
		return $service->getGroupMessage($messageId);
	}
	/**
	 * 讀取一條群組/多人消息
	 * @param $userId
	 * @param $messageId
	 * @return unknown_type
	 */
	function readGroupMessages($userId, $messageId) {
		$service = $this->_serviceFactory("groupsms");
		return $service->readGroupMessages($userId, $messageId);
	}
	/**
	 * 屏蔽多人消息
	 * @param $userId
	 * @param $relationId
	 * @param $messageId
	 * @return unknown_type
	 */
	function shieldGroupMessage($userId, $relationId, $messageId) {
		$service = $this->_serviceFactory("groupsms");
		return $service->shieldGroupMessage($userId, $relationId, $messageId);
	}
	/**
	 * 恢復多人消息
	 * @param $userId
	 * @param $relationId
	 * @param $messageId
	 * @return unknown_type
	 */
	function recoverGroupMessage($userId, $relationId, $messageId) {
		$service = $this->_serviceFactory("groupsms");
		return $service->recoverGroupMessage($userId, $relationId, $messageId);
	}
	/**
	 * 啟用群組消息
	 * @param $userId
	 * @param $groupId
	 * @param $messageId
	 * @return unknown_type
	 */
	function openGroupMessage($userId, $groupId, $messageId) {
		$service = $this->_serviceFactory("groupsms");
		return $service->openGroupMessage($userId, $groupId, $messageId);
	}
	/**
	 * 關閉群組消息
	 * @param $userId
	 * @param $groupId
	 * @param $messageId
	 * @return unknown_type
	 */
	function closeGroupMessage($userId, $groupId, $messageId) {
		$service = $this->_serviceFactory("groupsms");
		return $service->closeGroupMessage($userId, $groupId, $messageId);
	}
	/*************************************************/
	/**
	 * 根椐類型名稱獲取類型值
	 * @param $typeName
	 * @return unknown_type
	 */
	function getConst($typeName) {
		$service = $this->_serviceFactory("default");
		return $service->getConst($typeName);
	}
	/**
	 * 根椐類型ID獲取類型名稱
	 * @return unknown_type
	 */
	function getReverseConst($id) {
		$service = $this->_serviceFactory("default");
		return $service->getReverseConst($id);
	}
	/**
	 * 獲取用戶屏蔽群組消單
	 * @param $userId
	 * @return array 屏蔽的群組ID數組
	 */
	function getBlackColony($userId) {
		$service = $this->_serviceFactory("default");
		return $service->getBlackColony($userId);
	}
	/**
	 * 設置消息中心配置
	 * @param $userId
	 * @param array $fieldData
	 * @return unknown_type
	 */
	function setMsConfig($fieldData, $userId) {
		$service = $this->_serviceFactory("default");
		return $service->setMsConfig($fieldData, $userId);
	}
	/**
	 * 獲取消息中心配置
	 * @param $userId
	 * @param $mKey
	 * @return unknown_type
	 */
	function getMsConfig($userId, $mKey) {
		$service = $this->_serviceFactory("default");
		return $service->getMsConfig($userId, $mKey);
	}
	/**
	 * 根椐鍵名獲取鍵值
	 * @return unknown_type
	 */
	function getMsKey($key) {
		$service = $this->_serviceFactory("default");
		return $service->getMsKey($key);
	}
	/**
	 * 根椐用戶名獲取所有用戶配置
	 * @param $userId
	 * @return unknown_type
	 */
	function getMsConfigs($userId) {
		$service = $this->_serviceFactory("default");
		return $service->getMsConfigs($userId);
	}
	/**
	 * 獲取默認屏蔽消息
	 * @param array $app_array
	 * @return unknown_type
	 */
	function getDefaultShields($app_array = array()) {
		$service = $this->_serviceFactory("default");
		return $service->setDefaultShield($app_array);
	}
	/**
	 * 獲取某個用戶的屏蔽信息
	 * @param $userId
	 * @param $key
	 * @param array $app_array
	 * @return unknown_type
	 */
	function getMessageShield($userId, $key, $app_array = array()) {
		$service = $this->_serviceFactory("default");
		return $service->getMessageShield($userId, $key, $app_array);
	}
	/**
	 * 根椐用戶名獲取某個用戶的屏蔽信息 
	 * @param $userName
	 * @param $key
	 * @param $app_array
	 * @return unknown_type
	 */
	function getMessageShieldByUserName($userName, $key, $app_array = array()) {
		$service = $this->_serviceFactory("default");
		return $service->getMessageShieldByUserName($userName, $key, $app_array);
	}
	/****************************************************/
	/**
	 * 根椐好友名字與類型搜索信息
	 * @param int $userId  誰搜索 
	 * @param string $userName  搜索誰
	 * @param int $type
	 * @param int $page
	 * @param int $perpage
	 * @return array
	 */
	function searchMessages($userId, $userName, $type = null, $page, $perpage) {
		$service = $this->_serviceFactory("search");
		return $service->searchMessages($userId, $userName, $type, $page, $perpage);
	}
	/**
	 * TODO 後台消息管理接口 刪除
	 * @param $keyWords  關鍵字
	 * @param $startTime 起始時間
	 * @param $endTime   結束時間
	 * @param $sender    發送者
	 * @param $isDelete  是否直接刪除
	 * @param $page      頁數
	 * @param $perpage   每頁數
	 * @return array(總頁數,消息數組)
	 */
	function manageMessage($keyWords = null, $startTime = null, $endTime = null, $sender = null, $isDelete = 0, $page = 1, $perpage = 30) {
		$service = $this->_serviceFactory("search");
		return $service->manageMessage($keyWords, $startTime, $endTime, $sender, $isDelete, $page, $perpage);
	}
	/**
	 * TODO 後台消息管理接口 刪除
	 * @param $category 大類名稱
	 * @param $unRead   是否保留末讀
	 * @param $isDelete 是否直接刪除
	 * @return array(總頁數,消息數組)
	 */
	function manageMessageWithCategory($category, $unRead = 0, $isDelete = 0, $page = 1, $perpage = 30) {
		$service = $this->_serviceFactory("search");
		return $service->manageMessageWithCategory($category, $unRead, $isDelete, $page, $perpage);
	}
	/**
	 * TODO 後台消息管理接口 刪除
	 * @param $messageIds
	 * @return unknown_type
	 */
	function manageMessageWithMessageIds($messageIds) {
		$service = $this->_serviceFactory("search");
		return $service->manageMessageWithMessageIds($messageIds);
	}
	/*******************************************/
	/**
	 * 根椐用戶ID獲取歷史消息
	 * @param $userId
	 * @param $page
	 * @param $perpage
	 * @return unknown_type
	 */
	function getHistoryMessages($userId, $page, $perpage) {
		$service = $this->_serviceFactory("history");
		return $service->getHistoryMessages($userId, $page, $perpage);
	}
	/**
	 * 根椐用戶ID統計歷史消息
	 * @param $userId
	 * @return unknown_type
	 */
	function countHistoryMessage($userId) {
		$service = $this->_serviceFactory("history");
		return $service->countHistoryMessage($userId);
	}
	/**
	 * 批量刪除歷史消消息
	 * @param int $userId
	 * @param array $relationIds
	 * @return unknown_type
	 */
	function deleteHistoryMessages($userId, $relationIds) {
		$service = $this->_serviceFactory("history");
		return $service->deleteHistoryMessages($userId, $relationIds);
	}
	/**
	 * 獲取一條歷史消息詳細信息
	 * @param $messageId
	 * @return unknown_type
	 */
	function getHistoryMessage($messageId) {
		$service = $this->_serviceFactory("history");
		return $service->getHistoryMessage($messageId);
	}
	/**
	 * 獲取回復歷史消息
	 * @param $userId
	 * @param $messageId
	 * @param $relationId
	 * @return unknown_type
	 */
	function getHistoryReplies($userId, $messageId, $relationId) {
		$service = $this->_serviceFactory("history");
		return $service->getHistoryReplies($userId, $messageId, $relationId);
	}
	/**
	 * 把消息轉化為歷史時間段
	 * @param $timeSegment 時間段  unix 時間戳
	 * @return unknown_type
	 */
	function setHistorys($timeSegment) {
		$service = $this->_serviceFactory("history");
		return $service->setHistorys($timeSegment);
	}
	/**
	 * 重置用戶統計數
	 * @param array $userIds
	 * @param string $mKey
	 * @param $mValue
	 * @return unknown_type
	 */
	function resetStatistics($userIds, $mKey) {
		$service = $this->_serviceFactory("default");
		return $service->resetStatistics($userIds, $mKey);
	}
	/**
	 * 根椐用戶ID獲取用戶統計信息
	 * @param $userId
	 * @return array('站內信數','通知數','請求數','群消息數')
	 */
	function getUserStatistics($userId) {
		$service = $this->_serviceFactory("default");
		return $service->getUserStatistics($userId);
	}
	/**
	 * 根椐用戶ID獲取用戶特殊信息
	 * @param $userId
	 * @return unknown_type
	 */
	function getUserSpecialStatistics($userId) {
		$service = $this->_serviceFactory("default");
		return $service->getUserSpecialStatistics($userId);
	}
	/*****************************************************/
	/**
	 * 抓取用戶組信息 系統通知
	 * @param int $userId
	 * @param array $groupIds
	 * @param int $lastgrab
	 * @return unknown_type
	 */
	function grabMessage($userId, $groupIds, $lastgrab) {
		$service = $this->_serviceFactory("notice");
		return $service->grabMessage($userId, $groupIds, $lastgrab);
	}
	/**
	 * 創建消息任務 用戶組信息
	 * @param array $groupIds   用戶組數組
	 * @param array $messageinfo array('create_uid','create_username','title','content','expand') 消息體數組
	 * @return bool
	 */
	function createMessageTasks($groupIds, $messageInfo) {
		$service = $this->_serviceFactory("notice");
		return $service->createMessageTasks($groupIds, $messageInfo);
	}
	/**
	 * 給在線用戶發送消息函數
	 * @param array $onlineUserIds 在線用戶
	 * @param array $messageinfo array('create_uid','create_username','title','content','expand') 消息體數組
	 * @return unknown_type
	 */
	function sendOnlineMessages($onlineUserIds, $messageInfo) {
		$service = $this->_serviceFactory("notice");
		return $service->sendOnlineMessages($onlineUserIds, $messageInfo);
	}
	
	function sendTaskMessages($userIds, $messageInfo, $messageId = null) {
		$service = $this->_serviceFactory("task");
		return $service->sendTaskMessages($userIds, $messageInfo, $messageId);
	}
	/**
	 * 私有加載消息中心服務入口
	 * @param $name
	 * @return unknown_type
	 */
	function _serviceFactory($name) {
		static $classes = array();
		$name = strtolower($name);
		$filename = R_P . "lib/message/message/" . $name . ".ms.php";
		if (!is_file($filename)) {
			return null;
		}
		$class = 'MS_' . ucfirst($name);
		if (isset($classes[$class])) {
			return $classes[$class];
		}
		if (!class_exists('MS_BASE')) require (R_P . 'lib/message/message/base.ms.php');
		if (!class_exists($class)) include S::escapePath($filename);
		$classes[$class] = new $class();
		return $classes[$class];
	}
	
	function matchTidByConetnt($content) {
		if (strrpos($content,'read.php?tid=') === false) return false;
		preg_match_all('/read.php\?tid=(.*)\]/U', $content, $matches);
		$tid = intval($matches[1][0]);
		return $tid ? $tid : false;
	}
	
	function countAllByUserId($userId) {
		$userId = intval($userId);
		if ($userId < 1) return array();
		$relationsDb = L::loadDB('ms_relations', 'message');
		foreach ($relationsDb->countAllByUserId($userId) as $k=>$v) {
			$result[$k] = $v['total'];
		}
		return $result;
	}
}

