<?php
!function_exists('readover') && exit('Forbidden');
/**
 * 消息中心默認服務層/配置/歷史消息/搜索等服務
 * @2010-4-6 liuhui
 */
class MS_Default extends MS_Base {
	
	function getConst($typeName){
		return $this->getMap($typeName);
	}
	function getReverseConst($id){
		$maps = array_flip($this->maps());
		return isset($maps[$id]) ? $maps[$id] : '';
	}
	function getBlackColony($userId){
		$userId = intval($userId);
		if( 1 > $userId ) return false;
		if(!($config =  $this->getMsConfig($userId,$this->_c_blackcolony))){
			return false;
		}
		return $config ? unserialize($config) : false;
	}
	function getMsConfigs($userId){
		$userId = intval($userId);
		if( 1 > $userId ) return false;
		$configsDao = $this->getConfigsDao();
		return $configsDao->get($userId);
	}
	function setMsConfig($fieldData,$userId){
		$userId = intval($userId);
		if( 1 > $userId || !$fieldData ) return false;
		return $this->_setMsConfig($fieldData,$userId);
	}
	function getMsConfig($userId,$mKey){
		$userId = intval($userId);
		if( 1 > $userId || !$mKey ) return false;
		return $this->_getMsConfig($userId,$mKey);
	}
	/**
	 * 設置基礎消息中心鍵值
	 * @return unknown_type
	 */
	function setMsKeys(){
		$msConfigs = array(  );
		return array_merge($msConfigs,$this->_msConfigs());
	}
	function getMsKey($key){
		$configs = $this->setMsKeys();
		return in_array($key,$configs) ? $key : '';
	}
	function getUserStatistics($userId){
		$configsDao = $this->getConfigsDao();
		$config = $configsDao->get($userId);
		if(!$config) return array(0,0,0,0);
		return array($config[$this->_c_sms_num],$config[$this->_c_notice_num],$config[$this->_c_request_num],$config[$this->_c_groupsms_num]);
	}
	function getUserSpecialStatistics($userId){
		$configsDao = $this->getConfigsDao();
		$config = $configsDao->get($userId);
		if(!$config) return array(0,0,0,0);
		return array( $this->_sms     => $config[$this->_c_sms_num],
					  $this->_notice   => $config[$this->_c_notice_num],
					  $this->_request  => $config[$this->_c_request_num],
					  $this->_groupsms => $config[$this->_c_groupsms_num]
		);
	}
	
	function resetStatistics($userIds,$mKey){
		if( !$userIds || "" == $mKey ) return false;
		$configsDao = $this->getConfigsDao();
		switch ($mKey){
			case $this->_c_sms_num :
				$fieldData = array($this->_c_sms_num      => 0 );
				break;
			case $this->_c_notice_num :
				$fieldData = array($this->_c_notice_num   => 0 );
				break;
			case $this->_c_request_num :
				$fieldData = array($this->_c_request_num  => 0 );
				break;
			case $this->_c_groupsms_num :
				$fieldData = array($this->_c_groupsms_num => 0 );
				break;
			default:
				break;
		}
		return $fieldData ? $configsDao->updateByUserIds($fieldData,$userIds) : false;
	}
	function setDefaultShield($app_array = array()){
		$default = array(
				    //'sms_message'       	 => 1,//留言
				    //'sms_comment_write' 	 => 1,//評論記錄
				    //'sms_comment_diary' 	 => 1,//評論日誌
				    //'sms_comment_photo' 	 => 1,//照片相冊
					//'sms_comment_share' 	 => 1,//分享相冊
				    'sms_share_diary' 		 => 1,//日誌分享
				    'sms_share_photo' 		 => 1,//照片分享
				    'sms_share_post' 		 => 1,//帖子分享
				    'sms_share_group' 		 => 1,//群組分享
				    'sms_share_video' 		 => 1,//視頻分享
				    'sms_share_music' 		 => 1,//音樂分享
				    'sms_share_link' 		 => 1,//鏈接分享
				    'sms_ratescore'          => 1,//評分
					'sms_reply'              => 1,//帖子回復
				    'notice_postcate'        => 1,//團購通知
				    'notice_active'          => 1,//活動通知
					'notice_apps'            => 1,//應用通知
				    $this->_s_notice_system  => 1,//系統通知
				    //$this->_notice_comment   => 1,//評論通知
				    'notice_comment_write' 	 => 1,//評論記錄
				    'notice_comment_diary' 	 => 1,//評論日誌
				    'notice_comment_photo' 	 => 1,//照片相冊
					'notice_comment_share' 	 => 1,//分享相冊
				    $this->_notice_guestbook => 1,//留言通知
				    'request_friend' 		 => 1,//好友請求
				    'request_group' 		 => 1,//群組請求
				    'request_apps' 			 => 1,//應用請求
		);
		if(!empty($app_array)){
		 	foreach($app_array as $key=>$value){
		 		$default['notice_app_'.$value['appid']] = 1;
		 	}
		}
		return $default;
	}
	function getMessageShield($userId,$key,$app_array=array()){
		$defaultShield = $this->setDefaultShield($app_array);
		$shieldinfo    = $this->getMsConfig($userId,'shieldinfo');
		$newShield     = $shieldinfo ? array_merge($defaultShield,unserialize($shieldinfo)) : $defaultShield;
		return in_array($key,array_keys($newShield)) ? $newShield[$key] : '';		
	}
	function getMessageShieldByUserName($userName,$key,$app_array=array()){
		$userService = $this->_getUserService();
		$member = $userService->getByUserName($userName);
		if (!$member) return false;
		
		return $this->getMessageShield($member['uid'],$key,$app_array);
	}
}