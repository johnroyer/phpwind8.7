<?php
! defined ( 'P_W' ) && exit ( 'Forbidden' );
/**
 * 通用聚合服務
 */
class GatherInfo_General_Service {

	/**
	 * 該帖子的內容改變了，需要更新帖子緩存
	 *
	 * @param array $information 格式array('tid'=>$tids)
	 * @return boolean
	 */
	function changeThreadWithThreadIds($information) {
		if (!Perf::checkMemcache() || !isset($information['tid'])) return true;
		$threadIds = is_array ( $information ['tid'] ) ? $information ['tid'] : array ($information ['tid'] );
		$_cacheService = Perf::gathercache ( 'pw_threads' );
		$_cacheService->clearCacheForTmsgByThreadIds ( $threadIds );
		return $_cacheService->clearCacheForThreadByThreadIds ( $threadIds );
	}
	
	/**
	 * 有些查詢語句無法改造成結構化查詢，所以通過gatherinfo收集
	 *
	 * @param array $information
	 * @return boolean
	 */
	function changeThreads($information) {
		return Perf::gatherQuery('update', array('pw_threads'), $information);
	}
	
	/**
	 * 有些查詢語句無法改造成結構化查詢，所以通過gatherinfo收集
	 *
	 * @param array $information
	 * @return boolean
	 */
	function changePosts($information) {
		return Perf::gatherQuery('update', array($information['_tablename']), $information);
	}
	
	function deletePosts($information){
		return Perf::gatherQuery('delete', array($information['_tablename']), $information);
	}

	/**
	 * 帖子詳細信息改變時，需要更新緩存
	 *
	 * @param array $information
	 * @return boolean
	 */
	function changeTmsgWithThreadIds($information){
		if (!Perf::checkMemcache() || !isset($information['tid'])) return true;
		$threadIds = is_array ( $information ['tid'] ) ? $information ['tid'] : array ($information ['tid'] );
		$_cacheService = Perf::gathercache ( 'pw_threads' );
		return $_cacheService->clearCacheForTmsgByThreadIds ( $threadIds );
	}

	/**
	 * 該板塊改變， 需要更新帖子列表緩存, 格式array('fid'=>$fids)
	 *
	 * @param array $information
	 * @return boolean
	 */
	function changeThreadWithForumIds($information){
		if (!Perf::checkMemcache() || !isset($information['fid'])) return true;
		$forumIds = is_array ( $information ['fid'] ) ? $information ['fid'] : array ($information ['fid'] );
		$_cacheService = Perf::gathercache ( 'pw_threads' );
		return $_cacheService->clearCacheForThreadListByForumIds ( $forumIds );
	}

	/**
	 * 帖子更新時，需要清理帖子列表 ， 在lib/forum/postmodify.class.php(363)調用
	 *
	 * @param array $information  
	 * @return boolean
	 */
	function changeThreadListWithThreadIds($information){
		if (!Perf::checkMemcache() || !isset($information['tid'])) return true;
		$threadIds = is_array ( $information ['tid'] ) ? $information ['tid'] : array ($information ['tid'] );
		$_cacheService = Perf::gathercache ( 'pw_threads' );
		$threads = $_cacheService->getThreadsByThreadIds($threadIds);
		if (is_array($threads)){
			$fid = array();
			foreach ($threads as $thread){
				$fid[] = $thread['fid'];
			}
			$fid && $_cacheService->clearCacheForThreadListByForumIds ( $fid );
		}
		return true;
	}

	/**
	 * 更新用戶基本信息時，清除相應緩存
	 *
	 * @param array $information 格式array('uid'=>$uids)
	 * @return boolean
	 */
	function changeMembersWithUserIds($information){
		if (!isset($information['uid'])) return true;
		$userIds = is_array ( $information ['uid'] ) ? $information ['uid'] : array ($information ['uid'] );
		if (Perf::checkMemcache()){
			$_cacheService = Perf::gathercache ( 'pw_members' );
			return $_cacheService->clearCacheForMembersByUserIds( $userIds );
			//$_cacheService->clearCacheForMemberDataByUserIds( $userIds );
			//return $_cacheService->clearCacheForMemberInfoByUserIds( $userIds );
		}else {
			$_cacheService = Perf::gatherCache('pw_membersdbcache');
			return $_cacheService->clearMembersDbCacheByUserIds( $userIds );			
		}
	}

	/**
	 * 更新用戶Data信息時，清除相應緩存
	 *
	 * @param array $information 格式array('uid'=>$uids)
	 * @return boolean
	 */
	function changeMemberDataWithUserIds($information){
		if (!Perf::checkMemcache() || !isset($information['uid'])) return true;
		$userIds = is_array ( $information ['uid'] ) ? $information ['uid'] : array ($information ['uid'] );
		$_cacheService = Perf::gathercache ( 'pw_members' );
		return $_cacheService->clearCacheForMemberDataByUserIds( $userIds );
	}

	/**
	 * 更新用戶Info信息時，清除相應緩存
	 *
	 * @param array $information 格式array('uid'=>$uids)
	 * @return boolean
	 */
	function changeMemberInfoWithUserIds($information){
		if (!Perf::checkMemcache() || !isset($information['uid'])) return true;
		$userIds = is_array ( $information ['uid'] ) ? $information ['uid'] : array ($information ['uid'] );
		$_cacheService = Perf::gathercache ( 'pw_members' );
		return $_cacheService->clearCacheForMemberInfoByUserIds( $userIds );
	}

	/**
	 * 更新用戶SingleRight信息時，清除相應緩存
	 *
	 * @param array $information
	 * @return boolean
	 */
	function changeSingleRightWithUserIds($information){
		if (!Perf::checkMemcache() || !isset($information['uid'])) return true;
		$userIds = is_array ( $information ['uid'] ) ? $information ['uid'] : array ($information ['uid'] );
		$_cacheService = Perf::gathercache ( 'pw_members' );
		return $_cacheService->clearCacheForSingleRightByUserIds( $userIds );
	}

	/**
	 * 更新用戶MemberCredit信息時，清除相應緩存
	 *
	 * @param array $information
	 * @return boolean
	 */
	function changeMemberCreditWithUserIds($information){
		if (!isset($information['uid'])) return true;
		$userIds = is_array ( $information ['uid'] ) ? $information ['uid'] : array ($information ['uid'] );
		if (Perf::checkMemcache()){
			$_cacheService = Perf::gathercache ( 'pw_members' );
			return $_cacheService->clearCacheForMemberCreditByUserIds( $userIds );			
		}else{
			$_cacheService = Perf::gatherCache('pw_membersdbcache');
			return $_cacheService->clearCreditDbCacheByUserIds( $userIds );				
		}		
	}

	/**
	 * 更新用戶群組信息（CmemberAndColony）時，清除相應緩存 
	 *
	 * @param array $information
	 * @return boolean
	 */
	function changeCmemberAndColonyWithUserIds($information){
		if (!isset($information['uid'])) return true;
		$userIds = is_array ( $information ['uid'] ) ? $information ['uid'] : array ($information ['uid'] );
		if (Perf::checkMemcache()){
			$_cacheService = Perf::gathercache ( 'pw_members' );
			return $_cacheService->clearCacheForCmemberAndColonyByUserIds( $userIds );			
		}else{
			$_cacheService = Perf::gatherCache('pw_membersdbcache');
			return $_cacheService->clearColonyDbCacheByUserIds( $userIds );				
		}
	}
	
	/**
	 * 更新用戶群組信息（CmemberAndColony）時，清除相應緩存 
	 *
	 * @param array $information
	 * @return boolean
	 */
	/**
	function changeALLMembers($information = null){
		if (!Perf::checkMemcache() || !isset($information['uid'])) return true;
		$userIds = is_array ( $information ['uid'] ) ? $information ['uid'] : array ($information ['uid'] );
		$_cacheService = Perf::gathercache ( 'pw_members' );
		return $_cacheService->clearCacheForCmemberAndColonyByUserIds( $userIds );
	}
	**/
	
	/**
	 * 有些查詢語句無法改造成結構化查詢，所以通過gatherinfo收集
	 *
	 * @param array $information
	 * @return boolean
	 */
	function changeForumData($information){
		return Perf::gatherQuery('update', array('pw_forumdata'), $information);
	}
	
	/**
	 * 當發帖和發回復時，從memcache讀取緩存數據然後更新它，而不是直接刪除緩存
	 *
	 * @param array $information
	 * @return boolean
	 */
	function changeForumDataWithForumId($information = null){
		if (!Perf::checkMemcache()) return true;
		if (!S::isArray($information) || !($information = current($information)) || !isset($information['fid'])) return false;
		$fid = intval($information['fid']);
		$_cacheService = Perf::getCacheService();
		$_cacheInfo = $_cacheService->get(array('all_forums_info', 'forumdata_announce_' . $fid));
		$_unique = $GLOBALS['db_memcache']['hash'];
		// 更新index頁面裡版塊緩存
		if (isset($_cacheInfo[$_unique . 'all_forums_info'])){
			$allForums = $_cacheInfo[$_unique . 'all_forums_info'];
			foreach ($information as $key => $value){
				if (in_array($key, array('article', 'topic','tpost','subtopic'))){
					$allForums[$fid][$key] = $allForums[$fid][$key] + $value;
				}else {
					$allForums[$fid][$key] = $value;
				}
			}
			$_cacheService->set('all_forums_info', $allForums, 300);
		}
		// 更新thread頁面裡版塊和通告緩存
		if (isset($_cacheInfo[$_unique . 'forumdata_announce_' . $fid])){
			$forums = $_cacheInfo[$_unique . 'forumdata_announce_' . $fid];
			foreach ($information as $key => $value){
				if (in_array($key, array('article', 'topic','tpost','subtopic'))){
					$forums[$key] = $forums[$key] + $value;
				}else {
					$forums[$key] = $value;
				}
			}
			$_cacheService->set('forumdata_announce_' . $fid, $forums, 300);			
		}
		return true;
	}
}