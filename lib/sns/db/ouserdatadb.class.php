<?php
/**
 * 記錄數據庫操作對像
 * 
 * @package Write
 */

!defined('P_W') && exit('Forbidden');

/**
 * 用戶相關數據庫操作對像
 * 
 * @package Ouserdata 
 */
class PW_OuserdataDB extends BaseDB {

	var $_tableName = "pw_ouserdata";
	
	/**
	 * 根據用戶範圍，篩選記錄權限
	 * 
	 * @param array		$userIds		array(1,2,3,4,......n)	
	 * @return array 	
	 */ 
	function findUserOwritePrivacy($userIds) {
		$qurey = $this->_db->query(" SELECT * FROM ".$this->_tableName . 
				" WHERE uid " . $this->_parseSqlIn($userIds) . " AND owrite_privacy !=2");
		return $this->_getAllResultFromQuery($qurey);
	}

	function findUserAtPrivacy($userIds) {
		$qurey = $this->_db->query(" SELECT uid FROM ".$this->_tableName . 
				" WHERE uid " . $this->_parseSqlIn($userIds));
		return $this->_getAllResultFromQuery($qurey);
	}
	
	function findUserPhotoPrivacy($userIds) {
		if(empty($userIds) || !is_array($userIds)){
			return array();
		}
		$qurey = $this->_db->query(" SELECT photos_privacy,uid FROM ".$this->_tableName . 
				" WHERE uid " . $this->_parseSqlIn($userIds) . " AND owrite_privacy !=2");
		return $this->_getAllResultFromQuery($qurey);
	}
	
	/**
	 * 根據用戶id，解析in 查詢
	 * 
	 * @param $userIds
	 */
	function _parseSqlIn($userIds) {
		return "IN (" . S::sqlImplode($userIds) . ")";
	}
	
	/**
	 * 根據用戶id，取得用戶相關信息
	 * 
	 * @param $userId
	 */
	function get($userId) {
		static $sArray = array();
		if (!isset($sArray[$userId])) {
			$sArray[$userId] = $this->_get($userId);
		}
		return $sArray[$userId];
	}

	function _get($userId) {
		return $this->_db->get_one( "SELECT * FROM " . $this->_tableName . " WHERE uid=" . $this->_addSlashes($userId));
	}
}