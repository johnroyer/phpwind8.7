<?php

!defined('P_W') && exit('Forbidden');

/**
 * 話題功能數據庫DAO服務
 * @package PW_TopicDB
 */
class PW_Weibo_TopicAttentionsDB extends BaseDB{
	
	var $_tableName = 'pw_weibo_topicattention';
	
	/**
	 * 
	 * 添加用戶關注的話題
	 * @param int $topicId
	 * @param int $userid
	 * @return boolean
	 */
	function addAttentionTopic($topicId,$userid){
		$userid = intval($userid);
		$topicId = intval($topicId);
		if(!$userid || !$topicId) return false;
		$fields = array('topicid'=>$topicId,'userid'=>$userid,'crtime'=>$GLOBALS['timestamp']);
		pwQuery::replace($this->_tableName, $fields);
		return true;
	}
	
	/**
	 * 
	 * 刪除用戶關注的話題
	 * @param int $topicId
	 * @param int $userid
	 * @return boolean
	 */
	function deleteAttentionedTopic($topicId,$userid){
		$userid = intval($userid);
		$topicId = intval($topicId);
		if(!$userid || !$topicId) return false;
		pwQuery::delete($this->_tableName, "userid=:userid and topicid=:topicid", array($userid,$topicId));
		return true;
	}
	
	/**
	 * 
	 * 檢查是否已經關注某話題
	 * @param int $topicId
	 * @param int $userid
	 * @return array
	 */
	function getOneAttentionedTopic($topicId,$userid) {
		$userid = intval($userid);
		$topicId = intval($topicId);
		if(!$userid || !$topicId) return array();
		return $this->_db->get_one('SELECT userid,topicid FROM ' . $this->_tableName . ' WHERE userid = ' . $this->_addSlashes($userid) . ' AND  topicid = ' . $this->_addSlashes($topicId));
	}
	
	/**
	 * 
	 * 檢查是否已經關注某些話題
	 * @param int $topicIds
	 * @param int $userid
	 * @return array
	 */
	function getAttentionedTopicByTopicIds($topicIds,$userid) {
		if(!$userid && !S::isArray($topicIds)) return array(); 
		$query = $this->_db->query('SELECT userid,topicid FROM ' . $this->_tableName . ' WHERE userid = ' . $this->_addSlashes($userid) . ' AND topicid IN (' . S::sqlImplode($topicIds) .')');
		return $this->_getAllResultFromQuery($query,'topicid');
	}
	
	function getUserAttentionTopicNum($uid){
		$uid = intval($uid);
		return $this->_db->get_value('SELECT COUNT(*) FROM ' . $this->_tableName . ' WHERE userid = ' . $uid);
	}
}
?>