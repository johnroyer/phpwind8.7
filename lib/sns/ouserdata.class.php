<?php 
/**
 * 用戶相關服務類文件
 * 
 * @package Ouserdata
 */
!defined('P_W') && exit('Forbidden');

/**
 * 用戶相關服務對像
 * 
 * @package Ouserdata
 */
class PW_Ouserdata {
	
	function get($userId) {
		$ouserdataDb = $this->_getOuserdataDB();
		return 	$ouserdataDb->get($userId);	
		
	}

	/**
	 * 根據用戶範圍，篩選記錄權限
	 * 
	 * @param array		$userIds		array(1,2,3,4,......n)	
	 * @return array 	
	 */ 
	function findUserOwritePrivacy($userIds) {
		$ouserdataDb = $this->_getOuserdataDB();
		return 	$ouserdataDb->findUserOwritePrivacy($userIds);	
	}
	
	
	/**
	 * Get PW_OuserdataDB
	 * 
	 * @access protected
	 * @return PW_OuserdataDB
	 */
	function _getOuserdataDB() {
		return L::loadDB('Ouserdata', 'sns');
	}	
	
	
}


?>