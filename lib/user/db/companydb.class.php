<?php 
!defined('P_W') && exit('Forbidden');

/**
 * 公司DAO
 * @package PW_SchoolDB
 */
class PW_CompanyDB extends BaseDB{
	
	var $_tableName = 'pw_company';
	var $_primaryKey = 'companyid';

	/**
	 * 添加一個公司
	 * @param string $companyName 公司名稱
	 * @return int $companyId
	 */
	function addCompany($companyName){
		if (!$companyName) return false;
		$companyName = trim($companyName);
		$tmpCompanyId = $this->_db->get_value("SELECT `companyid` FROM $this->_tableName WHERE companyname =" . S::sqlEscape($companyName)."");
		if($tmpCompanyId > 0) return $tmpCompanyId;
		return $this->_insert(array('companyname' => $companyName));
	}
	
	/**
	 * 根據公司ID獲取公司名稱
	 * @param int $companyId
	 * @return array
	 */
	function getCompanyNameById($companyId){
		$companyId = intval($companyId);
		if($companyId < 1) continue;
		$companyData = $this->_get($companyId);
		return $companyData['companyname'];
	}
	
	/**
	 * 根據公司ID批量獲取公司名稱
	 * @param int $companyIds
	 * @return array
	 */
	function getCompanyNameByIds($companyIds){
		if(!S::isArray($companyIds)) return false;
		$query = $this->_db->query("SELECT companyname FROM  $this->_tableName WHERE companyid in (" . S::sqlImplode($companyIds) . ")");
		return $this->_getAllResultFromQuery($query);
	}
	
	/**
	 * 根據公司名稱獲取公司數據
	 * @param int $companyNames
	 * @return array
	 */
	function getCompanyByNames($companyNames){
		if(!S::isArray($companyNames)) return false;
		$query = $this->_db->query("SELECT * FROM  $this->_tableName WHERE companyname in (" . S::sqlImplode($companyNames) . ")");
		return $this->_getAllResultFromQuery($query,'companyid');
	}
	
	/**
	 * 根據公司名稱獲取公司ID
	 * @param string $companyName
	 * @return array
	 */
	function getCompanyIdByName($companyName){
		if (!$companyName) return array();
		return $this->_db->get_value("SELECT companyid FROM  $this->_tableName WHERE companyname = " . S::sqlEscape($companyName) . "");
	}
	
	/**
	 * 根據公司名稱批量獲取公司ID
	 * @param string $companyNames
	 * @return array
	 */
	function getCompanyIdsByName($companyNames){
		if (!S::isArray($companyNames)) return array();
		$query = $this->_db->query("SELECT companyid FROM  $this->_tableName WHERE companyname in (" . S::sqlImplode($companyNames) . ")");
		return $this->_getAllResultFromQuery($query);
	}
	
	/**
	 * 編輯公司名稱
	 * @param int $companyId 公司Id
	 * @param string $companyName 公司名稱
	 * @return bool
	 */
	function editCompany($companyId,$companyName){
		if (!$companyId || !$companyName) return false;
		return pwQuery::update($this->_tableName, "companyid=:companyid", array($companyId), array('companyname'=>$companyName));
	}
	
	/**
	 * 根據公司id刪除記錄
	 * @param int $companyId 工作公司id
	 * @return bool
	 */
	function deleteCompany($companyId){
		if(!$companyId) return false;
		return pwQuery::delete($this->_tableName, 'companyid=:companyid', array($companyId));
	}
}