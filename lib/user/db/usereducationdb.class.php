<?php 
!defined('P_W') && exit('Forbidden');

/**
 * 學習經歷DAO
 * @package PW_UserEducationDB
 */
class PW_UserEducationDB extends BaseDB{
	
	var $_tableName = 'pw_user_education';
	var $_schoolTable = 'pw_school';
	var $_primaryKey = 'educationid';
	
	/**
	 * 添加教育經歷
	 * @param array $data
	 * @return bool
	 */
	function addEducation($data){
		if (!S::isArray($data)) return false;
		return $this->_insert($data);
	}
	
	/**
	 * 批量添加教育經歷
	 * @param array $data
	 * @return bool
	 */
	function addEducations($data){
		if (!S::isArray($data)) return false;
		return $this->_db->update("INSERT INTO " . $this->_tableName . "(uid,schoolid,educationlevel,starttime) VALUES " . S::sqlMulti($data));
	}
	
	/**
	 * 根據用戶ID獲取教育經歷
	 * @param int uid
	 * @return array
	 */
	function getEducations($uid){
		if(!$uid) return array();
		$query = $this->_db->query("SELECT ue.*, s.schoolname FROM  $this->_tableName ue LEFT JOIN $this->_schoolTable s USING(schoolid) WHERE ue.uid = " . S::sqlEscape($uid) . " ORDER BY educationid ASC");
		return $this->_getAllResultFromQuery($query,'educationid');
	}
	
	/**
	 * 根據學校或學院ID獲取用戶ID
	 * @param int schoolId
	 * @return array
	 */
	function getUserId($schoolId){
		if(!$schoolId) return array();
		$query = $this->_db->query("SELECT uid FROM  $this->_tableName WHERE schoolid = " . S::sqlEscape($schoolId) . "");
		return $this->_getAllResultFromQuery($query,'uid');
	}
	
	/**
	 * 根據教育經歷ID獲取一條教育經歷
	 * @param int educationId
	 * @return array
	 */
	function getEducation($educationId){
		if(!$educationId) return array();
		return $this->_get($educationId);
	}
	
	/**
	 * 編輯一條教育經歷
	 * @param int $uid 用戶id
	 * @param int $schoolId 學校Id
	 * @param int $startTime 入學年份
	 * @return bool
	 */
	function editEducation($educationId,$educationLevel,$schoolId,$startTime){
		if (!$educationId || !$educationLevel || !$schoolId || !$startTime) return false;
		return pwQuery::update($this->_tableName, "educationid=:educationid", array($educationId), array('schoolid'=>$schoolId,'educationlevel'=>$educationLevel,'starttime'=>$startTime));
	}
	
	/**
	 * 根據學校ID刪除數據
	 * @param int schoolId 學校id
	 * @return bool
	 */
	function deleteEduBySchoolId($schoolId){
		if(!$schoolId) return false;
		return pwQuery::delete($this->_tableName, 'schoolid=:schoolid', array($schoolId));
	}
	
	/**
	 * 根據學校ID刪除多條數據
	 * @param int schoolIds 學校id
	 * @return bool
	 */	
	function deleteEduBySchoolIds($schoolIds){
		if(!S::isArray($schoolIds)) return false;
		return pwQuery::delete($this->_tableName, 'schoolid IN (:schoolid)', array($schoolIds));
	}
	
	/**
	 * 根據教育經歷id刪除一條教育經歷
	 * @param int educationId 教育經歷id
	 * @return bool
	 */
	function deleteEducation($educationId){
		if(!$educationId) return false;
		return pwQuery::delete($this->_tableName, 'educationid=:educationid', array($educationId));
	}
	
	/**
	 * 根據教育經歷id刪除多條教育經歷
	 * @param int educationIds 教育經歷id
	 * @return bool
	 */
	function deleteEducations($educationIds){
		if(!S::isArray($educationIds)) return false;
		return pwQuery::delete($this->_tableName, 'educationid IN (:educationid)', array($educationIds));
	}
}