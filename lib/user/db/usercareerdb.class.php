<?php 
!defined('P_W') && exit('Forbidden');

/**
 * 工作經歷DAO
 * @package PW_User_CareerDB
 */
class PW_UserCareerDB extends BaseDB{
	
	var $_tableName = 'pw_user_career';
	var $_companyTable = 'pw_company';
	var $_primaryKey = 'careerid';
	
	/**
	 * 添加工作經歷
	 * @param array $data
	 * @return bool
	 */
	function addCareers($data){
		if (!S::isArray($data)) return false;
		return $this->_db->update("INSERT INTO $this->_tableName (uid,companyid,starttime) VALUES " . S::sqlMulti($data)."");
	}
	
	/**
	 * 根據用戶ID獲取工作經歷
	 * @param int uid
	 * @return array
	 */
	function getCareers($uid){
		$uid = (int) $uid;
		if ($uid < 1) return array();
		$query = $this->_db->query("SELECT uc.*, c.companyname FROM  $this->_tableName uc LEFT JOIN $this->_companyTable c USING(companyid) WHERE uc.uid = " . S::sqlEscape($uid) . ' ORDER BY careerid ASC');
		return $this->_getAllResultFromQuery($query,'careerid');
	}
	
	/**
	 * 根據公司ID獲取用戶id
	 * @param int companyId
	 * @return array
	 */
	function getUserIdsByCompanyId($companyId){
		if (!$companyId) return array();
		$query = $this->_db->query("SELECT uid FROM  $this->_tableName WHERE companyid = " . S::sqlEscape($companyId) . "");
		return $this->_getAllResultFromQuery($query,'uid');
	}
	
	/**
	 * 根據工作經歷ID獲取一條工作經歷
	 * @param int careerId
	 * @return array
	 */
	function getCareer($careerId){
		if (!$careerId) return array();
		return $this->_get($careerId);
	}
	
	/**
	 * 編輯一條工作經歷
	 * @param int $uid 用戶id
	 * @param int $companyId 公司Id
	 * @param int $startTime 入公司年份
	 * @return bool
	 */
	function editCareer($careerId,$companyId,$startTime){
		if (!$careerId || !$companyId || !$startTime) return false;
		return pwQuery::update($this->_tableName, "careerid=:careerid", array($careerId), array('companyid'=>$companyId,'starttime'=>$startTime));
	}
	
	/**
	 * 根據工作經歷id批量刪除工作經歷
	 * @param int careerIds 工作經歷id
	 * @return bool
	 */
	function deleteCareers($careerIds){
		if(!$careerIds) return false;
		return pwQuery::delete($this->_tableName, 'IN(:careerid)', array($careerIds));
	}
	
	/**
	 * 根據工作經歷id刪除一條工作經歷
	 * @param int careerId 工作經歷id
	 * @return bool
	 */
	function deleteCareer($careerId){
		if(!$careerId) return false;
		return pwQuery::delete($this->_tableName, 'careerid=:careerid', array($careerId));
	}
}