<?php 
!defined('P_W') && exit('Forbidden');

/**
 * 學校設置DAO
 * @package PW_SchoolDB
 */
class PW_SchoolDB extends BaseDB{
	
	var $_tableName = 'pw_school';
	var $_primaryKey = 'schoolid';
	
	/**
	 * 添加一所學校
	 * @param string $schoolName 學校名稱
	 * @param int $areaId 地區Id
	 * @param int $type 學校類型：1為小學，2為中學，3為大學
	 * @return int $Id 學校
	 */
	function addSchool($areaId,$schoolName,$type){
		$areaId = intval($areaId);
		$schoolName = trim($schoolName);
		$type = intval($type);
		if (!$schoolName || $areaId < 1 || $type < 0) return false;
		$tmpSchoolId = $this->_db->get_value("SELECT `schoolid` FROM $this->_tableName WHERE areaid =". S::sqlEscape($areaId). "AND schoolname= ". S::sqlEscape($schoolName)."");
		if(count($tmpSchoolId) > 0) return $tmpSchoolId;
		$fieldData = array($areaId,$schoolName,$type);
		return $this->_insert($fieldData);
	}
	
	/**
	 * 批量添加多所學校
	 * @param string $schoolNames 學校名稱
	 * @return int $Id 學校
	 */
	function addSchools($fieldData){
		if (!S::isArray($fieldData)) return false;
		return $this->_db->update("INSERT INTO " . $this->_tableName . " (schoolname,areaid,type) VALUES " . S::sqlMulti($fieldData));
	}
	
	/**
	 * 批量檢驗學校名是否重複
	 * @param int $areaIds 地區id
	 * @param string $schoolNames 學校名稱
	 * @param array schoolids
	 */
	function checkSchoolNames($areaid,$type,$schoolNames){
		if (!$areaid || !$schoolNames || !$type) return false;
		return $this->_db->get_value("SELECT count(*) FROM $this->_tableName WHERE areaid = " . S::sqlEscape($areaid) . " AND type = " . S::sqlEscape($type) . " AND schoolname IN (" . S::sqlImplode($schoolNames) . ")");
	}
	
	/**
	 * 根據一個學校ID獲取數據
	 * @param int schoolId
	 * @return array
	 */
	function getBySchoolId($schoolId){
		$schoolId = intval($schoolId);
		if($schoolId < 1) return false;
		return $this->_get($schoolId);
	}
	
	/**
	 * 根據多個學院ID獲取數據
	 * @param array schoolIds
	 * @return array
	 */
	function getSchoolsBySchoolIds($schoolIds){
		if(!S::isArray($schoolIds)) return array();
		$query = $this->_db->query("SELECT * FROM  $this->_tableName WHERE schoolid IN (" . S::sqlImplode($schoolIds) . ")");
		return $this->_getAllResultFromQuery($query,'schoolid');
	}
	
	/**
	 * 根據地區ID獲取學校
	 * @param int areaId 地區ID
	 * @param int type 學校類型
	 * @return array 學校數據
	 */
	function getSchoolByArea($areaId,$type){
		if(!$areaId || !$type) return false;
		$result = array();
		$query = $this->_db->query("SELECT * FROM  $this->_tableName WHERE areaid = " . S::sqlEscape($areaId) . " And type = " . S::sqlEscape($type)."");
		while ($rt = $this->_db->fetch_array($query)) {
			$rt['schoolname'] = str_replace('&nbsp;', ' ', $rt['schoolname']);
			$result[$rt[schoolid]] = $rt;
		}
		return $result;
		//return $this->_getAllResultFromQuery($query,'schoolid');
	}
	
	/**
	 * 編輯一條學校數據
	 * @param int id
	 * @param string name
	 * @return bool
	 */
	function editSchool($schoolId,$schoolName){
		if (!$schoolId || !$schoolName) return false;
		return pwQuery::update($this->_tableName, "schoolid=:schoolid", array($schoolId), array('schoolname'=>$schoolName));
	}
	
	/**
	 * 根據學校ID刪除數據
	 * @param int schoolId 學校id
	 * @return bool
	 */
	function deleteSchool($schoolId){
		if(!$schoolId) return false;
		return pwQuery::delete($this->_tableName, 'schoolid=:schoolid', array($schoolId));
	}
	
	/**
	 * 根據學校ID刪除多條數據
	 * @param int schoolIds 學校id
	 * @return bool
	 */	
	function deleteSchools($schoolIds){
		if(!S::isArray($schoolIds)) return false;
		return pwQuery::delete($this->_tableName, 'schoolid IN (:schoolid)', array($schoolIds));
	}
}
?>