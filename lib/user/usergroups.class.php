<?php 
!defined('P_W') && exit('Forbidden');
/**
 * 用戶組信息service
 * @package PW_UserGroups
 */
class PW_UserGroups{
	
	/**
	 * 根據用戶組類型獲取用戶組id
	 * 
	 * @param string $type 用戶組類型
	 * @return int gid 用戶組id
	 */
	function getUserGroupIds($type){
		$type = trim($type);
		if(!$type) return false;
		$userGroupsDb = $this->_getUserGroupsDao(); 
		return $userGroupsDb->getUserGroups($type);
	}
	
	function _getUserGroupsDao(){
		return L::loadDB('usergroups', 'user');
	}
}
