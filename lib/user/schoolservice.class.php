<?php 
!defined('P_W') && exit('Forbidden');
define('PW_UNIVERSITY', 3); //學校類型為大學
/**
 * 學校設置service
 * @package PW_School
 */
class PW_SchoolService{
	
	/**
	 * 添加一所學校返回學校id
	 * @param string $schoolName 學校名稱
	 * @param int $areaId 地區Id
	 * @param int $type 學校類型：1為小學，2為中學，3為大學
	 * @return int
	 */
	function addSchool($areaId,$type,$schoolName){
		$schoolName = trim($schoolName);
		$schoolName = trim(substrs($schoolName, 32, 'N'), ' &nbsp;');
		$areaId = intval($areaId);
		$type = $type ? intval($type) : 1;
		if (!$schoolName || !$areaId < 0 || $type < 1) return false;
		$schoolDb = $this->_getSchoolDao();
		return $schoolDb->addSchool($areaId,$type,$schoolName);
	}
	
	/**
	 *加入多條學校數據
	 * @param array $data數據
	 * @return array $schoolIds學校id
	 */
	function addSchools($data){
		if (!S::isArray($data)) return false;
		$fieldData = array();
		$schoolNames = array();
		$schoolDb = $this->_getSchoolDao();
		foreach ($data as $value){
			$value['areaid']  = intval($value['areaid']);
			$value['schoolname']= trim($value['schoolname']);
			$value['schoolname'] = trim(substrs($value['schoolname'], 32, 'N'), ' &nbsp;');
			$schoolNames[] = trim($value['schoolname']);
			$value['type'] = $value['type'] ? intval($value['type']) : 1;
			if(!$value['schoolname'] || $value['areaid'] < 0 || $value['type'] < 0) continue;
			$fieldData[] = $value;
		}
		$schoolIds = $schoolDb->checkSchoolNames((int)$value['areaid'],$value['type'],$schoolNames); 
		if($schoolIds > 0) return $schoolIds;
		return $schoolDb->addSchools($fieldData); 
	}
	
	/**
	 * 根據學校類型和地區ID獲取學校
	 * @param int areaId 地區ID
	 * @param int type 學校類型
	 * @return array 學校數據
	 */
	function getByAreaAndType($areaId,$type){
		$areaId = intval($areaId);
		$type = intval($type);
		if ($areaId < 0 || $type < 1) return false;
		$schoolDb = $this->_getSchoolDao();
		return $schoolDb->getSchoolByArea($areaId,$type);		
	}
	
	/**
	 * 根據一個學校ID獲取數據
	 * @param int schoolId
	 * @return array
	 */
	function getBySchoolId($schoolId){
		$schoolId = intval($schoolId);
		if (!$schoolId < 1) return false;
		$schoolDb = $this->_getSchoolDao();
		return $schoolDb->getBySchoolId($schoolId);
	}
	
	/**
	 * 根據多個學校ID獲取數據
	 * @param array schoolIds
	 * @return array
	 */
	function getBySchoolIds($schoolIds){
		if(!S::isArray($schoolIds)) return array();
		$schoolDb = $this->_getSchoolDao();
		return $schoolDb->getSchoolsBySchoolIds($schoolIds);
	}
	
	/**
	 * 根據學校ID和名稱編輯一條學校數據 
	 * @param int schoolId
	 * @param string newSchoolName
	 * @return bool
	 */
	function editSchool($schoolId,$newSchoolName){
		$schoolId = intval($schoolId);
		$newSchoolName = trim($newSchoolName);
		if (!newSchoolName || $schoolId < 1) return false;
		$schoolDb = $this->_getSchoolDao();
		return $schoolDb->editSchool($schoolId,$newSchoolName);
	}
	
	/**
	 * 根據學校ID刪除數據
	 * @param int $schoolId 學校id
	 * @param int $type 學校類型
	 * @return bool
	 */
	function deleteSchool($schoolId,$type){
		$schoolId = intval($schoolId);
		$type = intval($type);
		if($schoolId < 1 || $type < 0) return false;
	  /*if($type = PW_UNIVERSITY){
			$collegeDb = $this->_getCollegeService();
			$deleteCollege = $collegeDb->deleteBySchoolId($schoolId);
		}*/
		$schoolDb = $this->_getSchoolDao();
		$educationDb = $this->_getEducationService();
		$deleteEducation = $educationDb->deleteEduBySchoolId($schoolId);
		return $deleteSchool = $schoolDb->deleteSchool($schoolId);
	}

	/**
	 * 根據多個學校ID刪除數據
	 * @param int schoolIds 學校id
	 * @return bool
	 */
	function deleteSchools($schoolIds){
		if(!S::isArray($schoolIds)) return false;
		foreach($schoolIds as $value){
			$value['schoolid'] = intval($value['schoolid']);
			if($value['schoolid'] < 1) continue;
		}
		/*$collegeDb = $this->_getCollegeService();
		$deleteCollege = $collegeDb->deleteBySchoolIds($schoolIds);*/
		$schoolDb = $this->_getSchoolDao();
		$educationDb = $this->_getEducationService();
		$deleteEducation = $educationDb->deleteEduBySchoolIds($schoolIds);
		return $deleteSchool = $schoolDb->deleteSchools($schoolIds);
	}
	
	/**
	 * 組裝單個下拉框
	 * 
	 * @param int $parentid 上一級areaid
	 * @param int $defaultValue 默認選中值的id 
	 * @return string
	 */
	function getSchoolsSelectHtml($parentid, $type, $defaultValue = null) {
		$parentid = intval($parentid);
		$type = intval($type);
		if ($parentid < 0 || $type < 0) return null;
		$schools = $this->getByAreaAndType($parentid,$type);
		$schoolsSelect = '';
		foreach ((array)$schools as $value) {
			$selected = ($defaultValue && $value['schoolid'] == $defaultValue) ? 'selected' : '';
			$schoolsSelect .= "<option value=\"$value[schoolid]\" $selected>{$value[schoolname]}</option>\r\n";
		}
		return $schoolsSelect;
	}
	
	function _getSchoolDao(){
		return L::loadDB('School', 'user'); 
	}
	
	/*function _getCollegeService(){
		return L::loadClass('CollegeService', 'user'); 
	}*/
	
	function _getEducationService(){
		return L::loadClass('EducationService', 'user'); 
	}
}
?>