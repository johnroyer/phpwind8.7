<?php
/**
 * 用戶服務類文件
 * 
 * @package User
 */

!defined('P_W') && exit('Forbidden');

/**
 * 用戶服務對像
 * 
 * @package User
 */
class PW_UserService {
	
	/**
	 * 獲取用戶信息
	 *
	 * @param int $userId 用戶ID
	 * @param bool $withMainFields 是否取用戶主要信息
	 * @param bool $withMemberDataFields 是否取用戶基本信息
	 * @param bool $withMemberInfoFields 是否取用戶相關信息
	 * @return array|null 用戶數據數組，找不到返回null
	 */
	function get($userId, $withMainFields = true, $withMemberDataFields = false, $withMemberInfoFields = false) {
		$userId = (int) $userId;
		if ($userId <= 0) return null;
		if (perf::checkMemcache()){
			$_cacheService = Perf::gatherCache('pw_members');
			return $_cacheService->getAllFieldByUserId($userId, $withMainFields, $withMemberDataFields, $withMemberInfoFields);			
		}
		$membersDb = $this->_getMembersDB();
		return $membersDb->getWithJoin($userId, $withMainFields, $withMemberDataFields, $withMemberInfoFields);
		/*
		$member = array();
		if ($withMainFields) {
			$membersDb = $this->_getMembersDB();
			$data = $membersDb->get($userId);
			if ($data) $member = array_merge($member, $data);
		}
		if ($withMemberDataFields) {
			$memberDataDb = $this->_getMemberDataDB();
			$data = $memberDataDb->get($userId);
			if ($data) $member = array_merge($member, $data);
		}
		if ($withMemberInfoFields) {
			$memberInfoDb = $this->_getMemberInfoDB();
			$data = $memberInfoDb->get($userId);
			if ($data) $member = array_merge($member, $data);
		}
		return $member ? $member : null;
		*/
	}
	
	/**
	 * 根據用戶id批量獲取用戶信息
	 * @param array $userIds
	 * @return array
	 */
	function getByUserIds($userIds) {
		if (!is_array($userIds) || !count($userIds)) return array();
		if (perf::checkMemcache()){
			$_cacheService = Perf::gatherCache('pw_members');
			return $_cacheService->getMembersByUserIds($userIds);
		}
		$membersDb = $this->_getMembersDB();
		return $membersDb->getUsersByUserIds($userIds);
	}
	
	/**
	 * 根據用戶id批量獲取用戶信息，包含memberdata表信息
	 * @param array $userIds
	 * @return array
	 */
	function getUsersWithMemberDataByUserIds($userIds) {
		if (!is_array($userIds) || !count($userIds)) return array();
		if (perf::checkMemcache()){
			$_cacheService = Perf::gatherCache('pw_members');
			return $_cacheService->getAllFieldByUserIds($userIds, true, true);
		}	
		$membersDb = $this->_getMembersDB();
		return $membersDb->getUserInfosByUserIds($userIds);
	}
	
	/**
	 * 根據用戶名獲取用戶信息
	 *
	 * @param string $userName
	 * @param bool $withMainFields 是否取用戶主要信息
	 * @param bool $withMemberDataFields 是否取用戶基本信息
	 * @param bool $withMemberInfoFields 是否取用戶相關信息
	 * @return array|null 用戶數據數組，找不到返回null
	 */
	function getByUserName($userName, $withMainFields = true, $withMemberDataFields = false, $withMemberInfoFields = false) {
		$userName = trim($userName);
		if (!$userName) return null;
		
		$member = array();
		$membersDb = $this->_getMembersDB();
		$data = $membersDb->getUserByUserName($userName);
		if (!$data || !$data['uid']) return null;
		
		$userId = (int) $data['uid'];
		$withMainFields && $member = array_merge($member, $data);
		if ($withMemberDataFields) {
			$memberDataDb = $this->_getMemberDataDB();
			$data = $memberDataDb->get($userId);
			if ($data) $member = array_merge($member, $data);
		}
		if ($withMemberInfoFields) {
			$memberInfoDb = $this->_getMemberInfoDB();
			$data = $memberInfoDb->get($userId);
			if ($data) $member = array_merge($member, $data);
		}
		return $member ? $member : null;
	}
	
	/**
	 * 根據用戶名批量獲取用戶信息
	 * 
	 * @param array $userNames
	 * @return array
	 */
	function getByUserNames($userNames) {
		if (!is_array($userNames) || !count($userNames)) return array();
		
		$membersDb = $this->_getMembersDB();
		return $membersDb->getUsersByUserNames($userNames);
	}
	
	/**
	 * 根據用戶名獲取用戶id
	 * 
	 * @param string $userName 用戶名
	 * @return int
	 */
	function getUserIdByUserName($userName) {
		if (!$data = $this->getByUserName($userName)) return 0;
		return (int) $data['uid'];
	}
	
	/**
	 * 根據用戶email獲取用戶id
	 * 
	 * @param string $email 郵箱
	 * @return int
	 */
	function getUserIdByEmail($email) {
		if (!$data = $this->getByEmail($email)) return 0;
		return (int) $data['uid'];
	}
	
	/**
	 * 根據用戶id獲取用戶名
	 * 
	 * @param int $userId 用戶id
	 * @return string|null
	 */
	function getUserNameByUserId($userId) {
		$userId = S::int($userId);
		if ($userId < 1) return false;
		if (perf::checkMemcache()){
			$_cacheService = Perf::gatherCache('pw_members');
			return $_cacheService->getUserNameByUserId($userId);
		}				
		if (!$data = $this->get($userId)) return null;
		return $data['username'];
	}
	
	/**
	 * 根據用戶id批量獲取用戶名
	 * 
	 * @param array $userIds 用戶id數組
	 * @return array 以uid為key，用戶名為值的數組
	 */
	function getUserNamesByUserIds($userIds) {
		if (!is_array($userIds) || !count($userIds)) return array();
		if (perf::checkMemcache()){
			$_cacheService = Perf::gatherCache('pw_members');
			return $_cacheService->getUserNameByUserIds($userIds);
		}
		$userNames = array();
		$members = $this->getByUserIds($userIds);
		foreach ($members as $member) {
			$member['uid'] && $userNames[$member['uid']] = $member['username'];
		}
		return $userNames;
	}
	
	/**
	 * 根據email獲取用戶信息
	 * 
	 * @param string $email
	 * @return array|null 用戶數據數組，找不到返回null
	 */
	function getByEmail($email) {
		$email = trim($email);
		if ('' == $email) return null;
		
		$membersDb = $this->_getMembersDB();
		$users = $membersDb->getUserByUserEmails(array($email));
		return !empty($users) ? current($users) : null;
	}
	
	/**
	 * 根據email批量獲取用戶信息
	 * @param array $emails
	 * @return array
	 */
	function getByEmails($emails) {
		if (!is_array($emails) || !count($emails)) return array();
		
		$membersDb = $this->_getMembersDB();
		return $membersDb->getUserByUserEmails($emails);
	}

	
	/**
	 * 根據groupid獲取多個用戶信息
	 * @param array $groupIds
	 * @return array
	 */
	function getByGroupId($groupId) {
		$membersDb = $this->_getMembersDB();
		return $membersDb->getUsersByGroupId($groupId);
	}
	
	
	/**
	 * 根據groupid批量獲取多個用戶信息
	 * @param array $groupIds
	 * @return array
	 */
	function getByGroupIds($groupIds) {
		if (!is_array($groupIds) || !count($groupIds)) return array();
		
		$membersDb = $this->_getMembersDB();
		return $membersDb->getUsersByGroupIds($groupIds);
	}
	
	/**
	 * 查找最新用戶
	 * 
	 * @return array|null 最新用戶信息，找不到返回null
	 */
	function getLatestNewUser() {
		$membersDb = $this->_getMembersDB();
		$users = $membersDb->findUsersOrderByUserId();
		return count($users) ? current($users) : null;
	}
	
	/**
	 * 查找最新的幾個用戶
	 * 
	 * @return array
	 */
	function findLatestNewUsers($number = 10) {
		$number = intval($number);
		if ($number <= 0) return array();
		
		$membersDb = $this->_getMembersDB();
		return $membersDb->findUsersOrderByUserId($number);
	}
	
	/**
	 * 查找最新的幾個未被禁言的用戶
	 * 
	 * @return array
	 */
	function findNotBannedNewUsers($number = 10) {
		$number = intval($number);
		if ($number <= 0) return array();
		
		$membersDb = $this->_getMembersDB();
		return $membersDb->findNotBannedUsersOrderByUserId($number);
	}
	
	
	/**
	 * 獲得Members全部數據的個數
	 */
	function count() {
		$membersDb = $this->_getMembersDB();
		return $membersDb->_count();
	}
	
	/**
	 * 添加一個用戶
	 * 
	 * @param array $mainFields 用戶主要信息數組
	 * @param array $memberDataFields 用戶基本信息數組
	 * @param array $memberInfoFields 用戶相關信息數組
	 * @return int 新增用戶id，失敗返回0
	 */
	function add($mainFields, $memberDataFields = array(), $memberInfoFields = array()) {
		if (!is_array($mainFields) || !count($mainFields)) return 0;
		if (!isset($mainFields['username']) || !isset($mainFields['password'])) return 0;
		if ('' == $mainFields['username'] || '' == $mainFields['password']) return 0;
		
		$membersDb = $this->_getMembersDB();
		$userId = $membersDb->insert($mainFields);
		if (!$userId) return 0;
		
		$memberDataFields['uid'] = $userId;
		$memberDataDb = $this->_getMemberDataDB();
		$memberDataDb->insert($memberDataFields);
		
		$this->_replaceMemberInfo($userId, $memberInfoFields, false);
		
		return $userId;
	}
	
	/**
	 * 更新用戶信息
	 * 
	 * @param int $userId
	 * @param array $mainFields 用戶主要信息數組
	 * @param array $memberDataFields 用戶基本信息數組
	 * @param array $memberInfoFields 用戶相關信息數組
	 * @return bool 是否更新
	 */
	function update($userId, $mainFields = array(), $memberDataFields = array(), $memberInfoFields = array()) {
		$userId = intval($userId);
		if ($userId <= 0) return false;
		
		$updates = 0;
		if (is_array($mainFields) && count($mainFields)) {
			$membersDb = $this->_getMembersDB();
			$updates += $membersDb->update($mainFields, $userId); //TODO refactor update
		}
		if (is_array($memberDataFields) && count($memberDataFields)) {
			$memberDataDb = $this->_getMemberDataDB();
			$updates += $memberDataDb->update($memberDataFields, $userId);
		}
		$updates += $this->_replaceMemberInfo($userId, $memberInfoFields);
		
		return (bool) $updates;
	}
	
	/**
	 * 批量更新用戶信息
	 * 
	 * @param array $userIds
	 * @param array $mainFields 用戶主要信息數組
	 * @param array $memberDataFields 用戶基本信息數組
	 * @param array $memberInfoFields 用戶相關信息數組
	 * @return int 更新個數
	 */
	function updates($userIds, $mainFields = array(), $memberDataFields = array(), $memberInfoFields = array()) {
		if (!is_array($userIds) || !count($userIds)) return 0;
		
		$updates = 0;
		if (is_array($mainFields) && count($mainFields)) {
			$membersDb = $this->_getMembersDB();
			$updates += $membersDb->updates($mainFields, $userIds); //TODO refactor update
		}
		if (is_array($memberDataFields) && count($memberDataFields)) {
			$memberDataDb = $this->_getMemberDataDB();
			$updates += $memberDataDb->updates($memberDataFields, $userIds);
		}
		if (is_array($memberInfoFields) && count($memberInfoFields)) {
			foreach ($userIds as $userId) {
				$updates += $this->_replaceMemberInfo($userId, $memberInfoFields);
			}
		}
		
		return $updates;
	}
	
	function clearUserMessage($uid){
		$uid = intval($uid);
		if ($uid < 1) return false;
		$this->update($uid, array('newpm'=>0), array('newfans'=>0,'newreferto'=>0,'newnotice'=>0,'newrequest'=>0));
	}
	
	/**
	 * 增量更新用戶信息
	 * 
	 * @param int $userId
	 * @param array $mainFields 用戶主要信息數組
	 * @param array $memberDataFields 用戶基本信息數組
	 * @param array $memberInfoFields 用戶相關信息數組
	 * @return bool
	 */
	function updateByIncrement($userId, $mainFields = array(), $memberDataFields = array(), $memberInfoFields = array()) {
		$userId = intval($userId);
		if ($userId <= 0) return false;
		
		$updates = 0;
		if (is_array($mainFields) && count($mainFields)) {
			$membersDb = $this->_getMembersDB();
			$updates += $membersDb->increase($userId, $mainFields);
		}
		if (is_array($memberDataFields) && count($memberDataFields)) {
			$memberDataDb = $this->_getMemberDataDB();
			$updates += $memberDataDb->increase($userId, $memberDataFields);
		}
		if (is_array($memberInfoFields) && count($memberInfoFields)) {
			$memberInfoDb = $this->_getMemberInfoDB();
			$updates += $memberInfoDb->increase($userId, $memberInfoFields);
		}
		return (bool) $updates;
	}
	
	/**
	 * 批量按增量更新用戶信息
	 * 
	 * @param array $userIds
	 * @param array $mainFields 用戶主要信息數組
	 * @param array $memberDataFields 用戶基本信息數組
	 * @param array $memberInfoFields 用戶相關信息數組
	 * @return int 更新個數
	 */
	function updatesByIncrement($userIds, $mainFields = array(), $memberDataFields = array(), $memberInfoFields = array()) {
		if (!is_array($userIds) || !count($userIds)) return 0;
		
		$updates = 0;
		foreach ($userIds as $userId) {
			$updates += (int) $this->updateByIncrement($userId, $mainFields, $memberDataFields, $memberInfoFields);
		}
		return $updates;
	}
	/**
	 * 處理溢出數據
	 * @param $type	溢出字段
	 */
	function updateOverflow($type) {
		$memberDataDb = $this->_getMemberDataDB();
		return $memberDataDb->updateOverflow($type);
	}

	/**
	 * 設置用戶某個類型的狀態
	 * 
	 * @param int $userId 用戶id
	 * @param int $type 用戶狀態類型 常量：PW_USERSTATUS_*
	 * @param bool|int $status 狀態值，0-false, 1-true, other
	 * @return bool
	 */
	function setUserStatus($userId, $type, $status = true) {
		list($userId, $type) = array(intval($userId), intval($type));
		if ($userId <= 0 || $type <= 0) return false;

		$num = $this->_getUserStatusNumberWithUserStatusType($type);
		$membersDb = $this->_getMembersDB();
		return (bool)$membersDb->setUserStatus($userId, $type, $status, $num);
	}

	
	/**
	 * 獲取用戶某個類型的狀態
	 * 
	 * @param int $userId 用戶id
	 * @param int $type 用戶狀態類型 常量：PW_USERSTATUS_*
	 * @return int
	 */
	function getUserStatus($userId, $type) {
		list($userId, $type) = array(intval($userId), intval($type));
		if ($userId <= 0 || $type <= 0) return false;
		if (!$user = $this->get($userId)) return false;
		$num = $this->_getUserStatusNumberWithUserStatusType($type);
		$user['userstatus'] >>= --$type;
		return bindec(substr(sprintf('%0'.$num.'b', $user['userstatus']), -$num));
	}
	
	/**
	 * 刪除用戶
	 * 
	 * @param int $userId
	 * @return bool
	 */
	function delete($userId) {
		$membersDb = $this->_getMembersDB();
		$memberDataDb = $this->_getMemberDataDB();
		$memberInfoDb = $this->_getMemberInfoDB();
		$banUserDb = $this->_getBanUserDB();
		
		$memberDataDb->delete($userId);
		$memberInfoDb->delete($userId);
		$banUserDb->deleteByUserId($userId);
		return (bool) $membersDb->delete($userId);
	}
	
	/**
	 * 刪除多個用戶
	 * 
	 * @param array $userIds
	 * @return int 刪除個數
	 */
	function deletes($userIds) {
		if (!is_array($userIds) || !count($userIds)) return 0;
		
		$deletes = 0;
		foreach ($userIds as $userId) {
			$deletes += $this->delete($userId);
		}
		return $deletes;
	}
	
	/**
	 * 根據用戶id判斷用戶是否存在
	 * 
	 * @param int $userId
	 * @return boolean
	 */
	function isExist($userId) {
		if (!$data = $this->get($userId)) return false;
		return (bool)$data['uid'];
	}
	
	/**
	 * 根據用戶名判斷用戶是否存在
	 * 
	 * @param string $userName
	 * @return boolean
	 */
	function isExistByUserName($userName) {
		if (!$data = $this->getByUserName($userName)) return false;
		return (bool)$data['uid'];
	}
	
	function findOnlineUsers($onlineTimestamp) { //TODO move to OnlineUserService
		$onlineTimestamp = intval($onlineTimestamp);
		
		$memberDataDb = $this->_getMemberDataDB();
		return $memberDataDb->getOnlineUsers($onlineTimestamp);
	}
	
	/**
	 * 激活碼激活用戶
	 * 
	 * @param int $userId
	 * @param string $activateCode 激活碼
	 * @param string $siteHash 站點hash
	 * @param string $toemail 激活郵箱地址
	 * @return bool 是否激活成功
	 */
	function activateUser($userId, $activateCode, $siteHash,$toemail) {
		$userId = (int) $userId;
		$activateCode = trim($activateCode);
		if ($userId <= 0 || '' == $activateCode) return false;
		
		$membersDb = $this->_getMembersDB();
		$user = $membersDb->get($userId);
		if($user['email'] != $toemail) return false;
		if (!$user) return false;
		
		$comparedActivateCode = $this->_generateUserActivateCode($user, $siteHash);
		if ($comparedActivateCode == $activateCode) {
			$this->update($userId, array('yz' => 1));
			return true;
		}
		return false;
	}
	
	/**
	 * 獲取未激活用戶信息
	 * 
	 * @param int $userId 用戶id
	 * @param string $email 用戶email，這兩個參數傳入一個即可
	 * @param string $siteHash 站點hash
	 * @return array|null 用戶數據數組（帶activateCode字段，為該用戶的激活碼），找不到返回null
	 */
	function getUnactivatedUser($userId, $email, $siteHash) {
		$user = null;
		if ($userId) $user = $this->get($userId);
		if (!$user) $user = $this->getByEmail($email);
		
		if (!$user) return null;
		if ($user['yz'] <= 1) return null;
		
		$user['activateCode'] = $this->_generateUserActivateCode($user, $siteHash);
		return $user;
	}

	/**
	 * 返回某個狀態類型所佔bit位個數
	 * 
	 * @param int $type 用戶狀態類型 常量：PW_USERSTATUS_*
	 * @return int
	 */
	function _getUserStatusNumberWithUserStatusType($type) {
		switch ($type) {
			case PW_USERSTATUS_CFGFRIEND : $num = 2; break;
			default: $num = 1;
		}
		return $num;
	}
	
	function _generateUserActivateCode($userData, $siteHash) {
		return md5($userData['yz'] . substr(md5($siteHash), 0, 5) . substr(md5($userData['username']), 0, 5));
	}
	
	function _replaceMemberInfo($userId, $fieldsData, $checkExist = true) {
		if (!is_array($fieldsData) || !count($fieldsData)) return 0;
		
		$memberInfoDb = $this->_getMemberInfoDB();
		
		if ($checkExist && $memberInfoDb->get($userId)) {
			return $memberInfoDb->update($fieldsData, $userId);
		} else {
			$fieldsData['uid'] = $userId;
			return $memberInfoDb->insert($fieldsData);
		}
	}
	
	/**
	 * 組裝在線用戶現居地、家鄉、教育、工作經歷等信息
	 * 
	 * @param int $userId 用戶id
	 * @return array
	 */
	function getOnLineUsers() {
		global $winduid;
		$onlineUsers = GetOnlineUser();
		if (!s::isArray($onlineUsers)) return array();
		$userIds = array();
		foreach ($onlineUsers as $key => $v) {
			if ($key == $winduid) continue;
			$userIds[] = $key;
		}
		return $userIds;
	}
	
	/**
	 * 組裝用戶uids
	 * 
	 * @param array $fieldsData 用戶信息
	 * @return array
	 */
	function buildUids($fieldsData) {
		$uids = array();
		foreach ((array)$fieldsData as $v) {
			$uids[] = $v['uid'];
		}
		return array_diff($uids,$winduid);
	}
	
	/**
	 * 組裝用戶信息uid、username、face、在線圖標
	 * 
	 * @param array $uids
	 * @return array
	 */
	function buildUserInfo($uids) {
		if (!s::isArray($uids)) return array();
		require_once(R_P.'require/showimg.php');
		$userInfo = array();
		foreach ((array)$this->getUsersWithMemberDataByUserIds($uids) as $data) {
			$user['uid'] = $data['uid'];
			$user['username'] = $data['username'];
			$user['thisvisit'] = $data['thisvisit'];
			list($user['face']) = showfacedesign($data['icon'], '1', 's');
			$userInfo[] = $user;
		}
		return $userInfo;
	}
	
	/**
	 * 可能認識的人
	 * 
	 * @param int $userId 用戶id
	 * @return array
	 */
	function getMayKnownUserIds($fieldsData,$num = 12) {
		$onlineUserIds = $this->getOnLineUsers();
		if (!s::isArray($onlineUserIds)) return array();
		if (count($onlineUserIds) <= $num) return $onlineUserIds;
		
		$tmpApartmentUsers = $this->getUsersByApartmentAndUserIds($fieldsData['apartment'],$onlineUserIds,$num);
		$countApartmentUser = count($tmpApartmentUsers);
		$apartmentUsers = $this->buildUids($tmpApartmentUsers);
		if ($countApartmentUser >= $num) return $apartmentUsers;
		$homeUids = array_diff($onlineUserIds,$apartmentUsers);
		$homeNum = $num - $countApartmentUser;

		$tmpHomeUsers = $this->getUsersByHomeAndUserIds($fieldsData['home'],$homeUids,$homeNum);
		$countHomeUser = count($tmpHomeUsers);
		$homeUsers = $this->buildUids($tmpHomeUsers);
		if ($countHomeUser >= $homeNum) return array_merge($apartmentUsers,$homeUsers);
		$companyUids = array_diff($homeUids,$homeUsers);
		$companyNum = $homeNum - $countPlaceUser;
			
		$tmpCompanyUsers = $this->getUsersByCompanyidAndUserIds($fieldsData['companyid'],$companyUids,$companyNum);
		$countCompanyUser = count($tmpCompanyUsers);
		$companyUsers = $this->buildUids($tmpCompanyUsers);
		if ($countCompanyUser >= $companyNum) return array_merge($apartmentUsers,$homeUsers,$companyUsers);
		$educationUids = array_diff($companyUids,$companyUsers);
		$educationNum = $companyNum - $countCompanyUser;

		$tmpEducationUsers = $this->getUsersBySchoolidsAndUserIds($fieldsData['schoolid'],$educationUids,$educationNum);
		$countEducationUser = count($tmpEducationUsers);
		$educationUsers = $this->buildUids($tmpEducationUsers);
		if ($countEducationUser >= $educationNum) return array_merge($apartmentUsers,$homeUsers,$companyUsers,$educationUsers);
		$endUids = array_diff($educationUids,$educationUsers);
		$endNum = $educationNum - $countEducationUser;
		
		return array_merge($apartmentUsers,$homeUsers,$companyUsers,$educationUsers,array_slice($endUids,0,$endNum));
	}
	
	/**
	 * 根據所在地apartment和userIds統計用戶
	 * 
	 * @param int $apartment 所在地
	 * @param array $userIds 用戶ids
	 * @return int
	 */
	function countUsersByApartmentAndUserIds($apartment,$userIds) {
		$apartment = intval($apartment);
		if ($apartment < 1 || !s::isArray($userIds)) return 0;
		$membersDb = $this->_getMembersDB();
		return $membersDb->countUsersByApartmentAndUserIds($apartment,$userIds);
	}
	
	/**
	 * 根據所在地apartment和userIds獲取用戶
	 * 
	 * @param int $apartment 所在地
	 * @param array $userIds 用戶ids
	 * @return array
	 */
	function getUsersByApartmentAndUserIds($apartment,$userIds,$num) {
		$apartment = intval($apartment);
		if ($apartment < 1 || !s::isArray($userIds)) return array();
		$membersDb = $this->_getMembersDB();
		if ($this->countUsersByApartmentAndUserIds($apartment,$userIds) < 1) return array();
		return $membersDb->getUsersByApartmentAndUserIds($apartment,$userIds,$num);
	}
	
	/**
	 * 根據家鄉home和userIds統計用戶
	 * 
	 * @param int $home 所在地
	 * @param array $userIds 用戶ids
	 * @return int
	 */
	function countUsersByHomeAndUserIds($home,$userIds) {
		$home = intval($home);
		if ($home < 1 || !s::isArray($userIds)) return 0;
		$membersDb = $this->_getMembersDB();
		return $membersDb->countUsersByHomeAndUserIds($home,$userIds);
	}
	
	/**
	 * 根據家鄉home和userIds獲取用戶
	 * 
	 * @param int $home 家鄉
	 * @param array $userIds 用戶ids
	 * @return array
	 */
	function getUsersByHomeAndUserIds($home,$userIds,$num) {
		$home = intval($home);
		if ($home < 1 || !s::isArray($userIds)) return array();
		$membersDb = $this->_getMembersDB();
		if ($this->countUsersByHomeAndUserIds($home,$userIds) < 1) return array();
		return $membersDb->getUsersByHomeAndUserIds($home,$userIds,$num);
	}
	
	/**
	 * 根據工作經歷companyids和userIds統計用戶
	 * 
	 * @param array $companyids
	 * @param array $userIds 用戶ids
	 * @return array
	 */
	function countUsersByCompanyidAndUserIds($companyids,$userIds) {
		if (!s::isArray($companyids) || !s::isArray($userIds)) return 0;
		$membersDb = $this->_getMembersDB();
		return $membersDb->countUsersByCompanyidAndUserIds($companyids,$userIds);
	}
	
	/**
	 * 根據工作經歷companyids和userIds獲取用戶
	 * 
	 * @param array $companyids
	 * @param array $userIds 用戶ids
	 * @return array
	 */
	function getUsersByCompanyidAndUserIds($companyids,$userIds,$num) {
		if (!s::isArray($companyids) || !s::isArray($userIds)) return array();
		$membersDb = $this->_getMembersDB();
		if ($this->countUsersByCompanyidAndUserIds($companyids,$userIds) < 1) return array();
		return $membersDb->getUsersByCompanyidAndUserIds($companyids,$userIds,$num);
	}
	
	/**
	 * 根據教育經歷schoolids和userIds統計用戶
	 * 
	 * @param array $schoolids
	 * @param array $userIds 用戶ids
	 * @return array
	 */
	function countUsersBySchoolidsAndUserIds($schoolids,$userIds) {
		if (!s::isArray($schoolids) || !s::isArray($userIds)) return 0;
		$membersDb = $this->_getMembersDB();
		return $membersDb->countUsersBySchoolidsAndUserIds($schoolids,$userIds);
	}
	
	/**
	 * 根據教育經歷schoolids和userIds獲取用戶
	 * 
	 * @param array $companyids
	 * @param array $userIds 用戶ids
	 * @return array
	 */
	function getUsersBySchoolidsAndUserIds($schoolids,$userIds,$num) {
		if (!s::isArray($schoolids) || !s::isArray($userIds)) return array();
		$membersDb = $this->_getMembersDB();
		if ($this->countUsersBySchoolidsAndUserIds($schoolids,$userIds) < 1) return array();
		return $membersDb->getUsersByCompanyidAndUserIds($schoolids,$userIds,$num);
	}
	
	/**
	 * 獲取單條用戶、教育、所在地、家鄉、工作經歷等信息
	 * 
	 * @param int $userId 用戶id
	 * @return array
	 */
	function getUserInfoByUserId($userId) {
		$userId = intval($userId);
		if ($userId < 1) return array();
		$membersDb = $this->_getMembersDB();
		return $membersDb->getUserInfoByUserId($userId);
	}
	
	/**
	 * 用戶組權限
	 * 
	 * @param int $groupId 用戶組
	 * @return array
	 */
	function getRightByGroupId($groupId){
		static $groupRight;
		if (file_exists(D_P . "data/groupdb/group_$groupId.php")) {
			extract(pwCache::getData(S::escapePath(D_P . "data/groupdb/group_$groupId.php"),false));
			$groupRight = $_G;
		}
		return $groupRight;
	}

	function getUserInfoWithFace($uids) {
		if(!S::isArray($uids)) return array();
		require_once (R_P . 'require/showimg.php');
		$usersInfo = array();
		$users = $this->getByUserIds($uids); //'m.uid','m.username','m.icon','m.groupid'

		foreach ($users as $key => $value) {
			list($value['icon']) = showfacedesign($value['icon'], 1, 's');
			$usersInfo[$value['uid']] = $value;
		}
		return $usersInfo;
	}
	
	/**
	 * get PW_MembersDB
	 * 
	 * @access protected
	 * @return PW_MembersDB
	 */
	function _getMembersDB() {
		return L::loadDB('Members', 'user');
	}
	
	/**
	 * get PW_MemberdataDB
	 * 
	 * @return PW_MemberdataDB
	 */
	function _getMemberDataDB() {
		return L::loadDB('MemberData', 'user');
	}
	
	/**
	 * get PW_MemberinfoDB
	 * 
	 * @return PW_MemberinfoDB
	 */
	function _getMemberInfoDB() {
		return L::loadDB('MemberInfo', 'user');
	}
	
	/**
	 * @return PW_BanUserDB
	 */
	function _getBanUserDB() {
		return L::loadDB('BanUser', 'user');
	}
}

