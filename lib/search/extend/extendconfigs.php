<?php
! function_exists ( 'readover' ) && exit ( 'Forbidden' );
/**
 * 配置說明
 * 二維數組 array('調用位置1'=> array(
 * '調用項1'=>array('文件路徑=>'1','調用類名稱'=>'classname1'),
 * '調用項2'=>array('文件路徑=>'2','調用類名稱'=>'classname2'),
 * '調用位置2'=> array(
 * '調用項3'=>array('文件路徑=>'3','調用類名稱'=>'classname3'),
 * '調用項4'=>array('文件路徑=>'4','調用類名稱'=>'classname4'),
 * );
 * 1,調用位置1:表示展示的位置，如right_search 將展示在搜索結果集的右邊,一個位置可調用多個擴展類結果
 * 2,調用項1:調用的名稱標識符
 * 3,文件路徑:表示加載擴展搜索類的文件路徑
 * 4,調用類名稱:PW_classname1Searcher 注意只需要提供classname1 如PW_dianpuSearcher的classname為dianpu
 */
$configs = array ();

if ($db_modes ['dianpu'] ['ifopen']) {
	$configs ['right_search'] = array ('dianpu_dianpu' => array ('path' => R_P . 'mode/dianpu/lib/searcher/dianpusearcher.class.php', 'classname' => 'dianpu' ) );
}
