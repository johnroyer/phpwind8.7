<?php
!function_exists('readover') && exit('Forbidden');
class PW_ExtendSearcherAbstract {
	
	function getSearchResult() {
	}
	
	/**
	 * HTML內容輸出
	 * @param $htmlFile HTML模板路徑
	 * @param $params   頁面參數
	 */
	function _outputHtml($htmlFile, $params = array()) {
		ob_start();
		ob_implicit_flush(false);
		require S::escapePath($htmlFile);
		return ob_get_clean();
	}
	
	/**
	 * 獲取擴展搜索的HTML模板路徑
	 * @param $direcotry 擴展服務的目錄,正常當前為 dirname(__FILE__)或其它自定義目錄
	 * @param $htmlname  擴展搜索服務HTML模板名稱
	 */
	function _getHtmlFile($direcotry, $htmlname) {
		$filePath = S::escapePath($direcotry . '/template/' . $htmlname);
		if (!is_file($filePath)) return '';
		return $filePath;
	}
}