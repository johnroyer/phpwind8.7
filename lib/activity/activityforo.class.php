<?php
!defined('P_W') && exit('Forbidden');
L::loadClass('PostActivity', 'activity', false);

class PW_ActivityForO extends PW_PostActivity {
	/**
	 * 獲取功能名稱
	 * @param string $see
	 * @return string
	 */
	function PW_ActivityForO() {
		$this->initGlobalValue();
	}
	function getSeeTitleBySee($see) {
		if ('fromgroup' == $see) {
			$seeTitle = '來自群組';
		} elseif ('feeslog' == $see) {
			$seeTitle = '費用流通日誌';
		} else {
			$seeTitle = '來自版塊';
		}
		return $seeTitle;
	}
	/**
	 * 返回搜索時間的預設值
	 * @return multitype:multitype:string  
	 */
	function getTimeOptions() {
		return array (
			'+86400' => '一天內',
			'+259200' => '三天內',
			'+604800' => '一周內',
			'+2592000' => '一月內',
		);
	}
	/**
	 * 返回時間select的HTML
	 * @param string $selected 選中的option的value
	 * @param bool $withEmptySelection 是否包含'時間不限'的option
	 * @param string $selectTagName select的name的值，如無，返回的HTML不包含select這個Tag(只有option Tag)
	 * @return HTML
	 */
	function getTimeSelectHtml($selected, $withEmptySelection=1, $selectTagName = 'time') {
		$options = array();
		if ($withEmptySelection) {
			$options[''] = '時間不限';
		}
		$options += $this->getTimeOptions();
		return getSelectHtml($options, $selected, $selectTagName);
	}
	/**
	 * 返回活動狀態
	 * @param int $status 狀態
	 * @return string|array 若$status有值，返回string狀態；否則，返回array所有狀態
	 */
	function getFeesLogStatus($status = '') {
		$Array = array(0 => '所有活動', 1 => '進行中', 2 => '已結束', 3 => '已取消', 4 => '已刪除');
		if ($status !== '') {
			if (array_key_exists($status, $Array)) {
				return $Array[$status];
			} else {
				return '';
			}
		} else {
			return $Array;
		}
	}
	/**
	 * 返回活動費用類型
	 * @param int $type 類型
	 * @return string|array 若$type有值，返回string類型；否則，返回array所有類型
	 */
	function getFeesLogCostType($type = '') {
		//$Array = array(1 => '支付費用', 2 => '追加', 3 => '退款', 4 => '確認支付');
		$Array = array(1 => '', 2 => '追加', 3 => '退款', 4 => '');
		if ($type !== '') {
			if (array_key_exists($type, $Array)) {
				return $Array[$type];
			} else {
				return '';
			}
		} else {
			return $Array;
		}
	}
	/**
	 * 返回收入或支出類型
	 * @param string $type 類型
	 * @return string|array 若$type有值，返回string類型；否則，返回array所有類型
	 */
	function getExpenseOrIncomeName($type = '') {
		$Array = array('expense' => '<span class="s3">支出</span>', 'income' => '<span class="s2">收入</s3>');
		if ($type !== '') {
			if (array_key_exists($type, $Array)) {
				return $Array[$type];
			} else {
				return '未知';
			}
		} else {
			return $Array;
		}
	}
	function getSignupHtml($data) {
		$tid = $data['tid'];
		$activityStatusKey = $this->getActivityStatusKey($data, $this->timestamp, $this->peopleAlreadySignup($tid));
		$replaceArray = array();
		if ('activity_is_ended' == $activityStatusKey) {
			/*支付成功費用流通日誌*/
			$this->UpdatePayLog($tid,0,2);
		} elseif ('activity_is_cancelled' == $activityStatusKey) {
			$replaceArray = array($data['minparticipant']);
		} elseif ('signup_is_available' == $activityStatusKey) {
			$replaceArray = array($tid, $data['authorid'], $this->actmid);
			if ($this->getOrderMemberUid($tid) == $this->winduid && $this->winduid) {
				$activityStatusKey = 'additional_signup_is_available_for_member';
			} elseif ($this->winduid) {
				$activityStatusKey = 'signup_is_available_for_member';
			} else {
				$activityStatusKey = 'signup_is_available_for_guest';
			}
		}
		$signupHtml = '<p class="t3">';
		$signupHtml .= $this->getSignupHtmlByActivityKey($activityStatusKey, $replaceArray);
		$signupHtml .= '</p>';
		return $signupHtml;
	}

	function getGroupSignupHtml($data) {//群組活動的狀態獲取
		global $winduid;
		$replaceArray = $data;
		require_once(A_P . 'groups/lib/active.class.php');
		$newActive = new PW_Active();
		if ($this->timestamp > $data['endtime']) {
			$activityStatusKey = 'activity_is_ended';
		} elseif ($this->timestamp > $data['deadline'] && $this->timestamp > $data['begintime']) {
			$activityStatusKey = 'activity_group_running';
		} elseif ($this->timestamp > $data['deadline']) {
			$activityStatusKey = 'signup_is_ended';
		} elseif ($data['limitnum'] && $data['members'] >= $data['limitnum']) {
			$activityStatusKey = 'signup_number_limit_is_reached';
		} elseif ($newActive->isJoin($data['id'], $winduid) && $winduid != $data['uid']) {
			$activityStatusKey = 'activity_is_joined';
		} elseif ($winduid == $data['uid']) {
			$activityStatusKey = 'activity_group_edit';
		} else {
			$activityStatusKey = 'group_is_available_for_member';
		}
		$signupHtml = '<p class="t3">';
		$signupHtml .= $this->getSignupHtmlByActivityKey($activityStatusKey, $replaceArray);
		$signupHtml .= '</p>';
		return $signupHtml;
	}
	
	function getSignupHtmlByActivityKey ($key, $replaceArray = NULL) {
		switch ($key) {
			case 'signup_not_started_yet': //未開始報名
				$html = '<span class="bt"><span><button type="button" disabled>報名未開始</button></span></span>';
				break;
			case 'activity_is_cancelled': //報名結束但未達到最低人數限制，活動取消
				$html = '<span class="bt"><span><button type="button" disabled>活動已取消</button></span></span>';
				break;
			case 'activity_is_running':
			case 'signup_is_ended': //正常情況下報名結束
				$html = '<span class="bt"><span><button type="button" disabled>報名已結束</button></span></span>';
				break;
			case 'activity_is_ended': //活動結束
				$html = '<span class="bt"><span><button type="button" disabled>活動已結束</button></span></span>';
				break;
			case 'signup_number_limit_is_reached': //報名人數已滿
				$html = '<span class="bt"><span><button type="button" disabled>報名人數已滿</button></span></span>';
				break;
			case 'signup_is_available':
			case 'signup_is_available_for_guest': //未登錄狀態報名提示
				$text = '我要報名';
			case 'signup_is_available_for_member': //登錄狀態，先前未報名，報名提示
				$text || $text = '我要報名';
			case 'additional_signup_is_available_for_member': //登錄狀態，先前已報名，報名提示
				$text || $text = '我要補報';
				$html = "<span class=\"btn\"><span><button type=\"button\" onclick=\"window.location='read.php?tid=$replaceArray[0]'\">$text</button></span></span>";
				break;
			case 'group_is_available_for_member' :
				$text || $text = '我要報名';
				$html = "<span class=\"btn\"><span><button type=\"button\" onclick=\"window.location='apps.php?q=group&a=active&job=view&cyid=".$replaceArray['cid']."&id=".$replaceArray['id']."'\">$text</button></span></span>";
				break;
			case 'activity_group_running'://群組活動的特殊狀態
				$html = '<span class=\"bt\"><span><button type=\"button\>活動進行中</button></span></span>';
				break;
			case 'activity_is_joined':
				$html = "<span class=\"bt\"><span><button type=\"button\" id=\"active_quit\" onclick=\"sendmsg('apps.php?q=group&a=active&job=quit&cyid=".$replaceArray['cid']."&id=".$replaceArray['id']."', '', this.id);\">退出活動</button></span></span>";
				break;
			case 'activity_group_edit':
				$html = "<span class=\"btn\"><span><button type=\"button\" onclick=\"window.location='apps.php?q=group&a=active&job=edit&cyid=".$replaceArray['cid']."&id=".$replaceArray['id']."'\">編輯活動</button></span></span>";
				break;
			default:
				$html = '';
		}
		return $html;
	}
	/**
	 * 獲取用戶參與的所有活動
	 * @param int $uid 用戶ID
	 * @return array 活動ID
	 */
	function getAllParticipatedActivityIdsByUid ($uid) {
		$activityIds = array();
		$query = $this->db->query("SELECT DISTINCT tid FROM pw_activitymembers WHERE uid = ". S::sqlEscape($uid));
		while ($rt = $this->db->fetch_array($query)) {
			$activityIds[] = $rt['tid'];
		}
		return $activityIds;
	}
	
	function getActivityNumberAndLastestTimestampByUid ($uid) {
		$allActivityIdsIHaveParticipated = $this->getAllParticipatedActivityIdsByUid($uid);
		$fids = trim(getSpecialFid() . ",'0'",',');
		!empty($fids) && $where .= ($where ? ' AND ' : '')."dv.fid NOT IN($fids)";
		$where .= ($where ? ' AND ' : ''). "(tr.authorid = ".S::sqlEscape($uid).($allActivityIdsIHaveParticipated ? ' OR tr.tid IN ('.S::sqlImplode($allActivityIdsIHaveParticipated).')' : '').')';
		$where && $where = " WHERE ".$where;
		$activitynum = $this->db->get_value("SELECT COUNT(*) FROM pw_threads tr LEFT JOIN pw_activitydefaultvalue dv USING (tid) $where");
		$activity_lastpost = $this->db->get_value("SELECT postdate FROM pw_threads tr LEFT JOIN pw_activitydefaultvalue dv USING (tid) $where ORDER BY postdate DESC LIMIT 1");
		return array ($activitynum, $activity_lastpost);
	}
	
	/**
	 * 獲取費用支付說明
	 * @param int $ifpay  0=>未支付 1=>已支付 2=>確認支付 3=>交易關閉 4=>退款完畢 
	 * @param int $fromuid 幫助支付的用戶ID
	 * @param string $fromusername 幫助支付的用戶名
	 * @return string 說明字符串
	 */
	function getPayLogDescription(&$rt){
		global $winduid;
		$u = $winduid;
		if ($rt['ifpay'] == 1){
			if ($rt['issubstitute']){
				if ($rt['fromuid'] == $u){
					$rt['otherParty'] = $rt['author'];
					$rt['expenseOrIncome'] = 'expense';	
					$rt['payDescription'] = '我幫<a href="'.USER_URL. $rt['uid'] . '" target="_blank">' .  $rt['username'] . '</a>支付';
				}elseif($rt['uid'] == $u){
					$rt['otherParty'] = $rt['author'];
					$rt['expenseOrIncome'] = 'expense';	
					$rt['payDescription'] = '<a href="'.USER_URL. $rt['fromuid'] . '" target="_blank">' .  $rt['fromusername'] . '</a>幫我支付';
				}elseif ($rt['authorid'] == $u){
					$rt['otherParty'] = $rt['username'];
					$rt['expenseOrIncome'] = 'income';	
					$rt['payDescription'] = '<a href="'.USER_URL. $rt['fromuid'] . '" target="_blank">' .  $rt['fromusername'] . '</a>幫<a href="' .USER_URL . $rt['uid'] . '" target="_blank">' .  $rt['username'] . '</a>支付';
				}
			}else{
				$rt['otherParty'] = $rt['uid'] == $u ? $rt['author'] : $rt['username'];
				$rt['expenseOrIncome'] = $rt['uid'] == $u ? 'expense' : 'income';
				$rt['payDescription'] = '支付寶支付';
			}
		} elseif ($rt['ifpay'] == 4){
				$rt['otherParty'] = $rt['uid'] == $u ? $rt['author'] : $rt['username'];
				$rt['expenseOrIncome'] = $rt['uid'] == $u ? 'expense' : 'income';
				$rt['payDescription'] = '支付寶支付';
		} else{
			$rt['otherParty'] = $rt['uid'] == $u ? $rt['author'] : $rt['username'];
			if ($rt['costtype'] == 3){
				$rt['expenseOrIncome'] = $rt['uid'] != $u ? 'expense' : 'income';
				$rt['payDescription'] = '支付寶支付';
			}else{
				$rt['expenseOrIncome'] = $rt['uid'] == $u ? 'expense' : 'income';
				$rt['payDescription'] = '其他方式支付';
			}
		}
		$rt['expenseOrIncome'] = $this->getExpenseOrIncomeName($rt['expenseOrIncome']);
	}
}