<?php
!defined('P_W') && exit('Forbidden');
class PW_Activity {
	var $activitycatedb;
	var $activitymodeldb;

	function setActCache() {
		//* include pwCache::getPath(D_P.'data/bbscache/activity_config.php');
		extract(pwCache::getData(D_P.'data/bbscache/activity_config.php', false));
		$this->activitycatedb = $activity_catedb;
		$this->activitymodeldb = $activity_modeldb;
	}
	
	function getActivityCateDb() {
		if (!$this->activitycatedb) {
			$this->setActCache();
		}
		return $this->activitycatedb;
	}
	
	function getActivityModelDb() {
		if (!$this->activitymodeldb) {
			$this->setActCache();
		}
		return $this->activitymodeldb;
	}
	/**
	 * 返回活動子分類select的HTML
	 * @param int $selectedActmid 選中的活動分類
	 * @param bool $withEmptySelection 是否包含「所有分類」選項
	 * @param string $selectName select的name的值，如無，返回的HTML不包含select這個Tag
	 * @return HTML
	 */
	function getActmidSelectHtml ($selectedActmid = 0, $withEmptySelection = 1, $selectTagName = 'actmid') {
		$options = array();
		if ($withEmptySelection) {
			$options['0'] = getLangInfo('other','act_activity_class');
		}
		$activityCateDb = $this->getActivityCateDb();
		$activityModelDb = $this->getActivityModelDb();
		$newModelDb = array();
		foreach ($activityModelDb as $value) {
			$newModelDb[$value['actid']][] = $value;
		}

		foreach($activityCateDb as $value) {
			foreach($newModelDb[$value['actid']] as $val){
				$options[$value['name']][$val['actmid']] = $val['name'];
			}
		}
		
		$return = getSelectHtml($options, $selectedActmid, $selectTagName);
		return $return;
	}
	/**
	 * 獲取活動狀態的Key
	 * @param array $data 活動數據
	 * @param int $currentTimestamp 當前時間戳
	 * @param int $numberOfPeopleAlreadySignup 已報名人數
	 * @return string 活動狀態Key
	 */
	function getActivityStatusKey ($data, $currentTimestamp, $numberOfPeopleAlreadySignup) {
		if ($data['iscancel']) {
			return 'activity_is_cancelled';//活動取消
		} elseif ($data['signupstarttime'] > $currentTimestamp) {
			return 'signup_not_started_yet';// 報名未開始
		} elseif ($data['endtime'] < $currentTimestamp) {
			return 'activity_is_ended';//活動結束
		} elseif ($currentTimestamp > $data['starttime'] && $currentTimestamp < $data['endtime']) {
			return 'activity_is_running';//活動進行中
		} elseif ($data['signupendtime'] < $currentTimestamp) {
			return 'signup_is_ended';//報名結束，活動未開始
		} elseif ($numberOfPeopleAlreadySignup >= $data['maxparticipant'] && $data['maxparticipant']) {
			return 'signup_number_limit_is_reached';//報名人數已滿
		} else {
			return 'signup_is_available';
		}
	}
}