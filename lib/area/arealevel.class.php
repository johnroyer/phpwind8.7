<?php
!defined('P_W') && exit('Forbidden');
/**
 * 門戶權限服務層
 * @author liuhui @2010-3-8
 */
class PW_AreaLevel {
	
	/**
	 * 根椐頻道ID和模塊名稱獲取用戶權限
	 * @param $userId 
	 * @param $channel
	 * @param $invoke
	 * @return bool
	 */
	function getAreaLevel($userId,$channel,$invoke=''){
		$userId = intval($userId);
		$invoke = trim($invoke);
		if($userId < 1/* || $channel < 1*/){
			return false;
		}
		$userLevel = $this->getAreaUser($userId);
		if(!$userLevel){
			return false;
		}
		if( 1 == $userLevel['super']){
			return $userLevel;
		}
		if( "" == $userLevel['level'] ){
			return false;
		}
		$levle = unserialize($userLevel['level']);
		if( !$levle || !isset($levle[$channel])){
			return false;
		}
		
		if("" != $invoke && ( !isset($levle[$channel]['invokes']) ||  !isset($levle[$channel]['invokes'][$invoke]))){
			return false;
		}
		return $userLevel;
	}
	
	function getAreaLevelByUserId($userId){
		$userId = intval($userId);
		if($userId < 1 ){
			return false;
		}
		$userLevel = $this->getAreaUser($userId);
		if(!$userLevel){
			return false;
		}
		if( 1 == $userLevel['super']){
			return true;
		}
		if( "" == $userLevel['level'] ){
			return false;
		}
		return true;
	}
	
	function language($key){
		$messages = array(
			"username_empty"      =>"抱歉,請輸入用戶名",
			"uid_empty"           =>"抱歉,用戶ID不正確",
			"username_not_exist"  =>"抱歉,用戶名不存在",
			"add_success"         =>"增加用戶權限完成",
			"update_success"      =>"更新用戶權限完成",
			"update_fail"         =>"更新用戶權限失敗",
			"userlevel_not_exist" =>"抱歉,用戶權限不存在",
			"delete_success"      =>"刪除用戶權限完成",
			"do_success"          =>"操作完成",
			"area_no_level"       =>"抱歉,你沒有管理此模塊的權限",
			"area_no_pushto"      =>"抱歉,您沒有門戶頻道的管理權限不能進行推送",
			"area_no_invoke"      =>"抱歉,模塊不存在",
		);
		return isset($messages[$key]) ? $messages[$key] : '';
	}
	
	/**
	 * 增加多個門戶管理員
	 * @param $fields
	 * @return true/false
	 */
	function addAreaUsers($fields){
		$haystack = trim($fields['username']);
		if("" == $haystack){
			return array(false,$this->language("username_empty"));
		}
		$mows = explode(",",$haystack);
		$userNames = array();
		foreach($mows as $username){
			$userNames[] = strip_tags(trim($username));
		}
		
		$userService = $this->_getUserService();
		$users = $userService->getByUserNames(array_unique($userNames));
		if(!$users){
			return array(false,$this->language("username_not_exist"));
		}
		$needles = array();
		$needles['hasedit'] = $fields['hasedit'];
		$needles['hasattr'] = $fields['hasattr'];
		$needles['super'] = $fields['super'];
		$needles['level'] = $fields['level'];
		foreach($users as $user){
			$needles['uid'] = $user['uid'];
			$needles['username'] = $user['username'];
			$this->addAreaUser($needles);
		}
		$this->_updateAreaUserConfig();
		return array(true,$this->language("add_success"));
	}
	/**
	 * 增加一個門戶管理員
	 * @param $fields
	 * @param $areaLevelDB
	 * @return last_insert_id / false
	 */
	function addAreaUser($fields,$areaLevelDB=false){
		$fields['uid'] = intval($fields['uid']);
		$fields['username'] = trim($fields['username']);
		$fields['hasedit'] = intval($fields['hasedit']);
		$fields['hasattr'] = intval($fields['hasattr']);
		$needles['super'] = intval($fields['super']);
		$fields['level'] = (is_array($fields['level'])) ? serialize($fields['level']) : $fields['level'];
		if( 1 > $fields['uid']){
			return false;
		}
		if($this->getAreaUser($fields['uid'])){
			return false;
		}
		$areaLevelDB = ($areaLevelDB) ? $areaLevelDB : $this->_getAreaLevelDB();
		return $areaLevelDB->add($fields);
	}
	
	function updateAreaUserByUserName($fields,$userName){
		if("" == $userName){
			return array(false,$this->language("username_empty"));
		}
		$userService = $this->_getUserService();
		$userId = $userService->getUserIdByUserName($userName);
		if(!$userId){
			return array(false,$this->language("username_empty"));
		}
		$this->_updateAreaUserConfig();
		return $this->updateAreaUser($fields,$userId);
	}
	/**
	 * 更新一個門戶管理員
	 * @param $fields
	 * @param $userId
	 * @return unknown_type
	 */
	function updateAreaUser($fields,$userId,$areaLevelDB=false){
		$fields['hasedit'] = intval($fields['hasedit']);
		$fields['hasattr'] = intval($fields['hasattr']);
		$fields['super'] = intval($fields['super']);
		$fields['level'] = (is_array($fields['level'])) ? serialize($fields['level']) : $fields['level'];
		if( 1 > $userId ){
			return array(false,$this->language("uid_empty"));
		}
		$areaLevelDB = ($areaLevelDB) ? $areaLevelDB : $this->_getAreaLevelDB();
		$areaLevelDB->update($fields,$userId);
		$this->_updateAreaUserConfig();
		return array(true,$this->language("update_success"));
	}
	/**
	 * 刪除一個門戶管理員
	 * @param $userId
	 * @return unknown_type
	 */
	function deleteAreaUser($userId){
		if( 1 > $userId ){
			return array(false,$this->language("uid_empty"));
		}
		$areaLevelDB = $this->_getAreaLevelDB();
		$areaLevelDB->delete($userId);
		$this->_updateAreaUserConfig();
		return array(true,$this->language("delete_success"));
	}
	/**
	 * 根椐用戶名獲取用戶權限
	 * @param $userName
	 * @return unknown_type
	 */
	function getAreaUserByUserName($userName){
		if("" == $userName){
			return array(false,$this->language("username_empty"),'');
		}
		
		$userService = $this->_getUserService();
		$userId = $userService->getUserIdByUserName($userName);
		if(!$userId){
			return array(false,$this->language("username_empty"),'');
		}
		if(!($areaUser = $this->getAreaUser($userId))){
			return array(false,$this->language("userlevel_not_exist"),'');
		}
		return array(true,$this->language("do_success"),$areaUser);
	}
	
	function _updateAreaUserConfig() {
		require_once(R_P.'admin/cache.php');
		$users = $this->getAllAreaUser();
		$temp = array();
		foreach ($users as $value) {
			$temp[] = $value['uid'];
		}
		setConfig('db_portal_admins', $temp);
		updatecache_c();
	}
	
	/**
	 * 獲取一個門戶管理員
	 * @param $userId
	 * @return unknown_type
	 */
	function getAreaUser($userId){
		if( 1 > $userId ){
			return false;
		}
		$areaLevelDB = $this->_getAreaLevelDB();
		$result = $areaLevelDB->get($userId);
		if ($result) return $result;
		return $this->_getGMLevel();
	}
	/**
	 * 創始人有特殊權限
	 */
	function _getGMLevel() {
		global $manager,$windid;
		if (!S::inArray($windid, $manager)) return false;
		return array('uid'=>$windid,'hasedit'=>1,'hasattr'=>1,'super'=>1);
	}
	/**
	 * 獲取多個門戶管理員
	 * @param $page
	 * @param $perpage
	 * @return unknown_type
	 */
	function getAreaUsers($page,$perpage){
		if( 1 > $page || 1 > $perpage ){
			return false;
		}
		$areaLevelDB = $this->_getAreaLevelDB();
		return $areaLevelDB->gets($page,$perpage);
	}
	/**
	 * 統計門戶管理員
	 * @return unknown_type
	 */
	function countAreaUser(){
		$areaLevelDB = $this->_getAreaLevelDB();
		return $areaLevelDB->count();
	}
	function getAllAreaUser() {
		$areaLevelDB = $this->_getAreaLevelDB();
		return $areaLevelDB->getAll();
	}
	/**
	 * 門戶權限數據層
	 * @return unknown_type
	 */
	function _getAreaLevelDB() {
        return L::loadDB('AreaLevel', 'area');
    }
    
    /**
     * @return PW_UserService
     */
    function _getUserService() {
    	return L::loadClass('UserService', 'user');
    }
	
}
?>