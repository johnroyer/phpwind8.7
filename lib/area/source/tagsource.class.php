<?php
!defined('P_W') && exit('Forbidden');
require_once (R_P.'lib/base/systemdata.php');
class PW_TagSource extends SystemData {
	var $_element;
	var $_lang = array(
		'title'	=> '標籤名稱',
	);
	function getSourceData($config,$num) {
		$config = $this->_initConfig($config);
		$element = $this->_getElement();
		return $element->getTags($config['tagsort'],$num);
	}
	
	function getRelateType() {
		return false;
	}

	function getSourceConfig() {
		return array(
			'tagsort' 	=> array(
				'name' 	=> '標籤排行',
				'type'	=> 'select',
				'value'	=> array(
					'hot'		=>	'熱門標籤',
					'new'		=>	'最新標籤',
				),
			),
		);
	}
	
	function _getElement() {
		if (!$this->_element) {
			$this->_element = L::loadClass('element');
		}
		return $this->_element;
	}
	
	function _initConfig($config) {
		$temp = array();
		$temp['tagsort'] = $config['tagsort'];

		return $temp;
	}
	
}