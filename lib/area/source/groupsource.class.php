<?php
!defined('P_W') && exit('Forbidden');
require_once (R_P.'lib/base/systemdata.php');
class PW_GroupSource extends SystemData {
	var $_element;
	var $_lang = array(
		'title'		=> '群組名稱',
		'tnum'		=> '主題數',
		'pnum'		=> '帖子數',
		'members'	=> '成員數',
		//'todaypost'	=> '今日發帖',
		'createtime'=> '創建時間',
		'stylename'	=> '分類名稱',
		'credit'	=> '積分'
	);
	function getSourceData($config,$num) {
		$config = $this->_initConfig($config);
		
		return $this->_getData($config['groupsort'],$config['groupclass'],$num);
	}
	
	function _getData($groupSort,$gourpClass,$num) {
		$num = (int) $num ? (int) $num : 10;
		$temp = $this->_getGroupSortData($groupSort,$gourpClass,$num);

		foreach ($temp as $key=>$value) {
			$temp[$key] = $this->_cookData($value);
		}
		return $temp;
	}
	
	function _getGroupSortData($groupSort,$gourpClass,$num) {
		$groupDAO = L::loadDB('colonys', 'colony');
		return $groupDAO->getSortByTypeAndClassId($groupSort,$gourpClass,$num);
	}
	
	
	function _cookData($data) {
		global $db_bbsurl;
		$data['url'] = $db_bbsurl.'/apps.php?q=group&cyid='.$data['id'];
		$data['title'] = $data['cname'];
		$data['image'] = $this->_getGroupImage($data['cnimg']);
		$data['descrip'] = substrs(strip_tags(stripWindCode($data['descrip'])),100);
		if ($data['credit']) $data['credit'] = (int) $data['credit'];
		return $data;
	}
	
	function _getGroupImage($cnimg) {
		if (!$cnimg) {
			global $imgpath;
			return $imgpath.'/g/groupnopic.gif';
		}
		list($cnimg) = geturl("cn_img/$cnimg",'lf');
		return $cnimg;
	}

	function getSourceConfig() {
		return array(
			'groupsort' 	=> array(
				'name' 	=> '群組排行',
				'type'	=> 'select',
				'value'	=> array(
					'tnum'		=>	'主題排行',
					'pnum'		=>	'發帖排行',
					'members'	=>	'成員排行',
					//'todaypost'	=>	'今日發帖排行',
					'createtime'=>	'最新群組',
					'credit'=>	'群組積分排行',
				),
			),
			'groupclass' 	=> array(
				'name' 	=> '群組分類',
				'type'	=> 'select',
				'value'	=> $this->_getGroupClass(),
			),
		);
	}
	
	function _getGroupClass() {
		require_once(R_P.'apps/groups/lib/groupstyle.class.php');
		
		$temp = array();
		$groupStyle = new GroupStyle();
		$firstGradeStyleIds	= $groupStyle -> getFirstGradeStyles();
		
		$secondGradeStyles  = $groupStyle -> getGradeStylesByUpid(array_keys($firstGradeStyleIds));
		
		$temp = array();
		$temp[] = '全部調用';
		foreach ($firstGradeStyleIds as $first) {
			$temp[$first['id']] = '&gt;&gt; '.strip_tags($first['cname']);
			$tempSecond = isset($secondGradeStyles[$first['id']]) ? $secondGradeStyles[$first['id']] : array();
			$this->_initSecondGradeStyles($temp,$tempSecond);
		}
		return $temp;
	}
	
	function _initSecondGradeStyles(&$temp,$styles) {
		foreach ($styles as $second) {
			$temp[$second['id']] = ' &nbsp;|- '.strip_tags($second['cname']);
		}
	}
	
	function _initConfig($config) {
		$temp = array();
		$temp['groupsort'] = $config['groupsort'];
		$temp['groupclass'] = $config['groupclass'];

		return $temp;
	}
	
}