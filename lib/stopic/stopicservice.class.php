<?php
/**
 * 專題業務服務類文件
 * 
 * @package STopic
 */

!defined('P_W') && exit('Forbidden');

/**
 * 專題業務服務對像
 * 
 * 提供所有專題業務操作的服務接口，包括專題本身的業務操作、專題分類操作、專題背景圖片操作等。
 *
 * @package STopic
 */
class PW_STopicService {
	/**
	 * 專題配置
	 * 
	 * @var array
	 */
	var $_stopicConfig = null;

	/**
	 * 獲取專題評論數
	 * 
	 * @return int
	 */
	function getCommentNum($stopicId) {
		$stopicId = intval($stopicId);
		if (!$stopicId) return false;
		$stopicDB = $this->_getSTopicDB();
		return $stopicDB->getCommentNum($stopicId);
	}
	
	/**
	 * 更新回複數
	 * 
	 * @param array $fieldsData
	 * @param int $commentid 
	 * @return boolean 
	 */
	function updateCommentnum($num,$stopicId) {
		$stopicId = intval($stopicId);
		if($stopicId < 1 || !$num) return false;
		$stopicDB = $this->_getSTopicDB();
		return $stopicDB->updateCommentnum($num,$stopicId);
	}
	
	/**
	 * 獲取專題可用佈局列表
	 * 
	 * @return array 專題佈局列表數組
	 */
	function getLayoutList() {
		$layoutTypes= $this->_getSTopicConfig('layoutTypes');
		$layoutList	= array ();
		foreach ( $layoutTypes as $typeName => $typeDesc ) {
			$tmp = $this->getLayoutInfo ($typeName);
			if ($tmp)
				$layoutList [$typeName] = $tmp;
		}
		return $layoutList;
	}

	/**
	 * 獲取專題佈局配置信息
	 * 
	 * @param string $typeName 佈局名稱，如type1v0,type1v1等
	 * @return array 佈局配置數組
	 */
	function getLayoutInfo($typeName) {
		$stopicConfig = $this->_getSTopicConfig ();
		$checkDir = $stopicConfig ['layoutPath'] . $typeName . "/";
		if (! is_dir ( $checkDir ))
			return false;

		foreach ( $stopicConfig ['layoutConfig'] as $checkFile ) {
			if (! is_file ( $checkDir . $checkFile ))
				return false;
		}
		$checkData = array ();
		$checkData ['logo'] = $stopicConfig ['layoutBaseUrl'] . $typeName . "/" . $stopicConfig ['layoutConfig'] ['logo'];
		$checkData ['html'] = $checkFile . $stopicConfig ['layoutConfig'] ['html'];
		$checkData ['desc'] = $stopicConfig ['layoutTypes'] [$typeName];
		return $checkData;
	}
	
	/**
	 * 獲取專題默認的風格樣式css配置
	 * 
	 * @param string $defaultStyle 默認風格樣式名，默認值為baby_org
	 * @return array 風格樣式css配置數組
	 */
	function getLayoutDefaultSet($defaultStyle = 'baby_org') {
		$styleConfig = $this->getStyleConfig('baby_org');
		if (empty($styleConfig)) { 
			return $this->_getSTopicConfig('layout_set');
		} else {
			$layoutSet = $styleConfig['layout_set'];
			$layoutSet['bannerurl'] = $this->getStyleBanner($defaultStyle);
			return $layoutSet;
		}
	}

	/**
	 * 獲取專題的風格樣式css配置
	 * 
	 * @param string $style 風格樣式名
	 * @return array 風格樣式css配置數組
	 */
	function getLayoutSet($style) {
		$stylePath = $this->_getSTopicConfig('stylePath');
		if ($style && is_dir($stylePath.$style)) {
			return $this->getStyleConfig($style,'layout_set');
		}
		return $this->getLayoutDefaultSet();
	}

	/**
	 * 獲取風格樣式列表
	 * 
	 * @return array 風格樣式列表數組
	 */
	function getStyles() {
		$stylePath = $this->_getSTopicConfig('stylePath');
		$fp	= opendir($stylePath);
		$styles	= array();
		while ($styleDir = readdir($fp)) {
			if (in_array($styleDir,array('.','..')) || strpos($styleDir,'.')!==false) continue;
			$styles[$styleDir] = array(
				'name'=>$this->getStyleConfig($styleDir,'name'),
				'minipreview'=>$this->getStyleMiniPreview($styleDir),
				'preview'=>$this->getStylePreview($styleDir),
			);
		}
		return $styles;
	}

	/**
	 * 獲取風格樣式預覽縮略圖url
	 * 
	 * @param string $style 風格樣式名
	 * @return string 縮略圖url
	 */
	function getStyleMiniPreview($style) {
		return $this->_getSTopicConfig('styleBaseUrl').$style.'/'.$this->_getSTopicConfig('styleMiniPreview');
	}

	/**
	 * 獲取風格樣式預覽圖url
	 * 
	 * @param string $style 風格樣式名
	 * @return string 預覽圖url
	 */
	function getStylePreview($style) {
		return $this->_getSTopicConfig('styleBaseUrl').$style.'/'.$this->_getSTopicConfig('stylePreview');
	}
	
	/**
	 * 獲取風格樣式的橫幅圖片url
	 * 
	 * @param string $style 風格樣式名
	 * @return string 橫幅圖片url
	 */
	function getStyleBanner($style) {
		$temp = $this->getStyleConfig($style,'banner');
		if ($temp) {
			if (strpos($temp,'http')===false) {
				$temp = $GLOBALS['db_bbsurl'].'/'.$temp;
			}
			return $temp;
		}
		if ($style && file_exists($this->_getSTopicConfig('stylePath').$style.'/'.$this->_getSTopicConfig('styleBanner'))) {
			return $this->_getSTopicConfig('styleBaseUrl').$style.'/'.$this->_getSTopicConfig('styleBanner');
		}
		return 'http://';
	}
	
	/**
	 * 獲取風格樣式的配置
	 * 
	 * @param string $style 風格樣式名
	 * @param string $key 風格樣式的配置項
	 * @return mixed 如果$key為空則返回風格樣式配置數組，否則返回指定項的配置值
	 */
	function getStyleConfig($style,$key='') {
		static $styles = array();
		if (!isset($styles[$style])) {
			$stylePath = $this->_getSTopicConfig('stylePath');
			if (file_exists($stylePath.$style.'/config.php')) {
				$styles[$style] = include S::escapePath($stylePath.$style."/config.php");
			} else {
				$styles[$style] = array();
			}
		}
		if ($key) {
			return isset($styles[$style][$key]) ? $styles[$style][$key] : '';
		}
		return $styles[$style];
	}

	/**
	 * 生成專題html文件
	 * 
	 * @param int $stopic_id 專題id
	 * @return null
	 */
	function creatStopicHtml($stopic_id) {
		global $db_charset,$wind_version,$db_bbsurl,$db_htmifopen;
		$stopic	= $this->getSTopicInfoById($stopic_id);
		if (!$stopic) return false;
		$tpl_content	= $this->getStopicContent($stopic_id,0);
		@extract($stopic, EXTR_SKIP);
		if (defined('A_P')) {
			include(A_P.'template/stopic.htm');
		} else {
			include(R_P.'apps/stopic/template/stopic.htm');
		}
		$output = str_replace(array('<!--<!---->','<!---->'),array('',''),ob_get_contents());
		ob_end_clean();
		$stopicDir	= $this->getStopicDir($stopic_id, $stopic['file_name']);
		$output = parseHtmlUrlRewrite($output, $db_htmifopen);
		pwCache::writeover($stopicDir,$output);
		ObStart();
	}

	/**
	 * 新增專題
	 * 
	 * @param $fieldsData 專題數據數組，對應數據庫中字段
	 * @return int 專題id，失敗則返回0
	 */
	function addSTopic($fieldsData) {
		if (!is_array($fieldsData) || !count($fieldsData)) return 0;
		$fieldsData['create_date'] = time();

		$stopicDB = $this->_getSTopicDB();
		$stopicPicturesDB = $this->_getSTopicPicturesDB();
		$stopicId = $stopicDB->add($fieldsData);
		//if ($stopicId && isset($fieldsData['copy_from']) && $fieldsData['copy_from']) $stopicDB->increaseField($fieldsData['copy_from'], 'used_count');
		if ($stopicId && isset($fieldsData['bg_id']) && $fieldsData['bg_id']) $stopicPicturesDB->increaseField($fieldsData['bg_id'], 'num');
		return $stopicId;
	}

	/**
	 * 刪除多個專題
	 * 
	 * @param array $stopicIds 專題id數組
	 * @return int 刪除個數
	 */
	function deleteSTopics($stopicIds) {
		$success = 0;
		foreach ( $stopicIds as $stopicId ) {
			$success += $this->deleteSTopicById ( $stopicId );
		}
		return $success;
	}

	/**
	 * 刪除單個專題
	 * 
	 * @param int $stopicId 專題id
	 * @return bool 是否成功
	 */
	function deleteSTopicById($stopicId) {
		$stopicDB = $this->_getSTopicDB();
		$stopicPicturesDB = $this->_getSTopicPicturesDB();
		$stopicUnitDB = $this->_getSTopicUnitDB();

		$stopicData = $stopicDB->get($stopicId);
		if (null == $stopicData) return false;
		$isSuccess = (bool) $stopicDB->delete($stopicId);
		if ($isSuccess && $stopicData['bg_id']) $stopicPicturesDB->increaseField($stopicData['bg_id'], 'num', -1);
		if ($isSuccess) {
			$stopicUnitDB->deleteAll($stopicId);
			$this->_delFile($this->getStopicDir($stopicId, $stopicData['file_name']));
		}
		return $isSuccess;
	}

	/**
	 * 刪除文件
	 * 
	 * @access protected
	 * @see P_unlink
	 * @param string $fileName 文件名
	 * @return bool 是否成功
	 */
	function _delFile($fileName) {
		return P_unlink($fileName);
	}

	/**
	 * 更新專題記錄
	 * 
	 * @param int $stopicId 專題id
	 * @param array $updateData 要更新的數據數組
	 * @return bool 是否有更新
	 */
	function updateSTopicById($stopicId, $updateData) {
		$stopicDB = $this->_getSTopicDB();
		$stopicPicturesDB = $this->_getSTopicPicturesDB();
		$stopicData = $stopicDB->get($stopicId);
		if (null == $stopicData) return false;

		$isSuccess = (bool) $stopicDB->update($stopicId,$updateData);
		if (isset($updateData['bg_id']) && $updateData['bg_id'] != $stopicData['bg_id']) {
			if ($stopicData['bg_id']) $stopicPicturesDB->increaseField($stopicData['bg_id'], 'num', -1);
			if ($updateData['bg_id']) $stopicPicturesDB->increaseField($updateData['bg_id'], 'num');
		}
		if (isset($updateData['file_name'])) {
			$stopicDB->updateFileName($stopicId, $updateData['file_name']);
			if ($updateData['file_name'] != $stopicData['file_name'] && '' != $stopicData['file_name']) {
				$this->_delFile($this->getStopicDir($stopicId, $stopicData['file_name']));
			}
		}
		return $isSuccess;
	}

	/**
	 * 獲取專題信息
	 * 
	 * @param $stopicId
	 * @return array|null 專題數據數組
	 */
	function getSTopicInfoById($stopicId) {
		$stopicDB = $this->_getSTopicDB();

		$stopic = $stopicDB->get($stopicId);
		if ($stopic) $stopic['bg_url'] = $stopic['bg_id'] ? $this->_getBackgroundUrl($stopic['bg_id']) : "";

		return $stopic;
	}

	/**
	 * 獲取空專題（專題未添加任何內容）
	 * 
	 * @return array|null 專題數據數組
	 */
	function getEmptySTopic() {
		$stopicDB = $this->_getSTopicDB();
		$stopic = $stopicDB->getEmpty();
		return $stopic;
	}

	/**
	 * 獲取專題個數
	 * 
	 * @param string $keyword 查詢關鍵字
	 * @param int $categoryId 分類id
	 * @return int
	 */
	function countSTopic($keyword = '', $categoryId = 0) {
		$stopicDB = $this->_getSTopicDB();
		return $stopicDB->countByKeyWord ($keyword, $categoryId);
	}

	/**
	 * 分頁查詢專題
	 * 
	 * @param int $page 第幾頁
	 * @param int $perPage 每頁記錄數
	 * @param string $keyword 關鍵字
	 * @param int $categoryId 分類id
	 * @return array 專題數據二維數組
	 */
	function findSTopicInPage($page, $perPage, $keyword = '', $categoryId = 0) {
		$stopicDB = $this->_getSTopicDB();
		$page = intval ( $page );
		$perPage = intval ( $perPage );
		if ($page <= 0 || $perPage <= 0) return array ();
		$result	= $stopicDB->findByKeyWordInPage($page, $perPage, $keyword, $categoryId);
		foreach ($result as $key=>$value) {
			$result[$key]['url'] = $this->getStopicUrl($value['stopic_id'], $value['file_name']);
			$result[$key]['create_date'] = get_date($value['create_date']);
		}
		return $result;
	}

	/**
	 * 根據分類分頁獲取有效的專題列表
	 * 
	 * @param int $page 第幾頁
	 * @param int $perPage 每頁記錄數
	 * @param int $categoryId 分類id
	 * @return array 專題數據二維數組
	 */
	function findValidCategorySTopicInPage($page, $perPage, $categoryId = 0) {
		$stopicDB = $this->_getSTopicDB();
		$page = intval ( $page );
		$perPage = intval ( $perPage );
		if ($page <= 0 || $perPage <= 0)
			return array ();

		return $stopicDB->findValidByCategoryIdInPage ( $page, $perPage, $categoryId );
	}

	/**
	 * 根據分類分頁獲取使用率高的專題列表
	 * 
	 * @param int $limit 個數
	 * @param int $categoryId 分類id
	 * @return array 專題數據二維數組
	 */
	function findUsefulSTopicInCategory($limit, $categoryId = 0) {
		$stopicDB = $this->_getSTopicDB();
		$limit = intval ( $limit );
		if ($limit <= 0) return array ();

		return $this->_lardBackground($stopicDB->findByCategoryIdOrderByUsedInPage(1, $limit, $categoryId));
	}

	/**
	 * 得到和生成專題數據存放目錄
	 * 
	 * @param int $stopic_id 專題id
	 * @param string $file_name 文件名
	 * @return string 文件路徑
	 */
	function getStopicDir($stopic_id, $file_name='') {
		$stopic_id = (int) $stopic_id;
		if (!$stopic_id) return false;
		if ('' == $file_name) $file_name = $stopic_id;
		$stopicDir	= S::escapePath($this->_getSTopicConfig('htmlDir'));
		if (!file_exists($stopicDir)) {
			if (mkdir($stopicDir)) {
				@chmod($stopicDir,0777);
			} else {
				showmsg('stopic_htm_is_not_777');
			}
		}
		return $stopicDir.'/'.$file_name.$this->_getSTopicConfig('htmlSuffix');
	}

	/**
	 * 獲取專題的url
	 * 
	 * @param int $stopic_id 專題id
	 * @param string $file_name 文件名
	 * @return string|bool 專題url
	 */
	function getStopicUrl($stopic_id, $file_name) {
		if ('' == $file_name) return false;
		$stopicDir = $this->getStopicDir($stopic_id, $file_name);
		if ($stopicDir && file_exists($stopicDir)) {
			return $this->_getSTopicConfig('htmlUrl').'/'.$file_name.$this->_getSTopicConfig('htmlSuffix');
		} else {
			return false;
		}
	}

	/**
	 * 獲取專題的html內容
	 * 
	 * @param int $stopic_id 專題id
	 * @param bool $ifadmin 是否是後台管理時使用的html
	 * @return string
	 */
	function getStopicContent($stopic_id,$ifadmin) {
		$stopic	= $this->getSTopicInfoById($stopic_id);
		$units	= $this->getStopicUnitsByStopicId($stopic_id);
		$blocks	= $this->getBlocks();

		$parseStopicTpl	= L::loadClass('ParseStopicTpl','stopic');
		$tpl_content	= $parseStopicTpl->exute($this,$stopic,$units,$blocks,$ifadmin);
		return $tpl_content;
	}
	
	/**
	 * 專題保存的文件是否被使用
	 * 
	 * @param int $stopicId 專題id
	 * @param string $fileName 文件名
	 * @return bool
	 */
	function isFileUsed($stopicId, $fileName) {
		$stopicId = intval($stopicId);
		$stopicDB = $this->_getSTopicDB();
		$isFind = $stopicDB->getByFileNameAndExcept($stopicId, $fileName);
		return $isFind && file_exists($this->getStopicDir($stopicId, $fileName));
	}

	/**
	 * 新增一個專題分類
	 *
	 * @param array $fieldData 專題分類數據數組
	 * @return int 分類id，失敗返回0
	 */
	function addCategory($fieldData) {
		$stopicCategoryDB = $this->_getSTopicCategoryDB();
		return $stopicCategoryDB->add($fieldData);
	}

	/**
	 * 更新一個專題分類
	 *
	 * @param array $fieldData 專題分類數據數組
	 * @param int $categoryId 分類id
	 * @return int|null 是否更新成功
	 */
	function updateCategory($fieldData, $categoryId) {
		$stopicCategoryDB = $this->_getSTopicCategoryDB();
		$categoryId = intval ( $categoryId );
		if ($categoryId<= 0) {
			return NULL;
		}
		return $stopicCategoryDB->update($fieldData,$categoryId);
	}

	/**
	 * 刪除一個專題分類 同時更新背景分類
	 *
	 * @param int $categoryId 分類id
	 * @return int
	 */
	function deleteCategory($categoryId) {
		$stopicPicturesDB = $this->_getSTopicPicturesDB();
		$stopicCategoryDB = $this->_getSTopicCategoryDB();

		$categoryId = intval ( $categoryId );
		if ($categoryId <= 0 || ! $this->isAllowDeleteCategory ( $categoryId )) {
			return NULL;
		}
		return ($stopicCategoryDB->delete ( $categoryId )) ? $stopicPicturesDB->updateByCategoryId ( array("categoryid"=>0),$categoryId ) : NULL;
	}

	/**
	 * 是否允許刪除分類
	 * 
	 * 默認專題不能刪除/分類下如果有專題不能刪除
	 *
	 * @param int $categoryId 分類id
	 * @return bool
	 */
	function isAllowDeleteCategory($categoryId) {
		$stopicDB = $this->_getSTopicDB();
		$stopicCategoryDB = $this->_getSTopicCategoryDB();
		if ($stopicDB->countByCategoryId($categoryId)) return false;
		$category = $stopicCategoryDB->get($categoryId);
		if (!$category || $category['status'] == 1) return false;
		return true;
	}

	/**
	 * 獲取所有專題分類
	 *
	 * @return array 專題分類二維數組
	 */
	function getCategorys() {
		$stopicCategoryDB = $this->_getSTopicCategoryDB();
		return $stopicCategoryDB->gets ();
	}

	/**
	 * 獲取某個分類信息
	 *
	 * @param int $categoryId 分類id
	 * @return array 專題分類數據數組
	 */
	function getCategory($categoryId) {
		$stopicCategoryDB = $this->_getSTopicCategoryDB();
		return $stopicCategoryDB->get ( $categoryId );
	}

	/**
	 * 分類名是否存在
	 * 
	 * @param string $categoryName 分類名稱
	 * @return bool
	 */
	function isCategoryExist($categoryName) {
		$stopicCategoryDB = $this->_getSTopicCategoryDB();
		return $stopicCategoryDB->getByName($categoryName) ? true : false;
	}

	/**
	 * 上傳背景圖片 並增加一條圖片 記錄
	 *
	 * @param array $fileArray 上傳文件數據數組
	 * @return string 文件名如[20090819152809.jpg]
	 */
	function uploadPicture($fileArray, $categoryId, $creator) {
		$stopicPicturesDB = $this->_getSTopicPicturesDB();
		$uploadPictureClass = $this->_setUploadPictureClass();
		if (count ( $fileArray ) < 0 || intval ( $categoryId ) < 0 || trim ( $creator ) == "") {
			return null;
		}
		$filename = $uploadPictureClass->upload ( $fileArray );
		if ($filename === FALSE) {
			return null;
		}
		$fieldData = array (
			"title" => time(),
			"categoryid" => intval($categoryId),
			"path" => trim ($filename),
			"creator" => $creator
		);
		return $stopicPicturesDB->add ( $fieldData );
	}

	/**
	 * 獲取文件上傳類
	 * 
	 * @access protected
	 */
	function _setUploadPictureClass() {
		$tempUpdatePicture = L::loadClass('UpdatePicture');
		$tempUpdatePicture->init($this->_getSTopicConfig ('bgUploadPath'));
		return $tempUpdatePicture;
		//return new UpdatePicture ($this->_getSTopicConfig ('bgUploadPath'));
	}

	/**
	 * 更新背景圖片
	 *
	 * @param int $fieldData 背景圖片數據數組
	 * @param int $pictureId 背景圖片if
	 * @return int|null
	 */
	function updatePicture($fieldData, $pictureId) {
		$stopicPicturesDB = $this->_getSTopicPicturesDB();
		$pictureId = intval ( $pictureId );
		if ($pictureId <= 0) {
			return NULL;
		}
		return $stopicPicturesDB->update($fieldData,$pictureId);
	}

	/**
	 * 刪除背景圖片 刪除數據並刪除物理圖片
	 *
	 * @param int $pictureId
	 * @return int|null
	 */
	function deletePicture($pictureId) {
		$stopicPicturesDB = $this->_getSTopicPicturesDB();
		$uploadPictureClass = $this->_setUploadPictureClass();
		$pictureId = intval ( $pictureId );
		if ($pictureId <= 0) return null;
		if (!$this->isAllowDeletePicture($pictureId)) return null;
		$picture = $stopicPicturesDB->get($pictureId);
		if (!$picture) return null;
		return ($stopicPicturesDB->delete ( $pictureId )) ? $uploadPictureClass->delete ( $picture ['path'] ) : "";
	}

	/**
	 * 是否允許刪除背景圖片
	 *
	 * @param int $pictureId
	 * @return bool
	 */
	function isAllowDeletePicture($pictureId) {
		$stopicDB = $this->_getSTopicDB();
		return $stopicDB->countByBackgroundId($pictureId) ? false : true;
	}

	/**
	 * 獲取背景圖片
	 *
	 * @param int $categoryId 分類id，為0則找所有
	 * @return array
	 */
	function getPictures($categoryId = 0) {
		$stopicPicturesDB = $this->_getSTopicPicturesDB();
		$categoryId = intval ( $categoryId );
		if ($categoryId < 0) return array();

		return $this->_lardBackground( $categoryId
			? $stopicPicturesDB->getsByCategoryId ($categoryId)
			: $stopicPicturesDB->gets() );
	}

	/**
	 * 分頁獲取專題背景圖片
	 * 
	 * @param int $page
	 * @param int $perPage
	 * @param int $categoryId 分類id
	 * @return array 背景圖片二維數組
	 */
	function getBackgroundsInPage($page, $perPage, $categoryId=0) {
		$stopicPicturesDB = $this->_getSTopicPicturesDB();
		return $this->_lardBackground($stopicPicturesDB->getsInPage($page, $perPage, $categoryId));
	}

	/**
	 * 獲取系統默認背景圖片和用戶上傳的背景圖片列表
	 * 
	 * 系統默認背景圖片的id為負數，如-1,-2
	 * 
	 * @param int $categoryId 分類id
	 * @return array 背景圖片二維數組
	 */
	function getPicturesAndDefaultBGs($categoryId = 0) {
		$defaults = $this->_getDefaultBackGrounds();
		$thisTypePictures = $this->getPictures($categoryId);
		return array_merge($defaults,$thisTypePictures);
	}

	/**
	 * 獲取背景圖片url
	 * 
	 * @access protected
	 * @param int $bgId 背景圖片id
	 * @return string
	 */
	function _getBackgroundUrl($bgId) {
		$stopicPicturesDB = $this->_getSTopicPicturesDB();
		if ($bgId<0) return $this->_getDefaultBackgroundUrl($bgId);

		$bg = $stopicPicturesDB->get($bgId);
		return $bg['path'] ? $this->_getSTopicConfig ('bgBaseUrl') . $bg ['path'] : "";
	}

	/**
	 * 獲取系統默認背景圖片url
	 * 
	 * @access protected
	 * @param int $bgId 背景圖片id
	 * @return string
	 */
	function _getDefaultBackgroundUrl($bgId) {
		$bgId = (int) $bgId;
		$bgId = abs($bgId);
		if (file_exists($this->_getSTopicConfig('bgDefalutPath').$bgId.'.jpg')) {
			return $this->_getSTopicConfig('bgDefalutUrl').$bgId.'.jpg';
		}
		return '';
	}

	/**
	 * 獲取系統默認背景圖片列表
	 * 
	 * @access protected
	 * @return array
	 */
	function _getDefaultBackGrounds() {
		$backPath = $this->_getSTopicConfig('bgDefalutPath');
		$fp	= opendir($backPath);
		$backs	= array();

		while ($back = readdir($fp)) {
			if (in_array($back,array('.','..')) || !strpos($back,'.jpg')) continue;
			$id	= $this->_getDefaultBackGroudId($back);
			$backs[] = array(
				'id'	=> $id,
				'categoryid'	=> 'defalut',
				'thumb_url'	=> $this->_getDefaultBackgroundUrl($id)
			);
		}
		return $backs;
	}

	/**
	 * 獲取系統默認背景圖片id
	 * 
	 * 目前的轉換規則為圖片名轉為負數即為圖片id
	 * 
	 * @access protected
	 * @param string $filename 系統默認背景圖片名
	 * @return int
	 */
	function _getDefaultBackGroudId($filename) {
		$temp = (int) $filename;
		if (!$temp || $temp<0) return false;
		return 0-$temp;
	}

	/**
	 * 統計背景圖片個數
	 *
	 * @param int $categoryId 分類id，為0則統計所有
	 * @return int
	 */
	function countPictures($categoryId = 0) {
		$stopicPicturesDB = $this->_getSTopicPicturesDB();
		return $categoryId ? $stopicPicturesDB->countByCategoryId($categoryId) : $stopicPicturesDB->count();
	}

	/**
	 * 加工背景圖片數據
	 * 
	 * 數據中新增背景圖片url
	 * 
	 * @access protected
	 * @param array $bgList 背景圖片數組
	 * @return array
	 */
	function _lardBackground($bgList) {
		foreach ($bgList as $key => $bg) {
			$bgList[$key]['thumb_url'] = $bg['path'] ? $this->_getSTopicConfig('bgBaseUrl') . "thumb_" . $bg ['path'] : "";
		}
		return $bgList;
	}

	/**
	 * 獲取模塊類型列表
	 * 
	 * @return 模塊類型列表
	 */
	function getBlocks() {
		return $this->_getSTopicConfig('blockTypes');
	}

	/**
	 * 獲取模塊類型
	 * 
	 * @param string $typeId 模塊類型名
	 * @return array
	 */
	function getBlockById($typeId) {
		$blockTypes = $this->_getSTopicConfig('blockTypes');
		return $blockTypes[$typeId];
	}

	/**
	 * 添加專題模塊
	 * 
	 * @param array $fieldData 模塊數據數組
	 * @return int 模塊id
	 */
	function addUnit($fieldData) {
		$stopicUnitDB = $this->_getSTopicUnitDB();
		return $stopicUnitDB->add($fieldData);
	}

	/**
	 * 更新模塊數據
	 * 
	 * @param int $stopic_id 專題id
	 * @param string $html_id 模板在html中的id
	 * @param array $fieldData 更新數據
	 * @return bool 是否更新成功
	 */
	function updateUnitByFild($stopic_id,$html_id,$fieldData) {
		$stopicUnitDB = $this->_getSTopicUnitDB();
		return $stopicUnitDB->updateByFild($stopic_id,$html_id,$fieldData);
	}

	/**
	 * 刪除多個專題模塊
	 * 
	 * @param int $stopic_id 專題id
	 * @param array $html_ids 模塊id數組
	 * @return int 刪除個數
	 */
	function deleteUnits($stopic_id,$html_ids) {
		$stopicUnitDB = $this->_getSTopicUnitDB();
		return $stopicUnitDB->deletes($stopic_id,$html_ids);
	}

	/**
	 * 獲取專題模塊列表
	 * 
	 * @param int $stopic_id 專題id
	 * @return array
	 */
	function getStopicUnitsByStopicId($stopic_id) {
		$stopicUnitDB = $this->_getSTopicUnitDB();
		return $stopicUnitDB->getStopicUnits($stopic_id);
	}

	/**
	 * 獲取專題的一個模塊數據
	 * 
	 * @param int $stopic_id 專題id
	 * @param string $html_id 模塊id
	 * @return array
	 */
	function getStopicUnitByStopic($stopic_id,$html_id) {
		$stopicUnitDB = $this->_getSTopicUnitDB();
		return $stopicUnitDB->getByStopicAndHtml($stopic_id,$html_id);
	}

	/**
	 * 獲取模塊html模板內容
	 * 
	 * @param array $block_data 模塊填充數據
	 * @param string $block_type 模塊類型
	 * @param int $block_id 模塊id
	 * @return string
	 */
	function getHtmlData($block_data, $block_type, $block_id=null) {
		$block_job = 'show';
		include S::escapePath(A_P."/template/admin/block/$block_type.htm");
		$output = ob_get_contents();
		ob_clean();
		return $output;
	}

	/**
	 * 返回專題數據庫操作對像
	 * 
	 * @access protected
	 * @return PW_STopicDB
	 */
	function _getSTopicDB() {
		return L::loadDB('STopic', 'stopic');
	}
	
	/**
	 * 返回專題背景圖片數據庫操作對像
	 * 
	 * @access protected
	 * @return PW_STopicPicturesDB
	 */
	function _getSTopicPicturesDB() {
		return L::loadDB('STopicPictures', 'stopic');
	}
	
	/**
	 * 返回專題分類數據庫操作對像
	 * 
	 * @access protected
	 * @return PW_STopicCategoryDB
	 */
	function _getSTopicCategoryDB() {
		return L::loadDB('STopicCategory', 'stopic');
	}
	
	/**
	 * 返回專題模塊數據庫操作對像
	 * 
	 * @access protected
	 * @return PW_STopicUnitDB
	 */
	function _getSTopicUnitDB() {
		return L::loadDB('STopicUnit', 'stopic');
	}

	/**
	 * 獲取專題配置
	 * 
	 * @access protected
	 * @param string $key 獲取專題配置的項，為空則獲取所有
	 * @return mixed 配置值
	 */
	function _getSTopicConfig($key = '') {
		if (null == $this->_stopicConfig) {
			$this->_stopicConfig = include A_P."config.php";
		}
		if ($key) {
			return $this->_stopicConfig[$key];
		}
		return $this->_stopicConfig;
	}
}

