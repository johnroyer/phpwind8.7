<?php
!defined('P_W') && exit('Forbidden');

/**
 * 公用驗證器
 */
class PW_Validator {
	
	/**
	 * 驗證字符串
	 * 
	 * @param string $data 數據字符串
	 * @param string $type 類型
	 * @return bool 是否合法
	 */
	function validate($data, $type) {
		if (empty($type)) return false;
		if (null !== ($reg = PW_Validator::_getRegValidator($type))) {
			return PW_Validator::_validateByReg($data, $reg);
		} elseif (null !== ($specify = PW_Validator::_getSpecifyValidator($type))) {
			return PW_Validator::_validateBySpecify($data, $specify);
		}
		return false;
	}
	
	/**
	 * 獲取正則驗證器
	 * 
	 * @access protected
	 * @param string $type 類型
	 * @return string|null 正則表達式
	 */
	function _getRegValidator($type) {
		$regValidateConfig = array(
			"username" => "/^[a-zA-Z0-9_]{4,20}$/",
			"email" => "/^[-a-zA-Z0-9_\.]+@([0-9A-Za-z][0-9A-Za-z-]+\.)+[A-Za-z]{2,5}$/",
			"url" => "/^http:\/\/[A-Za-z0-9]*\.[A-Za-z0-9]*[\/=\?%\-&_~@\.A-Za-z0-9]*$/",
		);
		return isset($regValidateConfig[$type]) ? $regValidateConfig[$type] : null;
	}
	
	/**
	 * 獲取定制的驗證器
	 * @param string $type 類型
	 * @return Object 定制的驗證器對像
	 */
	function _getSpecifyValidator($type) {
		return L::loadClass('Validate' . ucfirst(strtolower($type)), 'utility/validate/specify');
	}
	
	/**
	 * 通過正則驗證
	 * 
	 * @param string $data 數據
	 * @param string $reg 正則
	 * @return bool 是否合法
	 */
	function _validateByReg($data, $reg) {
		return (bool) preg_match($reg, $data);
	}
	/**
	 * 通過定制的驗證器驗證
	 * 
	 * @param string $data 數據
	 * @param Object $specify 驗證器對像
	 * @return bool 是否合法
	 */
	function _validateBySpecify($data, $specify) {
		return $specify->validate($data);
	}
}
