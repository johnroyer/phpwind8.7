<?php
!defined('P_W') && exit('Forbidden');

/**
 * 地區服務層
 * @package  PW_AreasService
 * @author phpwind @2010-1-18
 */
class PW_AreasService {

	/**
	 * 添加
	 * 
	 * @param array $fieldsData 數據數組，以數據庫字段為key
	 * @return int 
	 */
	function addArea($fieldsData) {
		$fieldsData = $this->checkFieldsData($fieldsData);
		if (!S::isArray($fieldsData)) return false;
		$areaDb = $this->_getAreasDB();
		$result = $areaDb->insert($fieldsData);
		$this->setAreaCache();
		return $result;
	}

	/**
	 * 批量添加
	 * 
	 * @param array $fieldsData 二維數據數組
	 * @return int 
	 */
	function addAreas($fieldsData) {
		foreach ($fieldsData as $v) {
			$tmpData = $this->buildAddData($v);
			$fieldsDatas[] = $this->checkFieldsData($tmpData);
		}
		if (!S::isArray($fieldsDatas)) return false;
		$areaDb = $this->_getAreasDB();
		$result = $areaDb->addAreas($fieldsDatas);
		$this->setAreaCache();
		return $result;
	}
	
	/**
	 * 更新
	 * 
	 * @param array $fieldsData 數據數組，以數據庫字段為key
	 * @param int $areaid  地區ID
	 * @return boolean 
	 */
	function updateArea($fieldsData,$areaid) {
		$areaid = intval($areaid);
		$fieldsData = $this->buildAddData($fieldsData);
		$fieldsData = $this->checkFieldsData($fieldsData);
		if ($areaid < 1 || !S::isArray($fieldsData)) return false;
		$areaDb = $this->_getAreasDB();
		$result = $areaDb->update($fieldsData,$areaid);
		$this->setAreaCache();
		return $result; 
	}
	
	/**
	 * 單個刪除
	 * 
	 * @param int $areaid  地區ID
	 * @return boolean
	 */
	function deleteAreaByAreaId($areaid) {
		$areaid = intval($areaid);
		if ($areaid < 1) return false;
		$areaDb = $this->_getAreasDB();
		$result = $areaDb->delete($areaid);
		$this->setAreaCache();
		return $result; 
	}
	
	/**
	 * 批量刪除
	 * 
	 * @param array $areaids  地區IDs
	 * @return boolean
	 */
	function deleteAreaByAreaIds($areaids) {
		if(!S::isArray($areaids)) return false;
		$areaDb = $this->_getAreasDB();
		$result = $areaDb->deleteByAreaIds($areaids);
		$this->setAreaCache();
		return $result; 
	}
	
	/**
	 * 根據地區ID獲取信息
	 * 
	 * @param int $areaid  地區ID
	 * @return array
	 */
	function getAreaByAreaId($areaid) {
		$areaid = intval($areaid);
		if ($areaid < 1) return array();
		$areaDb = $this->_getAreasDB();
		return $areaDb->getAreaByAreaId($areaid);
	}
	
	function getFullAreaByAreaIds($areaids){
		if (!S::isArray($areaids)) return array();
		$areaDb = $this->_getAreasDB();
		return $areaDb->getFullAreaByAreaIds($areaids);
	}
	
	/**
	 * 根據多個地區id獲取信息
	 * @param array $areaids
	 * @return array
	 */
	function getAreasByAreadIds($areaids) {
		if (!S::isArray($areaids)) return array();
		$areaDb = $this->_getAreasDB();
		return $areaDb->getAreasByAreadIds($areaids);
	}
	
	/**
	 * 根據地區名獲取信息
	 * 
	 * @param string $areaName 地區名
	 * @return array
	 */
	function getAreaByAreaName($areaName) {
		$areaName = trim($areaName);
		if (!$areaName) return array();
		$areaDb = $this->_getAreasDB();
		return $areaDb->getAreaByAreaName($areaName);
	}
	
	/**
	 * 根據level獲取地區,暫時沒用
	 * 
	 * @param int $level  1國家2省份3市4縣區
	 * @return array
	 */
	function getAreaByAreaLevel($level) {
		$level = intval($level);
		if ($level < 1) return array();
		$areaDb = $this->_getAreasDB();
		return $areaDb->getAreaByAreaLevel($level);
	}
	
	/**
	 * 獲取多個地區的上級或者上兩級ids
	 * @param array $areaids
	 * @return array
	 */
	function getParentidByAreaids($areaids) {
		if (!S::isArray($areaids)) return array();
		$tempResult = $this->getAreasByAreadIds($areaids);
		$upids = $upperids = $tempids = array();
		foreach ($tempResult as $key => $value) {
			$upids[$key] = $tempids[] = $value['parentid'];
		}
		$tempids = array_filter(array_unique($tempids));
		if (!S::isArray($tempids)) return array($upids, $upperids);
		$anotherTempResult = $this->getAreasByAreadIds($tempids);
		foreach ($anotherTempResult as $k => $v) {
			$upperids[$k] = $v['parentid'];
		}
		return array($upids, $upperids);
	}
	
	/**
	 * 根據parent獲取地區
	 * 
	 * @param int $parentid 上一級areaid
	 * @return array
	 */
	function getAreaByAreaParent($parentid = 0) {
		$parentid = intval($parentid);
		if ($parentid < 0) return array();
		$areaDb = $this->_getAreasDB();
		return $areaDb->getAreaByAreaParent($parentid);
	}
	
	/**
	 * 組裝單個下拉框
	 * 
	 * @param int $parentid 上一級areaid
	 * @param int $defaultValue 默認選中值的id 
	 * @return array
	 */
	function getAreasSelectHtml($parentid = null, $defaultValue = null) {
		$parentid = intval($parentid);
		if ($parentid < 0) return null;
		$areas = $this->getAreaByAreaParent($parentid);
		if (!S::isArray($areas)) return null;
		$areaSelect = '';
		foreach ($areas as $value) {
			$selected = ($defaultValue && $value['areaid'] == $defaultValue) ? 'selected' : '';
			$areaSelect .= "<option value=\"$value[areaid]\" $selected>{$value[name]}</option>\r\n";
		}
		return $areaSelect;
	}

	/**
	 * 獲取數據庫中所有地區
	 * @return array
	 */
	function getAllAreas() {
		$areaDb = $this->_getAreasDB();
		return $areaDb->getAllAreas();
	}
	
	/**
	 * 構造地區select框
	 * @param array $initValues 默認選中框 。格式如：array(array('parentid'=>0,'selectid'=>'country','defaultid'=>''));
	 * 										其中parentid為上級id,selectid為select框的id,defaultid為默認選中值id
	 * @return string 組裝後字符串
	 */
	function buildAllAreasLists($initValues = array(),$forJs = false) {
		static $sHasArea = null, $sKey = 0;
		$areaString = $forJs?'':'<script type="text/javascript">';
		if (!isset($sHasArea)) {
			$areas = $this->getAllAreas();
			//if (!$areas) return false;
			!$forJs && $areaString .= "\r\n var initValues = new Array();\r\n";
			$areaString .= "var areas = new Array();\r\n";
			foreach ($areas as $value) {
				$areaString .= "areas['$value[areaid]']=['$value[name]','$value[parentid]','$value[vieworder]'];\r\n";
			}
			$sHasArea = true;
		}
		if ($initValues && S::isArray($initValues)) {
			foreach ($initValues as $v) {
				!$v['defaultid'] && $v['defaultid'] = -1;
				!$v['hasfirst'] && !$v['hasfirst'] = 0;
				!$forJs && $areaString .= "initValues[$sKey] = {'parentid':'$v[parentid]','selectid':'$v[selectid]','defaultid':$v[defaultid],'hasfirst':$v[hasfirst]};\r\n";
				$sKey++;
			}
		}
		!$forJs && $areaString .= '</script>';
		return $areaString;
	}
	
	function setAreaCache(){
		$file = D_P .'data/bbscache/areadata.js';
		$basicValue = array(array('parentid'=>0,'selectid'=>'province','defaultid'=>''));
		$data = $this->buildAllAreasLists($basicValue,true);
		$data && writeover($file,$data);
	}
	
	/**
	 *檢查數組key
	 * 
	 * @return array 檢查後$fieldsData
	 */
	function checkFieldsData($fieldsData){
		$data = array();
		if(isset($fieldsData['areaid'])) $data['areaid'] = intval($fieldsData['areaid']);
		if(isset($fieldsData['name'])) {
			$data['name'] = trim($fieldsData['name']);
			$data['name'] = trim(substrs($data['name'], 60, 'N'), ' &nbsp;');
		}
		if(isset($fieldsData['joinname'])) $data['joinname'] = trim($fieldsData['joinname']);
		if(isset($fieldsData['parentid'])) $data['parentid'] = intval($fieldsData['parentid']);
		if(isset($fieldsData['vieworder'])) $data['vieworder'] = intval($fieldsData['vieworder']);
		return $data;
	}
	
	/**
	 *根據parent獲取joinname值
	 * 
	 * @param int $parentid 上一級areaid
	 * @return array 檢查後$fieldsData
	 */
	function buildAddData($fieldsData){
		if (!isset($fieldsData['parentid']) || !$fieldsData['parentid']) {
			$fieldsData['joinname'] = $fieldsData['name'];
			return $fieldsData;
		}
		$parentData = $this->getAreaByAreaId($fieldsData['parentid']);
		$fieldsData['joinname'] = $parentData['joinname'].','.$fieldsData['name'];
		return $fieldsData;
	}
	
	/**
	 *加載dao
	 * 
	 * @return PW_AreasDB
	 */
	function _getAreasDB() {
		return L::loadDB('areas', 'utility');
	}
}