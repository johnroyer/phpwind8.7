<?php
!defined('P_W') && exit('Forbidden');

/**
 * 地區數據層
 * @package  PW_AreasDB
 * @author phpwind @2010-1-18
 */
class PW_AreasDB extends BaseDB {
	var $_tableName 	= 	'pw_areas';
	var $_primaryKey 	= 	'areaid';

	/**
	 * 添加
	 * 
	 * @param array $fieldsData 數據數組，以數據庫字段為key
	 * @return boolean
	 */
	function insert($fieldsData) {
		if(!S::isArray($fieldsData)) return false;
		return $this->_insert($fieldsData);
	}

	/**
	 * 批量添加
	 * 
	 * @param array $fieldsData
	 * @return boolean
	 */
	function addAreas($fieldsData) {
		if(!S::isArray($fieldsData)) return false;
		$this->_db->update("INSERT INTO " . $this->_tableName . " (name,joinname,parentid,vieworder) VALUES  " . S::sqlMulti($fieldsData));
		return true;
	}
	
	/**
	 * 更新
	 * 
	 * @param int $areaid  地區ID
	 * @param array $fieldsData 數據數組，以數據庫字段為key
	 * @return boolean
	 */
	function update($fieldsData,$areaid) {
		$areaid = intval($areaid);
		if($areaid < 1 || !S::isArray($fieldsData)) return false;
		return (bool)$this->_update($fieldsData,$areaid);
	}

	/**
	 * 單個刪除
	 * 
	 * @param int $areaid  地區ID
	 * @return boolean 
	 */
	function delete($areaid) {
		$areaid = intval($areaid);
		if ($areaid < 1) return false;
		return (bool)$this->_delete($areaid);
	}

	/**
	 * 批量刪除
	 * 
	 * @param array $areaids  地區IDs
	 * @return boolean
	 */
	function deleteByAreaIds($areaids) {
		if(!S::isArray($areaids)) return false;
		return (bool)pwQuery::delete($this->_tableName, "$this->_primaryKey in(:$this->_primaryKey)", array($areaids));
	}
	
	/**
	 * 根據地區ID獲取信息
	 * 
	 * @param int $areaid  地區ID
	 * @return array
	 */
	function getAreaByAreaId($areaid) {
		$areaid = intval($areaid);
		if ($areaid < 1) return array();
		return $this->_db->get_one("SELECT * FROM " . $this->_tableName . " WHERE  " . $this->_primaryKey . " = " . $this->_addSlashes($areaid));
	}
	
	/**
	 * 根據多個地區id獲取全稱
	 * @param array $areaids
	 * @return array
	 */
	function getFullAreaByAreaIds($areaids) {
		$result = array();
		$query = $this->_db->query("SELECT areaid,joinname FROM " . $this->_tableName . " WHERE " . $this->_primaryKey . " IN (" . $this->_getImplodeString($areaids) . ")");
		while ($rt = $this->_db->fetch_array($query)) {
				$result[$rt['areaid']] = $rt['joinname'];
		}
		return $result;
	}
	
	/**
	 * 根據多個地區id獲取信息
	 * @param array $areaids
	 * @return array
	 */
	function getAreasByAreadIds($areaids) {
		if (!S::isArray($areaids)) return array();
		$query = $this->_db->query("SELECT * FROM " . $this->_tableName . " WHERE " . $this->_primaryKey . " IN (" . $this->_getImplodeString($areaids) . ")");
		return $this->_getAllResultFromQuery($query, $this->_primaryKey);
	}
	/**
	 * 根據地區名獲取信息
	 * 
	 * @param string $areaName 地區名
	 * @return array
	 */
	function getAreaByAreaName($areaName) {
		$areaName = trim($areaName);
		if (!$areaName) return array();
		return $this->_db->get_one("SELECT * FROM " . $this->_tableName . " WHERE name = " . $this->_addSlashes($areaName));
	}
	
	/**
	 * 根據parent獲取地區
	 * 
	 * @param int $parent 上一級areaid
	 * @return array
	 */
	function getAreaByAreaParent($parentid) {
		$parentid = intval($parentid);
		if ($parentid < 0) return array();
		$query = $this->_db->query("SELECT * FROM  " . $this->_tableName . " WHERE parentid = " . $this->_addSlashes($parentid) . " ORDER BY vieworder ASC");
		return $this->_getAllResultFromQuery($query);
	}

	/**
	 * 獲取數據庫中所有地區
	 * @return array
	 */
	function getAllAreas() {
		$query = $this->_db->query('SELECT * FROM ' . $this->_tableName . ' ORDER BY vieworder ASC,name ASC');
		return $this->_getAllResultFromQuery($query);
	}
}