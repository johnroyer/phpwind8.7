<?php
!function_exists('readover') && exit('Forbidden');

/**
 * 用戶任務系統
 * 
 * @package UserJobs
 */
class PW_Job {
	var $_db = null;
	var $_hour = 3600;
	var $_timestamp = null;
	var $_cache = true;
	function PW_Job() {
		global $db, $timestamp;
		$this->_db = & $db;
		$this->_timestamp = $timestamp;
	}
	/*
	function run($userid, $groupid) {
		//$this->jobAutoController($userid, $groupid);
	}
	*/
	/**
	 * 檢查是否可以申請任務
	 */
	function checkApply($id, $userId, $groupid, $job = array()) {
		$id = intval($id);
		if ($id < 1) {
			return array(
				false,
				$this->getLanguage("job_id_error"),
				''
			);
		}
		$job = $job ? $job : $this->getJob($id);
		if (!$job) {
			return array(
				false,
				$this->getLanguage("job_not_exist"),
				''
			);
		}
		/*任務是否關閉*/
		if ($job['isopen'] == 0) {
			return array(
				false,
				$this->getLanguage("job_close"),
				''
			);
		}
		/*用戶組限制*/
		if (isset($job['usergroup']) && $job['usergroup'] != "") {
			if (!in_array($groupid, explode(",", $job['usergroup']))) {
				return array(
					false,
					$this->getLanguage("job_usergroup_limit"),
					''
				);
			}
		}
		/*人數限制 當前申請人數*/
		if (isset($job['number']) && $job['number'] != 0) {
			$number = $this->countJoberByJobId($job['id']);
			if ($number >= $job['number']) {
				return array(
					false,
					$this->getLanguage("job_apply_number_limit"),
					''
				);
			}
		}
		$current = $next = $this->_timestamp;
		/*時間限制 當前申請人數*/
		if ((isset($job['endtime']) && $job['endtime'] != 0 && $job['endtime'] < $current)) {
			return array(
				false,
				$this->getLanguage("job_time_limit"),
				''
			);
		}
		if ((isset($job['starttime']) && $job['starttime'] != 0 && $job['starttime'] > $current)) {
			return array(
				false,
				$this->getLanguage("job_time_early"),
				''
			);
		}
		/*是否存在前置任務*/
		if (isset($job['prepose']) && $job['prepose'] != 0) {
			$prepose = $this->getJob($job['prepose']);
			if ($prepose) {
				/*是否已經完成前置任務*/
				$jober = $this->getJoberByJobId($userId, $prepose['id']);
				if (!$jober) {
					return array(
						false,
						$this->getLanguage("job_has_perpos") . $prepose['title'],
						''
					);
				}
				if ($jober['status'] != 3) {
					return array(
						false,
						$this->getLanguage("job_has_perpos_more") . $prepose['title'],
						''
					);
				}
			}
		}
		//是否已經申請
		$hasApply = $this->getJoberByJobId($userId, $id);
		//一次性任務過濾
		if ($hasApply && $hasApply['total'] > 0 && $job['period'] < 1) {
			return array(
				false,
				"你已經完成這個任務",
				""
			);
		}
		//實名認證任務
		if (S::inArray($job['job'],array('doAuthAlipay','doAuthMobile'))) {
			global $db_authstate;
			$userService = $this->_getUserService();
			$userdb = $userService->get($userId, true, false, false);
			if (!$db_authstate)  return array(false,'站點未開啟實名認證','');
			
			if ($job['job'] == 'doAuthAlipay' && getstatus($userdb['userstatus'], PW_USERSTATUS_AUTHALIPAY)){
				return array(false,'您已經綁定支付寶','');
			}
			if ($job['job'] == 'doAuthMobile' && getstatus($userdb['userstatus'], PW_USERSTATUS_AUTHMOBILE)){
				return array(false,'您已經綁定手機號','');
			}
		}
		//任務是否為週期性任務 用用戶是否已經申請，下次開始的時間
		$again = 0;
		if (isset($job['period']) && $job['period'] != 0) {
			//如果已經申請，檢查是否到下次申請時間
			if ($hasApply && $hasApply['next'] > $current) {
				return array(
					false,
					$this->getLanguage("job_apply_next_limit"),
					''
				);
			}
			if ($hasApply && $hasApply['next'] < $current) {
				$again = 1;
			}
			$next = $current + $job['period'] * $this->_hour;
		}
		$job['next'] = $next;
		//老用戶申請限制
		if (($hasApply && $again == 0)) {
			return array(
				false,
				$this->getLanguage("job_has_apply"),
				''
			);
		}
		return array(
			true,
			$this->getLanguage("job_apply_success"),
			$job
		);
	}
	
	function appendJobDetailInfo($jobs) {
		$list = array();
		foreach ($jobs as $key => $job) {
			if ($job['isopen'] == 0)  continue;
			if ((isset($job['isuserguide']) && $job['isuserguide'])) continue; //新用戶引導增加
			$job['icon'] = (isset($job['icon']) && $job['icon'] != "") ? "attachment/job/" . $job['icon'] : "images/job/" . strtolower($job['job']) . ".gif";
			
			$reward = '';
			if (isset($job['reward'])) $reward = $this->getCategoryInfo($job['reward']);
			$job['reward'] = $reward ? $reward : array();
		
			if ($job['status'] < 2) {
				$jobClass = $this->loadJob(strtolower($job['job']));
				$job['link'] = $jobClass ? $jobClass->getUrl($job) : "";
			}
			
			if (isset($job['status'])) {
				$operationTypes = array(0 => 'start', 1 => 'start', 2 => 'gain');
				$job['operationType'] = isset($operationTypes[$job['status']]) ? $operationTypes[$job['status']] : 'start';
				if (!$job['factor']) $job['operationType'] = 'gain';
			} else {
				$job['operationType'] = 'apply';
			}
			
			$job['condition'] = $this->getCondition($job);
			$list[$key] = $job;
		}

		return $list;
	}
	
	function buildLists($joblists, $action, $userId, $groupid) {
		if (!$joblists) {
			return array();
		}
		global $winduid;
		if($userId != $winduid) return array();
		$jobs = array();
		foreach($joblists as $job) {
			//顯示條件 是否顯示
			if ($job['isopen'] == 0) {
				continue;
			}
			$lists = array();
			$lists['id'] = $job['id'];
			$lists['title'] = $job['title'];
			$lists['step'] = $job['step'];
			$lists['description'] = html_entity_decode($job['description']);
			$lists['period'] = ($job['period']) ? '每隔' . $job['period'] . '小時可以申請一次' : "一次性任務";
			$reward = '';
			if (isset($job['reward'])) {
				$reward = implode(' ', $this->getCategoryInfo($job['reward']));
			}
			$lists['reward'] = $reward ? $reward : "無";
			$lists['number'] = (isset($job['number']) && $job['number'] != 0) ? $job['number'] . "人" : "";
			$isFactor = (isset($job['factor']) && $job['factor'] != '') ? true : false;
			if ($isFactor) {
				$factor = unserialize($job['factor']);
				$lists['factor'] = $factor;
			}
			$lists['timelimit'] = (isset($factor['limit']) && $factor['limit'] != "") ? $factor['limit'] : "不限制";
			/*前置任務*/
			$prepose = $doPrepose = '';
			if (isset($job['prepose']) && $job['prepose'] != 0) {
				$prepose = $this->getJob($job['prepose']); /*是否完成*/
				$prepose = "(必須完成 " . $prepose['title'] . " 才能申請)";
				$preposeJob = $this->getJoberByJobId($userId, $job['prepose']);
				$doPrepose = ($preposeJob && $preposeJob['total'] > 0) ? true : false;
			}
			$lists['prepose'] = $prepose ? $prepose : "";
			$lists['icon'] = (isset($job['icon']) && $job['icon'] != "") ? "attachment/job/" . $job['icon'] : "images/job/" . strtolower($job['job']) . ".gif";
			$lists['condition'] = $this->getCondition($job);
			$lists['usergroup'] = (isset($job['usergroup']) && $job['usergroup'] != '') ? $this->getUserGroup($job['usergroup']) : '';
			if ($action == "list") { /*可申請*/
				/*任務還沒有開始*/
				if (!$this->checkJobCondition($userId, $groupid, $job) || ($prepose && !$doPrepose)) { /*申請條件*/
					$lists['btn'] = $this->getJobBtn($job['id'], "apply_old");
				} else {
					$lists['btn'] = $this->getJobBtn($job['id'], "apply");
				}
			} elseif (empty($action) || $action == "applied") { /*已申請*/
				$lists['gain'] = $lists['qbtn'] = '';
				if (isset($job['status'])) {
					$status = $job['status'];
					$info = ($job['period'] > 0) ? "是否確認放棄本次任務" : "本次任務為一次性任務，放棄後將不可再次申請。是否確認放棄本次任務";
					if ($status < 2) { /*url 引導*/
						$jobClass = $this->loadJob(strtolower($job['job']));
						$link = $jobClass ? $jobClass->getUrl($job) : "";
					}
					if ($status == 0) {
						/*是否週期性*/
						$lists['btn'] = $this->getJobBtn($job['id'], "start", $link);
						($job['finish'] == 0) && $lists['qbtn'] = $this->getJobBtn($job['id'], "quit", $info); /*是否可放棄*/
					} elseif ($status == 1) {
						if ($factor && isset($factor['limit']) && $factor['limit'] > 0) {
							$jober = $this->getJoberByJobId($userId, $job['id']);
							if ($jober['last'] + $factor['limit'] * $this->_hour < $this->_timestamp) {
								$this->updateJober(array(
									'status' => 5
								), $jober['id']);
								$this->reduceJobNum($userid);
								continue;
							}
						}
						$lists['btn'] = $this->getJobBtn($job['id'], "start_old", $link);
						($job['finish'] == 0) && $lists['qbtn'] = $this->getJobBtn($job['id'], "quit", $info);
					} elseif ($status == 2) {
						$lists['gain'] = "(100%)"; /*完成提示*/
						$lists['btn'] = $this->getJobBtn($job['id'], "gain");
					}
				}
				if (!$isFactor) {
					$lists['gain'] = "(100%)";
					$lists['btn'] = $this->getJobBtn($job['id'], "gain");
					$lists['qbtn'] = '';
				}
				//任務完成進度條
				$lists['degree'] = '100%';
			} elseif ($action == "finish") { /*已完成*/
				$lists['lastfinish'] = "最後完成於 " . get_date($job['last'], "Y-m-d H:i");
				$lists['btn'] = '';
				if (isset($job['period']) && $job['period'] != 0) {
					$lists['btn'] = '<a  href="javascript:;" id="apply_' . $job['id'] . '" class="tasks_again">再次申請</a>';
				}
			} elseif ($action == "quit") { /*已放棄*/
			/*	if (isset($job['period']) && $job['period'] != 0) {
					$lists['btn'] = $this->getJobBtn($job['id'], "apply");
					
				}*/
				if (!$this->checkJobCondition($userId, $groupid, $job) || ($prepose && !$doPrepose)) { /*申請條件*/				
					$lists['btn'] = $this->getJobBtn($job['id'], "apply_old");
				} else {
					$jober = $this->getJoberByJobId($job['userid'],$job['jobid']);
					$lists['btn'] = $this->getJobBtn($job['id'], "apply_again",'',$jober['id']);
				}

				/*失敗或放棄*/
				$lists['info'] = ($job['status'] == 5) ? "任務失敗" : "放棄於 " . get_date($job['last'], "Y-m-d H:i");
			}
			$jobs[] = $lists;
		}
		return $jobs;
	}
		
	function getJobBtn($id, $k, $info = '', $jobId = null) {
		$job && $job = strtolower($job);
		$btn = array();
		$btn['apply'] = '<a href="javascript:;" class="tasks_apply" hidefocus="true" id="apply_' . $id . '">立即申請</a>';
		$btn['apply_old'] = '<a href="javascript:;" class="tasks_apply_old" hidefocus="true" title="不滿足申請條件" id="apply_' . $id . '">立即申請</a>';
		$btn['apply_again'] =  '<a href="jobcenter.php?action=reapply&step=2&id=' .$id .'&joberid=' .$jobId .'" class="tasks_apply" hidefocus="true" id="apply_' . $id . '">重新申請</a>';	
		$btn['start'] = '<a href="javascript:;" link="' . $info . '" class="tasks_startB" hidefocus="true" id="start_' . $id . '">立即開始</a>';
		$btn['start_old'] = '<a href="javascript:;" link="' . $info . '" class="tasks_startB_old" hidefocus="true" id="start_' . $id . '">立即開始</a>';
		$btn['quit'] = '<a href="javascript:;" class="tasks_quit" hidefocus="true" id="quit_' . $id . '" info="' . $info . '">放棄</a>';
		$btn['gain'] = '<a href="javascript:;"  hidefocus="true" id="gain_' . $id . '" class="tasks_receiving">領取獎勵</a>';
		return $btn[$k];
	}
	/*
	* 任務開始控制中心，獲取任務開始後鏈接
	*/
	function jobStartController($userid, $jobid) {
		$jobid = intval($jobid);
		if ($jobid < 1) {
			return array(
				false,
				"抱歉，任務ID無效",
				''
			);
		}
		//是否存在這個任務
		$job = $this->getJob($jobid);
		if (!$job) {
			return array(
				false,
				"抱歉，任務不存在",
				''
			);
		}
		$current = $this->_timestamp;
		if (isset($job['end']) && $job['end'] != 0 && $job['end'] > $current) {
			return array(
				false,
				"抱歉，你申請的任務已經結束",
				''
			);
		}
		$jober = $this->getJoberByJobId($userid, $jobid);
		if (!$jober) {
			return array(
				false,
				"抱歉，你還沒有申請這個任務",
				''
			);
		}
		if ($jober['status'] > 1) {
			return array(
				false,
				"抱歉，你已經開始了這個任務",
				''
			);
		}
		//if($jober['next']>$current){
		//	return array(false,"抱歉，還沒有到執行任務的時間",'');
		//}
		$jobClass = $this->loadJob(strtolower($job['job']));
		if (!$jobClass) {
			return array(
				false,
				"抱歉，任務有誤，請重試",
				''
			);
		}
		$link = $jobClass->getUrl($job);
		//更新任務狀態
		if ($jober['status'] == 0) {
			$this->updateJober(array(
				"status" => 1
			), $jober['id']);
		}
		return array(
			true,
			"",
			$link
		);
	}
	/*
	* 任務調度控制中心
	*/
	function jobController($userid, $jobName, $factor = array()) {
		$jobName = trim($jobName);
		$jobs = $this->getJobByJobName($jobName);
		if (!$jobs) {
			return array();
		}
		$jobIds = $tmp = array();
		foreach($jobs as $job) {
			$jobIds[] = $job['id'];
			$tmp[$job['id']] = $job;
		}
		$jobers = $this->getInProcessJobersByUserIdAndJobIds($userid, $jobIds);
		if (!S::isArray($jobers)) return false;
		$isSuccess = false;
		foreach ($jobers as $jober) { //完成所有已經開始的並且符合條件的任務
			//任務完成
			if ($jober['status'] >= 2) continue;
			$job = $tmp[$jober['jobid']]; /*當前任務*/
			if ($jober['total'] > 0 && $job['period'] < 1) continue;
			$current = $next = $this->_timestamp;
			/*是否週期性任務*/
			if (isset($job['period']) && $job['period'] != 0) {
				$next = $current + $job['period'] * $this->_hour;
			}
			$status = $this->jobFinishController($job, $jober, $factor); /*任務完成狀態*/
			if ($status == 0) continue;
			$data = array();
			//($status > 2 ) &&  $data['last'] = $current;
			$data['current'] = $jober['current'] + 1; /*當前步數*/
			$data['step'] = $jober['step'] + 1; /*總步數*/
			$data['next'] = $next; /*週期性任務下一個時間開始點*/
			$data['status'] = $status;
			$this->updateJober($data, $jober['id']);
			$isSuccess = true;
		}
		return $isSuccess;
	}
	
	function jobFinishController($job, $jober, $factor = array()) {
		if (!$factor) {
			return 2; /*為空表示直接完成任務*/
		}
		/*任務狀態*/
		/*沒有條件的任務*/
		$isFactor = (isset($job['factor']) && $job['factor'] != "") ? true : false;
		if (!$isFactor) {
			return 2;
		}
		$jobClass = $this->loadJob($job['job']);
		return $jobClass->finish($job, $jober, $factor);
	}
	function jobApplyController($userId, $jobId) {
		$job = $this->getJob($jobId);
		if (!$job) {
			return array();
		}
		$current = $this->_timestamp;
		$jober = $this->getJoberByJobId($userId, $jobId);
		if (isset($job['period']) && $job['period'] != 0) {
			$next = $current + $job['period'] * $this->_hour;
		}
		//檢查任務是否存在
		if ($jober && $jober['total'] > 0 && $job['period'] < 1) {
			return array();
		}
		if ($jober && $job['period'] > 0 && $jober['status'] > 1 && ($jober['total'] > 0 || ($jober['status'] == 4))) {
			return $this->_againJober($userId, $jobId, $next, $current, $jober);
		}
		if (!$jober) {
			return $this->_createJober($userId, $jobId, $next, $current, $jober);
		}
		return array();
	}
	function _createJober($userId, $jobId, $next, $current, $jober = array()) {
		$jober = $jober ? $jober : $this->getJoberByJobId($userId, $jobId);
		if ($jober) {
			return array();
		}
		$data = array();
		$data['jobid'] = $jobId;
		$data['userid'] = $userId;
		$data['current'] = 1; /*當前步數*/
		$data['step'] = 0; /*總步數*/
		$data['last'] = $current;
		$data['next'] = $next; /*週期性任務下一個時間開始點*/
		$data['status'] = 0;
		$data['creattime'] = $current;
		return $this->addJober($data);
	}
	function _againJober($userId, $jobId, $next, $current, $jober = array()) {
		$jober = $jober ? $jober : $this->getJoberByJobId($userId, $jobId);
		if (!$jober) {
			return array();
		}
		$data = array();
		$data['current'] = 1; /*當前步數*/
		$data['step'] = 0; /*總步數*/
		$data['last'] = $current;
		$data['next'] = $next; /*週期性任務下一個時間開始點*/
		$data['status'] = 0;
		$result = $this->updateJober($data, $jober['id']);
		if ($result) {
			$this->increaseJobNum($userId);
		}
		return $result;
	}
	/*
	* 展示控制
	*/
	function jobDisplayController($userid, $groupid, $action) {
		return $this->buildLists($this->getCanApplyJobs($userid, $groupid), $action, $userid, $groupid);
	}
	function getCanApplyJobs($userid, $groupid) {
		$joblists = $this->getJobAll();
		if (!$joblists) {
			return array();
		}
		$current = $this->_timestamp;
		$jobs = array();
		/*過濾部分*/
		$jobIds = array();
		foreach($joblists as $job) {
			$jobIds[] = $job['id'];
		}
		/*是否已經參加*/
		$joins = $this->getJobersByJobIds($userid, $jobIds);
		$jobers = array();
		if ($joins) {
			foreach($joins as $join) {
				$jobers[$join['jobid']] = $join;
			}
		}
		foreach($joblists as $job) {
			/*是否開啟任務*/
			if ($job['isopen'] == 0) {
				continue;
			}
			/*開啟符合條件才顯示*/
			if ($job['display'] == 1) {
				if (!$this->checkJobCondition($userid, $groupid, $job)) {
					continue;
				}
			}
			/*任務是否已經申請*/
			//$isApplied = $this->getJoberByJobId($userid,$job['id']);
			$isApplied = (isset($jobers[$job['id']])) ? $jobers[$job['id']] : '';
			if ($isApplied && $isApplied['status'] <= 2) {
				continue;
			}
			/*週期性任務*/
			if ($isApplied && $job['period'] == 0) {
				continue;
			}
			if ((isset($job['isuserguide']) && $job['isuserguide'])) continue; //新用戶引導增加
			if ((isset($job['endtime']) && $job['endtime'] != 0 && $job['endtime'] < $current)) {
				continue;
			}
			$jobs[] = $job;
		}
		return $jobs;
	}
	/*檢查任務條件是否符合*/
	function checkJobCondition($userId, $groupid, $job) {
		//用戶組條件限制
		if (isset($job['usergroup']) && $job['usergroup'] != '') {
			$usergroups = explode(",", $job['usergroup']);
			if (!in_array($groupid, $usergroups)) {
				return false;
			}
		}
		//申請人數條件限制
		if (isset($job['number']) && $job['number'] > 0) {
			$number = $this->countJoberByJobId($job['id']);
			if ($number >= $job['number']) {
				return false;
			}
		}
		//前置任務
		if (isset($job['prepose']) && $job['prepose'] > 0) {
			$prepose = $this->getJob($job['prepose']);
			if ($prepose) {
				$jober = $this->getJoberByJobId($userId, $prepose['id']);
				if (!$jober) {
					return false; /*前置任務沒完成*/
				}
				if ($jober['status'] != 3) {
					return false;
				}
			}
		}
		//實名認證
		if (S::inArray($job['job'],array('doAuthAlipay','doAuthMobile'))) {
			global $db_authstate;
			if (!$db_authstate) return false;
			$userService = $this->_getUserService();
			$userdb = $userService->get($userId, true, false, false);
			if ($job['job'] == 'doAuthAlipay' && getstatus($userdb['userstatus'], PW_USERSTATUS_AUTHALIPAY)){
				return false;
			}
			if ($job['job'] == 'doAuthMobile' && getstatus($userdb['userstatus'], PW_USERSTATUS_AUTHMOBILE)){
				return false;
			}
		}
		return true;
	}
	/*
	* 自動申請控制
	*/
	/*
	function jobAutoController($userid, $groupid) {
		$userid = intval($userid);
		$groupid = intval($groupid);
		if ($groupid < 1 || $userid < 1) {
			return;
		}
		if (!$jobLists = $this->_jobAutoFilterHandler($userid, $groupid)) {
			return;
		}
		$current = $this->_timestamp;
		foreach($jobLists as $job) {
			$this->_jobAutoCreateHandler($userid, $job, $current);
		}
	}
	*/
	
	/*
	* 自動申請增加一條可重複申請的用戶任務
	*/
	/*
	function _jobAutoAgainHandler($userid, $job, $current) {
		$next = $current;
		if (isset($job['period']) && $job['period'] != 0) {
			$next = $current + $job['period'] * $this->_hour;
		}
		$job['next'] = $next ? $next : $current;
		$this->_againJober($userid, $job['id'], $job['next'], $current);
	}
	*/
	/*
	* 自動申請任務過濾
	* 簡化SQL查詢次數
	* 週期性自動申請任務則直接更新/週期性人數限制任務則直接查詢
	*/
//	function _jobAutoFilterHandler($userid, $groupid) {
//		$jobs = $this->getJobsAuto();
//		if (!$jobs) {
//			return false;
//		}
//		$current = $this->_timestamp;
//		$jobLists = $jobIds = $periods = $preposes = array();
//		/*過濾任務申請*/
//		foreach($jobs as $job) {
//			/*任務狀態過濾*/
//			if ($job['isopen'] == 0) {
//				continue;
//			}
//			/*時間限制過濾*/
//			if ((isset($job['endtime']) && $job['endtime'] != 0 && $job['endtime'] < $current)) {
//				continue;
//			}
//			if ((isset($job['starttime']) && $job['starttime'] != 0 && $job['starttime'] > $current)) {
//				continue;
//			}
//			if (isset($job['usergroup']) && $job['usergroup'] != '') { /*用戶組過濾*/
//				$usergroups = explode(",", $job['usergroup']);
//				if (!in_array($groupid, $usergroups)) {
//					continue;
//				}
//			}
//			if (isset($job['period']) && $job['period'] > 0) {
//				$periods[] = $job['id']; /*週期性任務過濾*/
//			}
//			if (isset($job['prepose']) && $job['prepose'] > 0) {
//				$preposes[$job['prepose']] = $job['id']; /*前置任務過濾*/
//			}
//			/*人數限制 當前申請人數*/
//			if (isset($job['number']) && $job['number'] != 0) {
//				$number = $this->countJoberByJobId($job['id']);
//				if ($number >= $job['number']) {
//					continue;
//				}
//			}
//			$jobLists[$job['id']] = $job;
//			$jobIds[] = $job['id'];
//		}
//		if (!$jobLists) {
//			return false;
//		}
//		/*是否已經參加過，並結合是否是週期性任務*/
//		$joins = $this->getJobersByJobIds($userid, $jobIds);
//		if ($joins) {
//			foreach($joins as $join) {
//				//如果是週期性的重複任務，則直接自動更新申請
//				$t_job = array();
//				$t_job = $jobLists[$join['jobid']];
//				if (in_array($join['jobid'], $periods)) {
//					if ($join['status'] >= 3 && $join['total'] > 0) {
//						/*時間間隔計算 下一次執行時間*/
//						if ($join['next'] < $current) {
//							$this->_jobAutoAgainHandler($userid, $t_job, $current);
//						}
//					}
//				}
//				unset($t_job);
//				unset($jobLists[$join['jobid']]); /*清除已經參加的記錄，不是週期性任務*/
//			}
//		}
//		if (!$jobLists) {
//			return false;
//		}
//		/*是否有前置任務*/
//		if ($preposes) {
//			$joins = $this->getJobersByJobIds($userid, array_keys($preposes));
//			if ($joins) {
//				foreach($joins as $join) {
//					if ($join['total'] > 0) {
//						unset($preposes[$join['jobid']]); /*放過已經完成的任務*/
//					}
//				}
//			}
//			/*剩下都是些沒有完成前置任務的*/
//			if ($preposes) {
//				foreach($preposes as $jobid) {
//					unset($jobLists[$jobid]); /*過濾*/
//				}
//			}
//		}
//		return $jobLists;
//	}
	/*
	* 放棄任務控制
	*/
	function jobQuitController($userid, $jobId) {
		$jobId = intval($jobId);
		if ($jobId < 1) {
			return array(
				false,
				"任務ID無效"
			);
		}
		$job = $this->getJob($jobId);
		if (!$job) {
			return array(
				false,
				"任務不存在"
			);
		}
		if ($job['finish'] == 1) {
			return array(
				false,
				"該任務必須完成，不能放棄"
			);
		}
		$jober = $this->getJoberByJobId($userid, $jobId);
		if (!$jober) {
			return array(
				false,
				"抱歉，你還沒有申請這個任務"
			);
		}
		if ($jober && $jober['total'] > 0 && $job['period'] < 1) {
			return array(
				false,
				"抱歉，任務為一次性任務，你已經完成"
			);
		}
		if ($jober && $jober['status'] == 3) {
			return array(
				false,
				"抱歉，你已經完成這個任務"
			);
		}
		if ($jober && $jober['status'] > 1) {
			return array(
				false,
				"抱歉，請檢查是否完成任務"
			);
		}
		$result = $this->updateJoberByJobId(array(
			'status' => 4
		), $jobId, $userid);
		if (!$result) {
			return array(
				false,
				"放棄任務失敗，請重試"
			);
		}
		$this->reduceJobNum($userid);
		return array(
			true,
			"放棄任務完成"
		);
	}
	/*
	* 領取獎勵控制
	*/
	function jobGainController($userid, $jobid) {
		$jobid = intval($jobid);
		if ($jobid < 1) {
			return array(
				false,
				"抱歉，任務ID無效"
			);
		}
		//是否存在這個任務
		$job = $this->getJob($jobid);
		if (!$job) {
			return array(
				false,
				"抱歉，任務不存在"
			);
		}
		if (procLock('job_save', $userid)) {
			$jober = $this->getJoberByJobId($userid, $jobid);
			if (!$jober) {
				return $this->_unlockUserJob($userid, array(
					false,
					"抱歉，你還沒有申請這個任務"
				));
			}
			/*檢查是否是一次性任務或完成*/
			if (!$job['period'] && $jober['total'] > 1) {
				return $this->_unlockUserJob($userid, array(
					false,
					"抱歉，你已經完成這個任務"
				));
			}
			/*任務時間限制 start*/
			$timeout = 0;
			$factor = (isset($job['factor']) && $job['factor'] != "") ? unserialize($job['factor']) : array();
			if ($factor && isset($factor['limit']) && $factor['limit'] > 0) {
				if ($jober['last'] + $factor['limit'] * $this->_hour < $this->_timestamp) {
					$timeout = 1;
				}
			}
			/*下次執行時間*/
			if (isset($job['period']) && $job['period'] > 0) {
				$next = $this->_timestamp + $job['period'] * $this->_hour;
				$next = $next ? $next : $this->_timestamp;
			}
			if ($timeout) {
				$this->updateJober(array(
					'status' => 5,
					'next' => $next
				), $jober['id']);
				$this->reduceJobNum($userid);
				return $this->_unlockUserJob($userid, array(
					true,
					"抱歉，任務沒有在規定的時間內完成"
				));
			}
			/*任務時間限制 end */
			if ($factor) {
				if ($jober['status'] < 2) {
					return $this->_unlockUserJob($userid, array(
						true,
						"抱歉，你還沒有完成任務"
					));
				}
				if ($jober['status'] > 3) {
					return $this->_unlockUserJob($userid, array(
						true,
						"抱歉，數據錯誤，請重試"
					));
				}
			}
			if ($jober['status'] == 3) {
				return $this->_unlockUserJob($userid, array(
					true,
					"抱歉，你已經領取過獎勵，不能重複領取"
				));
			}

			$data = array();
			$data['status'] = 3; /*任務完成*/
			$data['total'] = $jober['total'] + 1;
			$data['next'] = $next;
			$result = $this->updateJober($data, $jober['id']);
			if (!$result) {
				return $this->_unlockUserJob($userid, array(
					false,
					"抱歉，領取獎勵失敗，請重試"
				));
			}
			if (isset($job['reward'])) {
				$this->jobRewardHandler($userid, $job);
			}
			$this->reduceJobNum($userid); /*任務完成*/
			$information = implode(' ', $this->getCategoryInfo($job['reward']));
			$information = $information ? "，" . $information : "";
			return $this->_unlockUserJob($userid, array(
				true,
				"恭喜你完成任務" . $information
			));
		} else {
			return array(
				false,
				"抱歉，領取獎勵失敗，請重試"
			);
		}
	}
	
	function _unlockUserJob($userId, $returnData) {
		procUnLock('job_save', $userId);
		return $returnData;
	}
	
	/*獲取任務獎勵*/
	function jobRewardHandler($userid, $job) {
		if (!isset($job['reward'])) {
			return array();
		}
		$reward = unserialize($job['reward']);
		$category = $reward['category'];
		switch ($category) {
			case "credit":
				$this->jobRewardCredit($userid, $reward, $job);
				break;

			case "tools":
				$this->jobRewardTools($userid, $reward);
				break;

			case "medal":
				$this->jobRewardMedal($userid, $reward);
				break;

			case "usergroup":
				$this->jobRewardUsergroup($userid, $reward);
				break;

			case "invitecode":
				$this->jobRewardInviteCode($userid, $reward);
				break;

			default:
				return "無";
				break;
		}
		require_once R_P.'u/require/core.php';
		updateMemberid($userid, false);
	}
	/*積分獎勵*/
	function jobRewardCredit($userid, $reward, $job) {
		global $credit;
		(!S::isObj($credit)) && require_once R_P . "require/credit.php";
		$userService = $this->_getUserService();
		$user = $userService->get($userid);
		$GLOBALS[job] = $job['title']; /*任務名稱*/
		$credit->addLog('other_finishjob', array(
			$reward['type'] => $reward['num']
		), array(
			'uid' => $userid,
			'username' => $user['username'],
			'ip' => $GLOBALS['onlineip']
		));
		$credit->set($userid, $reward['type'], $reward['num']);
	}
	/*道具獎勵*/
	function jobRewardTools($userid, $reward) {
		/*數據初始化*/
		$toolid = $reward['type'];
		$nums = $reward['num'];
		$this->_db->pw_update("SELECT uid FROM pw_usertool WHERE uid=" . S::sqlEscape($userid) . " AND toolid=" . S::sqlEscape($toolid), "UPDATE pw_usertool SET nums=nums+" . S::sqlEscape($nums) . " WHERE uid=" . S::sqlEscape($userid) . " AND toolid=" . S::sqlEscape($toolid), "INSERT INTO pw_usertool SET " . S::sqlSingle(array(
			'nums' => $nums,
			'uid' => $userid,
			'toolid' => $toolid,
			'sellstatus' => 0
		)));
	}
	/*勳章獎勵*/
	function jobRewardMedal($userid, $reward) {
		$medalId = $reward['type'];
		$medalService = L::loadClass('medalservice','medal');
		$medalService->awardMedal($userid,$medalId);
	}
	/*用戶組獎勵*/
	function jobRewardUsergroup($userid, $reward) {
		global $winddb;
		$gid = $reward['type'];
		$days = $reward['day'];
		$timestamp = $this->_timestamp;
		
		$userService = $this->_getUserService();
		$mb = $userService->get($userid);
		$groups = $mb['groups'] ? $mb['groups'] . $gid . ',' : ",$gid,";
		$userService->update($userid, array('groups' => $groups));
		
		$this->_db->pw_update("SELECT uid FROM pw_extragroups WHERE uid=" . S::sqlEscape($userid) . " AND gid=" . S::sqlEscape($gid), "UPDATE pw_extragroups SET " . S::sqlSingle(array(
			'togid' => $winddb['groupid'],
			'startdate' => $timestamp,
			'days' => $days
		)) . " WHERE uid=" . S::sqlEscape($userid) . "AND gid=" . S::sqlEscape($gid), "INSERT INTO pw_extragroups SET " . S::sqlSingle(array(
			'uid' => $userid,
			'togid' => $winddb['groupid'],
			'gid' => $gid,
			'startdate' => $timestamp,
			'days' => $days
		)));
	}
	/*註冊邀請碼獎勵*/
	function jobRewardInviteCode($userid, $reward) {
		$timestamp = $this->_timestamp;
		$invnum = $reward['num'];
		$day = $reward['day'];
		for ($i = 0; $i < $invnum; $i++) {
			$invcode = randstr(16);
			$this->_db->update("INSERT INTO pw_invitecode" . " SET " . S::sqlSingle(array(
				'invcode' => $invcode,
				'uid' => $userid,
				'usetime' => $day,
				'createtime' => $timestamp
			)));
		}
	}
	/*
	* 獲取他人完成任務情況
	*/
	function jobDetailHandler($userid, $jobid) {
		$total = $this->countJobersByJobIdAndUserId($userid, $jobid);
		if (!$total) {
			return array(
				'',
				0
			);
		}
		$others = $this->getJobersByJobIdAndUserId($userid, $jobid);
		$userIds = array();
		foreach($others as $other) {
			$userIds[] = $other['userid'];
		}
		if (!$userIds) {
			return array(
				'',
				0
			);
		}
		/*獲取用戶信息*/
		require_once (R_P . 'require/showimg.php');
		$userService = $this->_getUserService();
		$users = array();
		foreach ($userService->getByUserIds($userIds) as $rs) {
			list($rs['face']) = showfacedesign($rs['icon'], 1, 's');//統一小圖標
			$users[] = $rs;
		}
		return array(
			$users,
			$total
		);
	}
	function getUserGroup($usergroup) {
		//list($result , $selects) = $this->getLevels();
		list($result, $selects) = $this->getCacheLevels();
		$usergroups = explode(",", $usergroup);
		$groupinfo = '';
		foreach($usergroups as $usergroup) {
			if (isset($selects[$usergroup])) {
				$groupinfo .= $selects[$usergroup] . ',';
			}
		}
		$groupinfo = trim($groupinfo, ',');
		return $groupinfo;
	}
	/* 數據操作部分 */
	function addJob($fields) {
		$jobDao = $this->_getJobDao();
		$result = $jobDao->add($fields);
		if ($result) {
			$this->setFileCache();
		}
		return $result;
	}
	function updateJob($fields, $id) {
		$jobDao = $this->_getJobDao();
		$result = $jobDao->update($fields, $id);
		if ($result) {
			$this->setFileCache();
		}
		return $result;
	}
	function getJobs($page, $prepage) {
		if ($page < 1) return false;
		$start = ($page - 1) * $prepage;
		$jobDao = $this->_getJobDao();
		return $jobDao->gets($start, $prepage);
	}
	function countJobs() {
		$jobDao = $this->_getJobDao();
		return $jobDao->count();
	}
	function getJobAll() {
		//file cache
		if ($this->_cache) {
			$jobs = $this->getFileCache();
			if ($jobs) {
				return $jobs;
			}
		}
		$jobDao = $this->_getJobDao();
		return $jobDao->getAll();
	}
	function getJob($id) {
		//file cache
		if ($this->_cache) {
			$jobs = $this->getFileCache();
			if ($jobs) {
				foreach($jobs as $job) {
					if ($job['id'] == $id) {
						return $job;
					}
				}
			}
		}
		$jobDao = $this->_getJobDao();
		return $jobDao->get($id);
	}
	/*
	function getJobsAuto() {
		//file cache
		if ($this->_cache) {
			$jobs = $this->getFileCache();
			if ($jobs) {
				$autos = array();
				foreach($jobs as $job) {
					if ($job['auto'] == 1) {
						$autos[] = $job;
					}
				}
				return $autos;
			}
		}
		$jobDao = $this->_getJobDao();
		return $jobDao->getByAuto();
	}
	*/
	function getJobByJobName($jobName) {
		//file cache
		if ($this->_cache) {
			$jobs = $this->getFileCache();
			if ($jobs) {
				$result = array();
				foreach($jobs as $job) {
					if ($job['job'] == $jobName) {
						$result[] = $job;
					}
				}
				return $result;
			}
		}
		$jobDao = $this->_getJobDao();
		return $jobDao->getByJobName($jobName);
	}
	function getJoberByJobIds($userid, $jobIds) {
		$joberDao = $this->_getJoberDao();
		return $joberDao->getsByJobIds($userid, $jobIds);
	}
	
	function getInProcessJobersByUserIdAndJobIds($userid, $jobIds) {
		$joberDao = $this->_getJoberDao();
		return $joberDao->getInProcessJobersByUserIdAndJobIds($userid, $jobIds);
	}
	
	function getJobersByJobIds($userid, $ids) {
		$joberDao = $this->_getJoberDao();
		return $joberDao->getJobersByJobIds($userid, $ids);
	}
	function countJobersByJobIdAndUserId($userid, $jobid) {
		$joberDao = $this->_getJoberDao();
		return $joberDao->countJobersByJobIdAndUserId($userid, $jobid);
	}
	function getJobersByJobIdAndUserId($userid, $jobid) {
		$joberDao = $this->_getJoberDao();
		return $joberDao->getJobersByJobIdAndUserId($userid, $jobid);
	}
	function deleteJob($id) {
		$jobDao = $this->_getJobDao();
		$result = $jobDao->delete($id);
		if ($result) {
			$this->setFileCache();
		}
		return $result;
	}
	function addJober($fields) {
		$fields['userid'] = intval($fields['userid']);
		$fields['jobid'] = intval($fields['jobid']);
		if ($fields['userid'] < 1 || $fields['jobid'] < 1) {
			return null;
		}
		$joberDao = $this->_getJoberDao();
		$result = $joberDao->add($fields);
		if ($result) {
			$this->increaseJobNum($fields['userid']);
		}
		return $result;
	}
	function getJoberByJobId($userId, $jobId) {
		$joberDao = $this->_getJoberDao();
		return $joberDao->getByJobId($userId, $jobId);
	}
	function updateJober($fields, $id) {
		$joberDao = $this->_getJoberDao();
		return $joberDao->update($fields, $id);
	}
	function countJoberByJobId($jobid) {
		$joberDao = $this->_getJoberDao();
		return $joberDao->countByJobId($jobid);
	}
	function updateJoberByJobId($fieldData, $jobid, $userid) {
		$joberDao = $this->_getJoberDao();
		return $joberDao->updateByJobId($fieldData, $jobid, $userid);
	}
	function deleteJober($id){
		$joberDao = $this->_getJoberDao();
		return $joberDao->delete($id);
	}
	/*
	* 已申請任務列表
	*/
	function getAppliedJobs($userid) {
		$joberDao = $this->_getJoberDao();
		$jobers = $joberDao->getAppliedJobs($userid);
		if (!$jobers) {
			return array();
		}
		return $this->buildJobListByIds($jobers);
	}
	/*
	* 已完成任務
	*/
	function getFinishJobs($userid) {
		$joberDao = $this->_getJoberDao();
		$jobers = $joberDao->getFinishJobs($userid);
		if (!$jobers) {
			return array();
		}
		return $this->buildJobListByIds($jobers);
	}
	/*
	* 已放棄任務
	*/
	function getQuitJobs($userid) {
		$joberDao = $this->_getJoberDao();
		$jobers = $joberDao->getQuitJobs($userid);
		if (!$jobers) {
			return array();
		}
		return $this->buildJobListByIds($jobers);
	}
	function buildJobListByIds($jobers) {
		if (!$jobers) {
			return array();
		}
		$jobIds = $tmp = array();
		foreach($jobers as $job) {
			$jobIds[] = $job['jobid'];
			$tmp[$job['jobid']] = $job;
		}
		$jobs = $this->getJobsByIds($jobIds);
		if (!$jobs) {
			return array();
		}
		$result = array();
		foreach($jobs as $job) {
			if ((isset($job['isuserguide']) && $job['isuserguide'])) continue; //新用戶引導增加
			$result[] = array_merge($tmp[$job['id']], $job);
		}
		return $result;
	}
	/*任務IDs獲取任務列表*/
	function getJobsByIds($jobIds) {
		//file cache
		if ($this->_cache) {
			$jobs = $this->getFileCache();
			if ($jobs) {
				$result = array();
				foreach($jobs as $job) {
					if (in_array($job['id'], $jobIds)) {
						$result[] = $job;
					}
				}
				return $result;
			}
		}
		$jobDao = $this->_getJobDao();
		return $jobDao->getByIds($jobIds);
	}
	/*
	* 任務圖片上傳
	*/
	function upload($fileArray) {
		$pictureClass = L::loadClass('updatepicture');
		$pictureClass->init($this->_getDicroty());
		$filename = $pictureClass->upload($fileArray);
		return $filename;
	}
	/*
	* 任務圖片上傳目錄
	*/
	function _getDicroty() {
		global $db_attachname;
		$attachment = $db_attachname ? $db_attachname : 'attachment';
		return R_P . $attachment . "/job/";
	}
	function _getJobDao() {
		$job = L::loadDB('job', 'job');
		return $job;
	}
	function _getJoberDao() {
		$job = L::loadDB('jober', 'job');
		return $job;
	}
	function _getJobDoerDao() {
		$job = L::loadDB('jobdoer', 'job');
		return $job;
	}
	function getConfig() {
		$config = $this->loadJob('config');
		return $config;
	}
	function getJobTypes($k = null) {
		$config = $this->getConfig();
		return $config->getJobType($k);
	}
	function getJobType($k = null) {
		$config = $this->getConfig();
		return $config->jobs($k);
	}
	function getCondition($job) {
		$config = $this->getConfig();
		return $config->condition($job);
	}
	function getJobLists($checked) {
		$config = $this->getConfig();
		$jobs = $config->jobs();
		$jobHtml = $jobInfo = "";
		foreach($jobs as $k => $v) {
			$jobHtml .= '<li id="' . $k . '"><a href="javascript:;" hidefocus="true">' . $v . '</a></li>';
			$jobInfo .= $this->getJobData($k, $config->$k(), $checked);
		}
		return array(
			$jobHtml,
			$jobInfo
		);
	}
	function getJobData($id, $data, $checked) {
		$html = '<ul id="job_' . $id . '" style="display:none;" class="list_A">';
		foreach($data as $k => $v) {
			$checkHtml = ($checked == $k) ? "checked" : "";
			$html .= '<li><input name="factor[job]" type="radio" value="' . $k . '" ' . $checkHtml . '/>' . $v . '</li>';
		}
		$html .= '</ul>';
		return $html;
	}
	/*
	* 任務下拉選擇
	*/
	function getJobsSelect($select, $name, $id) {
		$jobs = $this->getJobAll();
		$result = array();
		foreach($jobs as $job) {
			$result[$job['id']] = $job['title'];
		}
		$result = ($result) ? $result : array(
			'-1' => "暫無任務"
		);
		return $this->_buildSelect($result, $name, $id, $select, true);
	}
	/*
	* 獲取用戶組復選框
	*/
	function getLevelCheckbox($checkeds = array(), $name = 'usergroup[]', $id = 'usergroup') {
		list($result) = $this->getLevels();
		$html .= "<div class=\"admin_table_c\"><table cellpadding=\"0\" cellspacing=\"0\">";
		$html .= "<tr class=\"vt\"><th class=\"s4\">系統組</th><td><ul class=\"cc list_A list_120 fl\">" . $this->_buildCheckbox($result['system'], $name, $id, $checkeds) . "</ul></td></tr>";
		$html .= "<tr class=\"vt\"><th class=\"s4\">會員組</th><td><ul class=\"cc list_A list_120 fl\">" . $this->_buildCheckbox($result['member'], $name, $id, $checkeds) . "</ul></td></tr>";
		$html .= "<tr class=\"vt\"><th class=\"s4\">特殊組</th><td><ul class=\"cc list_A list_120 fl\">" . $this->_buildCheckbox($result['special'], $name, $id, $checkeds) . "</ul></td></tr>";
		$html .= "</table></div>";
		//$html .= "默認組：".$this->_buildCheckbox($result['default'],$name,$id,$checkeds)."<br />";
		return $html;
	}
	/*
	* 獲取用戶組下拉框
	*/
	function getLevelSelect($select, $name, $id, $gptype = '') {
		list($result, $selects) = $this->getLevels();
		if ($gptype) {
			$selects = $result[$gptype];
		}
		return $this->_buildSelect($selects, $name, $id, $select);
	}
	/*
	* 獲取用戶組
	*/
	function getLevels() {
		$query = $this->_db->query("SELECT * FROM pw_usergroups");
		$result = $selects = array();
		while ($rs = $this->_db->fetch_array($query)) {
			$result[$rs['gptype']][$rs['gid']] = $rs['grouptitle'];
			$selects[$rs['gid']] = $rs['grouptitle'];
		}
		return array(
			$result,
			$selects
		);
	}
	function getCacheLevels() {
		//* @include pwCache::getPath(R_P . "data/bbscache/level.php");
		extract(pwCache::getData(R_P . "data/bbscache/level.php", false));
		if ($ltitle) {
			return array(
				'',
				$ltitle
			);
		}
		return $this->getLevels();
	}
	/*
	* 獲取積分下拉框
	*/
	function getCreditSelect($select, $name, $id) {
		$credits = pwCreditNames();
		return $this->_buildSelect($credits, $name, $id, $select);
	}
	/*
	* 任務獎勵提示 前台
	*/
	function getCategoryInfo($reward, $num = 1) {
		$reward = unserialize($reward);
		$category = $reward['category'];
		switch ($category) {
			case "credit":
				$title = explode(" ", $reward['information']);
				return array('prefix'=>$title[0], 'title'=>$title[1], 'num'=>$reward['num'] * $num, 'unit'=>pwCreditUnits($reward['type']));
				break;

			case "tools":
				$title = explode(" ", $reward['information']);
				return array('prefix'=>$title[0], 'title'=>$title[1], 'num'=>$reward['num'] * $num, 'unit'=>'個');
				break;

			case "medal":
				$title = explode(" ", $reward['information']);
				return array('prefix'=>$title[0], 'title'=>$title[1], 'suffix'=>$title[2]);
				break;

			case "usergroup":
				$title = explode(" ", $reward['information']);
				return array('prefix'=>$title[0], 'title'=>$title[1], 'suffix'=>$title[2], 'num'=>$reward['day'] * $num, 'unit'=>'天');
				break;

			case "invitecode":
				$title = explode(" ", $reward['information']);
				return array('prefix'=>$title[0], 'title'=>$title[1], 'num'=>$reward['num'] * $num, 'unit'=>'個');
				break;

			default:
				return array();
				break;
		}
	}
	/*組裝獎勵前綴信息 後台*/
	function buildCategoryInfo($reward, $num = 1) {
		$category = $reward['category'];
		switch ($category) {
			case "credit":
				return "可獲得 " . pwCreditNames($reward['type']) . " ";
				break;

			case "tools":
				$tools = $this->getTools();
				return "可獲得道具 " . $tools[$reward['type']] . " ";
				break;

			case "medal":
				$medals = $this->getMedals();
				return "可獲得勳章 " . $medals[$reward['type']];
				break;

			case "usergroup":
				list($result, $selects) = $this->getLevels();
				return "成為 " . $selects[$reward['type']] . " 有效期 ";
				break;

			case "invitecode":
				return "可獲得 邀請註冊碼 ";
				break;

			default:
				return "";
				break;
		}
	}
	/*組裝前台完成任務詳細頁總數*/
	function buildCountCategoryInfo($reward, $num = 1) {
		$reward = unserialize($reward);
		$category = $reward['category'];
		switch ($category) {
			case "credit":
				return "共獲得 " . pwCreditNames($reward['type']) . " " . $reward['num'] * $num . " " . pwCreditUnits($reward['type']);
				break;

			case "tools":
				$tools = $this->getTools();
				return "共獲得道具 " . $tools[$reward['type']] . " " . $reward['num'] * $num . " 個";
				break;

			case "medal":
				$medals = $this->getMedals();
				return "共獲得勳章 " . $medals[$reward['type']] . " 有效期 " . $reward['day'] * $num . " 天";
				break;

			case "usergroup":
				list($result, $selects) = $this->getLevels();
				return "成為 " . $selects[$reward['type']] . " 有效期 " . $reward['day'] * $num . " 天";
				break;

			case "invitecode":
				return "共獲得邀請註冊碼 " . $reward['num'] * $num . " 個";
				break;

			default:
				return "";
				break;
		}
	}
	/*
	* 獲取勳章下拉框
	*/
	function getMedalSelect($select, $name, $id) {
		$medials = $this->getMedals();
		return $this->_buildSelect($medials, $name, $id, $select);
	}
	/*
	* 獲取勳章
	*/
	function getMedals() {
		$medalService = L::loadClass('MedalService', 'medal'); /* @var $medalService PW_MedalService */
		$medials = $medalService->getAllOpenManualMedals();
		$result = array();
		foreach ($medials as $v) {
			$result[$v['medal_id']] = $v['name'];
		}
	
		return $result;
	}
	/*
	* 語言包
	*/
	function getLanguage($k) {
		$data = array();
		$data['job_title_null'] = "任務名稱不能為空";
		$data['job_description_null'] = "任務描述不能為空";
		$data['upload_icon_fail'] = "任務圖標上傳失敗";
		$data['add_job_success'] = "增加任務完成";
		$data['job_id_null'] = "任務ID無效";
		$data['job_sequence_null'] = "任務順序不能小於0";
		$data['job_id_error'] = "抱歉，任務ID有誤"; /*申請任務*/
		$data['job_not_exist'] = "抱歉，你申請的任務不存在";
		$data['job_usergroup_limit'] = "抱歉，你所在的用戶組不能申請";
		$data['job_time_limit'] = "抱歉，你申請的任務已經結束";
		$data['job_time_early'] = "抱歉，你申請的任務還沒有開始";
		$data['job_close'] = "抱歉，你申請的任務已經關閉";
		$data['job_has_perpos'] = "抱歉，申請這個任務你必須先完成這個任務：";
		$data['job_has_apply'] = "抱歉，你已經申請了這個任務";
		$data['job_apply_next_limit'] = "抱歉，還沒到下一次申請任務時間";
		$data['job_apply_success'] = "恭喜，任務申請完成";
		$data['job_apply_number_limit'] = "抱歉，該任務申請人數已滿";
		$data['job_apply_fail'] = "抱歉，任務申請失敗";
		$data['job_has_perpos_more'] = "抱歉，你還沒有完成這個任務：";
		$data['job_stime_r_etime'] = "抱歉，任務開始時間大於結束時間";
		$data['use_not_exists'] = "抱歉，要選擇發消息的指定會員不能為空";
		return $data[$k];
	}
	/*
	* 獲取道具下拉框
	*/
	function getToolsSelect($select, $name, $id) {
		$tools = $this->getTools();
		return $this->_buildSelect($tools, $name, $id, $select);
	}
	/*
	* 獲取道具
	*/
	function getTools() {
		$query = $this->_db->query("SELECT * FROM pw_tools");
		$result = $special = $member = $default = $system = array();
		while ($rs = $this->_db->fetch_array($query)) {
			$result[$rs['id']] = $rs['name'];
		}
		return $result;
	}
	/*
	* 組裝下拉框
	*/
	function _buildSelect($arrays, $name, $id, $select = '', $isEmpty = false) {
		if (!is_array($arrays)) {
			return '';
		}
		$html = '<select name="' . $name . '" id="' . $id . '" class="select_wa">';
		($isEmpty == true) && $html .= '<option value=""></option>';
		foreach($arrays as $k => $v) {
			$selected = ($select == $k && $select != null) ? 'selected="selected"' : "";
			$html .= '<option value="' . $k . '" ' . $selected . '>' . $v . '</option>';
		}
		$html .= '</select>';
		return $html;
	}
	/*
	* 組裝復選框
	*/
	function _buildCheckbox($arrays, $name, $id, $checkeds = array()) {
		if (!is_array($arrays)) {
			return '';
		}
		$html = '';
		foreach($arrays as $k => $v) {
			$checked = (in_array($k, $checkeds)) ? "checked" : "";
			$html .= '<li><input type="checkbox" value="' . $k . '" name="' . $name . '" id="' . $id . '" ' . $checked . '/>' . $v . "</li>";
		}
		return $html;
	}
	/*
	* 設置文件緩存
	*/
	function setFileCache() {
		$jobDao = $this->_getJobDao();
		$jobs = $jobDao->getAll();
		$jobLists = "\$jobLists=" . pw_var_export($jobs) . ";";
		pwCache::setData($this->getCacheFileName(), "<?php\r\n" . $jobLists . "\r\n?>");
		return $jobs;
	}
	/*
	* 獲取文件緩存
	*/
	function getFileCache() {
		if (!$this->_cache) {
			return array(); /*not open cache*/
		}
		static $jobLists = null;
		if(!isset($jobLists)){
			//* @include_once pwCache::getPath(S::escapePath($this->getCacheFileName()),true);
			extract(pwCache::getData(S::escapePath($this->getCacheFileName()), false));
			$jobLists = ($jobLists) ? $jobLists : $GLOBALS['jobLists'];
		}
		if ($jobLists) {
			return $jobLists;
		}
		return $this->setFileCache();
	}
	/*獲取緩存文件路徑*/
	function getCacheFileName() {
		return R_P . "data/bbscache/jobs.php";
	}
	function checkIsOpenMedal() {
		global $db_md_ifopen;
		return $db_md_ifopen ? true:false;
	}
	function checkIsOpenInviteCode() {
		$fileName = D_P . 'data/bbscache/dbreg.php';
		if (!is_file($fileName)) {
			return false;
		}
		//* @include pwCache::getPath(S::escapePath($fileName));
		extract(pwCache::getData(S::escapePath($fileName), false));
		if ($rg_allowregister == 2) {
			return true;
		}
		return false;
	}
	function countAppliedJobs($userid) {
		$joberDao = $this->_getJoberDao();
		return $joberDao->countAppliedJobs($userid);
	}
	/*更新任務數*/
	function updateJobNum($userid) {
		$jobnum = $this->countJobnum($userid); /*直接查詢更新*/
		($jobnum > 0) ? $jobnum : 0;
		$userService = $this->_getUserService();
		return $userService->update($userid, array(), array('jobnum' => $jobnum));
	}
	
	/**
	 * 統計我申請的任務數
	 * @param int $userId
	 * 
	 */
	function countJobNum($userId) {
		if(!$userId) return false;
		$joblists = $this->getAppliedJobs($userId); 
		$joblists = $joblists ? $joblists : array();
		$num = 0;
		foreach($joblists as $job) {//顯示條件 是否顯示
			if ($job['isopen'] == 0) continue;
			$num++;
		}
		return $num;
	}
	
	/*增加一個任務數*/
	function increaseJobNum($userid) {
		$this->updateJobNum($userid);
	}
	/*減少一個任務數*/
	function reduceJobNum($userid) {
		$this->updateJobNum($userid);
	}
	/*組裝已申請任務列表*/
	function buildApplieds($winduid, $groupid) {
		$joblists = $this->getAppliedJobs($winduid);
		$jobs = $this->buildLists($joblists, 'applied', $winduid, $groupid);
		if (!$jobs) {
			return '';
		}
		$html = '';
		foreach($jobs as $job) {
			$html .= $this->buildApplied($job);
		}
		return $html;
	}
	/*組裝彈出框已申請任務*/
	function buildApplied($list) {
		$list['title'] = substrs($list['title'],56);
		$html = '';
		$html .= '<div id="applied_' . $list[id] . '">';
		$html .= '<div class="jobpop_h current"><a href="javascript:;" class="menu_tasksA_title" hidefocus="true"><b></b>' . $list[title] . ' <span>' . $list[gain] . '</span></a></div>';
		$html .= '	<dl class="cc taskA_dl" style="display:none;">';
		$html .= '    <dt><img src="' . $list[icon] . '" /></dt>';
		$html .= '    <dd>';
		$html .= '    <table width="100%" style="table-layout:fixed;">';
		$html .= '        <tr class="vt">';
		$html .= '			<td width="80">完成條件:</td>';
		$html .= '    		<td id="job_condition_' . $list[id] . '">' . $list[condition] . '</td>';
		$html .= '		  </tr>';
		$html .= '        <tr class="vt">';
		$html .= '            <td>完成獎勵:</td>';
		$html .= '            <td class="s2">' . $list[reward] . '</td>';
		$html .= '        </tr>';
		$html .= '            <tr class="vt">';
		$html .= '            <td>任務描述:</td>';
		$html .= '                <td>' . $list[description] . '</td>';
		$html .= '         </tr>';
		$html .= '         <tr class="vt">';
		$html .= '            <td></td>';
		$html .= '                <td><span class="fr">' . $list[btn] . '</span></td>';
		$html .= '         </tr>';
		$html .= '   </table>';
		$html .= '   </dd>';
		$html .= '  </dl>';
		$html .= '</div>';
		return $html;
	}
	/*
	* 加載任務類
	*/
	function loadJob($name) {
		static $classes = array();
		$name = strtolower($name);
		$filename = R_P . "lib/job/job/" . $name . ".job.php";
		if (!is_file($filename)) {
			return null;
		}
		$class = 'JOB_' . ucfirst($name);
		if (isset($classes[$class])) {
			return $classes[$class];
		}
		include S::escapePath($filename);
		$classes[$class] = new $class();
		return $classes[$class];
	}
	
	/**
	 * @return PW_UserService
	 */
	function _getUserService() {
		return L::loadClass('UserService', 'user');
	}
}
