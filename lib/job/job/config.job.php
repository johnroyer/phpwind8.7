<?php
!function_exists('readover') && exit('Forbidden');
class JOB_Config {
	/*配置文件*/
		
	var $_members    = "members";
	var $_forums     = "forums";
	var $_modes      = "modes";
	var $_moderators = "moderators";
	var $_gifts      = "gifts";
	
	/*
	 * 任務類型與具體任務對應
	 */
	function getJobType($k=null){
		$data  = array();
		$data[$this->_members] = $this->members();
		$data[$this->_forums] = $this->forums();
		$data[$this->_modes] = $this->modes();
		$data[$this->_moderators] = $this->moderators();
		$data[$this->_gifts] = $this->gifts();
		$types = array();
		foreach($data as $type =>$v){
			foreach($v as $job=>$v){
				$types[$job] = $type;
			}
		}
		return $k ? $types[$k] :$types;
	}
	
	function jobs($k = null){
		$data = array(
			$this->_members    => "會員信息類",
			$this->_forums     => "論壇操作類",
			//$this->_modes      => "圈子操作類",
			//$this->_moderators => "版主管理類",
			$this->_gifts     => "紅包獎勵類",
		);
		return $k ? $data[$k] : $data;
	}
	/*
	 * 會員信息類
	 */
	function members($k = null){
		$data = array(
			'doUpdatedata'    =>'完善資料',
			'doUpdateAvatar'  =>'上傳頭像',
			'doSendMessage'   =>'發送消息',
			'doAddFriend'     =>'加好友',
			'doAuthAlipay'     =>'支付寶認證',
			'doAuthMobile'     =>'手機認證',
		);
		return $k ? $data[$k] : $data;
	}
	/*
	 * 論壇操作類
	 */
	function forums($k = null){
		$data = array(
			'doPost'         =>'發帖',
			'doReply'        =>'回復',
			//'doFavor'        =>'收藏',
			//'doForumShare'   =>'分享',
			//'doVote'         =>'評價',
			//'doUseTools'     =>'使用道具',
			//'doLookCard'     =>'查看用戶名片',
		);
		return $k ? $data[$k] : $data;
	}
	/*
	 * 圈子操作類
	 */
	function modes($k = null){
		$data = array(
			//'doEntrySelf'    =>'進入個人空間',
			//'doEntryFriend'  =>'進入朋友個人空間',
			//'doWrite'        =>'發記錄',
			//'doDiary'        =>'發日誌',
			//'doPhoto'        =>'傳照片',
			//'doModeShare'    =>'分享',
			//'doComment'      =>'評論',
		);
		return $k ? $data[$k] : $data;
	}
	/*
	 * 版主管理類
	 */
	function moderators($k = null){
		$data = array(
			//'doPing'     =>'評分',
			//'doHead'     =>'置頂',
			//'doDigest'   =>'精華',
			//'doLock'     =>'鎖定',
			//'doUp'       =>'提前',
			//'doDown'     =>'壓帖',
			//'doHighline' =>'加亮',
			//'doPush'     =>'推送',
		);
		return $k ? $data[$k] : $data;
	}
	/*
	 * 紅包獎勵類
	 */
	function gifts($k = null){
		$data = array(
			'doSendGift'    =>'紅包發放',
		);
		return $k ? $data[$k] : $data;
	}
	
	/*
	 * 任務完成條件模板
	 */
	function condition($job){
		$jobName = $job['job'];
		$factor = unserialize($job['factor']);
		switch ($jobName){
			case 'doUpdatedata':
				return $this->finish_doUpdatedata($factor);
				break;
			case 'doUpdateAvatar':
				return $this->finish_doUpdateAvatar($factor);
				break;				
			case 'doSendMessage':
				return $this->finish_doSendMessage($factor);
				break;				
			case 'doAddFriend':
				return $this->finish_doAddFriend($factor);
				break;		
			case 'doPost':
				return $this->finish_doPost($factor);
				break;	
			case 'doReply':
				return $this->finish_doReply($factor);
				break;	
			case 'doSendGift':
				return $this->finish_doSendGift($factor);
				break;
			case 'doAuthAlipay':
				return $this->finish_doAuthAlipay($factor);
				break;
			case 'doAuthMobile':
				return $this->finish_doAuthMobile($factor);
				break;
			default :
				return '';
				break;
			
		}
	}
	
	function finish_doUpdatedata($factor){
		return '完善自己的個人資料後即可完成任務'.$this->getLimitTime($factor);
	}
	
	function finish_doUpdateAvatar($factor){
		return '成功上傳個人頭像後即可完成任務'.$this->getLimitTime($factor);
	}
	function finish_doAuthMobile($factor){
		return '成功綁定手機號碼即可完成任務'.$this->getLimitTime($factor);
	}
	function finish_doAuthAlipay($factor){
		return '成功綁定支付寶帳號即可完成任務'.$this->getLimitTime($factor);
	}
	
	function finish_doSendMessage($factor){
		return '給 '.$factor['user'].' 發送消息'.$this->getLimitTime($factor);
	}
	
	function finish_doAddFriend($factor){
		if($factor['type'] == 1){
			return '將 '.$factor['user'].' 加為好友  '.$this->getLimitTime($factor);
		}else{
			return '成功加 '.$factor['num'].' 個好友後即可完成任務'.$this->getLimitTime($factor);
		}
	}
	
	function finish_doPost($factor){
		$forum = L::forum($factor['fid']);
		$title = '<a target="_blank" href="thread.php?fid='.$forum['fid'].'">'.$forum['name'].'</a>';
		return '在 【'.$title.'】 版塊發 '.$factor['num'].' 個帖子即可完成任務 '.$this->getLimitTime($factor);
	}
	
	function finish_doReply($factor){
		if($factor['type'] == 1){
			$thread = $GLOBALS['db']->get_one("SELECT tid,subject FROM pw_threads WHERE tid=".S::sqlEscape($factor['tid']));
			if(!$thread){
				return '抱歉，指定的回復帖子不存在，請聯繫管理員';/*錯誤過濾*/
			}
			$title = '<a href="read.php?tid='.$thread['tid'].'" target="_blank">'.$thread['subject'].'</a>';
			return '在 【'.$title.'】這個帖子回復 '.$factor['replynum'].' 次即可完成任務  '.$this->getLimitTime($factor);
		}else{
			return '給【'.$factor['user'].'】 發佈的任意帖子回復 '.$factor['replynum'].' 次即可完成任務'.$this->getLimitTime($factor);
		}
	}
	
	function finish_doSendGift($factor){
		return "申請任務後即可完成任務，得到獎勵";
	}
	function getLimitTime($factor){
		return (isset($factor['limit']) && $factor['limit']>0 ) ? ",限制".$factor['limit']."小時內完成 " : "";
	}
	
}