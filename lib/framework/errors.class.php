<?php
!defined('P_W') && exit('Forbidden');
/**
 * 錯誤與日誌基類
 */
class PW_Errors {
	var $_errors = array(); //錯誤集合
	

	var $_logs = array(); //日誌集合
	

	/**
	 * 添加一條錯誤信息
	 * @param $errorInfo	錯誤信息
	 */
	function addError($errorInfo) {
		$this->_errors[] = $errorInfo;
	}
	/**
	 * 添加一條提醒信息
	 * @param $logInfo
	 */
	function addLog($logInfo) {
		$this->_logs[] = $logInfo;
	}
	/**
	 * 記錄錯誤信息
	 */
	function writeLog($method = 'rb+') {
		$logFile = D_P.'data/error.log';
		if (!$this->_logs) return false;
		$temp = pw_var_export($this->_logs);
		pwCache::writeover($logFile,$temp, 'rb+');
	}
	/**
	 * 檢查是否有錯誤信息，有的話及時報錯
	 */
	function checkError($jumpurl = '') {
		foreach ($this->_errors as $error) {
			$this->showError($error,$jumpurl);
		}
	}
	/**
	 * 及時報錯
	 * @param $error 錯誤信息
	 */
	function showError($error, $jumpurl = '') {
		Showmsg($error, $jumpurl);
	}
	
	function __destruct() {
		if (!defined('SHOWLOG')) return false;
		$this->writeLog();
	}
}