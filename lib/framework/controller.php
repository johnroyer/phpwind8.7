<?php
!defined('P_W') && exit('Forbidden');
/**
 * 基類控制層
 */
class Controller {
	var $_viewer; //全局視圖變量
	var $_controller; //控制器
	var $_action; //動作
	var $_layoutFile; //佈局文件
	var $_layoutExt = 'htm'; //佈局文件後綴
	var $_viewPath; //視圖路徑
	var $_template; //視圖模板
	var $_partial;//獨立模板目錄
	
	function __construct() {
		$this->_viewer = new stdClass();
		$this->_layoutExt = 'htm';
	}
	
	function Controller() {
		$this->__construct();
	}
	
	function execute($controller, $action, $viewerPath) {
		$this->_init($controller, $action, $viewerPath);
		if ($this->_before()) {
			$this->$action();
			$this->_after();
		}
		$this->_render();
		$this->_output();
	}
	/**
	 * 初始化抽像函數
	 */
	function _init($controller, $action, $viewerPath) {
		$this->_controller = $controller;
		$this->_action = $action;
		$this->_setViewPath($viewerPath);
	}
	/**
	 * 執行動作前的操作
	 */
	function _before() {
		return true;
	}
	/**
	 * 默認執行動作
	 */
	function run() {
	}
	/**
	 * 執行動作後的操作
	 */
	function _after() {
	}
	/**
	 * 執行動作後的輸出
	 */
	function _output() {
	}
	
	function _render() {
		$layoutService = L::loadClass('layout', 'framework');
		$layoutService->init($this->_viewPath, $this->_layoutFile, $this->_layoutExt);
		$layoutService->setPartial($this->_partial);
		$layoutService->setTemplate(($this->_template) ? $this->_template : $this->_controller . '.' . $this->_action);
		$layoutService->display($this->_layoutFile, $this->_viewer);
	}
	
	function _setTemplate($template){
		$this->_template = $template;
	}
	
	function _setViewPath($path){
		$this->_viewPath = $path;
	}
	
	function _setLayoutFile($file){
		$this->_layoutFile = $file;
	}
	
	function _setLayoutExt($ext){
		$this->_layoutExt = $ext;
	}
	
	function _setPartial($partial){
		$this->_partial = $partial;
	}
	
	/**
	 * 獲取全局變量 全局安全類庫
	 * @param $key 鍵名
	 */
	function _getGlobal($key) {
		return isset($GLOBALS[$key]) ? $GLOBALS[$key] : '';
	}
	/**
	 * 獲取$_POST或$_GET參數
	 * @param array $params 變量數組
	 */
	function _gp($params) {
		if (!S::isArray($params)) return array();
		S::gp($params,null,1,false);
		$tmp = array();
		foreach ($params as $param) {
			$tmp[] = $this->_getGlobal($param);
		}
		return $tmp;
	}
	function _isPost() {
		return (strtolower($_SERVER['REQUEST_METHOD']) === 'post');
	}
}