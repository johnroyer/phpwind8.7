<?php
!defined('P_W') && exit('Forbidden');
/**
 * 助手基類
 */
class Helper {
	/**
	 * 獲取全局變量 全局安全類庫
	 * @param $key 鍵名
	 */
	function _getGlobal($key) {
		return isset($GLOBALS[$key]) ? $GLOBALS[$key] : '';
	}
	/**
	 * 組裝視圖模板
	 * @param $controller
	 * @param $action
	 */
	function _buildTemplate($controller,$action){
		return $controller.'.'.$action;
	}
	
}