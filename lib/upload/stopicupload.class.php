<?php
!defined('P_W') && exit('Forbidden');
L::loadClass('upload', '', false);

/**
 * 專題圖片上傳類
 * 
 * @package upload
 * @author	zhuli
 * @abstract
 */
class StopicUpload extends uploadBehavior{
 	var $db;
	var $atype;
	var $attachs;
	var $inputId;
	
	/**
	 * 初始化
	 * 
	 * @return 
	 */
	 function StopicUpload($inputId) {
		global $db;
		parent::uploadBehavior();
		$this->db =& $db;
		$this->inputId = $inputId;
		
		$o_maxfilesize = 2000;

		$this->ftype = array(
			'gif'  => $o_maxfilesize,				'jpg'  => $o_maxfilesize,
			'jpeg' => $o_maxfilesize,				'bmp'  => $o_maxfilesize,
			'png'  => $o_maxfilesize
		);
	}
	
	/**
	 * 表單file名稱
	 * 
	 * @param string $key
	 * @return bool
	 */
	function allowType($key) {
		return $key == "image_upload_".$this->inputId;
	}
	
	/**
	 * 是否需要縮略圖
	 * 
	 * @return bool
	 */
	function allowThumb() {
		return false;
	}
	
	/**
	 * 獲取文件路勁
	 * 
	 * @return array
	 */
	function getFilePath($currUpload) {
		global $timestamp,$o_mkdir;
		$filename	= date("YmdHis", time()).'_'. $this->inputId .'.'. $currUpload['ext'];
		$savedir	= 'stopic/img/';
		return array($filename, $savedir);
	}
	
	/**
	 * 這邊可以進行數據庫更新操作等
	 * 
	 * @return bool
	 */
	function update($uploaddb) {
		return $uploaddb;
	}
	
	/**
	 * 獲取圖片路勁
	 * 
	 * @return string
	 */
	function getImagePath() {
		$imagePath = geturl($this->fileName);
		if ($imagePath[0]) return $imagePath[0];
		return '';
	}

	function getAttachs() {
		return $this->attachs;
	}	
}
?>