<?php
!defined('P_W') && exit('Forbidden');

/**
 * 友情鏈接關係數據層
 * 
 * @package PW_SharelinkstypeDB
 * @author	panjl
 * @abstract
 */

class PW_SharelinksRelationDB extends BaseDB {
	var $_tableName 	= 	'pw_sharelinksrelation';

	/**
	 * 添加
	 * 
	 * @param array $fieldsData 數據數組，以數據庫字段為key
	 * @return int 新增id
	 */
	function insert($fieldsData) {
		if (S::isArray($fieldsData)) {
			pwQuery::insert($this->_tableName, $fieldsData);
			return $this->_db->insert_id();
		}
	}

	/**
	 * 根據分類ID刪除數據
	 * 
	 * @param int $typeId 
	 * @return int 刪除行數
	 */
	function deleteByStid($stid){
		$stid = intval($stid);
		if ($stid < 1) return null;
		pwQuery::delete($this->_tableName, "stid=:stid", array($stid));
		return $this->_db->affected_rows();
	}

	/**
	 * 根據鏈接ID刪除數據
	 * 
	 * @param int $sid  鏈接ID
	 * @return int 刪除行數
	 */
	function deleteBySid($sid) {
		$sid = intval($sid);
		if ($sid < 1) return null;
		pwQuery::delete($this->_tableName, "sid=:sid", array($sid));
		return $this->_db->affected_rows();
	}

	/**
	 * 根據鏈接ID查分類ID
	 * 
	 * @param int $sid 友情鏈接ID
	 * @return int 分類ID
	 */
	function findStidBySid($sid) {
		$sid = intval($sid);
		if ($sid < 1) return null;
		$query = $this->_db->query('SELECT stid FROM ' . $this->_tableName . ' WHERE sid = ' . S::sqlEscape($sid) .' ORDER BY stid ASC');
		return $this->_getAllResultFromQuery($query);
	}

	/**
	 * 按分類ID查所有sid
	 * 
	 * @param int $stid 分類ID
	 * @return array 數組鏈接ID
	 */
	function findSidByStid($stid) {
		$stid = intval($stid);
		if ($stid < 1) return array();
		$query = $this->_db->query('SELECT sid FROM ' . $this->_tableName . ' WHERE stid = ' . S::sqlEscape($stid));
		return $this->_getAllResultFromQuery($query);
	}

}