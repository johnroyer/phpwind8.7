<?php
!defined('P_W') && exit('Forbidden');

/**
 * 友情鏈接數據層
 * 
 * @package PW_SharelinkstypeDB
 * @author	panjl @2010-11-5
 */

class PW_SharelinkstypeDB extends BaseDB {
	var $_tableName 	= 	'pw_sharelinkstype';
	var $_primaryKey 	= 	'stid';

	/**
	 * 添加
	 * 
	 * @param array $fieldsData 數據數組，以數據庫字段為key
	 * @return int 新增id
	 */
	function insert($fieldsData) {
		if (S::isArray($fieldsData)) {
			return $this->_insert($fieldsData);
		}
	}

	/**
	 * 刪除
	 * 
	 * @param int $typeId  分類ID
	 * @return int 刪除行數
	 */
	function delete($typeId){
		$typeId = intval($typeId);
		if ($typeId < 1) return null;
		return $this->_delete($typeId);
	}

	/**
	 * 編輯
	 * 
	 * @param array $fieldsData 數據數組，以數據庫字段為key
	 * @param int $typeId 分類ID
	 * @return int 最新編輯的分類ID
	 */
	function update($fieldsData,$typeId){
		if (S::isArray($fieldsData)) {
			return $this->_update($fieldsData,$typeId);
		}
	}

	/**
	 * 根據分類name查stid
	 * 
	 * @param string 分類名稱
	 * @return int stid值
	 */
	function getTypeIdByName($name) {
		if ( !$name ) return null;
		return $this->_db->get_one('SELECT stid FROM ' . $this->_tableName . ' WHERE ifable <> 0 AND name= ' . S::sqlEscape($name));
	}

	/**
	 * 根據分類stid查name
	 * 
	 * @param stid 分類id
	 * @return array 查詢結果數組
	 */
	function getTypesByStid($stid) {
		$stid = intval($stid);
		if ( !$stid ) return null;
		return $this->_db->get_one('SELECT name FROM ' . $this->_tableName . ' WHERE ifable <> 0 AND stid= ' . S::sqlEscape($stid));
	}

	/**
	 * 查詢啟用的鏈接分類
	 * 
	 * @return array 查詢結果
	 */
	function getAllTypes() {
		$query = $this->_db->query('SELECT stid,name,ifable,vieworder FROM ' . $this->_tableName . ' WHERE ifable <> 0 ORDER BY vieworder ASC');
		return $this->_getAllResultFromQuery($query);
	}

	/**
	 * 查詢所有鏈接分類
	 * 
	 * @return array 查詢結果
	 */
	function getAllTypesName() {
		$query = $this->_db->query('SELECT * FROM ' . $this->_tableName . ' ORDER BY vieworder ASC');
		return $this->_getAllResultFromQuery($query);
	}
}