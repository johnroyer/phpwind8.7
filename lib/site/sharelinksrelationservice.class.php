<?php
!defined('P_W') && exit('Forbidden');
/**
 * 友情鏈接關係服務層
 * @package  PW_SharelinksRelationService
 * @author panjl @2010-11-5
 */
class PW_SharelinksRelationService {

	/**
	 * 加載dao
	 * 
	 * @return PW_SharelinksRelationDB
	 */
	function _getTypeDB() {
		return L::loadDB('SharelinksRelation', 'site');
	}

	/**
	 * 添加
	 * 
	 * @param array $fieldsData 數據數組，以數據庫字段為key
	 * @return int 新增id
	 */
	function insert($fieldsData) {
		if (S::isArray($fieldsData)) {
			$typeDb = $this->_getTypeDB();
			return $typeDb->insert($fieldsData);
		}
	}

	/**
	 * 根據分類ID刪除關係數據
	 * 
	 * @param int $stid  鏈接分類ID
	 * @return int 刪除行數
	 */
	function deleteByStid($stid) {
		$stid = intval($stid);
		if ($stid < 1) return null;
		$typeDb = $this->_getTypeDB();
		return $typeDb->deleteBySid($stid);
	}

	/**
	 * 根據鏈接ID刪除關係數據
	 * 
	 * @param int $sid  鏈接ID
	 * @return int 刪除行數
	 */
	function deleteBySid($sid) {
		$sid = intval($sid);
		if ($sid < 1) return null;
		$typeDb = $this->_getTypeDB();
		return $typeDb->deleteBySid($sid);
	}

	/**
	 * 根據鏈接ID查分類ID
	 * 
	 * @param int $sid 友情鏈接ID
	 * @return int 分類ID
	 */
	function findStidBySid($sid) {
		$sid = intval($sid);
		if ($sid < 1) return null;
		$typeDb = $this->_getTypeDB();
		$typeNames = $typeDb->findStidBySid($sid);
		foreach ($typeNames as $value) {
			$names[] = $value['stid'];
		}
		return $names;
	}

	/**
	 * 根據分類ID查鏈接ID
	 * 
	 * @param int $stid 分類ID
	 * @return array 數組友情鏈接ID
	 */
	function findSidByStid($stid) {
		$stid = intval($stid);
		if ($stid < 1) return array();
		$typeDb = $this->_getTypeDB();
		$stidsArray = $typeDb->findSidByStid($stid);
		$stids = array();
		foreach ($stidsArray as $sids) {
			$sid[] = $sids['sid'];
		}
		return $sid;
	}
}