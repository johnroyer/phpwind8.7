<?php
!defined('P_W') && exit('Forbidden');
/**
 * 友情鏈接服務層
 * @package  PW_SharelinksService
 * @author panjl @2010-11-5
 */
class PW_SharelinksService {

	/**
	 * 加載dao
	 * 
	 * @return PW_SharelinkstypeDB
	 */
	function _getLinksDB() {
		return L::loadDB('sharelinks', 'site');
	}

	/**
	 * 按照分類、是否有logo查找鏈接信息
	 * 
	 * @param int $num 條數
	 * @param bool $haveLogo=false 是否有logo
	 * @param int $sids 鏈接ID，如array(1,2,3)
	 * @return array 友情鏈接信息
	 */
	function getData($num,$stid = '',$haveLogo = false) {
		$num = (int) $num;
		$stid && $sids = '';
		if ($stid) { 
			$stid = (int) $stid;
			$relationService = L::loadClass('SharelinksRelationService', 'site');
			$stid && $sids = $relationService->findSidByStid($stid);
		}
		if($stid && !$sids) return array();
		$linksDb = $this->_getLinksDB();
		return $linksDb->getData($num,$sids,$haveLogo);
	}

}
