<?php
!defined('P_W') && exit('Forbidden');
/**
 * 友情鏈接分類服務層
 * @package  PW_SharelinksTypeService
 * @author panjl @2010-11-5
 */
class PW_SharelinksTypeService {

	/**
	 * 加載dao
	 * 
	 * @return PW_SharelinkstypeDB
	 */
	function _getTypeDB() {
		return L::loadDB('sharelinkstype', 'site');
	}

	/**
	 * 添加
	 * 
	 * @param array $fieldsData 數據數組，以數據庫字段為key
	 * @return int 新增id
	 */
	function insert($fieldsData) {
		if (S::isArray($fieldsData)) {
			$typeDb = $this->_getTypeDB();
			return $typeDb->insert($fieldsData);
		}
	}

	/**
	 * 刪除
	 * 
	 * @param int $typeId  分類ID
	 * @return int 刪除行數
	 */
	function delete($typeId) {
		$typeId = intval($typeId);
		if ($typeId < 1) return null;
		$typeDb = $this->_getTypeDB();
		return $typeDb->delete($typeId);
	}

	/**
	 * 編輯
	 * 
	 * @param array $typeName 數據數組，以數據庫字段為key
	 * @param int $typeId 分類ID
	 * @return int 最新編輯的分類ID
	 */
	function update($fieldsData,$typeId) {
		if (!intval($typeId)) return null;
		if (S::isArray($fieldsData)) {
			$typeDb = $this->_getTypeDB();
			return $typeDb->update($fieldsData,$typeId);
		}
	}

	/**
	 * 根據分類name查stid
	 * 
	 * @param string 分類名稱
	 * @return array 查詢結果數組
	 */
	function getTypeIdByName($name) {
		if ( !$name ) return array();
		$typeDb = $this->_getTypeDB();
		return $typeDb->getTypeIdByName($name);
	}

	/**
	 * 根據分類stid查name
	 * 
	 * @param stid 分類id
	 * @return array 查詢結果數組
	 */
	function getTypesByStid($stid) {
		$stid = intval($stid);
		if ( !$stid ) return array();
		$typeDb = $this->_getTypeDB();
		return $typeDb->getTypesByStid($stid);
	}

	/**
	 * 查詢鏈接分類
	 * 
	 * @return array 查詢結果
	 */
	function getAllTypes() {
		$typeDb = $this->_getTypeDB();
		return $typeDb->getAllTypes();
	}

	/**
	 * 查詢所有鏈接分類
	 * 
	 * @return array 查詢結果
	 */
	function getAllTypesName() {
		$typeDb = $this->_getTypeDB();
		return $typeDb->getAllTypesName();
	}
}
