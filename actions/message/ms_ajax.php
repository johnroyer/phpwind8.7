<?php
!defined('P_W') && exit('Forbidden');
S::gp(array('action'));
define('FRIEND_SEPARATER', '#%');
!in_array($action, array('friend', 'mark', 'del', 'postReply', 'overlook', 'post', 'agree', 'markgroup', 'shield',
	'unshield','open','close','replay')) && ajaxExport("非法操作請返回");
if (in_array($action, array('friend', 'agree','overlook'))) {
	L::loadClass('friend', 'friend', false);
	$friendObj = new PW_Friend(FRIEND_SEPARATER);
}
if(!$winduid) ajaxExport(array('bool' => $bool, 'message' => '你還沒有登錄'));
if ('friend' == $action) {
	/*
	S::gp(array('gname'));
	if ($gname == '-2') {
		 $friend = $friendObj->getFriends($winduid);
		 $group	 = $friendObj->getFriendColonys($winduid);
		 $json	 = array('friend'=>$friend,'group'=>$group);
		 ajaxExport($json);
	} elseif ($gname == '0') {
		 ajaxExport($friendObj->getFriendsByColony($winduid, 0));
	} elseif ($gname) {
		 ajaxExport($friendObj->getFriendsByColony($winduid, $gname, 'name'));
	} else {
		$friends = array();
		$attentionService = L::loadClass('attention','friend');
		$attentionList = $attentionService->getUidsInFollowList($winduid);
		if(S::isArray($attentionList)) {
			$userService = L::loadClass('userservice','user');
			$friends = $userService->getUserNamesByUserIds($attentionList);
		}
		ajaxExport($friends);
	}
	*/
	$attention = array();
	$attentionService = L::loadClass('attention','friend');
	$attentionList = $attentionService->getUidsInFollowList($winduid);
	if(S::isArray($attentionList)) {
		$userService = L::loadClass('userservice','user');
		$attention = $userService->getUserNamesByUserIds($attentionList);
	}
	$friend = $friendObj->getFriends($winduid);
	$json	 = array('friend'=>$friend,'attention'=>implode(FRIEND_SEPARATER,$attention));
	ajaxExport($json);
	
} elseif ('mark' == $action) {
	S::gp(array('rids'), 'GP');
	empty($rids) && ajaxExport("非法操作請返回");
	!is_array($rids) && $rids = explode(',', trim($rids, ','));
	if (!($messageServer->markMessages($winduid, $rids))) {
		ajaxExport("標記已讀操作失敗");
	}
	ajaxExport("標記已讀操作成功!");
} elseif ('markgroup' == $action) {
	S::gp(array('rids'), 'GP');
	empty($rids) && ajaxExport("非法操作請返回");
	!is_array($rids) && $rids = explode(',', trim($rids, ','));
	if (!($messageServer->markGroupMessages($winduid, $rids))) {
		ajaxExport("標記已讀操作失敗");
	}
	ajaxExport("標記已讀操作成功!");
} elseif ('del' == $action) {
	S::gp(array('rids'), 'GP');
	empty($rids) && ajaxExport("非法操作請返回");
	!is_array($rids) && $rids = explode(',', trim($rids, ','));
	if (!($messageServer->deleteMessages($winduid, $rids))) {
		ajaxExport("刪除操作失敗");
	}
	ajaxExport("刪除操作成功!");
} elseif ('postReply' == $action) {
	S::gp(array('parentMid', 'atc_content','rid','gdcode','flashatt','tid','ifMessagePostReply'), 'GP');
	if(!$_G['allowmessege']) ajaxExport(array('bool' => false, 'message' => '你所在的用戶組不能發送消息'));
	if(($db_gdcheck & 8) && false === GdConfirm($gdcode,true)){
		ajaxExport(array('bool' => false, 'message' => '你的驗證碼不正確或過期'));
	}
	empty($parentMid) && ajaxExport(array('bool' => false, 'message' => '非法操作請返回'));
	empty($atc_content) && $atc_content !== '0' && ajaxExport(array('bool' => false, 'message' => '回復內容不能為空'));
	$atc_content = trim(strip_tags($atc_content));
	$filterUtil = L::loadClass('filterutil', 'filter');
	$atc_content = $filterUtil->convert($atc_content);
	$messageInfo = array('create_uid' => $winduid, 'create_username' => $windid, 'title' => $windid,
		'content' => $atc_content);
	if (!($message = $messageServer->sendReply($winduid, $rid, $parentMid, $messageInfo))) {
		ajaxExport(array('bool' => false, 'message' => '回復失敗'));
	} else {
		L::loadClass('messageupload', 'upload', false);
		if ($db_allowupload && $_G['allowupload'] && (PwUpload::getUploadNum() || $flashatt)) {
			S::gp(array('savetoalbum', 'albumid'), 'P', 2);
			$messageAtt = new messageAtt($parentMid,$rid);
			$messageAtt->setFlashAtt($flashatt, $savetoalbum, $albumid);
			$attachData = PwUpload::upload($messageAtt);
		}
	}
	if ($ifMessagePostReply) {
		$pingService = L::loadClass('ping', 'forum');
		if (($pingService->checkReplyRight($tid)) !== true) {
			ajaxExport(array('bool' => false, 'message' => '您不能對帖子進行回復'));
		}
		$atc_content = $atc_content."\r\n\r\n[size=2][color=#a5a5a5]內容來自[短消息][/color] [/size]";
		if ($result = $pingService->addPost($tid, $atc_content) !== true) {
			ajaxExport(array('bool' => false, 'message' => $result));
		}
	}
	ajaxExport(array('bool' => true, 'message' => '消息已發送'));
} elseif ($action == 'overlook') {
	/* 忽略請求 */
	S::gp(array('rids', 'typeid','fuid'), 'GP');
	empty($rids) && ajaxExport("非法操作請返回");
	!is_array($rids) && $rids = explode(',', trim($rids, ','));
	$ignoreType = $messageServer->getReverseConst($typeid);
	switch($ignoreType){
		case 'request_friend' : $msg = getLangInfo('message','friend_add_ignore');
								$friendObj->deleteMeFromFriends($winduid,$fuid);
								break;
		case 'request_group' : $msg = getLangInfo('message','colony_add_ignore');break;
		case 'request_app' : $msg = getLangInfo('message','app_add_ignore');break;
		default:$msg = getLangInfo('message','request_ignore');break;
	}
	$messageServer->overlookRequests($winduid, $rids);
	ajaxExport($msg);
} elseif ($action == 'post') {
	S::gp(array('_usernames', 'atc_title', 'atc_content','flashatt','gdcode'));
	$usernames = $_usernames;/*specia;*/
	$atc_title = trim($atc_title);
	$atc_content = trim($atc_content);
	if(($db_gdcheck & 8) && false === GdConfirm($gdcode,true)){
		ajaxExport(array('bool' => false, 'message' => '你的驗證碼不正確或過期'));
	}
	if(!$_G['allowmessege']){
		ajaxExport(array('bool' => false, 'message' => '你所在的用戶組不能發送消息'));
	}
	if ("" == $usernames) {
		ajaxExport(array('bool' => false, 'message' => '收件人不能為空'));
	}
	if (in_array($windid,$usernames)) {
		ajaxExport(array('bool' => false, 'message' => '你不能給自己發消息'));
	}
	if (count($usernames) > 1 && intval($_G['multiopen']) < 1 ) {
		ajaxExport(array('bool' => false, 'message' => '你不能發送多人消息'));
	}
	if($_FILES['attachment']){
		unset($_FILES['attachment']);
	}
	if( count($_FILES) > $db_attachnum ){
		ajaxExport(array('bool' => false, 'message' => '最多可上傳附件'.$db_attachnum.'個'));
	}
	$usernames = is_array($usernames) ? $usernames : explode(",", $usernames);
	if (in_array($windid, array($usernames))) {
		unset($usernames[$windid]);
	}
	if(!($messageServer->checkUserMessageLevle('sms',1))){
		ajaxExport(array('bool' => false, 'message' => '你已超過每日發送消息數或你的消息總數已滿'));
	}
	list($bool,$message) = $messageServer->checkReceiver($usernames);
	if(!$bool){
		ajaxExport(array('bool' => $bool, 'message' => $message));
	}
	if ("" == $atc_title) {
		ajaxExport(array('bool' => false, 'message' => '標題不能為空'));
	}
	if (200 < strlen($atc_title)) {
		ajaxExport(array('bool' => false, 'message' => '標題不能超過限度'));
	}
	if ("" == $atc_content) {
		ajaxExport(array('bool' => false, 'message' => '內容不能為空'));
	}
	if( isset($_G['messagecontentsize']) && $_G['messagecontentsize'] > 0 && strlen($atc_content) > $_G['messagecontentsize']){
		ajaxExport(array('bool' => false, 'message' => '內容超過限定長度'.$_G['messagecontentsize'].'字節'));
	}
	$filterUtil = L::loadClass('filterutil', 'filter');
	$atc_content = $filterUtil->convert($atc_content);
	$atc_title   = $filterUtil->convert($atc_title);
	$messageInfo = array('create_uid' => $winduid, 'create_username' => $windid, 'title' => $atc_title,
		'content' => $atc_content);
	$messageService = L::loadClass("message", 'message');
	if (($messageId = $messageService->sendMessage($winduid, $usernames, $messageInfo))) {
		initJob($winduid,'doSendMessage',array('user'=>$usernames));
		define('AJAX',1);
		L::loadClass('messageupload', 'upload', false);
		if ($db_allowupload && $_G['allowupload'] && (PwUpload::getUploadNum() || $flashatt)) {
			S::gp(array('savetoalbum', 'albumid'), 'P', 2);
			$messageAtt = new messageAtt($messageId);
			$messageAtt->setFlashAtt($flashatt, $savetoalbum, $albumid);
			PwUpload::upload($messageAtt);
		}
	}
	ajaxExport(array('bool' => true, 'message' => '消息已發送'));
} elseif ('agree' == $action) {
	/* 請求同意  */
	S::gp(array('rids', 'typeid', 'fid','cyid','check'), 'GP');
	empty($rids) && ajaxExport("非法操作請返回");
	!is_array($rids) && $rids = explode(',', trim($rids, ','));
	$fid && !is_array($fid) && $fid = array($fid);
	$agreeType = $messageServer->getReverseConst($typeid);
	switch ($agreeType) {
		case 'request_friend' :
			$return = $friendObj->argeeAddedFriends($winduid, $fid);
			/*xufazhang 2010-07-22 */
			$friendService = L::loadClass('Friend', 'friend'); /* @var $friendService PW_Friend */
			foreach($fid as $value){
			$friendService->addFriend($winduid, $value);
			$friendService->addFriend($value, $winduid);
			}
			$fid = $fid[0];
			$msg = getLangInfo('message',$return);
			break;
		case 'request_group' :
			$return = $check == 1 ? $friendObj->checkJoinColony($cyid,$fid):$friendObj->agreeJoinColony($cyid,$winduid, $windid);
			if($return == 'colony_check_fail'){
				/*無權限審核，直接將該消息刪除*/
				$messageServer->deleteMessages($winduid,$rids);
			}
			$msg = getLangInfo('message',$return);
			break;
		case 'request_app' :
			$return = $friendObj->agreeWithApp(0);
			$msg = getLangInfo('message',$return);
			break;
		default :
			break;
	}
	if (in_array($return,array('app_add_success','colony_joinsuccess','friend_add_success','colony_check_success'))) {
		$messageServer->agreeRequests($winduid, $rids);
	}else{
		$messageServer->updateRequest(array('status'=>0),$winduid, $rids[0]);
	}
	ajaxExport($msg);
} elseif ('shield' == $action) {
	/* 屏蔽多人消息 */
	S::gp(array('rid', 'mid'), 'GP');
	(empty($rid) || empty($mid)) && ajaxExport("非法操作請返回");
	if (!($messageServer->shieldGroupMessage($winduid, $rid, $mid))) {
		ajaxExport("屏蔽多人消息失敗");
	}
	ajaxExport("屏蔽操作成功!");
} elseif ('unshield' == $action) {
	/* 恢復多人消息 */
	S::gp(array('rid', 'mid'), 'GP');
	(empty($rid) || empty($mid)) && ajaxExport("非法操作請返回");
	if (!($messageServer->recoverGroupMessage($winduid, $rid, $mid))) {
		ajaxExport("恢復多人消息失敗");
	}
	ajaxExport("恢復操作成功!");
} elseif ('close' == $action) {
	/* 拒收群組消息 */
	S::gp(array('gid', 'mid'), 'GP');
	empty($gid) && ajaxExport("群組已刪除");
	empty($mid) && ajaxExport("非法操作請返回");
	if (!($messageServer->closeGroupMessage($winduid, $gid, $mid))) {
		ajaxExport("拒收群組消息失敗");
	}
	ajaxExport("拒收群組消息成功!");
} elseif ('open' == $action) {
	/* 啟用群組消息 */
	S::gp(array('gid', 'mid'), 'GP');
	(empty($gid) || empty($mid)) && ajaxExport("非法操作請返回");
	if (!($messageServer->openGroupMessage($winduid, $gid, $mid))) {
		ajaxExport("啟用群組消息失敗");
	}
	ajaxExport("啟用群組消息成功!");
} elseif ('replay' == $action){


}

?>