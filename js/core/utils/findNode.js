/**
 *用途：因為非ie類瀏覽器會將文字和換行等元素當作一個節點來處理，導致遍歷節點的時候存在很多兼容性問題。
	使用此方法統一忽略文字節點。
 *遍歷節點方法；
 *使用舉例：
 var a=getObj('nodeID');
 $n(a,0.1) 子節點
 $n(a,0.2) 子節點的子節點
 ....	   子節點的子節點的……
 $n(a,0.9) 最多支持0.9

 $n(a,-0.1)	 父節點
 $n(a,-0.2)	 父節點的父節點
 ....
 $n(a,-0.9) 最多支持-0.9

 $n(a,1)	 下一個節點
 $n(a,2)	 下一個節點的下一個節點
 ....

 $n(a,-1)	 上一個節點
 $n(a,-2)	 上一個節點的下一個節點
 ....

 綜合使用：

 $n(a,0.3,1,0.2,-1)  子子子節點的下一個節點的子子節點的上一個節點

 註：由於存在語義的分歧或者無意義的表達，目前尚不支持諸如：$n(a,0.21)，$n(a,1.1)，$n(a,-2.3)這樣的數值。
 */
function findNode(obj)
{
    var argu = [];
    for (var i = 1; i < arguments.length; i++)
    {
        argu.push(arguments[i]);
    }
    var n = obj;
    for (var i = 0; i < argu.length; i++)
    {
        if (argu[i] >= 1)
        {
            for (var j = 0; j < argu[i]; j++)
            {
                n = n.nextSibling;
                while (n && n.nodeType == 3)
                {
                    n = n.nextSibling;
                }
            }
        }
        if (argu[i] <= -1)
        {
            for (var j = 0; j < argu[i] * -1; j++)
            {
                n = n.previousSibling;
                while (n && n.nodeType == 3)
                {
                    n = n.previousSibling;
                }
            }
        }
        if ( - 1 < argu[i] && argu[i] < 0)
        {
            for (var j = 0; j > argu[i] * 10; j--)
            {
                n = n.parentNode;
            }
        }
        if (0 < argu[i] && argu[i] < 1)
        {
            for (var j = 0; j < argu[i] * 10; j++)
            {
                n.firstChild ? n = n.firstChild: 0;
                while (n && n.nodeType == 3)
                {
                    n = n.nextSibling;
                }
            }
        }
    }
    return  n;
};
$n=findNode;