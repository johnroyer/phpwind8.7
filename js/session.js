
function FlashPlayer(url,id) {
	return is_ie ? '<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0" width="1" height="1" id="'+id+'" name="'+id+'"><param name="movie" value="'+url+'" ></object>' : '<embed src="'+url+'" name="'+id+'" id="'+id+'" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash"  type="application/x-shockwave-flash" FlashVars="'+url.replace(/^[^\?]+\?/,'')+'" width="1" height="1"></embed>';
}
/**
 *本地數據緩存。
 注意，請將本js需要放在body標籤內，以免發生不兼容的問題。
 使用方法：Session.set(key,value);
 Session.get(key);
 */
//document.write(FlashPlayer("images/userData.swf","userData"));
var Session = {};

Session.init = function() {
	var obj = document.createElement('div');
	obj.innerHTML = FlashPlayer("images/userData.swf","userData");
	document.body.appendChild(obj);
	Session.obj = document["userData"];
	//alert(Session.obj);
}
/**
 *保存緩存數據
 *@param key 鍵名
 *@param value 鍵值
 */
Session.set =function(key,value)
{
	Session.obj.set(key,value);
};
/**
 *讀取緩存數據
 *@param key 鍵名
 */
Session.get =function(key)
{
	return document.userData.get(key);
};
/**
 *刪除緩存數據
 *@param key 鍵名
 */
Session.remove =function(key)
{
	document.userData.remove(key);
};
Session.init();