/*
* app.emotional 模塊
* 表情插入模塊
*/
Breeze.namespace('app.emotional', function (B) {
    var win = window, doc = document, IMGPATH = 'images/post/smile/',
	    defaultConfig = {
	        defaultface: defaultface,
	        face: face,
	        pageNum: 24,
			tabNum:3,
	        triggerType: 'click'
	    },
		myeditor,
    /**
    * 表情面板選擇器對像
    */
	    emotionalSelector = {
	        id: 'breeze-emotional',
	        load: function () {
	            B.require('dom', 'event', function (B) {
					var contain = B.createElement('<div class="B_menu B_p10B" id="' + emotionalSelector.id + '" style="display:none;z-index:2999"></div>'),
					outer = B.createElement('<div style="width:271px"></div>'),
					nav = B.createElement('<div class="B_menu_nav B_cc B_drag_handle"></div>'),
					closeButton = B.createElement('<a style="margin-top: 7px;" class="B_menu_adel" href="javascript:;">×</a>'),
					facePages = B.createElement('<div class="B_face_pages"></div>'),
					tabHead = B.createElement('<ul class="B_cc B_fl" style="width: 210px;"></ul>'),
					df = document.createDocumentFragment();
					/*
					* 產生表情組標籤以備tab
					*/
					var j=0;
					for (var i in facedb) {
						var navli = B.createElement('<li><a href="javascript:;">' + facedb[i] + '</a></li>');
						navli.className = i == defaultConfig.defaultface ? 'B_tab_trigger current' : 'B_tab_trigger';
						if(++j>defaultConfig.tabNum)navli.style.display = 'none';
						df.appendChild(navli);
						df.innerHTML = '';
					}
					tabHead.appendChild(df);
					nav.appendChild(B.createElement('<a class="B_menu_adel B_close" href="javascript:;">×</a>'));
					if (j > defaultConfig.tabNum) {
						nav.appendChild(B.createElement('<div title="上一頁" class="B_menu_pre fl">上一頁</div>'));
						nav.appendChild(B.createElement('<div title="下一頁" class="B_menu_next fr">下一頁</div>'));
					}
					nav.appendChild(tabHead);
					outer.appendChild(nav);

					/*
					* 產生表情圖片
					*/
					for (var i in faces) {//無法用forEach,因為老版本的數組原因
						if (faces.hasOwnProperty(i)) {
							var faceList = B.createElement('<div class="B_tab_panel"></div>'),
							faceGroup = B.createElement('<ul class="B_face_list B_cc"><ul>'), faceul = [];
							faceList.style.display = i == defaultConfig.defaultface ? 'block' : 'none';
							for (var j in faces[i]) {
								if (faces[i].hasOwnProperty(j)) {
									var li = '<li><img data-code="' + faces[i][j] + '" src="' + IMGPATH + face[faces[i][j]][0] + '" title="' + face[faces[i][j]][1] + '" onclick="" /></li>';
									faceul.push(li);
								}
							}
							faceGroup.innerHTML = faceul.slice(0, defaultConfig.pageNum).join(''); //只顯示第一頁的條數
							faceList.appendChild(faceGroup);
							outer.appendChild(faceList);

							/*
							* 產生表情分頁標籤
							*/
							var pageNum = defaultConfig.pageNum, itemCount = faceul.length;
							if (itemCount > pageNum) {//需要分頁時才創建分頁
								var pageCount = itemCount % pageNum == 0 ? itemCount / pageNum : Math.floor(itemCount / pageNum) + 1;
								var pageGroup = B.createElement('<ul class="B_face_pages B_cc"></ul>');
								for (var i = 0; i < pageCount; i++) {
									var pageEl = B.createElement('<a href="javascript:;">' + (i + 1) + '</a>');
									pageEl.className = i == 0 ? 'current' : "";
									(function (i, pageEl, faceul, faceGroup, pageGroup) {//分頁事件處理
										B.addEvent(pageEl, 'click', function (e) {
											e.halt();
											faceGroup.innerHTML = faceul.slice(i * pageNum, i * pageNum + defaultConfig.pageNum).join('');
											B.removeClass(B.$('.current', pageGroup), 'current');
											B.addClass(pageEl, 'current');
											B.$$('li img', faceGroup).forEach(function(n){
												B.addEvent(n,'click', function (e) {
													insertEmotional(n);
												});
											});
										});
									})(i, pageEl, faceul, faceGroup, pageGroup);
									var li = B.createElement('li');
									li.appendChild(pageEl);
									pageGroup.appendChild(li);
								}
								faceList.appendChild(pageGroup);
							}
						}
					}
					contain.appendChild(outer);
					doc.body.appendChild(contain);
	            });
	        }
	    }
    /**
    * 隱藏面板
    */
    function hideEmotionalSelector() {
        B.$('#' + emotionalSelector.id).style.display = 'none';
    }

	function insertEmotional(obj) {
		var code = B.attr(obj, 'data-code'),
			src = obj.src;
		//myeditor.saveRng();
		insertTrigger('<img src="'+ src +'" title="'+ obj.title +'" emotion="'+ code +'" >');
		B.UA.gecko && myeditor.focus();
		/*
		if (myeditor.currentMode == 'UBB' && B.UA.ie){
			setTimeout(function(){
				var rng = myeditor.getRng();
				rng.collapse(false);
				rng.select();
			}, 100);
		}
		*/
	}

	function showTab(p) {
		var index = -1;
		var menus = B.$$('#' + emotionalSelector.id + ' .B_tab_trigger');
		for (var i = 0; i < menus.length; i++) {
			if (menus[i].style.display != "none") {
				index = i;
				break;
			}
		}
		index += p;
		if (index < 0 || index + defaultConfig.tabNum > menus.length) return;
		for (i = 0; i < menus.length; i++){
			if (i >= index && i < index + defaultConfig.tabNum){
				menus[i].style.display = "";
			} else{
				menus[i].style.display = "none";
			}
		}
	}

    /**
    * emotional類
    */
    B.require('util.dialog', function (B) {//add event for Emotional
        var selector = '#' + emotionalSelector.id;
        if(!B.$(selector)) {
            emotionalSelector.load();
        }
        /**
        * 顯示表情選擇面板
        */
        B.util.dialog({
            id: emotionalSelector.id,
            reuse: true,
            callback:function(popup) {
                /**
                * 回調觸發
                */
                B.$$(selector + ' .B_face_list li img').forEach(function(n){
					B.addEvent(n,'mousedown', function (e) {
						insertEmotional(n);
						e.halt();
						//popup.closep();
					});
					
                });
                /**
                * 關閉面板事件
                */
                B.$$(selector + ' .B_menu_adel').forEach(function (n) {
                    B.addEvent(n, 'click', function (e) {
                        e.preventDefault();
                        hideEmotionalSelector();
                    });
                });


                /**
                * 點擊左右箭頭切換tab(上一頁)
                */
                B.addEvent(B.$(selector + ' .B_menu_pre'), 'click', function (e) {
					showTab(-1);
                    e.preventDefault();
                    //切換tab

                });

                /**
                * 點擊左右箭頭切換tab(下一頁)
                */
                B.addEvent(B.$(selector + ' .B_menu_next'), 'click', function (e) {
					showTab(1);
                    e.preventDefault();
                    //切換tab
                });
                /**
                * 切換tab時觸發
                */
                B.$$(selector + ' .B_tab_trigger').forEach(function (n) {
                    B.addEvent(n, 'click', function (e) {
                        B.$$(selector + ' .B_tab_trigger').forEach(function (n) {
                            B.removeClass(n, 'current');
                        });
                        B.addClass(this, 'current');
						e.preventDefault();
                    });
                });
                
                /**
                * 產生tab效果
                */
                B.require('util.scrollable', function (B) {
                    B.util.tabs("#" + emotionalSelector.id);
                });
                popup.closep();
            }
        });
		/**
		* @description 表情選擇器
		* @params {le} le
		* @params {Function} 點擊點擊具體表情產生的回調函數
		*/
		B.app.emotional = function (elem, callback, editor) {
			insertTrigger = callback;
			myeditor = editor;
			B.util.dialog({
				id: emotionalSelector.id,
				pos: ['leftAlign', 'bottom']
			},elem);
		}
    });
});

/*
*PS:此組件兼容老版本的表情選擇器,整個組件代碼由兩大部分組成：DOM結構生成(emotionalSelector類) + 事件處理(Emotional類)
*箭頭切tab換暫時沒有實現,因為tab組件中不包含,在這裡寫又顯得功能多餘了,待下一步實現
*/