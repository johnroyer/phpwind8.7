/*
* app.music 模塊
* 蝦米音樂插入模塊
* 因為沿用舊代碼，所有有很多全局變量。
*/
Breeze.namespace('app.pagecut', function (B) {
	B.app.pagecut = function(elem, callback, editor) {
		var code = "\n[###page###]\n";
		editor.pasteHTML(code, "");
	}
});