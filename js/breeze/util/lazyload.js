/*
* lazyload 模塊
* 
*/
Breeze.namespace('util.lazyload', function(B) {
    var win = window,doc = document,
        defaultConfig = {
            container :win,
            img_data : 'data-src',
            area_cls : 'bz-lazyLoad',
            delay : 100,//resize時和socrll時延遲處理,以免頻繁觸發,100毫秒基本無視覺問題
            placeholder :''//圖片佔位符
        }
    
    function Lazyload(selector,settings) {
        var self = this;
        B.merge(self, defaultConfig, settings);
        if( !(self instanceof Lazyload) ) {
			return new Lazyload(selector, settings);
		}
		B.require('dom','event',function(B) {
		   var lazyImgs = [],lazyAreas = [],
		    container = self.container.nodeType===1 ? self.container :win,
		    threshold = function() {
		        if(container===win) {
		            var scrollTop =  win.pageYOffset || container.scrollTop || doc.documentElement.scrollTop || doc.body.scrollTop,
		            eHeight = doc.documentElement.innerHeight || ducument.body.clientHeight || ducument.documentElement.clientHeight;
		            return scrollTop + eHeight;
		        }
		        return B.offset(container).top + container.clientHeight;
		    },
		    eHeight = function() {return container.innerHeight || container.clientHeight;}
		    B.$$(selector).forEach(function (n) {
                if(n.nodeName === 'IMG' && n.getAttribute(self.img_data)) {
                    lazyImgs.push(n);
                    if(self.placeholder!==''){
                        n.src = self.placeholderl;
                    }
                }
                if(n.nodeName === 'TEXTAREA' && B.hasClass(n,self.area_cls)) {
                    lazyAreas.push(n);
                }
            });
            //加載image	
            var _loadImgs = function() {
		        lazyImgs.forEach(function(n) {
		            if(!n.src || n.src==='') {
		                if(B.offset(n).top <= threshold()) {
		                    n.src = n.getAttribute(self.img_data);
		                }
		            }
		        });
	        };
        	
        	//加載textarea	
	        var _loadAreas = function() {
        	    lazyAreas.forEach(function(n) {
        	        var isHide = true,
        	            top = B.offset(isHide?n.parentNode:n).top;
        	        if(B.hasClass(n,self.area_cls)) {//當沒有加載的時候才加載,有可能已經加載過
        	            if(top <= threshold()) {
        	                n.style.display = 'none';n.className = '';
        	                var elem = B.createElement('<div>' + n.value + '</div>');
        	                B.insertBefore(elem,n);
        	            }
        	        }
        	    });
	        };
	        
	        var _loadAll = function() {
	            var timer;
                if(timer) {
                    clearTimeout(timer);
                }
                timer = setTimeout(function() {//延遲執行
                    _loadImgs();
                    _loadAreas();
                },self.delay);
	        }
            B.addEvent(container,'scroll',function() {
                _loadAll();
            });
            B.addEvent(container,'resize',function() {
                _loadAll();
            }); 
            _loadAll();//默認加載一次
		});
    }
    
    
    /**
	 * @description 數據延遲加載
	 * @params {String} 要延遲加載的元素
	 * @params {PlainObject} 設置參數 包含{
	 *                                   container://要產生滾動的元素,
	 *                                   delay://延遲加載的時間,
	 *                                   placeholder://圖片佔位符,
	 *                                   img_data : //圖片的自定義屬性,
     *                                   area_cls : //textarea的class名
	 *                                   }
	 */
	B.util.lazyload = function(selector,settings) {
	    Lazyload(selector,settings);
    }
});
/*
當前只考慮豎向滾動時的延遲加載,橫向基本上很少用到,所以暫時不考慮,在圖片延遲加載中,
只能加img-data參數,加了src就達不到延遲效果
*/