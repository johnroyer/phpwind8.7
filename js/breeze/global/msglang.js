Breeze.namespace('global.msglang', function(B){
	window.msglang = {
		'upload_content_error' : '附件內容非法,系統已經將其自動刪除!',
		'upload_error' : '上傳附件失敗，造成的原因可能有:附件目錄不可寫(777)、空間在安全模式下、空間大小已不足。',
		'data_error' : '參數不足!',
		'not_login' : '您還沒有登錄，不能上傳附件!',
		'upload_group_right' : '你所屬的用戶組沒有上傳附件的權限'
	};
});