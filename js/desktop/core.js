IE = document.all;
PW={};
/*
 * 刪除dom節點，並釋放內存
 *@param nodeElement htmlElement 節點對像
 */
$removeNode=function(htmlElement)
{
	var a=document.createElement("DIV");
	a.appendChild(htmlElement);
	a.innerHTML="";
	a=null;
};
/*
 *根據ID獲取節點對像
 *@param String id 節點id
 */
var getObj = function(id) 
{
    return document.getElementById(id);
};

//通用domready
(function(win,doc){
	var dom = [];
	dom.isReady  = false;
	win.onReady = function(fn){
		if ( dom.isReady ) {
			fn()
		} else {
			dom.push( fn );
		}
	}
	dom.fireReady = function() {
		if ( !dom.isReady ) {
			if ( !doc.body ) {
				return setTimeout( dom.fireReady, 16 );
			}
			dom.isReady = 1;
			if ( dom.length ) {
				for(var i = 0, fn;fn = dom[i];i++)
					fn()
			}
		}
	}
	if ( doc.readyState === "complete" ) {
		dom.fireReady();
	}else if(-[1,] ){
		doc.addEventListener( "DOMContentLoaded", function() {
	  		doc.removeEventListener( "DOMContentLoaded",  arguments.callee , false );
	  		dom.fireReady();
		}, false );
	}else {
		doc.attachEvent("onreadystatechange", function() {
		  if ( doc.readyState == "complete" ) {
			doc.detachEvent("onreadystatechange", arguments.callee );
			dom.fireReady();
		  }
	});
	(function(){
		if ( dom.isReady ) {
			return;
		}
		var node = new Image();
		try {
			node.doScroll();
			node = null;
		} catch( e ) {
			setTimeout( arguments.callee, 64 );
			return;
		}
	  	dom.fireReady();
	})();
  }
})(window,document);

(function(win){
	/**
	 *通用事件處理函數
	 */
	// http://dean.edwards.name/weblog/2005/10/add-event/
	win.addEvent = function(element, type, handler) {
		if (element.addEventListener) {
			element.addEventListener(type, handler, false);
		} else {
			if (!handler.$$guid) handler.$$guid = addEvent.guid++;
			if (!element.events) element.events = {};
			var handlers = element.events[type];
			if (!handlers) {
				handlers = element.events[type] = {};
				if (element["on" + type]) {
					handlers[0] = element["on" + type];
				}
			}
			handlers[handler.$$guid] = handler;
			element["on" + type] = handleEvent;
		}
	};
	addEvent.guid = 1;
	function removeEvent(element, type, handler) {
		if (element.removeEventListener) {
			element.removeEventListener(type, handler, false);
		} else {
			if (element.events && element.events[type]) {
				delete element.events[type][handler.$$guid];
			}
		}
	};
	win.removeEvent=removeEvent;
	function handleEvent(event) {
		var returnValue = true;
		event = event || fixEvent(((this.ownerDocument || this.document || this).parentWindow || window).event);
		var handlers = this.events[event.type];
		for (var i in handlers) {
			this.$$handleEvent = handlers[i];
			if (this.$$handleEvent(event) === false) {
				returnValue = false;
			}
		}
		return returnValue;
	};
	function fixEvent(event) {
		event.preventDefault = fixEvent.preventDefault;
		event.stopPropagation = fixEvent.stopPropagation;
		return event;
	};
	fixEvent.preventDefault = function() {
		this.returnValue = false;
	};
	fixEvent.stopPropagation = function() {
		this.cancelBubble = true;
	};
})(window);
/**
 *創建函數的代理，並將作用域(this)指向首個參數
 * @param Object b 當前作用域對象，將b替換函數內的this對像
 * @example 使用舉例：killError=function(){return true};
 * window.onerror= killError.delegate();
 */
Function.prototype.delegate = function(b)
{
    var _ = this;
    return function()
    {
        try{return _.call(b)}catch(e){}
    }
};
/**
 *類繼承
 *@param  JSON overrides 屬性和方法的重載
 */
Function.prototype.extend = function(overrides)
{
    var F = function(config)
    {
        if (typeof(config) == "object")
        {
            for (var i in config)
            {
               try{ this[i] = config[i];}catch(e){}
            }
        }
    };
    F.prototype = new this;
    if (overrides)
    {
        for (var i in overrides)
        {
			/**
			 *safri 瀏覽器在遍歷時會報一個異常
			 */
            try{F.prototype[i] = overrides[i];}catch(e){}
        }
    }
    return F;

};

/*
 *組件基類，這個是所有組件類的父類，或者父父類，任何組件類都繼承自此類，這樣可以有比較統一的配置項
 *@param JSON config 組件配置
 */
var baseClass=function(config)	 
{
	if (typeof(config) == "object")
	{
		for (var i in config)
		{
			try{this[i] = config[i];}catch(e){}
		}
	}
};