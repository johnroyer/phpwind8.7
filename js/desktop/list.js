/*<![CDATA[*/

/**
 * <p>
 * Title: 數據列表生成
 * </p>
 * <p>
 * @{#} list.js Created on 2009-21-04 18:01:53
 * </p>
 * <p>
 * Description:  根據指定的模塊和數據列生成頁面數據列表,如用戶列表.
 * </p>
 * <p>
 * </p>
 * 	更新功能，已支持直接寫函數，函數體寫在區域<f></f>之間
 * 2009-1-8 增強函數體內的{var}的轉義\{檢測，\{將不列入變量解析範圍。$0-9不做解析，因為函數體可能用到正則表達式
 * 匿名函數可縮寫為f(){}，return可縮寫為r。系統自動檢測r的書寫位置(兩邊非數字)是否可以被替換為return;
 * @version 1.0
 */
window.desk?0:desk={};
window.G?0:G={};
String.prototype.r=function(f,r)
{
	var c=this+"";
	if(f instanceof Array)
	{
		for (var i=0; i<f.length; i++)
		{
			c=c.replace(f[i],r[i]);
		}
	}
	else
	{
		c=c.replace(f,r);
	}
	return c;
};
desk.list={

	/**
	 * 根據指定的數據列生成頁面數據列表,如用戶列表.
	 * @param {String}	 data 		列表數據來源,數組列表
	 * @param {String}	 module 	列表生成規則模板字符串,模板字段以{數據字段名}傳遞,然後進行匹配替換.
	 *								{this}表示數據對像本體，要傳遞參數，在函數後用|分割開，如：cDate|__object__注意：函數體內不能出現|符號，應當轉義處理
	 * @param {String}	 str 		如無數據來源或數據來源為空時的顯示默認值
	 * @return {String}  			用於顯示數據列表的HTML代碼,直接innerHTML即可. 
	 */
simple: function(j, m, n) {
    var s = "",q_=[];
	var backup_j=j;
	var feedBack;
	if(j.json)
	{
		m=j.module;
		n=j.nullText;
		feedBack=j.each;
		j=j.json;

	}
    var q = m = m.r(/\s{2,}/g,' ').r(/\n/g,'');
    var r = a = d = "";
    for (var i = 0,l = j.length; i < l; i++) {
        try {
            j[i].list_id = i
        } catch(e) {
            j[i] = [j[i]]
        }
        //分割2010-2-26 by lh
        if(i==7){
        	s += "<li id='admin_hr'></li>";
    		q_.push(returnString);
        }
        m.r(/(^|[^\\])((?:\$\d+?)|(?:\{[\w|\|\'|\"]+\}))(?:<f>([\s|\S]+?)\<\/f\>)?/g,
        function(o, l, z, c) {
            z = z.r("$", "").r("{", "").r("}", "");
            a = [(c || "")];
            d = j[i];
            var m = z.split("||");
            var f = '';
            for (var p = 0,le = m.length; p < le; p++) {

                m[p]?f = f || (/[\'|\"]/.test(m[p])?eval(m[p]):d[m[p]]):0;
            }
			
            d[z] = f;
            var g = "";
            if (a[0] == "") {
                g = ""
            } else {
				
                G.TO = j[i];
                var b = a[0].match(/[^\\]\{[\w|\|]+\}/g) || [];
                for (var k = 0, le = b.length; k < le; k++) {
                    b[k] = b[k].r(/^[^\{]/, '');
                    if (b[k] == "{this}") {
                        a[0] = a[0].r(/\{this\}/g, "G.TO")
                    } else {
                        a[0] = a[0].r(eval("/" + b[k].r(/\|/g, "\\|").r("{", "\{").r("}", "\}") + "/g"), d[b[k].r("{", "").r("}", "")])
                    }
                } 
                var h;
                if (a[1] == "__object__" || a[1] == "this") {
                    h = "G.TO"
                } else {
                    h = "'" + d[a[1]] + "'"
                } 
                try {
                    if (a[0].indexOf(".") == 0) {
                        g = eval("('" + (d[z] || "").toString().r(/\'/g, "\'") + "')" + a[0])
                    } else {
							
                        g = eval("(" + a[0].r(/^f\(/, 'function(').r(/([\W]|[\{])r\s+/, '$1return ') + ")('" + (d[z] || "").toString().r(/\'/g, "\'") + "'" + (a[1] ? "," + h + "": "") + ")")
                    }
                } catch(e) {
                    g = ""
                }
            }
            q = q.r(o, l + (c ? g: (d[z] || "").toString()))
        });
		var returnString= q.r([/@LK;/g, /@RK;/g, /@LT;/g,/@GT;/g],["{","}","<",">"]);
        s += returnString;
		q_.push(returnString);

        q = m;

    }
	var fd=function(){if(backup_j.json.length==0){return feedBack(backup_j.nullText)}feedBack(q_.splice(0,backup_j.once||3).join(""));q_.length?setTimeout(fd,backup_j.timer||50):(backup_j.onload?backup_j.onload():0);};
		feedBack?fd():0;
    return j.length > 0 ? s: n
}
};

/*]]>*/