<?php

// uc_client 包的根目錄
define('UC_CLIENT_ROOT', dirname(__FILE__) . '/');
// uc_client 包使用的 lib 庫所在的目錄
// define('UC_LIB_ROOT', dirname(__FILE__) . '/../lib/');
// uc_client 包的版本
define('UC_CLIENT_VERSION', '0.1.0');
// uc_client 使用的API規範號
define('UC_CLIENT_API', '20090609');

/**
 用戶登錄
  @param  string $username   - 用戶名
  @param  string $pwd        - 密碼(md5)
  @param  int $logintype     - 登錄類型 0,1,2分別為 用戶名,uid,郵箱登錄
  @param  boolean $checkques - 是否要驗證安全問題
  @param  string $question   - 安全問題
  @param  string $answer     - 安全回答
  @return array 同步登錄的代碼
*/
function uc_user_login($username, $password, $logintype, $checkques = 0, $question = '', $answer = '') {
	return uc_data_request('user', 'login', array($username, $password, $logintype, $checkques, $question, $answer));
}

/**
 同步退出
  @return string 同步退出的代碼
*/
function uc_user_synlogout() {
	return uc_data_request('user', 'synlogout');
}

/**
 註冊
  @param  string $username - 註冊用戶名
  @param  string $password - 註冊密碼(md5)
  @param  string $email	   - 郵箱
  @return int 註冊用戶uid
*/
function uc_user_register($username, $password, $email) {
	$args = func_get_args();
	return uc_data_request('user', 'register', $args);
}
/**
 獲取用戶信息
  @param  string $username - 用戶名
  @param  int $bytype - 獲取方式 0,1,2分別為 用戶名,uid,郵箱
  @return array uid,用戶名,郵箱
*/
function uc_user_get($username, $bytype = 0) {
	return uc_data_request('user', 'get', array($username, $bytype));
}

/**
 驗證
  @param  string $uid - 用戶名
  @checkstr string password - uc_key+passwrord
  @return array uid,用戶名,密碼,郵箱
*/
function uc_user_check($uid, $checkstr) {
	$args = func_get_args();
	return uc_data_request('user', 'check', $args);
}

/**
 編輯用戶資料
  @param  int $uid - 用戶uid
  @param  string $oldname - 原用戶名
  @param  string $newname - 新用戶名
  @param  string $pwd - 新密碼
  @param  string $email - 新郵箱
*/
function uc_user_edit($uid, $oldname, $newname, $pwd, $email) {
	return uc_data_request('user', 'edit', array($uid, $oldname, $newname, $pwd, $email));
}

/**
 刪除指定 uid 的用戶
  @param  mixed $uids - 用戶uid序列，支持單個uid,多個uid數組或者用「,」隔開的字符串序列
  @param  int $del
*/
function uc_user_delete($uids) {
	return uc_data_request('user', 'delete', array($uids));
}

/**
 設置用戶積分增減
  @param  array $credit array($uid1 => array($ctype1 => $point1, $ctype2 => $point2), $uid2 => array())
  return array
 */
function uc_credit_add($credit, $isAdd = true) {
	return uc_data_request('credit', 'add', array($credit, $isAdd));
}

function uc_credit_get($uid) {
	return uc_data_request('credit', 'get', array($uid));
}


function uc_data_request($class,$method,$args = array()) {
	static $uc = null;
	if (empty($uc)) {
		require_once UC_CLIENT_ROOT . 'class_core.php';
		$uc = new UC();
	}
	$class = $uc->control($class);

	if (method_exists($class, $method)) {
		return call_user_func_array(array(&$class, $method), $args);
	} else {
		return 'error';
	}
}
?>