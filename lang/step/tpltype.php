<?php
!defined('PW_UPLOAD') && exit('Forbidden');
//732 to 75
//UPDATE pw_block
$db->update("REPLACE INTO `pw_block` (`bid`, `sid`, `func`, `name`, `rang`, `cachetime`, `iflock`) VALUES
(1, 1, 'newsubject', '最新主題', '', 1800, 1),
(2, 1, 'newreply', '最新回復', '', 600, 1),
(3, 1, 'digestsubject', '精華主題', '', 86400, 1),
(4, 1, 'replysort', '回復排行', '', 86400, 1),
(5, 1, 'hitsort', '人氣排行', '', 86400, 1),
(6, 2, 'usersort', '金錢排行', 'money', 3000, 1),
(7, 2, 'usersort', '威望排行', 'rvrc', 3000, 1),
(8, 3, 'forumsort', '今日發帖', 'tpost', 600, 1),
(9, 3, 'forumsort', '主題數排行', 'topic', 86400, 1),
(10, 3, 'forumsort', '發帖量排行', 'article', 86400, 1),
(11, 4, 'gettags', '熱門標籤', 'hot', 86400, 1),
(12, 4, 'gettags', '最新標籤', 'new', 86400, 1),
(13, 5, 'newpic', '最新圖片', '', 1700, 1),
(15, 6, 'hotactive', '熱門活動', '', 86400, 1),
(18, 2, 'usersort', '在線時間排行', 'onlinetime', 86400, 1),
(19, 2, 'usersort', '今日發帖排行', 'todaypost', 1200, 1),
(20, 2, 'usersort', '月發帖排行', 'monthpost', 40000, 1),
(21, 2, 'usersort', '發帖排行', 'postnum', 40000, 1),
(22, 2, 'usersort', '月在線排行', 'monoltime', 40000, 1),
(23, 1, 'replysortday', '今日熱帖', '', 1800, 1),
(25, 2, 'usersort', '貢獻值排行', 'credit', 3000, 1),
(26, 2, 'usersort', '交易幣排行榜', 'currency', 3000, 1),
(29, 1, 'highlightsubject', '加亮主題', '', 50000, 1),
(38, 6, 'todayactive', '今日活動', '', 3600, 1),
(41, 6, 'newactive', '最新活動', '', 1800, 1),
(49, 1, 'replysortweek', '近期熱帖', '', 86400, 1),
(47, 1, 'hitsortday', '今日人氣', '', 1800, 1)");

//update pw_stamp
$db->update("REPLACE INTO `pw_stamp` (`sid`, `name`, `stamp`, `init`, `iflock`, `iffid`) VALUES
(1, '帖子排行', 'subject', 1, 1, 1),
(2, '用戶排行', 'user', 6, 1, 0),
(3, '版塊排行', 'forum', 9, 1, 0),
(4, '標籤排行', 'tag', 11, 1, 0),
(5, '圖片', 'image', 13, 1, 1),
(6, '活動', 'active', 41, 1, 1)");

//update pw_tpltype
$db->update("REPLACE INTO `pw_tpltype` (`id`, `type`, `name`) VALUES
(1, 'subject', '帖子類'),
(2, 'image', '圖片類'),
(3, 'forum', '版塊類'),
(4, 'user', '用戶類'),
(5, 'tag', '標籤類'),
(6, 'player', '播放器')");

?>