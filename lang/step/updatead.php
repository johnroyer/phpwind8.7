<?php
!defined('PW_UPLOAD') && exit('Forbidden');
$maxid = $db->get_value("SELECT MAX(id) FROM pw_advert");
$maxid = $maxid < 100 ? 101 : $maxid+1;

$db->update("ALTER TABLE pw_advert AUTO_INCREMENT=$maxid",false);

//$query = $db->query("");//TODO 轉換id小於100的記錄
$query = $db->query("SELECT * FROM pw_advert WHERE id<=$maxid AND type=1");
while ($rt = $db->fetch_array($query)) {
	$ads[] = array($rt['type'],$rt['uid'],$rt['ckey'],$rt['stime'],$rt['etime'],$rt['ifshow'],$rt['orderby'],$rt['descrip'],$rt['config']);
}
if ($ads) {
	$db->update("INSERT INTO pw_advert(type,uid,ckey,stime,etime,ifshow,orderby,descrip,config) VALUES ".pwSqlMulti($ads,false));
}
$maxid = $maxid - 1;
$db->update("DELETE FROM pw_advert WHERE id<=$maxid AND type=1");

$arrSQL = array(
	"REPLACE INTO pw_advert VALUES(1, 0, 0, 'Site.Header', 0, 0, 1, 0, '頭部橫幅~	~顯示在頁面的頭部，一般以圖片或flash方式顯示，多條廣告時系統將隨機選取一條顯示', 'a:1:{s:7:\"display\";s:4:\"rand\";}');",

	"REPLACE INTO pw_advert VALUES(2, 0, 0, 'Site.Footer', 0, 0, 1, 0, '底部橫幅~	~顯示在頁面的底部，一般以圖片或flash方式顯示，多條廣告時系統將隨機選取一條顯示', 'a:1:{s:7:\"display\";s:4:\"rand\";}');",

	"REPLACE INTO pw_advert VALUES(3, 0, 0, 'Site.NavBanner1', 0, 0, 1, 0, '導航通欄[1]~	~顯示在主導航的下面，一般以圖片或flash方式顯示，多條廣告時系統將隨機選取一條顯示', 'a:1:{s:7:\"display\";s:4:\"rand\";}');",

	"REPLACE INTO pw_advert VALUES(4, 0, 0, 'Site.NavBanner2', 0, 0, 1, 0, '導航通欄[2]~	~顯示在頭部通欄廣告[1]位置的下面,與通欄廣告[1]可一起顯示,一般為圖片廣告', 'a:1:{s:7:\"display\";s:4:\"rand\";}');",

	"REPLACE INTO pw_advert VALUES(5, 0, 0, 'Site.PopupNotice', 0, 0, 1, 0, '彈窗廣告[右下]~	~在頁面右下角以浮動的層彈出顯示，此廣告內容需要單獨設置相關窗口參數', 'a:1:{s:7:\"display\";s:4:\"rand\";}');",

	"REPLACE INTO pw_advert VALUES(6, 0, 0, 'Site.FloatRand', 0, 0, 1, 0, '漂浮廣告[隨機]~	~以各種形式在頁面內隨機漂浮的廣告', 'a:1:{s:7:\"display\";s:4:\"rand\";}');",

	"REPLACE INTO pw_advert VALUES(7, 0, 0, 'Site.FloatLeft', 0, 0, 1, 0, '漂浮廣告[左]~	~以各種形式在頁面左邊漂浮的廣告，俗稱對聯廣告[左]', 'a:1:{s:7:\"display\";s:4:\"rand\";}');",

	"REPLACE INTO pw_advert VALUES(8, 0, 0, 'Site.FloatRight', 0, 0, 1, 0, '漂浮廣告[右]~	~以各種形式在頁面右邊漂浮的廣告，俗稱對聯廣告[右]', 'a:1:{s:7:\"display\";s:4:\"rand\";}');",

	"REPLACE INTO pw_advert VALUES(9, 0, 0, 'Mode.TextIndex', 0, 0, 1, 0, '文字廣告[論壇首頁]~	~顯示在頁面的導航下面，一般以文字方式顯示，每行四條廣告，超過四條將換行顯示', 'a:1:{s:7:\"display\";s:3:\"all\";}');",

	"REPLACE INTO pw_advert VALUES(10, 0, 0, 'Mode.Forum.TextRead', 0, 0, 1, 0, '文字廣告[帖子頁]~	~顯示在頁面的導航下面，一般以文字方式顯示，每行四條廣告，超過四條將換行顯示', 'a:1:{s:7:\"display\";s:3:\"all\";}');",

	"REPLACE INTO pw_advert VALUES(11, 0, 0, 'Mode.Forum.TextThread', 0, 0, 1, 0, '文字廣告[主題頁]~	~顯示在頁面的導航下面，一般以文字方式顯示，每行四條廣告，超過四條將換行顯示', 'a:1:{s:7:\"display\";s:3:\"all\";}');",

	"REPLACE INTO pw_advert VALUES(12, 0, 0, 'Mode.Forum.Layer.TidRight', 0, 0, 1, 0, '樓層廣告[帖子右側]~	~出現在帖子右側，一般以圖片或文字顯示，多條帖間廣告時系統將隨機選取一條顯示', 'a:1:{s:7:\"display\";s:4:\"rand\";}');",

	"REPLACE INTO pw_advert VALUES(13, 0, 0, 'Mode.Forum.Layer.TidDown', 0, 0, 1, 0, '樓層廣告[帖子下方]~	~出現在帖子下方，一般以圖片或文字顯示，多條帖間廣告時系統將隨機選取一條顯示', 'a:1:{s:7:\"display\";s:4:\"rand\";}');",

	"REPLACE INTO pw_advert VALUES(14, 0, 0, 'Mode.Forum.Layer.TidUp', 0, 0, 1, 0, '樓層廣告[帖子上方]~	~出現在帖子上方，一般以圖片或文字顯示，多條帖間廣告時系統將隨機選取一條顯示', 'a:1:{s:7:\"display\";s:4:\"rand\";}');",

	"REPLACE INTO pw_advert VALUES(15, 0, 0, 'Mode.Forum.Layer.TidAmong', 0, 0, 1, 0, '樓層廣告[樓層中間]~	~出現在帖子樓層之間，一般以圖片或文字顯示，多條帖間廣告時系統將隨機選取一條顯示', 'a:1:{s:7:\"display\";s:4:\"rand\";}');",

	"REPLACE INTO pw_advert VALUES(16, 0, 0, 'Mode.Layer.Index', 0, 0, 1, 0, '論壇首頁分類間~	~出現在首頁分類層之間，一般以圖片或文字顯示，多條帖間廣告時系統將隨機選取一條顯示', 'a:1:{s:7:\"display\";s:4:\"rand\";}');",

	"REPLACE INTO pw_advert VALUES(23, 0, 0, 'Mode.Forum.Layer.area.ThreadTop', 0, 0, 1, 0, '門戶帖子列表頁右上~	~帖子列表頁門戶模式瀏覽時，右上方的廣告位', 'a:1:{s:7:\"display\";s:4:\"rand\";}');",

	"REPLACE INTO pw_advert VALUES(24, 0, 0, 'Mode.Forum.Layer.area.ThreadBtm', 0, 0, 1, 0, '門戶帖子列表頁右下~	~帖子列表頁門戶模式瀏覽時，右下方的廣告位', 'a:1:{s:7:\"display\";s:4:\"rand\";}');",

	"REPLACE INTO pw_advert VALUES(25, 0, 0, 'Mode.Forum.Layer.area.ReadTop', 0, 0, 1, 0, '門戶帖子內容頁右上~	~帖子內容頁門戶模式瀏覽時，右上方的廣告位', 'a:1:{s:7:\"display\";s:4:\"rand\";}');",

	"REPLACE INTO pw_advert VALUES(26, 0, 0, 'Mode.Forum.Layer.area.ReadBtm', 0, 0, 1, 0, '門戶帖子內容頁右下~	~帖子內容頁門戶模式瀏覽時，右下方的廣告位', 'a:1:{s:7:\"display\";s:4:\"rand\";}');",

	"REPLACE INTO pw_advert VALUES(30, 2, 0, 'Wap.Header', 0, 0, 1, 0, 'WAP頭部~	~顯示在WAP頭部，可以是圖片或文字，存在多條廣告時將全部顯示', 'a:1:{s:7:\"display\";s:3:\"all\";}');",

	"REPLACE INTO pw_advert VALUES(31, 2, 0, 'Wap.Footer', 0, 0, 1, 0, 'WAP底部~	~顯示在WAP底部，可以是圖片或文字，存在多條廣告時將全部顯示', 'a:1:{s:7:\"display\";s:3:\"all\";}');",

	"REPLACE INTO pw_advert VALUES(32, 2, 0, 'Wap.Read.Header', 0, 0, 1, 0, 'WAP帖子內容頂部~	~顯示在WAP帖子內容頂部，可以是圖片或文字，存在多條廣告時將全部顯示', 'a:1:{s:7:\"display\";s:3:\"all\";}');",

	"REPLACE INTO pw_advert VALUES(33, 2, 0, 'Wap.Read.Footer', 0, 0, 1, 0, 'WAP帖子內容底部~	~顯示在WAP帖子內容底部，可以是圖片或文字，存在多條廣告時將全部顯示', 'a:1:{s:7:\"display\";s:3:\"all\";}');",

	"REPLACE INTO `pw_advert` (`id`, `type`, `uid`, `ckey`, `stime`, `etime`, `ifshow`, `orderby`, `descrip`, `config`) VALUES
(27, 0, 0, 'Site.Search.Thread.Right', 0, 0, 1, 0, '搜索帖子右側廣告~	~搜索帖子時，顯示在頁面的右側', 'a:1:{s:7:\"display\";s:4:\"rand\";}');",

	"REPLACE INTO `pw_advert` (`id`, `type`, `uid`, `ckey`, `stime`, `etime`, `ifshow`, `orderby`, `descrip`, `config`) VALUES
(28, 0, 0, 'Site.Search.Diary.Right', 0, 0, 1, 0, '搜索日誌右側廣告~	~搜索日誌時，顯示在頁面的右側', 'a:1:{s:7:\"display\";s:4:\"rand\";}');",

	"REPLACE INTO `pw_advert` (`id`, `type`, `uid`, `ckey`, `stime`, `etime`, `ifshow`, `orderby`, `descrip`, `config`) VALUES
(29, 0, 0, 'Site.u.IndexCenter', 0, 0, 1, 0, '個人中心首頁廣告位~	~', 'a:1:{s:7:\"display\";s:3:\"all\";}')",
);

foreach ($arrSQL as $sql) {
	if (trim($sql)) {
		$db->update($sql);
	}
}

$arrUpdate = array(
	'Site.NavBanner'		=> 'Site.NavBanner1',
	'Mode.Layer.TidRight'	=> 'Mode.Forum.Layer.TidRight',
	'Mode.Layer.TidDown'	=> 'Mode.Forum.Layer.TidDown',
	'Mode.Layer.TidUp'		=> 'Mode.Forum.Layer.TidUp',
	'Mode.Layer.TidAmong'	=> 'Mode.Forum.Layer.TidAmong'
);

foreach ($arrUpdate as $key=>$value) {
	$db->update("UPDATE pw_advert SET ckey=".pwEscape($value,false)."WHERE ckey=".pwEscape($key,false));
}

?>