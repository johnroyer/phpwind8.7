<?php
!defined('PW_UPLOAD') && exit('Forbidden');
$navConfigService = L::loadClass('navconfig', 'site'); /* @var $navConfigService PW_NavConfig */
$adds = 0;


//4.1.	原主導航

//原主導航升級，還是升級成主導航。門戶的導航屬性需要修改
$areaNav = $navConfigService->getByKey('area');
$areaNavId = $areaNav && isset($areaNav['nid']) ? $areaNav['nid'] : 0;
$navConfigService->update($areaNavId, array('floattype' => 'cross', 'listtype' => 'space', 'selflisttype' => 'space'));

//主導航中增加：門戶頻道
$channelService = L::loadClass('channelService', 'area');
foreach ($channelService->getChannels() as $alias => $channel) {
	if (!$navConfigService->getByKey('area_' . $alias)) {
		$link = "index.php?m=area&alias=" . $alias;
		$isShow = in_array($alias, array('bbsindex', 'home')) ? 0 : 1;
		$adds += (bool) $navConfigService->add(PW_NAV_TYPE_MAIN, array('nkey' => 'area_' . $alias, 'pos' => '-1', 'title' => $channel['name'], 'link' => $link, 'view' => $areaNav['view']++, 'upid' => 0, 'isshow' => $isShow));
	}
}
//主導航中增加：群組聚合
$adds += (bool)$navConfigService->add(PW_NAV_TYPE_MAIN, array('nkey' => 'group', 'pos' => '-1', 'title' => '群組', 'style' => '', 'link' => 'group.php', 'alt' => '', 'target' => 0, 'view' => 3, 'upid' => 0, 'isshow' => 1));

//4.2.	原模式導航

//原門戶模式導航：升級後成為主導航中「門戶」的二級導航。
$db->update("UPDATE pw_nav SET type=".pwEscape(PW_NAV_TYPE_MAIN).", upid=".pwEscape($areaNavId)." WHERE type='area_navinfo'");

//原論壇模式導航：升級後成為頂部右側導航。
$db->update("UPDATE pw_nav SET type=".pwEscape(PW_NAV_TYPE_HEAD_RIGHT).", pos='bbs,area' WHERE type='bbs_navinfo'");

//原圈子模式導航：刪除。
$db->update("DELETE FROM pw_nav WHERE type='o_navinfo'");

//4.3. 	原頂部導航：升級後成為頂部左側導航。
$db->update("UPDATE pw_nav SET type=".pwEscape(PW_NAV_TYPE_HEAD_LEFT)." WHERE type='head'");

//4.4.	原底部導航：自定義數據保持升級，增加幾個默認導航：聯繫我們、無圖版、手機瀏覽
$db->update("DELETE FROM pw_nav WHERE type=".pwEscape(PW_NAV_TYPE_FOOT)." AND link IN (".pwImplode(array($db_ceoconnect, 'simple/', 'm/index.php')).")");
$defaults = array(
	array('pos' => '-1', 'title' => '聯繫我們', 'link' => $db_ceoconnect, 'view'=>1, 'target' => 0, 'isshow' => 1),
	array('pos' => '-1', 'title' => '無圖版', 'link' => 'simple/', 'view'=>2, 'target' => 0, 'isshow' => 1),
	array('pos' => '-1', 'title' => '手機瀏覽', 'link' => 'm/', 'view'=>3, 'target' => 0, 'isshow' => 1),
);
foreach ($defaults as $key => $value) {
	$adds += (bool)$navConfigService->add(PW_NAV_TYPE_FOOT, $value);
}
