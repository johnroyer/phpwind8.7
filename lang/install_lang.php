<?php
!function_exists('readover') && exit('Forbidden');
$lang = array(
	'back'				=> '返 回',

	'chinaz'			=> '添加ChinaZ.com的圖片友情鏈接',
	'chinaz_alt'		=> '中國站長站——為中文網站提供動力',
	'chinaz_logo'		=> 'http://www.chinaz.com/images/chinaz.gif',
	'chinaz_name'		=> '中國站長站',
	'chinaz_url'		=> 'http://www.chinaz.com',
	'ckpwd'				=> '重複密碼',

	'dbattachurl'		=> '附件url地址，以http:// 開頭的絕對地址，為空使用默認',
	'dbcharset'			=> "Mysql編碼設置(常用編碼：gbk、big5、utf8、latin1){$crlf}如果您的論壇出現亂碼現象，需要設置此項來修復{$crlf}請不要隨意更改此項，否則將可能導致論壇出現亂碼現象",
	'dbhost'			=> '數據庫服務器',
	'dbhostweb'			=> '鏡像站點設置',
	'dbifhostweb'		=> '是否為主站點',
	'dbinfo'			=> '以下變量需根據您的數據庫服務器說明檔修改',
	'dbmanager'			=> "創始人將擁有論壇的所有權限，自pwforums v6.3起支持多重創始人，用戶名密碼更改方法：{$crlf}方法1、將./data/sql_config.php的文件屬性設置為777（非NT服務器）或取消只讀並將用戶權限設置為Full Control（完全控制，NT服務器），然後用原始創始人帳號登錄後台，在更改論壇創始人處進行相關添加修改操作，操作完畢後，再將./data/sql_config.php的文件屬性設置為644（非NT服務器）或只讀（NT服務器）。（推薦）{$crlf}方法2、用記事本打開./data/sql_config.php文件，在「創始人用戶名數組」中加入新的用戶名，如「\$manager = array('admin');」更改為「\$manager = array('admin','phpwind');」，在「創始人密碼數組」中加入新的密碼，如「\$manager_pwd = array('21232f297a57a5a743894a0e4a801fc3');」更改為「\$manager_pwd = array('21232f297a57a5a743894a0e4a801fc3','21232f297a57a5a743894a0e4a801fc3');」，其中「21232f297a57a5a743894a0e4a801fc3」是密碼為admin的md5的加密串，您可以創建一個新的文件在根目錄（test.php），文件內容為 \"<?php echo md5('您的密碼');?>\" ，在地址欄輸入http://你的論壇/test.php獲得md5加密後的密碼，用完記得刪除文件test.php。",
	'dbmanagername'		=> "創始人用戶名數組",
	'dbmanagerpwd'		=> '創始人密碼數組',
	'dbname'			=> '數據庫名',
	'database'			=> '數據庫類型',
	'dbpconnect'		=> '是否持久連接',
	'dbpre'				=> '表區分符',
	'dbpres'			=> '數據庫表前綴',
	'dbpw'				=> '數據庫密碼',
	'dbuser'			=> '數據庫用戶名',

	'error_777'			=> '<b>{#filename}</b> 文件或文件夾777屬性檢測不通過',
	'error_777s'		=> '<b>{#filenames}</b> 等文件或文件夾777屬性檢測不通過',
	'error_admin'		=> '將admin.php改成{#db_adminfile}後才能繼續升級',
	'error_adminemail'	=> '創始人電子郵箱不能為空',
	'error_adminname'	=> '創始人用戶名不能為空',
	'error_adminpwd'	=> '創始人密碼不能為空',
	'error_ckpwd'		=> '兩次輸入密碼不同',
	'error_delrecycle'	=> '請登錄後台刪除回收站版塊，在前台版主管理或後台回收站管理進行帖子操作',
	'error_nodatabase'	=> '您沒有該 <b>{#dbname}</b> 數據庫的操作權限或指定的數據庫 <b>{#dbname}</b> 不存在,且您無權限建立,請聯繫服務器管理員!',
	'error_dbhost'		=> '數據庫服務器不能為空',
	'error_dbname'		=> '數據庫名不能為空',
	'error_dbpw'		=> '你填的數據庫密碼為空，是否使用空的數據庫密碼',
	'error_dbuser'		=> '數據庫用戶名不能為空',
	'error_delinstall'	=> '系統無法刪除{#basename}，請登錄FTP刪除此文件',
	'error_forums'		=> '版塊ID錯誤，非法操作!',
	'error_forums1'		=> '論壇分類一不能為空',
	'error_forums2'		=> '論壇版塊一不能為空',
	'error_forums3'		=> '若填寫了論壇版塊二，論壇分類二不能為空',
	'error_forums4'		=> '若填寫了論壇分類二，論壇版塊二不能為空',
	'error_mysqli'		=> '注意：您的服務器的配置低於MySQL4.1.3版本，不允許使用 mysqli，程序自動設置為 mysql',
	'error_nothing'		=> '任意項目沒有填寫',
	'error_table'		=> '{#pw_table}已經存在而非PW論壇數據庫，請在數據庫管理裡改名或刪除{#pw_table}表，再進行升級',
	'error_unfind'		=> '<b>{#filename}</b> 文件或文件夾不存在,請新建文件夾或新建對應的文件（空文件即可）',
	'error_unfinds'		=> '<b>{#filenames}</b> 等文件或文件夾不存在，請新建文件夾或新建對應的文件（空文件即可）',

	'forums1'			=> '論壇分類一',
	'forums2'			=> '論壇版塊一',
	'forums3'			=> '論壇分類二',
	'forums4'			=> '論壇版塊二',
	'forumsmsg'			=> '填寫分類和版塊名稱',

	'hacklist'			=> '插件列表',
	'have_file'			=> '您已經安裝過 phpwind，如需重新安裝，請刪除此文件（{#bbsurl}/data/{#lockfile}）再進行安裝',
	'have_upfile'		=> '您已經升級過 phpwind，如需重新升級，請刪除此文件（{#bbsurl}/data/{#lockfile}.lock）再進行後續操作',
	'have_install'		=> '數據庫<span class="s1">{#dbname}</span>中已經安裝過phpwind.「繼續安裝」將清除原來的數據，使用其他數據庫請「返回上一步」重新設置',



	'header_pw'			=> '官方論壇',
	'header_help'		=> '使用手冊',

	'judg_1'			=> '正方獲勝',
	'judg_2'			=> '反方獲勝',
	'judg_3'			=> '雙方戰平',

	'last'				=> '繼 續',
	'link_index'		=> '系統前台地址',
	'link_admin'		=> '系統後台地址',
	'link_phpwind'		=> 'PW官方論壇',
	'log_help'			=> '<li><a href="http://www.phpwind.com/help/" target="_blank" class="black">全面在線幫助手冊</a></li><li><a href="http://www.phpwind.net/read-htm-tid-621045.html" target="_blank" class="black">phpwind7推薦環境</a></li><li><a href="http://www.phpwind.net/read-htm-tid-691673.html" target="_blank" class="black">phpwind 7.5 css風格詳解</a></li><li><a href="http://www.phpwind.net/read-htm-tid-696195.html" target="_blank" class="black">PW 6.32到PW風格升級教程</a></li><li><a href="http://www.phpwind.net/read-htm-tid-704022.html" target="_blank" class="black">門戶模式化說明</a></li><li><a href="http://www.phpwind.net/read-htm-tid-704023.html" target="_blank" class="black">模式化風格說明</a></li><li class="right"><a href="http://www.phpwind.net/thread-htm-fid-2.html" target="_blank" class="black">更多資源與幫助</a></li>',
	'log_install'		=> '<p>1、運行環境需求：PHP+MYSQL</p><p>2、安裝步驟</p><p>● Linux 或 Freebsd 服務器下安裝方法</p>
		<p>第一步：</p><p>使用ftp工具，用二進制模式將該軟件包裡的upload目錄下的所有文件上傳到您的空間，假設上傳後目錄為 upload。</p>
		<p>第二步：</p><p>先確認以下目錄或文件屬性為 (777) 可寫模式。</p><div class="c"></div>
		<div style="padding:.5em 2em">
			<span class="black" style="width:250px; float:left">attachment</span><span class="black" style="padding-right:2em">data</span><br />
			<span class="black" style="width:250px; float:left">attachment/cn_img</span><span class="black" style="padding-right:2em">data/bbscache</span><br />
			<span class="black" style="width:250px; float:left">attachment/photo</span><span class="black" style="padding-right:2em">data/groupdb</span><br/>
			<span class="black" style="width:250px; float:left">attachment/thumb</span><span class="black" style="padding-right:2em">data/guestcache</span><br/>
			<span class="black" style="width:250px; float:left">attachment/upload</span><span class="black" style="padding-right:2em">data/style</span><br/>
			<span class="black" style="width:250px; float:left">attachment/mini</span><span class="black" style="padding-right:2em">data/tmp</span><br/>
			<span class="black" style="width:250px; float:left">html</span><span class="black" style="padding-right:2em">data/tplcache</span><br/>
			<span class="black" style="width:250px; float:left">html/read</span><span class="black" style="padding-right:2em">data/tplcache</span><br/>
			<span class="black" style="width:250px; float:left">html/channel</span><span class="black" style="padding-right:2em">data/forums</span><br/>
			<span class="black" style="width:250px; float:left">html/portal/bbsindex</span><span class="black" style="padding-right:2em">html/portal/bbsindex/main.htm</span><br/>
			<span class="black" style="width:250px; float:left">html/portal/bbsindex/config.htm</span><span class="black" style="padding-right:2em">html/portal/bbsindex/index.html</span><br/>
			<span class="black" style="width:250px; float:left">html/portal/bbsradio</span><span class="black" style="padding-right:2em">html/portal/bbsradio/main.htm</span><br/>
			<span class="black" style="width:250px; float:left">html/portal/bbsradio/config.htm</span><span class="black" style="padding-right:2em">html/portal/bbsradio/index.html</span><br/>
			<span class="black" style="width:250px; float:left">html/portal/oindex</span><span class="black" style="padding-right:2em">html/portal/oindex/main.htm</span><br/>
			<span class="black" style="width:250px; float:left">html/portal/oindex/config.htm</span><span class="black" style="padding-right:2em">html/portal/oindex/index.html</span><br/>
			<span class="black" style="width:250px; float:left">html/portal/groupgatherleft/main.htm</span><span class="black" style="padding-right:2em">html/portal/groupgatherleft/config.htm</span><br/>
			<span class="black" style="width:250px; float:left">html/portal/groupgatherleft/index.html</span><span class="black" style="padding-right:2em">html/portal/groupgatherright/main.htm</span><br/>
			<span class="black" style="width:250px; float:left">html/portal/groupgatherright/config.htm</span><span class="black" style="padding-right:2em">html/portal/groupgatherright/index.html</span><br/>
			<span class="black" style="width:250px; float:left">html/portal/userlist/main.htm</span><span class="black" style="padding-right:2em">html/portal/userlist/config.htm</span><br/>
			<span class="black" style="width:250px; float:left">html/portal/userlist/index.html</span><span class="black" style="padding-right:2em">html/portal/usermix/main.htm</span><br/>
			<span class="black" style="width:250px; float:left">html/portal/usermix/config.htm</span><span class="black" style="padding-right:2em">html/portal/usermix/index.html</span><br/>
			<span class="black" style="width:250px; float:left">html/stopic</span><span class="black" style="padding-right:2em">js/</span><br/>
		</div>
		<p>第三步：</p><p>運行 <small>http://yourwebsite/upload/{#basename}</small> 安裝程序，填入安裝相關信息與資料，完成安裝！</p>
		<p>● Windows 服務器下安裝方法</p><p>第一步：</p><p>使用ftp工具，將該軟件包裡的upload目錄下的所有文件上傳到您的空間，假設上傳後目錄為 upload。</p>
		<p>第二步：</p><p>運行 <small>http://yourwebsite/upload/{#basename}</small> 安裝程序，填入安裝相關信息與資料，完成安裝！</p>',
	'log_install_t'		=> '安裝須知',
	'log_marketing'		=> '<div>尊敬的站長：</div><p>感謝您選擇phpwind 社區系統。基於彼此的信賴，我們誠邀您加入phpwind一站式營銷計劃。</p><p>phpwind一站式營銷是幫助廣大站長獲取收益的網絡廣告營銷平台，它的優勢與特色在於：</p><div style="padding:1em 2em">充分結合phpwind 社區系統，使廣告投放與管理操作更加得心應手；<br />經由專業力量優化的廣告代碼，更符合社區的屬性；<br />長達一年的實戰社區營銷經驗累積與調試；<br />值得信賴的合作夥伴與精心挑選的廣告產品——Google Adsense、北京新銳光芒科技有限公司；<br />更具優勢的結算方式——廣告收益次月以人民幣進行結算，最低收益提領額度為100元。</div><p><a href="http://union.phpwind.com/question.php" target="_blank" class="black"><u>關於phpwind一站式營銷</u></a>&nbsp; &nbsp;<a href="http://www.phpwind.net/thread.php?fid=82" target="_blank" class="black"><u>討論區</u></a></p><p>從建站到掙錢， phpwind永遠是您最真誠的夥伴！</p>',
	'log_marketing_t'	=> 'phpwind社區營銷',
	'log_partner'		=> '<p>跟隨國內開源事業不斷前進的腳步，phpwind攜手更多WEB應用開發夥伴，共創美好明天。在此向您推薦以下軟件，PW Forums與以下軟件的完美整合將為您提供更多建站選擇：</p><div class="c"></div><div style="padding:2em"><a href="http://www.dedecms.com" target="_blank" class="black" style="width:250px; float:left">DedeCms織夢內容管理系統</a><a href="http://www.phpwind.net/thread.php?fid=90" target="_blank" class="black" style="padding-right:2em">討論區</a><br /><a href="http://www.dedecms.com" target="_blank" class="black" style="width:250px; float:left">CMSWARE思維網站內容管理系統</a><a href="http://www.phpwind.net/thread.php?fid=92" target="_blank" class="black" style="padding-right:2em">討論區</a><a href="http://www.phpwind.net/read.php?tid=527223" target="_blank" class="black">最新整合教程</a><br /><a href="http://www.php168.com" target="_blank" class="black" style="width:250px; float:left">PHP168整站系統</a><a href="http://www.phpwind.net/thread.php?fid=91" target="_blank" class="black" style="padding-right:2em">討論區</a><a href="http://down2.php168.com/mv/6.rar" target="_blank" class="black">最新整合教程</a><br/><a href="http://www.shopex.com.cn" target="_blank" class="black" style="width:250px; float:left">ShopEx網上商店商城系統</a><a href="http://www.phpwind.net/thread.php?fid=93" target="_blank" class="black" style="padding-right:2em">討論區</a><a href="http://www.phpwind.net/read.php?tid=527222" target="_blank" class="black">最新整合教程</a></div><p>中國站長站ChinaZ.com是針對各類站點提供資訊及資源服務的網站——擁有國內最大的源碼發佈下載中心及聚集站長、網管的技術資訊社區。中國站長站致力於打造草根站長原創資訊圈，倡導分享建站經歷和建站經驗，讓更多人關注到個人站長、個人網站。</p><p>phpwind與中國站長站有著相互認同，結為戰略合作夥伴，希望能夠在未來攜手倡導網絡互助與共享精神，為站長提供盡可能多的服務。</p><p>現在訪問 <a href="http://www.chinaz.com/zhuanti/phpwind/index.htm" target="_blank" class="black"><u>ChinaZ.com</u></a> 瞭解phpwind的最新信息與更多技術參考資料！</p>',
	'log_partner_t'		=> '合作夥伴',
	'log_repair'		=> '<p>(一) 補丁版本：phpwind v{#from_version}（{#wind_repair}）</p>
		<p>(二) 適用版本：phpwind v{#from_version}</p>
		<p>(三) 更新步驟:</p>
		<p>第一步：打補丁前請務必備份數據, 以免遇到失敗導致數據丟失</p>
		<p>第二步：如果upload目錄下存在 images、attachment、data 目錄,請將改成你論壇相應的目錄名。注: 可以到論壇後台的 「核心-安全與優化-動態目錄」 裡查看</p>
		<p>第三步：使用ftp工具，將該軟件包裡的upload目錄下的所有文件上傳到您的空間，假設上傳後目錄為 upload。</p>
		<p>第四步：運行 <small>http://yourwebsite/upload/{#basename}</small> 補丁更新程序，按提示進行操作, 直到更新完成！</p>',
	'log_resources'		=> '<p>感謝您選擇使用phpwind 社區系統，如果您對我們的產品及服務有任何疑問或建議，隨時歡迎您將信息發表到官方論壇（http://www.phpwind.net）或發送至我們的電子郵箱<font color="#f79646"><b>client@phpwind.net</b></font>。</p><br /><p>如果您需要構建與論壇相關聯的內容發佈管理系統，我們誠意向您推薦我們的合作夥伴——PHP168。<br />　　<a href="http://bbs.php168.com/read-bbs-tid-205437.html" target="_blank" style="color:#00727c">合作主頁</a>&nbsp; &nbsp; &nbsp; &nbsp; <a href="http://www.phpwind.net/thread-htm-fid-91.html" target="_blank" style="color:#00727c">討論區</a></p><p>如果您需要構建與論壇相關聯的網店管理系統，我們誠意推薦我們的合作夥伴——ShopEX。<br />　　<a href="http://www.phpwind.com/shopex" target="_blank" style="color:#00727c">合作主頁</a>&nbsp; &nbsp; &nbsp; &nbsp; <a href="http://www.phpwind.net/thread-htm-fid-93.html" target="_blank" style="color:#00727c">討論區</a></p><p>如果您需要構建與論壇相關聯的博客系統，我們誠意向您推薦我們的博客產品——LxBlog。<br />　　<a href="http://www.phpwind.com/introduce.php?action=introduce&job=bloginfo" target="_blank" style="color:#00727c">官方主頁</a>&nbsp; &nbsp; &nbsp; &nbsp; <a href="http://www.phpwind.net/thread-htm-fid-21.html" target="_blank" style="color:#00727c">討論區</a>&nbsp; &nbsp; &nbsp; &nbsp; <a href="http://www.phpwind.net/read-htm-tid-620820.html" target="_blank" style="color:#00727c">論壇與博客整合圖文教程</a></p><p>如果您需要個性化您的論壇，歡迎您訪問phpwind官方論壇獲取更多資源。phpwind將聯合數十家第三方團隊與業務夥伴，與站長一起播種未來！</p><p><a href="http://www.phpwind.net/hack.php?H_name=hackcenter&action=style" target="_blank" style="color:#00727c">獲取更多免費論壇風格</a>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <a href="http://www.phpwind.net/hack.php?H_name=hackcenter&action=hack" target="_blank" style="color:#00727c">獲取更多免費論壇插件</a></p>',
	'log_union'			=> '<p>安裝完成後，您的無圖版帖子頂樓內容上方將出現阿里媽媽統一投放的廣告，所有收益都歸站長所有，並由阿里媽媽統一結算。收益詳情請見 <a href="http://union.phpwind.com/news.php?action=read&nid=46" target="_blank" style="color:#00727c">http://union.phpwind.com/news.php?action=read&nid=46</a>。</p><p>現在，在版本安裝完成之前，您還可開啟完整版以下3個阿里媽媽廣告位：</p><p><input type="checkbox" name="banners" value="1" CHECKED> 全站頂部 468*60 <input type="checkbox" name="atcbottoms" value="1"> 帖子內容下方 468*60 <input type="checkbox" name="footers" value="1"> 全站底部 760*90 </p><p>如果您尚未註冊阿里媽媽帳號，請不要擔心。無論是無圖版還是完整版的廣告，從第一個PV和點擊開始，數據將會保存在為您預留的帳戶中，直到您註冊登錄營銷平台（<a href="http://union.phpwind.com" target="_blank" style="color:#00727c">http://union.phpwind.com</a>）並綁定阿里媽媽帳號後（可在營銷平台一次性完成阿里媽媽帳號的註冊與綁定），您可以隨時登錄阿里媽媽平台（<a href="http://www.alimama.com" target="_blank" style="color:#00727c">http://www.alimama.com</a>）查看您無圖版廣告數據並提領收益。<a href="http://www.phpwind.net/read-htm-tid-633350.html" target="_blank" style="color:#00727c">查看圖文說明</a></p><p>如果您已擁有阿里媽媽帳號，只需登錄營銷平台進行激活操作即可。</p><p>7月15日-8月15日,「<a href="http://www.phpwind.net/read.php?tid=633351" target="_blank" style="color:#00727c">有流量就有現金，分級獎勵</a>」活動正式開始，敬請關注。</p>',
	'log_unionmsgt'		=> '歡迎您使用並測試phpwind V7.5 sp3 社區系統！',
	'log_unionmsgc'		=> '尊敬的站長：\n\n　　歡迎您使用並測試phpwind V7.5 sp3社區系統。官方v6.3 RC演示於3月15日開放至今，版本經過多次調試與完善，已趨於穩定。在RC版公開測試期間，如發現BUG，或對這個版本有任何建議、意見，歡迎您進入此帖進行回復[url]http://www.phpwind.net/read.php?tid=603810[/url]。\n\n　　另外，從4月30日起，通過phpwind社區營銷平台指導，站長將可以獲得阿里媽媽提供的在線廣告服務，由營銷後台登記註冊至阿里媽媽（[url]www.alimama.com[/url]）平台的站長，將會獲得如下[b]獨家優勢[/b]：\n　　1.高優先級別享有阿里媽媽廣告交易平台的眾多功能，包括優先三包、優先享有更多廣告推廣模式等；\n　　2.phpwind社區營銷平台累計收入超過100元的站長，將會直接享有網站最低收入保障服務，讓您在穩定的收入下，獲得更多價值；\n　　3.在未來，phpwind站長將會獲得阿里媽媽廣告交易平台的phpwind論壇插件，實現更多快捷、並具有獨立優勢的在線廣告服務。\n　　同時，phpwind社區營銷平台Google Adsense、下劃線、主題帖營銷第一期，將暫時停止投放。\n\n　　[b]詳情請訪問：[url]http://www.phpwind.net/read-htm-tid-602463.html[/url]。[/b]\n\nphpwind官方\n2008-4-29',
	'log_update'		=> '<li><a href="http://www.phpwind.net/read-htm-tid-704022.html" target="_blank" class="black">門戶模式化</a></li><li><a href="http://www.phpwind.net/read-htm-tid-681220.html" target="_blank" class="black">PW7.0之好友</a></li><li><a href="http://www.phpwind.net/read-htm-tid-677057.html" target="_blank" class="black">探索PW7.0之用戶中心</a></li><li><a href="http://www.phpwind.net/read-htm-tid-679230.html" target="_blank" class="black">PW7.0 之 自定義風格</a></li><li><a href="http://www.phpwind.net/read-htm-tid-674290.html" target="_blank" class="black">PW7.0 之密碼安全問題</a></li><li><a href="http://www.phpwind.net/read-htm-tid-680607.html" target="_blank" class="black">盤點7.0細節功能 (一）</a></li><li><a href="http://www.phpwind.net/read-htm-tid-680768.html" target="_blank" class="black">盤點7.0細節功能 (二）</a></li><li><a href="http://www.phpwind.net/read-htm-tid-681088.html" target="_blank" class="black">盤點7.0細節功能 (三）</a></li><li class="right"><a href="http://www.phpwind.net/thread-htm-fid-78.html" target="_blank" class="black">瞭解更多特性</a></li>',
	'log_upto'			=> '<p>1、運行環境需求：PHP+MYSQL。</p><p>2、適用版本：{#from_version}。</p><p>3、升級步驟：</p><p>● Linux 或 Freebsd 服務器下安裝方法。</p><p>第一步：</p><p>升級前請務必備份論壇文件與數據, 以免升級失敗導致數據丟失</p><p>第二步：</p><p>請將 upload 目錄內的 images 目錄改名為論壇的圖片目錄名。注: 可以到論壇後台的 全局 裡查看。</p><p>第三步：</p><p>使用ftp工具中的二進制模式，將該軟件包裡 upload 裡的所有文件覆蓋上傳到您的論壇目錄，假設上傳後目錄仍舊為 upload。將升級文件(<small>{#basename}</small>)上傳到 upload 下</p><p>第四步：</p><p>運行 <small>http://yourwebsite/upload/{#basename}</small> 升級程序，按升級提示進行升級, 直到升級結束！</p><p>● Windows 服務器下安裝方法。</p><p>第一步：</p><p>升級前請務必備份論壇文件與數據, 以免升級失敗導致數據丟失</p><p>第二步：</p><p>請將 upload 目錄內的 images 目錄改名為論壇的圖片目錄名。注: 可以到論壇後台的 核心設置 裡查看。</p><p>第三步：</p><p>使用ftp工具，將該軟件包裡 upload 裡的所有文件覆蓋上傳到您的論壇目錄，假設上傳後目錄仍舊為 upload。將升級文件(<small>{#basename}</small>)上傳到 upload 下</p><p>第四步：</p><p>運行 <small>http://yourwebsite/upload/{#basename}</small> 升級程序，按升級提示進行升級, 直到升級結束！</p>',
	'log_upto_t'		=> '升級須知',
	'log_repair_t'		=> '更新須知',

	'managermsg'		=> '創始人信息',

	'name'				=> '用戶名',
	'nf_reply'			=> '最新回復',
	'nf_new'			=> '最新帖子',
	'nf_dig'			=> '最新精華',
	'nf_pic'			=> '最新圖片',

	'promptmsg'			=> '提示信息',
	'pwd'				=> '密碼',

	'redirect'			=> '自動跳轉不成功請點擊這裡',
	'redirect_msg'		=> '數據正在更新升級，這個過程比較漫長，可能需要花費您幾分鐘的時間，請耐心等待......',

	'redirect_msgs'		=> ' &nbsp; &nbsp; <font color="red">{#start}</font> TO <font color="red">{#end}</font>',

	'setform_1'			=> '出租信息',
	'setfrom_1_inro'	=> '<table cellspacing="1" cellpadding="1" align="left" width="100%" style="background:#D4EFF7;text-align:left"><tr><td width="30%" align="center" style="background:#fff;height:25px;"><b>聯  系 人:</b></td><td style="background:#fff;padding-left:5px"><p contentEditable=true></p></td></tr><tr><td width="30%" align="center" style="background:#fff;height:25px;"><b>聯繫方式:</b></td><td style="background:#fff;padding-left:5px"><p contentEditable=true></p></td></tr><tr><td width="30%" align="center" style="background:#fff;height:25px;"><b>房屋類型:</b></td><td style="background:#fff;padding-left:5px"><p contentEditable=true></p></td></tr><tr><td width="30%" align="center" style="background:#fff;height:25px;"><b>房屋位置:</b></td><td style="background:#fff;padding-left:5px"><p contentEditable=true></p></td></tr><tr><td width="30%" align="center" style="background:#fff;height:25px;"><b><font color="#ff3300">出租</font>價格:</b></td><td style="background:#fff;padding-left:5px"><p contentEditable=true></p></td></tr><tr><td width="30%" align="center" style="background:#fff;height:25px;"><b>房屋層次:</b></td><td style="background:#fff;padding-left:5px"><p contentEditable=true></p></td></tr><tr><td width="30%" align="center" style="background:#fff;height:25px;"><b>房屋面積:</b></td><td style="background:#fff;padding-left:5px"><p contentEditable=true></p></td></tr><tr><td width="30%" align="center" style="background:#fff;height:25px;"><b>建造年份:</b></td><td style="background:#fff;padding-left:5px"><p contentEditable=true></p></td></tr><tr><td width="30%" align="center" style="background:#fff;height:25px;"><b>簡單情況:</b></td><td style="background:#fff;padding-left:5px"><p contentEditable=true></p></td></tr></table>',
	'showmsg'			=> '安裝提示信息',
	'showmsg_upto'		=> '升級提示信息',
	'sqlconfig_file'	=> '數據庫配置文件不可寫，將./data/sql_config.php的文件屬性設置為777（非NT服務器）或取消只讀並將用戶權限設置為Full Control（完全控制，NT服務器）',
	'step_null'			=> '',
	'step_next'			=> '下一步',
	'step_prev'			=> '上一步',
	'step_1_upto'		=> '升級須知',
	'step_1_left_upto'	=> '請務必認真閱讀軟件升級說明',
	'step_1_left_repair'=> '請務必認真閱讀軟件更新說明',
	'step_1_right_upto'	=> '開始升級',
	'step_1_right_repair'=> '開始更新',
	'step_readme'		=> '閱讀安裝協議',
	'step_readme_left'	=> '請務必認真閱讀軟件安裝協議',
	'step_readme_right'	=> '同意協議，下一步',
	'step_database'		=> '填寫數據庫信息與創始人信息',
	'step_database_left'	=> '閱讀安裝協議',
	'step_database_right'	=> '安裝詳細信息',
	'step_hack'			=> '選擇需要安裝的插件',
	'step_view'			=> '安裝詳細信息',
	'step_view_left'	=> '填寫數據庫信息',
	'step_view_right'	=> '確認安裝完成',
	'step_resources'	=> '資源導航',
	'step_resources_left'=> '建立版塊並填充帖子',
	'step_5'			=> '默認模式選擇',
	'step_5_left'		=> '安裝詳細信息',
	'step_5_right'		=> '資源導航',
	'step_6'			=> '建立版塊並填充帖子',
	'step_6_left'		=> '取消並退出',
	'step_6_right'		=> ' 提 交 ',
	'step_finish'		=> '完成操作',
	'step_union'		=> '社區營銷',
	'step_union_right'	=> '下一步',
	'step_app'		    => 'APP帖子交換成功！',
	'writable_success'	=> '可寫',
	'writable_error'	=> '不可寫',
	'success'			=> '完 成',
	'success_1'			=> '同意安裝!',
	'success_2'			=> '{#filename} 777屬性檢測通過!',
	'success_3'			=> '系統配置創建成功!',
	'success_3_1'		=> '建立數據表 {#tablename} ... 成功!',
	'success_3_2'		=> '創始人信息創建成功!',
	'success_5'			=> '默認模式選擇成功!',
	'success_7'			=> '版塊 {#value} 編輯完成!',
	'success_7_1'		=> '版塊 {#value} 創建成功!',
	'success_7_2'		=> '數據更新成功!',
	'success_4'			=> '插件添加成功!',
	'success_4_1'		=> '友情鏈接添加成功!',
	'success_4_2'		=> '系統緩存更新完成!',
	'success_install'	=> '恭喜您，您的 phpwind v{#wind_version}已經安裝成功！',
	'success_repair'	=> '恭喜您，您的 phpwind v{#wind_version}已經更新成功！',
	'success_upto'		=> '恭喜您，您的 phpwind v{#wind_version}已經升級成功！',
	'welcome_msg'		=> '歡迎來到 phpwind 安裝嚮導，安裝前請仔細閱讀安裝說明後才開始安裝。安裝文件夾裡同樣提供了有關軟件安裝的說明，請您仔細閱讀。　　<div style="margin-top:.5em">安裝過程中遇到任何問題 &nbsp;<a href="http://www.phpwind.net/thread-htm-fid-2.html" target="_blank" class="black"><u><b>請到官方討論區尋求幫助</b></u></a></div>',
	'welcome_msgupto'	=> '歡迎來到 phpwind 升級嚮導，升級前請仔細閱讀 升級說明裡的每處細節後才能開始升級。升級文件夾裡同樣提供了有關軟件升級的說明，請您同樣仔細閱讀，以保證升級進程的順利進行。',
	'welcome_msgrepair'	=> '歡迎來到 phpwind 更新嚮導，更新前請仔細閱讀 更新說明裡的每處細節後才能開始更新。更新文件夾裡同樣提供了有關軟件更新的說明，請您同樣仔細閱讀，以保證更新進程的順利進行。',
	'finish_exit'		=>'關  閉',
	'title_install'		=> 'phpwind 安裝程序',
	'title_repair'		=> 'phpwind 補丁程序',
	'title_upto'		=> 'phpwind 升級程序',
	'env_os'			=> '類UNIX',
	'unlimited'			=> '不限制',
	'environment_check' => '檢測環境',
	'insert_data'		=> '創建數據',
	'install_complete'	=> '完成安裝',
	'check_enrironment'	=> '環境檢測',
	'current_server'	=> '當前服務器',
	'recommend_env'		=> '推薦配置',
	'lowest_env'		=> '最低要求',
	'os'				=> '操作系統',
	'phpversion'		=> 'PHP版本',
	'attach_upload'		=> '附件上傳',
	'disk_space'		=> '磁盤空間',
	'right_check'		=> '目錄、文件權限檢查',
	'current_status'	=> '當前狀態',
	'required_status'	=> '所需狀態',
	'recheck'			=> '重新檢測',
	'databaseinfo'		=> '數據庫信息',
	'databasetiop'		=> '數據庫服務器地址，一般為localhost',
	'dbpretip'			=> '建議使用默認，同一數據庫安裝多個論壇時需修改',
	'manager'			=> '管理員帳號',
	'install_completed'	=> '安裝完成，點擊進入',
	'complete_tips'		=> '瀏覽器會自動跳轉，無需人工干預',
	'installing'		=> '正在安裝...',

	//7.3 Start
	'step_pre'			=> '上一步',
	'step_next'			=> '下一步',
	'accept'			=> '接 受',
	'left_info'			=> '<dt style="margin:0;">更新記錄</dt>
        <dd style="padding-top:5px;"><a href="http://www.phpwind.net/read-htm-tid-1251662.html" target="_blank">phpwind 8.5介紹帖</a></dd>
        <dd><a href="http://www.phpwind.net/read-htm-tid-1251648.html" target="_blank">phpwind 8.5 bug修復列表</a></dd>
        <dt>幫助文檔</dt>
        <dd style="padding-top:5px;"><a href="http://www.phpwind.net/read-htm-tid-1251651.html" target="_blank">安裝教程</a></dd>
		<dd><a href="http://www.phpwind.net/read-htm-tid-1251656.html" target="_blank">升級教程</a></dd>
		<dd><a href="http://www.phpwind.net/read-htm-tid-1251658.html" target="_blank">插件安裝教程</a></dd>
        <dd><a href="http://www.phpwind.net/read-htm-tid-1251659.html" target="_blank">風格安裝教程</a></dd>
        <dd><a href="http://www.phpwind.net/read-htm-tid-1250119.html" target="_blank">數據庫結構手冊</a></dd>
        <dd><a href="http://www.phpwind.net/thread-htm-fid-54.html" target="_blank">在線反饋</a></dd>
		<dd><a href="http://faq.phpwind.net/" target="_blank">幫助中心</a></dd>',
	'step_problem'		=> '<p>操作過程中若遇到任何問題，</p> <p><a href="http://www.phpwind.net/thread-htm-fid-2.html" target="_blank"><strong>請到官方論壇尋求幫助</strong></a></p>',
	'step_deldir'		=> '友情提示：請及時刪除admin/code.php、template/admin/code.htm和hack/app整個文件夾',

	//update
	'update_1'			=> '閱讀升級須知',
	'update_2'			=> '具體升級階段',
	'update_3'			=> '資源導航',
	'update_finish'		=> '升級成功！',
	'admin_name'		=> '創始人帳號：',
	'admin_pwd'			=> '密碼：',
	'admin_login_2'		=> ' 登 錄 ',
	'login_error'		=> '用戶名或密碼錯誤，登錄失敗',

	//install
	'install_1'			=> '閱讀安裝協議',
	'install_2'			=> '填寫數據信息',
	'install_3'			=> '選擇需要安裝的插件',
	'install_4'			=> '安裝詳細信息',
	'install_5'			=> '默認模式選擇',
	'install_6'			=> '資源導航',
	'install_finish'	=> '安裝成功！',
	'forum_finish'		=> '版塊創建完成',
	'app_limit'			=> '您設置的下載數量總和超過了250',

	'app_1'				=>'添加版塊',
	'app_2'				=>'您想為這個版塊填充何種類型的帖子',
	'app_3'				=>'每天下載數量',
	'app_4'				=>'提示：<br />1、以上操作將為您生成一個新的APP帳戶，帳戶信息會隨後發送到admin的「短消息信箱」中。<br />
2、目前，每日下載總數為250帖；以上設置，您可以隨時登錄「APP平台>帖子交換>自動下載」進行修改或關閉操作；版塊名稱可在「論壇後台>版塊管理」中修改。<br />
3、提交後，下載操作將在24小時內生效執行。<br />
4、在本地安裝的論壇，以上操作無效。<br />
5、查看詳細教程 <a href=http://www.phpwind.net/read.php?tid=753545 target=_blank>http://www.phpwind.net/read.php?tid=753545</a>',
	'app_5'				=>'APP 帖子交換是一項充實站點內容，推廣自己站點的應用。<br />
#充實內容<br />
通過phpwind APP帖子交換，為phpwind站長和站點提供一個平台，實現站點與站點之間的內容資源共享交換。上傳帖子到平台、從平台下載帖子是這個應用中最主要的兩個操作。根據設定可以選擇發帖內容分類，並且有手動下載和自動下載兩種方式。<br />
#推廣站點<br />
每一個帖子下載時，都保證帶上原創地址與鏈接，不允許修改。帖子被收錄的越多、收錄帖子的站點的PR值權重越高，你站點的外鏈權重也就越高，從而提升你站點的搜索引擎收錄量和PR值。<br />
在您申請開通免費空間一鍵安裝論壇的時候，系統將引導您開啟帖子交換並立即下載帖子。您也可以選擇跳過不開啟，之後可隨時到「APP平台>控制面板>帖子交換」中修改設置。<br />
詳細介紹 <a href=http://www.phpwind.net/read-htm-tid-700917.html target=_blank>http://www.phpwind.net/read-htm-tid-700917.html</a><br />
論壇咨詢 <a href=http://www.phpwind.net/thread-htm-fid-100.html target=_blank>http://www.phpwind.net/thread-htm-fid-100.html</a><br />
查看APP更多應用 <a href=http://www.phpwind.net/read-htm-tid-717216.html target=_blank>http://www.phpwind.net/read-htm-tid-717216.html</a>',
	'app_subject'		=> 'APP平台信息',
	'app_content1'		=> "尊敬的站長：<br />
通過您在APP平台上的臨時帳戶，您已開啟APP平台「帖子交換」中的「自動下載」，您的臨時帳戶信息如下。<br />
用戶名：{#username}<br />
密碼：{#pwd}<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;對於臨時用戶名，您有且僅有一次修改機會，請按照此教程進行操作（<a href=http://www.phpwind.net/read.php?tid=753581 target=_blank>http://www.phpwind.net/read.php?tid=753581</a>）進行操作。為了保證帳戶的信息安全，登錄APP平台時還需要您填寫自己的常用電子郵箱，此郵箱將作為激活、修改此帳戶信息的最重要憑證。<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;您已開啟的「帖子交換>自動下載」可隨時登錄APP平台修改設置，也可以嘗試手動下載方式，通過精確條件篩選與您站點相符的內容再進行下載。除了下載，您還可以推送您站點中原創帖子到帖子交換中心，當您的帖子被其他站點下載後，帖子中都會保留原創地址。通過這種方式，您可以提高站點外鏈權重，並進行推廣。<br />
帖子交換詳細教程請訪問：<a href=http://www.phpwind.net/read-htm-tid-746186.html target=_blank>http://www.phpwind.net/read-htm-tid-746186.html</a><br />
相信APP平台上其他應用也能幫到您，詳情請訪問<a href=http://www.phpwind.net/read-htm-tid-717216.html target=_blank>http://www.phpwind.net/read-htm-tid-717216.html</a>，如有任何問題請咨詢app@phpwind.com。",
	'app_content2'		=> "尊敬的站長：<br />APP 帖子交換是一項充實站點內容，推廣自己站點的應用。<br />
#充實內容
通過phpwind APP帖子交換，為 phpwind 站長和站點提供一個平台，實現站點與站點之間的內容資源共享交換。上傳帖子到平台、從平台下載帖子是這個應用中最主要的兩個操作。根據設定可以選擇發帖內容分類，並且有手動下載和自動下載兩種方式。<br />
#推廣站點
每一個帖子下載時，都保證帶上原創地址與鏈接，不允許修改。帖子被收錄的越多、收錄帖子的站點的PR值權重越高，你站點的外鏈權重也就越高，從而提升你站點的搜索引擎收錄量和PR值。<br />
在您申請開通免費空間一鍵安裝論壇的時候，系統將引導您開啟帖子交換並立即下載帖子。您也可以選擇跳過不開啟，之後可隨時到「APP平台>控制面板>帖子交換」中修改設置。<br />
詳細介紹 <a href=http://www.phpwind.net/read-htm-tid-700917.html target=_blank>http://www.phpwind.net/read-htm-tid-700917.html</a><br />
論壇咨詢 <a href=http://www.phpwind.net/thread-htm-fid-100.html target=_blank>http://www.phpwind.net/thread-htm-fid-100.html</a>l<br />
查看APP更多應用 <a href=http://www.phpwind.net/read-htm-tid-717216.html target=_blank>http://www.phpwind.net/read-htm-tid-717216.html</a>",

	//7.3 End
############################## SQL LANG ##############################
	'act_1'				=> '擺了個ＰＯＳＥ道：你、你、你沒我酷..',
	'act_2'				=> '撅了撅嘴說：氣死我了呀!呀!呀!',
	'act_3'				=> '仰天狂笑：普天之下,竟然沒有我的對手...',
	'act_4'				=> '鼻子一酸,開始叭嗒叭嗒掉眼淚了',
	'act_5'				=> '清清嗓子唱起歌來：東方紅,太陽升',
	'action_1'			=> '比酷',
	'action_2'			=> '生氣',
	'action_3'			=> '狂笑',
	'action_4'			=> '痛哭',
	'action_5'			=> '唱歌',
	'anonymousname'		=> '匿名',

	'colony'			=> '朋友圈',
	'credit_descrip'	=> '自定義積分',
	'credit_name'		=> '好評度',
	'credit_unit'		=> '點',

	'db_adminreason'	=> '廣告帖\n惡意灌水\n非法內容\n與版規不符\n重複發帖\n\n優秀文章\n原創內容',
	'db_admingradereason'=> '優秀文章，支持！\n神馬都是浮雲',
	'db_charset'		=> 'utf-8',
	'db_currencyname'	=> '銀元',
	'db_currencyunit'	=> '個',
	'db_creditname'		=> '貢獻值',
	'db_creditunit'		=> '點',
	'db_moneyname'		=> '銅幣',
	'db_moneyunit'		=> '枚',
	'db_rvrcname'		=> '威望',
	'db_rvrcunit'		=> '點',
	'db_whybbsclose'	=> '網站升級中，請稍後訪問',
	'db_visitmsg'		=> '本站僅限內部開放',
	'db_floorunit'		=> '樓',
	'db_floorname_1'	=> '樓主',
	'db_floorname_2'	=> '沙發',
	'db_floorname_3'	=> '板凳',
	'db_floorname_4'	=> '地板',
	'db_sitemsg_1'		=> '帶紅色*的都是必填項目，若填寫不全將無法註冊',
	'db_sitemsg_2'		=> '請添加能正常收發郵件的電子郵箱',
	'db_sitemsg_3'		=> '如果您在網吧或者非個人電腦，建議設置Cookie有效期為 即時，以保證賬戶安全',
	'db_sitemsg_4'		=> '如果您在寫長篇帖子又不馬上發表，建議存為草稿',
	'db_sitemsg_5'		=> '如果您提交過一次失敗了，可以用」恢復數據」來恢復帖子內容',
	'db_sitemsg_6'		=> '批量上傳需要先選擇文件，再選擇上傳',

	'debateclass1'		=> '科學教育',
	'debateclass2'		=> '影視娛樂',
	'debateclass3'		=> '休閒時尚',
	'debateclass4'		=> '體育健身',
	'debateclass5'		=> '星座聯盟',
	'debateclass6'		=> '社會百態',
	'debateclass7'		=> '其他類別',
	'default_atc'		=> '默認分類',
	'default_forum'		=> '默認版塊',
	'default_recycle'	=> '回收站',

	'help_1'			=> '新手入門',
	'help_2'			=> '註冊、登錄',
	'help_3'			=> '忘記密碼',
	'help_4'			=> '添加和編輯個人資料',
	'help_5'			=> '選擇風格',
	'help_6'			=> '選擇默認編輯器',
	'help_7'			=> '接收郵件',
	'help_8'			=> '發帖回帖',
	'help_9'			=> '發表主題',
	'help_10'			=> '發表出售帖',
	'help_11'			=> '發表交易帖、懸賞帖和投票帖',
	'help_12'			=> '發表匿名帖',
	'help_13'			=> '發表回復',
	'help_14'			=> '引用功能',
	'help_15'			=> '附件上傳',
	'help_16'			=> '購買主題',
	'help_17'			=> '我的主題',
	'help_18'			=> '常用功能',
	'help_19'			=> '收藏功能',
	'help_20'			=> '朋友圈功能',
	'help_21'			=> '短消息功能',
	'help_22'			=> '搜索功能',
	'help_23'			=> '帖子舉報功能',
	'help_24'			=> '社區插件',
	'help_25'			=> '道具使用',
	'help_26'			=> 'Rss聚合',
	'help_27'			=>'Wind Code',
	'helpd_2'			=> '註冊方法：如果您還沒有註冊，是以遊客狀態瀏覽論壇的，在頭部導航欄可以看到「您尚未登錄&#160;註冊」的字樣，點擊「註冊」，填寫相應的信息，就可以完成註冊了。\n因站長設置的不同，遊客的瀏覽及使用論壇的權限會受到很多限制，如果您喜歡這個論壇，建議您馬上註冊。\n登錄方法：如果您已經註冊了該論壇，可以在網站首頁頭部的登錄模塊進行登錄，也可以在頁面頭部導航欄點擊「登錄」，進入登錄頁面進行登錄，在限制遊客訪問的頁面，也會有登錄提示頁面出現。',
	'helpd_3'			=> '如果您忘記密碼，請在登錄頁面點擊「找回密碼」並輸入用戶名，系統將自動發送密碼到您的有效電子郵箱中。',
	'helpd_4'			=> '點擊進入「控制面板」下的「編輯個人資料」，就可以對自己的資料信息進行修改了。',
	'helpd_5'			=> '點擊進入「控制面板」下的「編輯個人資料」，找到「論壇可控制性數據」一欄，在該欄目下有「選擇風格」的選項，在下拉列表裡選擇喜歡的風格，點擊「提交」按鈕，就可以了。',
	'helpd_6'			=> '點擊進入「控制面板」下的「編輯個人資料」，找到「論壇可控制性數據」一欄，在該欄目下有「選擇默認使用的編輯器」選項，選擇習慣使用的編輯器，點擊「提交」。',
	'helpd_7'			=> '點擊進入「控制面板」下的「編輯個人資料」，在「論壇可控制性數據」一欄最下邊，找到「是否接受郵件」，選擇「接收郵件」，點擊「提交」。',
	'helpd_9'			=> '在帖子列表頁面和帖子閱讀頁面，可以看到「新帖」圖標，點擊即可進入主題帖發佈頁面，如果沒有發帖權限，會有提示「本論壇只有特定用戶組才能發表主題,請到其他版塊發帖,以提高等級!」出現。\n如果用不到這樣完全的發帖功能，也可以在帖子列表頁面底部的快速發帖模塊進行發帖操作。',
	'helpd_10'			=> '在帖子列表頁面和帖子閱讀頁面，點擊「新帖」圖標進入主題帖發佈頁面，發帖時在帖子編輯器下方找到「出售此帖」，在前面的復選框理勾選（如果復選框呈灰色，說明該版塊不允許發佈交易帖或者您的權限不夠），填寫好會員讀帖需要支付的銅幣數量（注意不能超過支付最大值）。\n同樣，也可以在帖子列表頁面底部的快速發帖模塊進行發帖操作時設置讀帖需要支付的銅幣數量。',
	'helpd_11'			=> '發表交易帖、懸賞帖和投票帖','','在帖子列表頁面和帖子閱讀頁面，當鼠標停在新帖圖標上時，如果你在該版塊有發表交易帖、懸賞帖或投票帖的權限時，就會出現一個下拉菜單，菜單項裡顯示：商品、懸賞、投票，點擊需要的帖子類型即可進入相應的主題發表頁面發佈新的主題。',
	'helpd_12'			=> '在帖子列表頁面和帖子閱讀頁面，點擊「新帖」圖標進入發帖頁面，在發帖時勾選內容編輯器下面的匿名帖復選框，或者在快速發帖處勾選（如果復選框呈灰色，說明該版塊不允許發佈匿名帖或者您的權限不夠）。',
	'helpd_13'			=> '在帖子閱讀頁面點擊「回復」按鈕進入回復頁面回復主題帖，也可以在頁面下方的快速發帖處進行回復。',
	'helpd_14'			=> '在需要引用的帖子樓層上點擊引用，即可引用當前樓層內容，也可以用Wind Code代碼進行引用，把需要引用的內容放入[quote] 您要引用的文字[/quote]中間即可。',
	'helpd_15'			=> '在發帖頁面下的附件上傳處點擊瀏覽按鈕，上傳有效後綴類型的附件，同時可以在描述框對附件進行描述，並設置下載附件所需要的威望值。',
	'helpd_16'			=> '讀一個出售帖時，首先要在點擊「購買」按鈕，如果銅幣數大於購買帖子所需的數量，就會扣掉相應的銅幣數，同時購買成功，可以閱讀到帖子內容。',
	'helpd_17'			=> '在「控制面板」的「我的主題」下，可以查看我的主題，我的回復，我的精華，我的投票，我的交易等',
	'helpd_19'			=> '在帖子閱讀頁面，可以看到「打印 | 加為IE收藏 | 收藏主題 | 上一主題 | 下一主題」，點擊「收藏主題」後，可以在「控制面板」下的「收藏夾」裡看到收藏的主題，同時可以對收藏的主題進行分類管理。',
	'helpd_20'			=> '朋友圈功能以插件形式存在，可以通過創建或加入已創建朋友圈，實現和同一朋友圈會員間更緊密的互動。',
	'helpd_21'			=> '可以通過會員頭像信息欄或者帖子樓層短消息按鈕實現會員之間互發短消息。',
	'helpd_22'			=> '點擊導航欄的「搜索」鏈接進入搜索頁面，在搜索頁面下，可以通過關鍵詞或用戶名對帖子主題、回復以及精華按版塊進行搜索。因級別不同和各論壇設置不同，搜索權限可能會受到限制。',
	'helpd_23'			=> '協助站長進行帖子監控、舉報不良帖子推薦優秀帖子的功能。在帖子樓層操作欄點擊「舉報」填寫理由並提交就能實現了對當前樓層帖子舉報的操作。',
	'helpd_24'			=> '是對論壇功能的一個重要補充。插件通過把程序文件上傳到hack目錄下，在後台「添加插件」處進行安裝，並能在「插件管理」中對相應的插件進行編輯管理和卸載。插件安裝好後並開啟後通過設置下拉顯示或直接顯示，讓插件鏈接顯示在「社區服務」的下拉菜單中或者直接顯示在導航欄上。',
	'helpd_25'			=> '對會員使用的道具在會員頭像信息欄的頭像按鈕處選擇，對帖子使用的道具在帖子操作欄的「使用道具」處選擇。打開道具列表後可以看到自己存有的道具數量，如果數量為零需要購買才能使用。',
	'helpd_26'			=> '程序提供了幾處常用的Rss訂閱。首頁的「Rss訂閱本版面最新帖子」、分類頁面下的「Rss訂閱本版面最新帖子」和版塊頁面下的「Rss訂閱本版面最新帖子」。',
	'helpd_27'			=> '<table><tr class="tr3 tr"><td><font color="#5a6633" face="verdana">[quote]</font>被引用的內容，主要作用是在發帖時引用並回復具體樓層的帖子<font color="#5a6633" face="verdana">[/quote]</font></td><td><table cellpadding="5" cellspacing="1" width="94%" bgcolor="#000000" align="center"><tr><td class="f_one">被引用的帖子和您的回復內容</td></tr></table></td></tr><tr class="tr3 tr"><td><font color="#5a6633" face="verdana">[code]</font><font color="#5a6633"></font><font face="courier" color="#333333"><br />echo "phpwind 歡迎您!"\r</font><font color="#5a6633" face="verdana">[/code]</font></td><th><div class="tpc_content" id="read_553959"><h6 class="quote">Copy code</h6><blockquote id="code1">echo "phpwind 歡迎您!"</blockquote></div></th></tr><tr class="tr3 tr"><td><font face="verdana" color="5a6633">[url]</font><font face="verdana">http://www.phpwind.net</font><font color="5a6633">[/url] </font></td><td><a href="http://www.phpwind.net" target="_blank"><font color="#000066">http://www.phpwind.net</font></a></td></tr><tr class="tr3 tr"><td><font face="verdana" color="5a6633">[url=http://www.phpwind.net]</font><font face="verdana">phpwind</font><font color="5a6633">[/url]</font></td><td><a href="http://www.phpwind.net"><font color="000066">PHPwind</font></a></font></td></tr><tr class="tr3 tr"><td><font face="verdana" color="5a6633">[email]</font><font face="verdana">fengyu@163.com</font><font color="5a6633">[/email]</font></td><td><a href="mailto:fengyu@163.com"><font color="000066">fengyu@163.com</font></a></td></tr><tr class="tr3 tr"><td><font face="verdana" color="5a6633">[email=fengyu@163.com]</font><font face="verdana">email me</font><font color="5a6633">[/email]</font></td><td><a href="mailto:fengyu@163.com"><font color="000066">email me</font></a></td></tr><tr class="tr3 tr"> <td><font face="verdana" color="5a6633">[b]</font><font face="verdana">粗體字</font><font color="5a6633" face="verdana">[/b]</font> </td><td><font face="verdana"><b>粗體字</b></font> </td></tr><tr class="tr3 tr"><td><font face="verdana" color="5a6633">[i]</font><font face="verdana">斜體字<font color="5a6633">[/i]</font> </font></td><td><font face="verdana"><i>斜體字</i></font> </td></tr><tr class="tr3 tr"><td><font face="verdana" color="5a6633">[u]</font><font face="verdana">下劃線</font><font color="5a6633">[/u]</font></td><td><font face="verdana"><u>下劃線</u></font> </td></tr><tr class="tr3 tr"> <td><font face=verdana color=5a6633>[align=center(可以是向左left，向右right)]</font>位於中間<font color="5a6633">[/align]</font></td><td><font face="verdana"><div align="center">中間對齊</div></font></td></tr><tr class="tr3 tr"> <td><font face="verdana" color="5a6633">[size=4]</font><font face="verdana">改變字體大小<font color="5a6633">[/size] </font> </font></td><td><font face="verdana">改變字體大小 </font></td></tr><tr class="tr3 tr"> <td><font face="verdana" color="5a6633">[font=</font><font color="5a6633">楷體_gb2312<font face="verdana">]</font></font><font face="verdana">改變字體<font color="5a6633">[/font] </font> </font></td><td><font face="verdana"><font face=楷體_gb2312>改變字體</font> </font></td></tr><tr class="tr3 tr"> <td><font face="verdana" color="5a6633">[color=red]</font><font face="verdana">改變顏色<font color="5a6633">[/color] </font> </font></td><td><font face="verdana" color="red">改變顏色</font><font face="verdana"> </font></td></tr><tr class="tr3 tr"> <td><font face="verdana" color="5a6633">[img]</font><font face="verdana">http://www.phpwind.net/logo.gif<font color="5a6633">[/img]</font> </font></td><td><img src="logo.gif" /></font> </td></tr><tr class="tr3 tr"> <td><font face=細明體 color="#333333"><font color="#5a6633">[fly]</font>飛行文字特效<font color="#5a6633">[/fly]</font> </font></td><td><font face=細明體&nbsp; &nbsp; color="#333333"><marquee scrollamount="3" behavior="alternate" width="90%">飛行文字特效</marquee></font></td></tr><tr class="tr3 tr"> <td><font face=細明體 color="#333333"><font color="#5a6633">[move]</font>滾動文字特效<font color="#5a6633">[/move]</font> </font></td><td><font face=細明體 color="#333333"> <marquee scrollamount="3" width="90%">滾動文字特效</marquee></font></td></tr><tr class="tr3 tr"><td><font face=細明體 color="#333333"><font color="#5a6633">[flash=400,300]</font>http://www.phpwind.net/wind.swf<font color="#5a6633">[/flash]</font> </font></td><td><font face=細明體 color="#333333">將顯示flash文件</font> </td></tr><tr class="tr3 tr"><td><font face=細明體 color="#333333"><font color="#5a6633">[iframe]</font>http://www.phpwind.net<font color="#5a6633">[/iframe]</font> </font></td><td><font face=細明體 color="#333333">將在帖子中粘貼網頁(後台默認關閉)</font> </td></tr><tr class="tr3 tr"><td><font color=#5a6633>[glow=255(寬度),red(顏色),1(邊界)]</font>要產生光暈效果的文字<font color="#5a6633">[/glow]</font></td><td align="center"><font face=細明體 color="#333333"><table width="255" style="filter:glow(color=red, direction=1)"><tr><td align="center">要產生彩色發光效果的文字</td></tr></table></font></td></tr></table>',
	'home_1'			=> '社區焦點',
	'home_2'			=> '熱門標籤',
	'home_3'			=> '推薦帖子',
	'home_4'			=> '熱門文章',
	'home_5'			=> '站點信息',
	'home_6'			=> '版塊排行',
	'home_7'			=> '最新回復',
	'home_8'			=> '最新帖子',
	'home_9'			=> '會員排行',

	'level_1'			=> '遊客',
	'level_3'			=> '管理員',
	'level_4'			=> '總版主',
	'level_5'			=> '論壇版主',
	'level_6'			=> '禁止發言',
	'level_7'			=> '未驗證會員',
	'level_8'			=> '新手上路',
	'level_9'			=> '俠客',
	'level_10'			=> '騎士',
	'level_11'			=> '聖騎士',
	'level_12'			=> '精靈王',
	'level_13'			=> '風雲使者',
	'level_14'			=> '光明使者',
	'level_15'			=> '天使',
	'level_16'			=> '榮譽會員',

	'medaldesc_1'		=> '謝謝您為社區發展做出的不可磨滅的貢獻!',
	'medaldesc_2'		=> '辛勞地為論壇付出勞動，收穫快樂，感謝您!',
	'medaldesc_3'		=> '謝謝您為社區積極宣傳,特頒發此獎!',
	'medaldesc_4'		=> '您為論壇做出了特殊貢獻,謝謝您!',
	'medaldesc_5'		=> '為社區提出建設性的建議被採納,特頒發此獎!',
	'medaldesc_6'		=> '謝謝您積極發表原創作品,特頒發此獎!',
	'medaldesc_7'		=> '帖圖高手,堪稱大師!',
	'medaldesc_8'		=> '能夠長期提供優質的社區水資源者,可得這個獎章!',
	'medaldesc_9'		=> '新人有很大的進步可以得到這個獎章!',
	'medaldesc_10'		=> '您總是能給別人帶來歡樂,謝謝您!',
	'medalname_1'		=> '終身成就獎',
	'medalname_2'		=> '優秀斑竹獎',
	'medalname_3'		=> '宣傳大使獎',
	'medalname_4'		=> '特殊貢獻獎',
	'medalname_5'		=> '金點子獎',
	'medalname_6'		=> '原創先鋒獎',
	'medalname_7'		=> '貼圖大師獎',
	'medalname_8'		=> '灌水天才獎',
	'medalname_9'		=> '新人進步獎',
	'medalname_10'		=> '幽默大師獎',
	'money'				=> '銅幣',

	'ol_whycolse'		=> '系統沒有開啟網上支付功能!',

	'plan_1'			=> '定時清理群發消息',
	'plan_2'			=> '自動解除禁言',
	'plan_3'			=> '發送生日短消息',
	'plan_4'			=> '懸賞帖到期通知',
	'plan_5'			=> '管理團隊工資發放',
	'plan_6'			=> '勳章自動回收',
	'plan_7'			=> '限期頭銜自動回收',

	'rg_banname'		=> '版主,管理員,admin,斑竹',
	'rg_rgpermit'		=> '當您申請用戶時，表示您已經同意遵守本規章。 <br /><br />歡迎您加入本站點參加交流和討論，本站點為公共論壇，為維護網上公共秩序和社會穩定，請您自覺遵守以下條款： <br /><br />一、不得利用本站危害國家安全、洩露國家秘密，不得侵犯國家社會集體的和公民的合法權益，不得利用本站製作、複製和傳播下列信息：<br />　 （一）煽動抗拒、破壞憲法和法律、行政法規實施的；<br />　（二）煽動顛覆國家政權，推翻社會主義制度的；<br />　（三）煽動分裂國家、破壞國家統一的；<br />　（四）煽動民族仇恨、民族歧視，破壞民族團結的；<br />　（五）捏造或者歪曲事實，散佈謠言，擾亂社會秩序的；<br />　（六）宣揚封建迷信、淫穢、色情、賭博、暴力、兇殺、恐怖、教唆犯罪的；<br />　（七）公然侮辱他人或者捏造事實誹謗他人的，或者進行其他惡意攻擊的；<br />　（八）損害國家機關信譽的；<br />　（九）其他違反憲法和法律行政法規的；<br />　（十）進行商業廣告行為的。<br /><br />二、互相尊重，對自己的言論和行為負責。<br />三、禁止在申請用戶時使用相關本站的詞彙，或是帶有侮辱、譭謗、造謠類的或是有其含義的各種語言進行註冊用戶，否則我們會將其刪除。<br />四、禁止以任何方式對本站進行各種破壞行為。<br />五、如果您有違反國家相關法律法規的行為，本站概不負責，您的登錄論壇信息均被記錄無疑，必要時，我們會向相關的國家管理部門提供此類信息。 ',
	'rg_welcomemsg'		=> '感謝您的註冊，歡迎您的到來，希望這裡能給您帶來快樂！多多發言吧！本站全體管理人員向您問好<br />您的註冊名為:$rg_name',
	'rg_whyregclose'	=> '管理員關閉了註冊!',
	'rvrc'				=> '威望',

	'sharelinks'		=> 'PHPwind官方論壇',
	'smile'				=> '默認表情',

	'tool_1'			=> '威望工具',
	'tool_1_inro'		=> '可將自己負威望清零',
	'tool_2'			=> '清零卡',
	'tool_2_inro'		=> '可將自己所有負分清零,包括銅幣,威望,貢獻值,積分',
	'tool_3'			=> '醒目卡',
	'tool_3_inro'		=> '可以將自己的帖子標題加亮顯示',
	'tool_4'			=> '置頂I',
	'tool_4_inro'		=> '可將自己發表的帖子在版塊中置頂，置頂時間為6小時',
	'tool_5'			=> '置頂II',
	'tool_5_inro'		=> '可將自己發表的帖子在分類中置頂，置頂時間為6小時',
	'tool_6'			=> '置頂III',
	'tool_6_inro'		=> '可將自己發表的帖子在整個論壇中置頂，置頂時間為6小時',
	'tool_7'			=> '提前帖子',
	'tool_7_inro'		=> '可以把自己發表的帖子提前到帖子所在版塊的第一頁',
	'tool_8'			=> '改名卡',
	'tool_8_inro'		=> '可更改自己在論壇的用戶名',
	'tool_9'			=> '精華I',
	'tool_9_inro'		=> '可以將自己的帖子加為精華I',
	'tool_10'			=> '精華II',
	'tool_10_inro'		=> '可以將自己的帖子加為精華II',
	'tool_11'			=> '鎖定帖子',
	'tool_11_inro'		=> '可以將自己發表的帖子鎖定，不讓其他會員回復此帖',
	'tool_12'			=> '解除鎖定',
	'tool_12_inro'		=> '可以解除自己被帖子鎖定，讓其他會員可以回復此帖',
	'tool_13'			=> '鮮花',
	'tool_13_inro'		=> '可以給帖子增加推薦數',
	'tool_14'			=> '雞蛋',
	'tool_14_inro'		=> '可以給帖子減少推薦數',
	'tool_15'			=> '運氣卡',
	'tool_15_inro'		=> '使用後隨機加減交易幣(-100,100)',
	'tool_16'			=> '生日卡',
	'tool_16_inro'		=> '以短消息方式發送給好友，祝好友生日快樂',
	'tool_17'			=> '沉澱卡',
	'tool_17_inro'		=> '帖子中使用，每用一次讓帖子丟到12小時前',
	'tool_18'			=> '豬頭術',
	'tool_18_inro'		=> '使用後讓對方頭像變為豬頭，此效果持續24小時或到對方使用清洗卡為止',
	'tool_19'			=> '還原卡',
	'tool_19_inro'		=> '清除豬頭卡的效果',
	'tool_20'			=> '透視鏡',
	'tool_20_inro'		=> '對用戶使用 查看用戶IP',
	'tool_21'			=> '護身符',
	'tool_21_inro'		=> '使用後，不能對該用戶實現豬頭術效果，48小時內有效',
	'tool_22'			=> '時空卡',
	'tool_22_inro'		=> '帖子中使用，讓帖子發佈到12小時後',

	'admin_login'		=> '請先用創始人登錄再進行升級操作',

	'block_1'			=> '最新主題',
	'block_2'			=> '最新回復',
	'block_3'			=> '精華主題',
	'block_4'			=> '回復排行',
	'block_5'			=> '人氣排行',
	'block_6'			=> '金錢排行',
	'block_7'			=> '威望排行',
	'block_8'			=> '今日發帖',
	'block_9'			=> '主題數排行',
	'block_10'			=> '發帖量排行',
	'block_11'			=> '熱門標籤',
	'block_12'			=> '最新標籤',
	'block_13'			=> '最新圖片',
	'block_14'			=> '最新圖片(頻道)',
	'block_15'			=> '熱門活動(整站)',
	'block_16'			=> '最新交易(整站)',
	'block_17'			=> '熱門交易(整站)',
	'block_18'			=> '在線時間排行',
	'block_19'			=> '今日發帖排行',
	'block_20'			=> '月發帖排行',
	'block_21'			=> '發帖排行',
	'block_22'			=> '月在線排行',
	'block_23'			=> '今日熱帖(整站)',
	'block_24'			=> '熱門活動(頻道)',
	'block_25'			=> '貢獻值排行',
	'block_26'			=> '交易幣排行榜',
	'block_27'			=> '首頁推送',
	'block_28'			=> '加亮主題(頻道)',
	'block_29'			=> '加亮主題(整站)',
	'block_30'			=> '今日熱帖(頻道)',
	'block_31'			=> '精華主題(頻道)',
	'block_32'			=> '精華主題(版塊)',
	'block_33'			=> '加亮主題(版塊)',
	'block_34'			=> '最新回復(版塊)',
	'block_35'			=> '頻道頁推送',
	'block_36'			=> '點擊排行(頻道)',
	'block_37'			=> '最新主題(頻道)',
	'block_38'			=> '今日活動(整站)',
	'block_39'			=> '今日活動(頻道)',
	'block_40'			=> '最新圖片(版塊)',
	'block_41'			=> '最新活動(整站)',
	'block_42'			=> '最新活動(頻道)',
	'block_43'			=> '最新交易(頻道)',
	'block_44'			=> '熱門交易(頻道)',
	'block_45'			=> '今日交易(整站)',
	'block_46'			=> '今日交易(頻道)',
	'block_47'			=> '今日人氣(整站)',
	'block_48'			=> '今日人氣(頻道)',
	'stamp_1'			=> '帖子排行',
	'stamp_2'			=> '用戶排行',
	'stamp_3'			=> '版塊排行',
	'stamp_4'			=> '標籤排行',
	'stamp_5'			=> '圖片',
	'stamp_6'			=> '活動',
	'stamp_7'			=> '交易',
	'stamp_8'			=> '推送',
	'fourmtype_2'		=> '懸賞',
	'fourmtype_5'		=> '頻道頁隱藏',
	'mode_area_name'	=> '門戶模式',
	'mode_pw_name'		=> '論壇模式',
	'mode_o_name'		=> '個人中心',
	'mode_area_header'	=> '門戶',
	'mode_pw_header'	=> '論壇',
	'mode_o_header'		=> '個人中心',
	'mode_pgcon_index'	=> '首頁',
	'mode_pgcon_cate'	=> '頻道頁',
	'mode_pgcon_thread'	=> '列表頁',
	'mode_pgcon_m_home'	=> '動態',

	//7.3.2
	'diary_o_uploadsize'=> 'a:5:{s:3:"jpg";i:300;s:4:"jpeg";i:300;s:3:"png";i:400;s:3:"gif";i:400;s:3:"bmp";i:400;}',

	'config_noexists'	=> '數據庫配置文件不存在,請重新填寫配置信息',
	'install_initdata'	=> "正在初始化數據,請稍候...$GLOBALS[stepstring]",
	'undefined_action'	=> '非法操作,請返回',
	'action_success'	=> '此步操作完成,請進入下一步操作',
	'promptmsg_database'=> '繼續安裝',

	'Site.Header'		=> '頭部橫幅~	~顯示在頁面的頭部，一般以圖片或flash方式顯示，多條廣告時系統將隨機選取一條顯示',
	'Site.Footer'		=> '底部橫幅~	~顯示在頁面的底部，一般以圖片或flash方式顯示，多條廣告時系統將隨機選取一條顯示',
	'Site.NavBanner1'	=> '導航通欄~	~顯示在主導航的下面，一般以圖片或flash方式顯示，多條廣告時系統將隨機選取一條顯示',
	'Site.PopupNotice'	=> '彈窗廣告[右下]~	~在頁面右下角以浮動的層彈出顯示，此廣告內容需要單獨設置相關窗口參數',
	'Site.FloatRand'	=> '漂浮廣告[隨機]~	~以各種形式在頁面內隨機漂浮的廣告',
	'Site.FloatLeft'	=> '漂浮廣告[左]~	~以各種形式在頁面左邊漂浮的廣告，俗稱對聯廣告[左]',
	'Site.FloatRight'	=> '漂浮廣告[右]~	~以各種形式在頁面右邊漂浮的廣告，俗稱對聯廣告[右]',
	'Mode.TextIndex'	=> '文字廣告[首頁]~	~顯示在頁面的導航下面，一般以文字方式顯示，每行四條廣告，超過四條將換行顯示',
	'Mode.Forum.TextRead'	=> '文字廣告[帖子頁]~	~顯示在頁面的導航下面，一般以文字方式顯示，每行四條廣告，超過四條將換行顯示',
	'Mode.Forum.TextThread'	=> '文字廣告[主題頁]~	~顯示在頁面的導航下面，一般以文字方式顯示，每行四條廣告，超過四條將換行顯示',
	'Mode.Forum.Layer.TidRight'	=> '樓層廣告[帖子右側]~	~出現在帖子右側，一般以圖片或文字顯示，多條帖間廣告時系統將隨機選取一條顯示',
	'Mode.Forum.Layer.TidDown'	=> '樓層廣告[帖子下方]~	~出現在帖子下方，一般以圖片或文字顯示，多條帖間廣告時系統將隨機選取一條顯示',
	'Mode.Forum.Layer.TidUp'		=> '樓層廣告[帖子上方]~	~出現在帖子上方，一般以圖片或文字顯示，多條帖間廣告時系統將隨機選取一條顯示',
	'Mode.Forum.Layer.TidAmong'	=> '樓層廣告[樓層中間]~	~出現在帖子樓層之間，一般以圖片或文字顯示，多條帖間廣告時系統將隨機選取一條顯示',
	'Mode.Layer.Index'		=> '首頁分類間廣告~	~出現在首頁分類層之間，一般以圖片或文字顯示，多條帖間廣告時系統將隨機選取一條顯示',

	'nav_home_main' => '首頁',
	'nav_bbs_main' => '論壇',
	'nav_o_main' => '個人中心',
	'nav_sort_bbs' => '統計排行',
	'nav_app_bbs' => 'APP應用',
	'nav_hack_bbs' => '社區服務',
	'nav_member_bbs' => '會員列表',
	'nav_lastpost_bbs' => '最新帖子',
	'nav_digest_bbs' => '精華區',
	'nav_search_bbs' => '搜索',
	'nav_faq_bbs' => '幫助',
	'nav_sort_basic_bbs' => '基本信息',
	'nav_sort_ipstate_bbs' => '到訪IP統計',
	'nav_sort_team_bbs' => '管理團隊',
	'nav_sort_admin_bbs' => '管理操作',
	'nav_sort_online_bbs' => '在線會員',
	'nav_sort_member_bbs' => '會員排行',
	'nav_sort_forum_bbs' => '版塊排行',
	'nav_sort_article_bbs' => '帖子排行',
	'nav_sort_taglist_bbs' => '標籤排行',
	'nav_index_o' => '個人中心首頁',
	'nav_home_o' => '我的首頁',
	'nav_user_o' => '個人空間',
	'nav_friend_o' => '朋友',
	'nav_browse_o' => '隨便看看',
);

$appinfo =array(
	'226'=>array(
		'cup' => '167',
		'cid' => '226',
		'name' => '國際軍情',
		'ifshow' => '0',
	),

	'210'=>array(
		'cup' => '170',
		'cid' => '210',
		'name' => '遊戲機',
		'ifshow' => '0',
	),

	'209'=>array(
		'cup' => '170',
		'cid' => '209',
		'name' => '相機|DV',
		'ifshow' => '0',
	),

	'208'=>array(
		'cup' => '170',
		'cid' => '208',
		'name' => 'PC|筆記本',
		'ifshow' => '0',
	),

	'207'=>array(
		'cup' => '170',
		'cid' => '207',
		'name' => '手機|PDA',
		'ifshow' => '0',
	),

	'206'=>array(
		'cup' => '170',
		'cid' => '206',
		'name' => '智庫資源',
		'ifshow' => '0',
	),

	'205'=>array(
		'cup' => '170',
		'cid' => '205',
		'name' => '業界資訊',
		'ifshow' => '0',
	),

	'204'=>array(
		'cup' => '171',
		'cid' => '204',
		'name' => '男人裝|女人裝',
		'ifshow' => '0',
	),

	'203'=>array(
		'cup' => '171',
		'cid' => '203',
		'name' => '達人指點',
		'ifshow' => '0',
	),

	'202'=>array(
		'cup' => '171',
		'cid' => '202',
		'name' => '城市精銳',
		'ifshow' => '0',
	),

	'201'=>array(
		'cup' => '171',
		'cid' => '201',
		'name' => '護膚|美妝',
		'ifshow' => '0',
	),

	'200'=>array(
		'cup' => '171',
		'cid' => '200',
		'name' => '名品精選',
		'ifshow' => '0',
	),

	'199'=>array(
		'cup' => '172',
		'cid' => '199',
		'name' => '國際旅遊',
		'ifshow' => '0',
	),

	'198'=>array(
		'cup' => '172',
		'cid' => '198',
		'name' => '國內旅遊',
		'ifshow' => '0',
	),

	'211'=>array(
		'cup' => '170',
		'cid' => '211',
		'name' => '其他家電',
		'ifshow' => '0',
	),

	'212'=>array(
		'cup' => '169',
		'cid' => '212',
		'name' => '熱點關注',
		'ifshow' => '0',
	),

	'225'=>array(
		'cup' => '167',
		'cid' => '225',
		'name' => '台海軍情',
		'ifshow' => '0',
	),

	'224'=>array(
		'cup' => '167',
		'cid' => '224',
		'name' => '中國軍情',
		'ifshow' => '0',
	),

	'223'=>array(
		'cup' => '167',
		'cid' => '223',
		'name' => '尖端武器',
		'ifshow' => '0',
	),

	'222'=>array(
		'cup' => '167',
		'cid' => '222',
		'name' => '軍事歷史',
		'ifshow' => '0',
	),

	'221'=>array(
		'cup' => '167',
		'cid' => '221',
		'name' => '網上談兵',
		'ifshow' => '0',
	),

	'220'=>array(
		'cup' => '168',
		'cid' => '220',
		'name' => '博客大觀',
		'ifshow' => '0',
	),

	'219'=>array(
		'cup' => '168',
		'cid' => '219',
		'name' => ' 音樂|影視',
		'ifshow' => '0',
	),

	'218'=>array(
		'cup' => '168',
		'cid' => '218',
		'name' => '獵奇|笑談',
		'ifshow' => '0',
	),

	'217'=>array(
		'cup' => '168',
		'cid' => '217',
		'name' => '星座|占卜',
		'ifshow' => '0',
	),

	'216'=>array(
		'cup' => '168',
		'cid' => '216',
		'name' => '遊戲|動漫',
		'ifshow' => '0',
	),

	'215'=>array(
		'cup' => '168',
		'cid' => '215',
		'name' => '明星|傳聞|八卦',
		'ifshow' => '0',
	),

	'214'=>array(
		'cup' => '169',
		'cid' => '214',
		'name' => '試駕資訊',
		'ifshow' => '0',
	),

	'213'=>array(
		'cup' => '169',
		'cid' => '213',
		'name' => '口碑大全',
		'ifshow' => '0',
	),

	'197'=>array(
		'cup' => '172',
		'cid' => '197',
		'name' => '旅行寶典',
		'ifshow' => '0',
	),

	'196'=>array(
		'cup' => '172',
		'cid' => '196',
		'name' => '演出|劇場',
		'ifshow' => '0',
	),

	'195'=>array(
		'cup' => '172',
		'cid' => '195',
		'name' => '家居|裝飾',
		'ifshow' => '0',
	),

	'184'=>array(
		'cup' => '174',
		'cid' => '184',
		'name' => '做站的那些事',
		'ifshow' => '0',
	),

	'177'=>array(
		'cup' => '176',
		'cid' => '177',
		'name' => '全日制教育',
		'ifshow' => '0',
	),

	'178'=>array(
		'cup' => '176',
		'cid' => '178',
		'name' => '成教|自學考',
		'ifshow' => '0',
	),

	'179'=>array(
		'cup' => '176',
		'cid' => '179',
		'name' => '技能培訓',
		'ifshow' => '0',
	),

	'181'=>array(
		'cup' => '176',
		'cid' => '181',
		'name' => '四六級',
		'ifshow' => '0',
	),

	'182'=>array(
		'cup' => '175',
		'cid' => '182',
		'name' => '美文',
		'ifshow' => '0',
	),

	'183'=>array(
		'cup' => '174',
		'cid' => '183',
		'name' => 'IT圈',
		'ifshow' => '0',
	),

	'180'=>array(
		'cup' => '176',
		'cid' => '180',
		'name' => '公務員',
		'ifshow' => '0',
	),

	'101'=>array(
		'cup' => '0',
		'cid' => '101',
		'name' => '社會',
		'ifshow' => '0',
	),

	'166'=>array(
		'cup' => '101',
		'cid' => '166',
		'name' => '國內時政',
		'ifshow' => '0',
	),

	'186'=>array(
		'cup' => '101',
		'cid' => '186',
		'name' => '國際時政',
		'ifshow' => '0',
	),

	'189'=>array(
		'cup' => '101',
		'cid' => '189',
		'name' => '房產|零售',
		'ifshow' => '0',
	),

	'190'=>array(
		'cup' => '173',
		'cid' => '190',
		'name' => '足球',
		'ifshow' => '0',
	),

	'191'=>array(
		'cup' => '173',
		'cid' => '191',
		'name' => '籃球',
		'ifshow' => '0',
	),

	'192'=>array(
		'cup' => '173',
		'cid' => '192',
		'name' => '其他',
		'ifshow' => '0',
	),

	'193'=>array(
		'cup' => '172',
		'cid' => '193',
		'name' => '烹飪|美食',
		'ifshow' => '0',
	),

	'194'=>array(
		'cup' => '172',
		'cid' => '194',
		'name' => '健康|醫療',
		'ifshow' => '0',
	),

	'187'=>array(
		'cup' => '101',
		'cid' => '187',
		'name' => '金融|能源|通訊',
		'ifshow' => '0',
	),

	'188'=>array(
		'cup' => '101',
		'cid' => '188',
		'name' => '航空|旅遊',
		'ifshow' => '0',
	),

	'167'=>array(
		'cup' => '0',
		'cid' => '167',
		'name' => '軍事',
		'ifshow' => '0',
	),

	'168'=>array(
		'cup' => '0',
		'cid' => '168',
		'name' => '娛樂',
		'ifshow' => '0',
	),

	'169'=>array(
		'cup' => '0',
		'cid' => '169',
		'name' => '汽車',
		'ifshow' => '0',
	),

	'170'=>array(
		'cup' => '0',
		'cid' => '170',
		'name' => '數碼',
		'ifshow' => '0',
	),

	'171'=>array(
		'cup' => '0',
		'cid' => '171',
		'name' => '時尚',
		'ifshow' => '0',
	),

	'172'=>array(
		'cup' => '0',
		'cid' => '172',
		'name' => '生活',
		'ifshow' => '0',
	),

	'174'=>array(
		'cup' => '0',
		'cid' => '174',
		'name' => 'IT',
		'ifshow' => '0',
	),

	'176'=>array(
		'cup' => '0',
		'cid' => '176',
		'name' => '教育培訓',
		'ifshow' => '0',
	),

	'175'=>array(
		'cup' => '0',
		'cid' => '175',
		'name' => '文學文藝',
		'ifshow' => '0',
	),

	'173'=>array(
		'cup' => '0',
		'cid' => '173',
		'name' => '體育',
		'ifshow' => '0',
	),

);