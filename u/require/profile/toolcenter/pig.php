<?php
!function_exists('readover') && exit('Forbidden');

/**
 * @name:豬頭卡
 * @type:會員類
 * @effect:使用後讓對方頭像變為豬頭，卡片效果持續24小時或到對方使用還原卡為止．
 */

S::gp(array('uid'),'GP',2);
if ($tooldb['type'] != 2){
	Showmsg('tooluse_type_error');  // 判斷道具類型是否設置錯誤
}
if (!$uid){
	Showmsg('不存在變為豬頭的對像');
}

$rt = $db->get_one("SELECT MAX(time) AS tooltime FROM pw_toollog WHERE touid=".S::sqlEscape($uid)."AND filename='defend'");

if($rt && $rt['tooltime'] > $timestamp-3600*48){
	Showmsg('該會員已經使用了護身符,不能對其使用豬頭術');
}

$userService = L::loadClass('UserService', 'user'); /* @var $userService PW_UserService */
$rt = $userService->get($uid);//icon
$user_a = explode('|',addslashes($rt['icon']));
if ($user_a[4]){
	Showmsg('已經變為豬頭，此豬頭卡失效');
}

$db->update("UPDATE pw_usertool SET nums=nums-1 WHERE uid=".S::sqlEscape($winduid)."AND toolid=".S::sqlEscape($toolid));
if(empty($rt['icon'])){
	$userface="||0||1";
} else{
	//$userface="$user_a[0]|$user_a[1]|$user_a[2]|$user_a[3]|1";
	$user_a[4] = 1;
	$userface = implode('|',$user_a);
}

$userService->update($uid, array('icon' => $userface), array(), array('tooltime' => $timestamp));

$logdata = array(
	'type'		=>	'use',
	'descrip'	=>	'tool_18_descrip',
	'uid'		=>	$winduid,
	'username'	=>	$windid,
	'ip'		=>	$onlineip,
	'time'		=>	$timestamp,
	'toolname'	=>	$tooldb['name'],
);
writetoollog($logdata);
//* $_cache = getDatastore();
//* $_cache->delete('UID_'.$uid);
Showmsg('toolmsg_success');
?>