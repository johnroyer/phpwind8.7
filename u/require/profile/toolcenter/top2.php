<?php
!function_exists('readover') && exit('Forbidden');

/****

@name:置頂道具
@type:帖子類
@effect:可將自己發表的帖子在分類中置頂，置頂時間為6小時。

****/

if($tooldb['type']!=1){
	Showmsg('tooluse_type_error');  // 判斷道具類型是否設置錯誤
}
if($tpcdb['authorid'] != $winduid){
	Showmsg('tool_authorlimit');
}
if($tpcdb['topped'] > 1){
	Showmsg('toolmsg_5_failed');
}
$toolfield = $timestamp + 3600*6;
//$db->update("UPDATE pw_threads SET topped='2',toolinfo=".S::sqlEscape($tooldb['name'],false).",toolfield=".S::sqlEscape($toolfield)."WHERE tid=".S::sqlEscape($tid));
pwQuery::update('pw_threads', 'tid=:tid', array($tid), array('topped'=>2, 'toolinfo'=>$tooldb['name'], 'toolfield'=>$toolfield));
$fid = $db->get_value("SELECT fid FROM pw_threads WHERE tid=".intval($tid));
//* $threadList = L::loadClass("threadlist", 'forum');
//* $threadList->refreshThreadIdsByForumId($fid);
Perf::gatherInfo('changeThreadWithForumIds', array('fid'=>$fid));
require_once(R_P.'require/updateforum.php');
setForumsTopped($tid,$fid,2,$toolfield);
updatetop();
delfcache($fid, $db_fcachenum);

$db->update("UPDATE pw_usertool SET nums=nums-1 WHERE uid=".S::sqlEscape($winduid)."AND toolid=".S::sqlEscape($toolid));
$logdata=array(
	'type'		=>	'use',
	'nums'		=>	'',
	'money'		=>	'',
	'descrip'	=>	'tool_5_descrip',
	'uid'		=>	$winduid,
	'username'	=>	$windid,
	'ip'		=>	$onlineip,
	'time'		=>	$timestamp,
	'toolname'	=>	$tooldb['name'],
	'subject'	=>	substrs($tpcdb['subject'],15),
	'tid'		=>	$tid,
);
writetoollog($logdata);
Showmsg('toolmsg_success');
?>