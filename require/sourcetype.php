<?php
!function_exists('readover') && exit('Forbidden');
return array(
	'subject'	=> array('title'=>'帖子排行'),
	'reward'	=> array('title'=>'懸賞排行'),
	'classify'	=> array('title'=>'分類信息'),
	'activity'	=> array('title'=>'活動排行'),
	'debate'	=> array('title'=>'辯論排行'),
	'user'		=> array('title'=>'會員排行'),
	'image'		=> array('title'=>'論壇圖片'),
	'forum'		=> array('title'=>'版塊排行'),
	'tag'		=> array('title'=>'標籤排行'),
	'group'		=> array('title'=>'群組排行'),
	'groupimage'=> array('title'=>'群組相片'),
	'trade'     => array('title'=>'商品排行'),
	'pcvalue'   => array('title'=>'團購排行'),
	'polls'     => array('title'=>'投票排行'),
	'tucool'    => array('title'=>'圖酷排行'),
	'grouparticle'	=> array('title'=>'群組話題'),
	'diary'		=> array('title'=>'日誌排行'),
	'photo'		=> array('title'=>'相冊排行'),
	//'owrite'	=> array('title'=>'記錄'),
	'announce'  => array('title'=>'公告'),
	'sharelinks'=> array('title'=>'友情鏈接'),
	'weibosort'	=> array('title'=>'微博排行'),
);
?>