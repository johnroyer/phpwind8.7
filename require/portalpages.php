<?php
!function_exists('readover') && exit('Forbidden');

return array(
	'bbsindex'  =>'首頁四格',
	'bbsradio'  =>'廣播台',
	'usermix'  =>'個人用戶日誌',
	'userlist'  =>'個人用戶列表',
	'groupgatherleft' => '群組聚合頁面',
	'groupgatherright' => '群組聚合頁面右側',
);