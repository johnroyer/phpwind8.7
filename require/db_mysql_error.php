<?php
!function_exists('readover') && exit('Forbidden');

Class DB_ERROR {
	function DB_ERROR($msg) {
		global $db_bbsname,$db_obstart,$REQUEST_URI,$dbhost,$pwServer,$db_charset;
		$sqlerror = mysql_error();
		$sqlerrno = mysql_errno();
		$sqlerror = str_replace($dbhost,'dbhost',$sqlerror);
		ob_end_clean();
		$db_obstart && function_exists('ob_gzhandler') ? ob_start('ob_gzhandler') : ob_start();
		if (defined('AJAX')) {
			header("Content-Type: text/xml;charset=$db_charset");
			echo "<?xml version=\"1.0\" encoding=\"$db_charset\"?><ajax><![CDATA[$msg]]></ajax>";
		} else {
			echo"<!doctype html><head><meta charset=utf-8'$db_charset' /><title>$db_bbsname</title><link rel=\"stylesheet\" href=\"images/pw_core.css?101128\" /><style>.tips{border:3px solid #dcc7ab;background:#fffef5;font:12px/1.5 Arial,Microsoft Yahei,mingliu;color:#000;padding:20px;width:500px;margin:50px auto;-moz-box-shadow:0 0 5px #eaeaea;box-shadow:0 0 5px #eaeaea;}a{text-decoration:none;color:#014c90;}a:hover,.alink a,.link{text-decoration:underline;}</style>";
			echo"<div class=\"tips\"><table width=\"100%\"><tr><td><h2 class=\"f14 b\">錯誤信息:</h2><p>$msg</p><br>";
			echo"<h2 class=\"f14 b\">鏈接地址(The URL Is):</h2>http://".$pwServer['HTTP_HOST'].$REQUEST_URI;
			echo"<br><br><h2 class=\"f14 b\">MySQL服務器錯誤(MySQL Server Error):</h2>$sqlerror  ( $sqlerrno )  <a target='_blank' href='http://faq.phpwind.net/mysql.php?id=$sqlerrno'>查看錯誤相關信息</a><br><br>";
			echo"<h2 class=\"f14 b\">尋求幫助(You Can Get Help In):</h2><a target='_blank' href='http://www.phpwind.net'>http://www.phpwind.net</a>";
			echo"</td></tr></table></div></body></html>";
		}
		$this->dblog($msg);
		exit;
	}
	function dblog($msg){
		$msg = str_replace(array("\n","\r","<"),array('','','&lt;'),$msg);
		if (file_exists(D_P.'data/bbscache/dblog.php')){
			pwCache::writeover(D_P.'data/bbscache/dblog.php',"$msg\n", 'ab');
		} else{
			pwCache::writeover(D_P.'data/bbscache/dblog.php',"<?php die;?>\n$msg\n");
		}
	}
}
?>