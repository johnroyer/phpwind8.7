<?php
!function_exists('readover') && exit('Forbidden');

/**
 * 發送社區短消息或系統通知
 * 重構新消息中心 
 * @param array $msg 信息格式如下:
 * 	$msg = array(
 *		'toUser'	=> 'admin', //接收者用戶名,可為數組群發:array('admin','abc')
 *		'toUid'		=> 1,		//接收者uid,可為數組群發:array(1,2),當與 toUser 同時存在時，自然失效
 *		'fromUid'	=> 2,		//發送者UID,與fromUser同時存在才有效 (可選,默認為'0')
 *		'fromUser'	=> 'pwtest',//發送者用戶名,與fromUid同時存在才有效(可選,默認為'SYSTEM')
 *		'subject'	=> 'Test',	//消息標題
 *		'content'	=> '~KO~',	//消息內容
 *		'other'		=> array()	//其他信息變量
 *	);
 * @return boolean 返回消息發送是否完成
 */
function pwSendMsg($msg) {
	global $db,$timestamp;
	if ((!$msg['toUser'] && !$msg['toUid']) || !$msg['subject'] || !$msg['content']) {
		return false;
	}
	$msg['subject'] = getLangInfo('writemsg',$msg['subject'],$msg);
	$msg['content'] = getLangInfo('writemsg',$msg['content'],$msg);
	$userService = L::loadClass('UserService', 'user'); /* @var $userService PW_UserService */
	$usernames = ($msg['toUser']) ? $msg['toUser'] : $userService->getUserNameByUserId($msg['toUid']);
	$usernames = (is_array($usernames)) ? $usernames : array($usernames);
	if(!$msg['fromUid'] || !$msg['fromUser']){
		M::sendNotice($usernames,array('title' => $msg['subject'],'content' => $msg['content']));
	}else{
		M::sendMessage($msg['fromUid'],$usernames,array('create_uid'=>$msg['fromUid'],'create_username'=>$msg['fromUser'],'title' => $msg['subject'],'content' => $msg['content']));
	}
	return true;
}

function delete_msgc($ids = null) {
	return true;
}

function send_msgc($msg,$isNotify=true) {
	global $db;
	if (!is_array($msg)) return;
	$uid = $sql = $mc_sql = array();
	$userService = L::loadClass('UserService', 'user'); /* @var $userService PW_UserService */
	foreach ($msg as $k => $v) {
		$username = $userService->getUserNameByUserId($v[0]);
		if (!$username) continue;
		M::sendNotice(
			array($username),
			array(
				'title' => $v[6],
				'content' => $v[7]
			)
		);
	}
}
?>