<?php
!function_exists('readover') && exit('Forbidden');
$filename=D_P.'data/bbscache/today.php';
$dbtdsize=100;
if(file_exists($filename)){
	$todaydata=readover($filename);
	if($offset=strpos($todaydata,"\n".$windid."\t")){/*使用精確匹配 必須是"\n".$windid."\t"*/
		$offset+=1;
		if($fp=@fopen($filename,"rb+")){
			flock($fp,LOCK_EX);
			list($node,$yestime)=nodeinfo($fp,$dbtdsize,$offset);/*修改頭結點*/
			$nowfp=$offset/($dbtdsize+1);
			if("$nowfp"!=$node && $node!=''){
				fputin($fp,$node,$dbtdsize,$nowfp);/*修改頭結點指向的數據段*/
				list($oldprior,$oldnext)=fputin($fp,$nowfp,$dbtdsize,'node',$node);/*修改需要更新的數據*/
				if($oldprior!='node'){
					fputin($fp,$oldprior,$dbtdsize,'M',$oldnext);/*修改前一結點的後趨*/
				}
				if($oldnext!='NULL' && $oldprior!='node'){
					fputin($fp,$oldnext,$dbtdsize,$oldprior);/*修改後一結點的前趨*/
				}
			}
			fclose($fp);
		}
	}else{
		$offset=filesize($filename);
		if($fp=@fopen($filename,"rb+")){
			flock($fp,LOCK_EX);
			list($node,$yestime)=nodeinfo($fp,$dbtdsize,$offset);
			if($node!=''){/*修改頭結點*/
				$nowfp=$offset/($dbtdsize+1);
				if($node!='NULL') {
					fputin($fp,$node,$dbtdsize,$nowfp);
				}
				if($node!=$nowfp) fputin($fp,$nowfp,$dbtdsize,'node',$node,Y);/*添加數據*/
			}
			fclose($fp);
		}
	}
}
if($yestime!=$tdtime) {
	//* P_unlink($filename);
	pwCache::deleteData($filename);
	pwCache::setData($filename,str_pad("<?php die;?>\tNULL\t$tdtime\t",$dbtdsize)."\n");/*24小時初始化一次*/
}
function fputin($fp,$offset,$dbtdsize,$prior='M',$next='M',$ifadd='N')
{
	$offset=$offset*($dbtdsize+1);/*將行數轉換成指針偏移量*/
	fseek($fp,$offset,SEEK_SET);
	if($ifadd=='N'){
		$iddata=fread($fp,$dbtdsize);
		$idarray=explode("\t",$iddata);
		fseek($fp,$offset,SEEK_SET);
	}
	if($next!='M' && $prior!='M'){/*說明這一數據是被更改的數據段.需要對其他輔助信息進行更改*/
		global $windid,$timestamp,$onlineip,$winddb;
		$idarray[0]=$windid;$idarray[3]=$winddb['regdate'];
		if($ifadd!='N') $idarray[4]=$timestamp;
		$idarray[5]=$timestamp;$idarray[6]=$onlineip;$idarray[7]=$winddb['postnum'];$idarray[8]=$winddb['rvrc'];
	}
	if($prior=='M') $prior=$idarray[1];
	if($next=='M') $next=$idarray[2];
	$data="$idarray[0]\t$prior\t$next\t$idarray[3]\t$idarray[4]\t$idarray[5]\t$idarray[6]\t$idarray[7]\t$idarray[8]\t";
	$data=str_pad($data,$dbtdsize)."\n";/*定長寫入*/
	fwrite($fp,$data);
	return array($idarray[1],$idarray[2]);/*傳回數據更新前的上一結點和下一結點*/
}
function nodeinfo($fp,$dbtdsize,$offset)
{
	$offset=$offset/($dbtdsize+1);
	$node=fread($fp,$dbtdsize);
	$nodedb=explode("\t",$node);/*頭結點在第二個數據段*/
	if(is_int($offset)){
		$nodedata=str_pad("<?php die;?>\t$offset\t$nodedb[2]\t",$dbtdsize)."\n";
		fseek($fp,0,SEEK_SET);/*將指針放於文件開頭*/
		fwrite($fp,$nodedata);
		return array($nodedb[1],$nodedb[2]);
	}else{
		return '';
	}
}
?>