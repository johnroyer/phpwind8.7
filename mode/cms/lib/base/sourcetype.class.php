<?php
!defined('M_P') && exit('Forbidden');

class PW_SourceType /*Abstract class*/ {
	/**
	 * 獲取數據，並且處理articleModule
	 * @param $articleModule
	 * @param $sourceId
	 */
	function cookArticleModule(&$articleModule,$sourceId) {
		$data = $this->getSourceData($sourceId);
		if (!$data) return $articleModule;
		foreach ($this->getSourceMap() as $key => $value) {
			$articleModule->{$value} = $data[$key];
		}
		$articleModule->setSourceType($this->getSourceType());
		$articleModule->setSourceId($sourceId);

		return $articleModule;
	}
	/**
	 * 獲取數據和ArticleModule的映射關係
	 */
	function getSourceMap() {
		return array(
			'subject' => 'subject',
			'content' => 'content',
			'descrip' => 'descrip',
			'author' => 'author',
			'frominfo' => 'fromInfo',
		);
	}
	/**
	 * 通過數據地址
	 * @param $sourceId
	 */
	function getSourceUrl($sourceId) /*Abstract function*/ {
		
	}
	/**
	 * 通過id獲取數據
	 * @param $sourceId
	 */
	function getSourceData($sourceId) /*Abstract function*/ {
		
	}
	/**
	 * 獲取數據類型
	 */
	function getSourceType() /*Abstract function*/ {
		
	}
}