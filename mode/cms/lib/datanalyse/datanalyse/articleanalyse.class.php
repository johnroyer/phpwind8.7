<?php
!defined('P_W') && exit('Forbidden');
include_once (R_P . 'lib/datanalyse/datanalyse.base.php');

class PW_Articleanalyse extends PW_Datanalyse {
	var $pk = 'article_id';
	var $actions = array();

	function PW_Articleanalyse() {
		$this->__construct();
	}

	function __construct() {
		parent::__construct();
	}

	/**
	 * 獲得根據類別評價類型
	 * @return array
	 */
	function _getExtendActions() {
		$columnservice = $this->_getColumnService();
		/* @var $columnservice PW_ColumnService */
		$columns = $columnservice->getAllOrderColumns();
		$_tmp = array();
		foreach ((array) $columns as $column) {
			$_tmp[] = 'article_' . $column['column_id'];
		}
		return $_tmp;
	}
	
	/**
	 * 根據文章ID數組獲取熱門文章列表
	 * @return array
	 */
	function _getHotArticlesByTags() {
		if (empty($this->tags)) return array();
		$articleDB = C::loadDB('article');
		/* @var $articleDB PW_ArticleDB */
		return $articleDB->getHotArticlesByIds($this->tags);
	}

	/**
	 * 根據日誌ID數組獲得日誌信息
	 * @return array
	 */
	function _getDataByTags() {
		if (empty($this->tags)) return array();
		$articleDB = C::loadDB('article');
		/* @var $articleDB PW_ArticleDB */
		return $articleDB->getArticlesByIds($this->tags);
	}

	function _getColumnService() {
		return C::loadClass('columnservice');
	}

}
?>