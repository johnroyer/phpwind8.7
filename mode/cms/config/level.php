<?php
!defined('P_W') && exit('Forbidden');
return array(
	'title' => '文章權限',
	'field' => array(
		'cms_post' => array(
			'name' => '發表文章',
			'type' => 'radio',
			'value'=> array(
				'1'=>'開啟',
				'0'=>'關閉'
			),
			'descrip' => '開啟後，普通會員用戶組的用戶可以投稿，管理權限用戶組的用戶可以發表文章'
		),

		'cms_replypost' => array(
			'name' => '評論文章',
			'type' => 'radio',
			'value'=> array(
				'1'=>'開啟',
				'0'=>'關閉'
			),
			'descrip' => '開啟後，該用戶組可以對文章進行評論'
		),
	),
);
?>