<?php
!defined('P_W') && exit('Forbidden');

$purview['area_channel_manage'] = array('頻道管理', "$admin_file?adminjob=mode&admintype=area_channel_manage");
$purview['area_pushdata'] = array('內容推送', "$admin_file?adminjob=mode&admintype=area_pushdata");
$purview['area_level_manage'] = array('權限管理 ', "$admin_file?adminjob=mode&admintype=area_level_manage");
$purview['area_selecttpl'] = array('模板中心', "$admin_file?adminjob=mode&admintype=area_selecttpl");
$purview['area_static_manage'] = array('靜態配置', "$admin_file?adminjob=mode&admintype=area_static_manage");
$purview['area_module'] = array('模塊管理', "$admin_file?adminjob=mode&admintype=area_module");
//$purview['area_page_manage'] = array('頁面管理', "$admin_file?adminjob=mode&admintype=area_page_manage");
?>