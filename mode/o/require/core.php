<?php
!defined('P_W') && exit('Forbidden');
class C extends PW_BaseLoader {

	/**
	 * 類文件的加載入口
	 * 
	 * @param string $className 類的名稱
	 * @param string $dir 目錄：末尾不需要'/'
	 * @param boolean $isGetInstance 是否實例化
	 * @return mixed
	 */
	function loadClass($className, $dir = '', $isGetInstance = true) {
		return parent::_loadClass($className, 'mode/o/lib/' . parent::_formatDir($dir), $isGetInstance);
	}

	/**
	 * 加載db類
	 * @param $className
	 */
	function loadDB($dbName, $dir = '') {
		parent::_loadBaseDB();
		return C::loadClass($dbName . 'DB', parent::_formatDir($dir) . 'db');
	}
}
