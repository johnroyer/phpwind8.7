<?php
!defined('P_W') && exit('Forbidden');

/**
*  群組自有分類處理
* 
*  @package 群組
*  @author zhudong
*/

class GroupStyle {
	
	var $db;

	/** 
	*  Construct
	*/

	function GroupStyle(){
		global $db;
		$this->db =& $db;
	}

	/** 
	*  獲取所有的分類
	* 
	*  @return $styledb array 分類的數組 
	*/

	function getAllStyles(){

		$query = $this->db->query("SELECT * FROM pw_cnstyles");
		while ($rt = $this->db->fetch_array($query)) {
			$styledb[$rt['id']] = $rt;
		}
		return $styledb;

	}


	/** 
	*  獲取所有開啟的分類
	* 
	*  @return $styledb array 分類的數組 
	*/

	function getOpenStyles(){

		$query = $this->db->query("SELECT * FROM pw_cnstyles WHERE ifopen=1");
		while ($rt = $this->db->fetch_array($query)) {
			$styledb[$rt['id']] = $rt;
		}
		return $styledb;

	}


	/** 
	*  獲取一級分類id
	* 
	*  @return $styledb array 分類id的數組 
	*/

	function getFirstGradeStyleIds(){

		$query = $this->db->query("SELECT * FROM pw_cnstyles WHERE upid='0'");
		while ($rt = $this->db->fetch_array($query)) {
			$styledb[] = $rt['id'];
		}
		return $styledb;

	}
	
	function getFirstGradeStyles(){
		$temp = array();
		$query = $this->db->query("SELECT * FROM pw_cnstyles WHERE upid='0'");
		while ($rt = $this->db->fetch_array($query)) {
			$temp[$rt['id']] = $rt;
		}
		return $temp;

	}

	

	/** 
	*  通過上級ID獲取分類
	* 
	*  @param  $upids   array 上級分類ID數組
	*  @return $styledb array 分類的數組 
	*/

	function getGradeStylesByUpid($upids){
		
		if(empty($upids)) return array();

		$query = $this->db->query("SELECT * FROM pw_cnstyles WHERE upid IN (".S::sqlImplode($upids).") ORDER BY vieworder ASC");
		while ($rt = $this->db->fetch_array($query)) {
			//if($rt['ifopen'] == 0) continue; 
			$styledb[$rt['upid']][$rt['id']] = $rt;
		}
		return $styledb;

	}

	/** 
	*  添加二級分類
	* 
	*  @param  $newSubStyle   array 新添加的二級分類的數組
	*  @return null
	*/

	function addNewSubStyle($newSubStyle) {
		$this->db->update("INSERT INTO pw_cnstyles(cname,ifopen,upid,vieworder) VALUES ". S::sqlMulti($newSubStyle));
	}
}
?>