<?php
!defined('A_P') && exit('Forbidden');
if (!$db_md_ifopen)  Showmsg('勳章功能未開啟'); 
/* 勳章前台顯示 */
S::gp(array('a')); 
!$winduid && Showmsg('not_login');
(!$a || !in_array($a, array('apply', 'my', 'all', 'behavior'))) && $a = 'all';
$basename =  'apps.php?q=' . $q;
$current[$a] = 'class="current"'; 
$typeArr = array('系統發放', '自動發放', '手動發放');
if ($a == 'my' || $a == 'all') {
	require_once S::escapePath($appEntryBasePath . 'action/my.php'); //我的勳章
} elseif ($a == 'apply') {
	require_once S::escapePath($appEntryBasePath . 'action/apply.php'); //我的勳章
} elseif ($a == 'behavior') {
	require_once S::escapePath($appEntryBasePath . 'action/behavior.php'); //用戶行為AJAX提交
}    


 