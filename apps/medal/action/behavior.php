<?php 
!defined('A_P') && exit('Forbidden');
/* 用戶行為 */
define('AJAX', 1);
S::gp(array('id', 'atype'));      
$id = (int) $id;      
if (!$winduid || $id < 1 || !$db_md_ifopen) exit;
//獲取勳章信息、用戶已經獲取的勳章信息、已經申請的勳章信息
$medalService = L::loadClass('MedalService', 'medal'); /* @var $medalService PW_MedalService */
$medalInfo = $medalService->getMedal($id);
$userMedal = $medalService->getUserMedals($winduid); //獲取會員的勳章
$isUserApply = $medalService->getApplyByUidAndMedalId($winduid,$medalInfo['medal_id']); //用戶已經申請
$userMedalInfo = $userMedalIdArr = array();
if (is_array($userMedal)) {
	foreach ($userMedal as $v) {
		$userMedalIdArr[] = $v['medal_id']; //存放用戶勳章ID，判斷用戶是否申請成功
		$userMedalInfo[$v['medal_id']] = $v;
	}
}
//HTML組裝
if ($medalInfo['type'] == 2) { //手動發放
	if ($db_md_ifapply) {
		$ifApply = (in_array($groupid, (array)$medalInfo['allow_group']) || !$medalInfo['allow_group']) ? '<a href="javascript:;" onclick="sendmsg(\'apps.php?q=medal&a=apply&id='.$id.'\',\'\',null,function(){setTimeout(function(){window.location.reload();},10)})" class="medal_pop_btn fr" >申請勳章</a>' : '你所在用戶組無法領取此勳章，請盡快升級。';
	} else {
		$ifApply = '';
	}
	
	$otherHtml = ($isUserApply) ? '<a href="javascript:" class="medal_pop_bt fr">已申請</a>' : $ifApply;
	if (in_array($id, $userMedalIdArr)) {
		$awardMedal = $medalService->getAwardMedalByUidAndMedalId($winduid, $id); //
		if ($medalInfo['confine'] == 0) {
			$otherHtml = '<p class="mb5">獲得時間：' . date('Y-m-d', $awardMedal['timestamp']) . '</p><p>有效期：永久</p>';
		} else {
			$otherHtml = '<p class="mb5">獲得時間：' . date('Y-m-d', $awardMedal['timestamp']) . '</p><p><span class="mr20">有效期：' . $medalInfo['confine'] . ' 天</span>'. '到期時間：' . date('Y-m-d', $awardMedal['deadline']) . '</p>';
		}
	}
} else { //自動發放
	$attention = $notice = $nowhave = '';
	if (in_array($medalInfo['associate'], array('continue_login', 'continue_post', 'continue_thread_post'))) {
		//獲取用戶行為信息
		$behaviorService = L::loadClass('behaviorService', 'user'); /* @var $medalService PW_MedalService */ 
		$behavior = $behaviorService->getBehaviorStatistic($winduid, $medalInfo['associate']);
		$num = ($behavior) ? $behavior['num'] : 0;
		$needNum = $medalInfo['confine'] - $num;
		if ($medalInfo['associate'] == 'continue_login') {
			$attention = '<p class="gray">注意：1天不登錄，現有天數會減1</p>';
			$notice = ($needNum > 0) ? '你還需連續登錄'.$needNum.'天' : '重新登錄一次即可獲得此勳章';
			$notice = $notice . '（現有天數：'.$num.'）';
			$nowhave = '（現有連續登錄天數：'.$num.'）';
		} elseif ($medalInfo['associate'] == 'continue_thread_post') {
			$attention = '<p class="gray">注意：1天不登錄，現有連續登錄天數會減1</p>';
			$notice = ($needNum > 0) ? '你還需連續發主題'.$needNum.'天' : '再發1主題帖即可獲得此勳章';
			$notice = $notice . '（現有天數：'.$num.'）';
			$nowhave = '（現有連續主題天數：'.$num.'）';
		} elseif ($medalInfo['associate'] == 'continue_post') {
			$attention = '<p class="gray">注意：1天不發帖，現有天數會減1</p>';
			$notice = ($needNum > 0) ? '你還需連續發帖'.$needNum.'天' : '再發1帖即可獲得此勳章';
			$notice = $notice . '（現有天數：'.$num.'）';
			$nowhave = '（現有連續發帖天數：'.$num.'）';
		}
	} else {
		$userService = L::loadClass('UserService', 'user'); /* @var $userService PW_UserService*/
		$userInfo = $userService->get($winduid, true, true, true);
		if ($medalInfo['associate'] == 'post') {
			$needNum = $medalInfo['confine'] -$userInfo['postnum'];
			$notice = ($needNum > 0) ? '你還需發'.$needNum.'個帖子' : '再發1帖即可獲得此勳章';
			$notice = $notice . '（現有帖數：'.$userInfo['postnum'].'）';
		} elseif ($medalInfo['associate'] == 'fans') {
			$needNum = $medalInfo['confine'] -$userInfo['fans'];
			$notice = ($needNum > 0) ? '你還需增加'.$needNum.'個粉絲' : '再增加1個粉絲即可獲得此勳章';
			$notice = $notice . '（現有粉絲數：'.$userInfo['fans'].'）';
		} elseif ($medalInfo['associate'] == 'shafa') {
			$needNum = $medalInfo['confine'] -$userInfo['shafa'];
			$notice = ($needNum > 0) ? '你還需搶'.$needNum.'個沙發' : '再搶1個沙發即可獲得此勳章';
			$notice = $notice . '（現有沙發數：'.$userInfo['shafa'].'）';
		}
	}
	if (in_array($id, $userMedalIdArr)) {
		$notice = '<p class="medal_pop_tips mb5">'.'恭喜獲得'.$medalInfo['name'].'勳章'.$nowhave.'</p>';
	} else {
		//沒有有用戶組權限的情況
		if ($medalInfo['allow_group'] && !in_array($groupid, (array)$medalInfo['allow_group'])) {
			$notice = '<p class="gray">你所在用戶組無法領取此勳章，請盡快升級。</p>';
		}
		$notice =  '<p class="medal_pop_tips mb5">'.$notice.'</p>';
	}
	$otherHtml = $notice . $attention;
}
$html = '<div class="medal_pop">
<span class="fl"><span class="medal_pop_angle"></span></span>
<div class="medal_pop_cont">
<div class="medal_pop_top">
<p class="mb10"><img src="'.$medalInfo['smallimage'].'" width="30" height="30" id="medal_img" align="absmiddle" class="mr10" /><span class="b s5 mr10">'.$medalInfo['name'].'</span><span class="s6">('.$typeArr[$medalInfo['type']].')</span></p>
<p class="s5">'.$medalInfo['descrip'].'</p>
</div>
<div class="medal_pop_bot cc" >
'.$otherHtml.'
</div>
</div>
</div>';
echo "success\t".$html;
ajax_footer();
