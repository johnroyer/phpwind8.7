<?php 
!defined('A_P') && exit('Forbidden');
if (!$space =& $newSpace->getInfo()) {
	Showmsg('您訪問的空間不存在!');
}

/* 前台勳章頁面 */
$medalService = L::loadClass('MedalService', 'medal'); /* @var $medalService PW_MedalService */
if ($a == 'all') {
	$userApply = $medalService->getUserApplys($winduid);
	$awardMedal = $medalService->getAwardMedalUsers(array(),1,10);//動態播放列表
	$medalAll  = $medalService->getAllMedals();
}
$medalTemp = $medalService->getUserMedals($winduid,'all'); //獲取會員已經擁有的勳章
$medalCount = count($medalTemp); //總數
$userMedalId = $medal = array();
foreach ($medalTemp as $v) {
	if ($v['is_have'] == 1) {
		$userMedal[$v['medal_id']] = $v;
	}
	$v['have_apply'] = $medalService->getApplyByUidAndMedalId($winduid,$v['medal_id']);
	$medal[$v['medal_id']] = $v;
}

$userMedalCount = count($userMedal); //開啟數
if ($a == 'my') $medal = $userMedal;

require_once PrintEot('m_medal'); 
pwOutPut();

?>