function delActivity(id){/*刪除活動*/
	ajax.send('apps.php?q=activity&a=delactivity&action=delactivity&ajax=1&id='+id,'',function(){
		var rText = ajax.request.responseText.split('\t');
		if (rText[0] == 'success') {
			var element = document.getElementById('activity_'+id);
			if (element) {
				element.parentNode.removeChild(element);
				showDialog('success','刪除成功!', 2);
			} else {
				window.location.reload();
			}
		} else if (rText[0] == 'mode_o_delactivity_permit_err') {
			showDialog('error','您沒有權限刪除別人的活動', 2);
		} else {
			showDialog('error','刪除失敗', 2);
		}
	});
}