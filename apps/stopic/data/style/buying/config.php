<?php
!defined('P_W') && exit('Forbidden');
return array(
	'name'	=>'團購',
	'banner'=>'apps/stopic/data/style/buying/banner.jpg',
	'layout_set' => array(
		'bgcolor'		=> '#000000',
		'areabgcolor'	=> '#ffffff',
		'fontcolor'		=> '#370123',
		'navfontcolor'	=> '#ffffff',
		'navbgcolor'	=> '#000000',
		'othercss'		=> '.wrap{width:960px;margin:0 auto 0;overflow:hidden;}/*專題內容框架*/
#main{padding:10px;}/*專題內邊距*/
.zt_nav li{float:left;line-height:35px; font-size:14px;margin:0 10px; white-space:nowrap;}/*導航樣式*/
.itemDraggable{border:1px solid #e4e4e4;margin-top:10px;overflow:hidden;}/*模型外邊框*/
.itemDraggable .itemHeader{background:#ffeae1 url(apps/stopic/data/style/buying/h.png) right 0 repeat-x;padding:10px 10px 0; font-weight:700;color:#dc4f12;}/*標題欄*/
.itemDraggable .itemContent{padding:4px 10px;}/*模型內邊距*/
.itemDraggable .itemContent li{line-height:24px;}/*列表行高*/
'
	),
);
?>