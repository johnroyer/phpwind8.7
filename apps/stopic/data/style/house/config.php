<?php
!defined('P_W') && exit('Forbidden');
return array(
	'name'	=>'房產',
	'banner'=>'apps/stopic/data/style/house/banner.jpg',
	'layout_set' => array(
		'bgcolor'		=> '#2e0307',
		'areabgcolor'	=> '#ffffff',
		'fontcolor'		=> '#000000',
		'navfontcolor'	=> '#ffffff',
		'navbgcolor'	=> '#862c07',
		'othercss'		=> '.wrap{width:960px;margin:0 auto 0;overflow:hidden;}/*專題內容框架*/
#main{padding:10px;}/*專題內邊距*/
.zt_nav li{float:left;line-height:35px; font-size:14px;margin:0 10px; white-space:nowrap;}/*導航樣式*/
.itemDraggable{border:1px solid #c9c9c9;margin-top:10px;overflow:hidden;}/*模型外邊框*/
.itemDraggable .itemHeader{border-bottom:1px solid #c9c9c9;background:#a33f1b url(apps/stopic/data/style/house/h.png) right 0 repeat-x;padding:4px 10px; font-weight:700;color:#fff;}/*標題欄*/
.itemDraggable .itemContent{padding:4px 10px;}/*模型內邊距*/
.itemDraggable .itemContent li{line-height:24px;}/*列表行高*/
'
	),
);
?>