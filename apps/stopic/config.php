<?php
/**
 * 專題配置文件
 * 
 * 包含配置項：
 * <code>
 *   bgUploadPath 背景圖片上傳路徑
 *   bgBaseUrl 背景圖片基礎url
 *   bgDefalutPath 背景圖片默認存放目錄
 *   bgDefalutUrl 背景圖片默認url
 *   layoutPath 佈局數據存放目錄
 *   layoutBaseUrl 佈局基礎url
 *   stylePath 風格樣式數據存放目錄
 *   styleBanner 風格樣式橫幅圖片文件名
 *   styleMiniPreview 風格樣式縮略圖文件名
 *   styleBaseUrl 風格樣式基礎url
 *   layoutConfig 佈局配置
 *   layoutTypes 佈局類型列表
 *   layout_set 默認風格樣式的css配置
 *   htmlSuffix 專題保存文件的擴展名
 *   htmlDir 專題保存目錄
 *   htmlUrl 專題url
 *   blockTypes 模塊類型列表
 * </code>
 * 
 * @package STopic
 */

!defined('P_W') && exit('Forbidden');

return array(
	"bgUploadPath" => R_P . (isset($GLOBALS['db_attachname']) && '' != $GLOBALS['db_attachname'] ? $GLOBALS['db_attachname'] : 'attachment') . "/stopic/",
	"bgBaseUrl" => $GLOBALS['db_bbsurl'] . "/attachment/stopic/",

	"bgDefalutPath" => A_P . "data/uploadbg/",
	"bgDefalutUrl" => $GLOBALS['db_bbsurl'] . "/apps/stopic/data/uploadbg/",

	"layoutPath" => A_P."data/layout/",
	"layoutBaseUrl" => $GLOBALS['db_bbsurl'] . "/apps/stopic/data/layout/",

	"stylePath" => A_P."data/style/",
	"styleBanner" => "banner.jpg",
	"stylePreview" => "preview.jpg",
	"styleMiniPreview" => "mini_preview.jpg",
	"styleBaseUrl"	=> $GLOBALS['db_bbsurl'] . "/apps/stopic/data/style/",

	"layoutConfig" => array(
		"logo" => "logo.png",
		"html" => "layout.htm",
	),
	"layoutTypes" => array(
		"type1v0" => "直列",
		"type1v1" => "1:1",
		"type1v2" => "1:2",
		"type2v1" => "2:1",
		"type1v1v1" => "1:1:1",
	),
	"layout_set" => array(
		'bannerurl'		=> $GLOBALS['db_bbsurl'] . '/apps/stopic/data/style/wedding_pink/banner.jpg',
		'bgcolor'		=> '#cd6587',
		'areabgcolor'	=> '#ffffff',
		'fontcolor'		=> '#e46882',
		'navfontcolor'	=> '#ffffff',
		'navbgcolor'	=> '#ce5683',
		"othercss"		=> <<<EOT
.wrap{width:960px;margin:0 auto 0;overflow:hidden;}/*專題內容框架*/
#main{padding:10px;}/*專題內邊距*/
.zt_nav li{float:left;line-height:35px; font-size:14px;margin:0 10px; white-space:nowrap;}/*導航樣式*/
.itemDraggable{border:1px solid #eaebe6;margin-bottom:10px;overflow:hidden;}/*模型外邊框*/
.itemDraggable .itemHeader{background:#ce5683 url(apps/stopic/data/style/wedding_pink/h-pink.png) right 0 repeat-x;padding:4px 10px; font-weight:700;color:#fff;}/*標題欄*/
.itemDraggable .itemContent{padding:4px 10px;}/*模型內邊距*/
.itemDraggable .itemContent li{line-height:24px;}/*列表行高*/
EOT
	),

	'htmlSuffix'=>'.html',

	"htmlDir" => R_P.('' != $GLOBALS['db_stopicdir'] ? $GLOBALS['db_stopicdir'] : $GLOBALS['db_htmdir'].'/stopic'),
	"htmlUrl" => $GLOBALS['db_bbsurl'].'/'.('' != $GLOBALS['db_stopicdir'] ? $GLOBALS['db_stopicdir'] : $GLOBALS['db_htmdir'].'/stopic'),
	
	"blockTypes" => array(
		"banner" => "頭部橫幅",
		"nvgt" => "導航",
		"thrd" => "帖子列表",
		"thrdSmry" => "帖子摘要",
		"pic" => "圖片",
		"picTtl" => "圖片及標題",
		"picArtcl" => "圖文混排",
		"picPlyr" => "圖片播放器",
		"spclTpc" => "交互主題",
		"html" => "自定義代碼",
		"comment" => "專題評論",
	),
);

?>