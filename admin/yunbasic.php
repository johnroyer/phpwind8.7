<?php
require_once (R_P . 'lib/cloudwind/cloudwind.class.php');
$_checkService = CloudWind::getPlatformCheckServerService ();
$cloudstatus = $_checkService->checkCloudWind ();
list ( $bbsname, $bbsurl, $bbsversion, $cloudversion ) = $_checkService->getSiteInfo ();
CLOUDWIND_SECURITY_SERVICE::gp ( array ('step' ) );
$step = (! $step && $cloudstatus < 9) ? (($cloudstatus == 1) ? 5 : (($cloudstatus == 3) ? 3 : (empty ( $step ) ? 1 : $step))) : $step;
if ($step == 1) {
	//show agreement
} elseif ($step == 2) {
	CLOUDWIND_SECURITY_SERVICE::gp ( 'agree' );
	if ($agree != 1)
		Showmsg ( '你沒有同意《phpwind雲服務使用協議》', $basename . '&step=1' );
	
	if (! $_checkService->checkHost ())
		Showmsg ( '無法連接雲服務，請檢查網絡是否為本地環境', $basename . '&step=1' );
	
	if (! $_checkService->getServerStatus ()) {
		list ( $fsockopen, $parse_url, $isgethostbyname, $gethostbyname ) = $_checkService->getFunctionsInfo ();
		list ( $searchHost, $searchIP, $searchPort, $searchPing ) = $_checkService->getSearchHostInfo ();
		list ( $defendHost, $defendIp, $defendPort, $defendPing ) = $_checkService->getDefendHostInfo ();
	} else {
		$step = 3;
	}
} elseif ($step == 3) {
	if (! $_checkService->getServerStatus ())
		Showmsg ( '環境檢測末通過，請聯繫論壇空間提供商解決' );
} elseif ($step == 4) {
	CLOUDWIND_SECURITY_SERVICE::gp ( array ('siteurl', 'sitename', 'bossname', 'bossphone' ) );
	
	if (! $siteurl || ! $sitename || ! $bossname || ! $bossphone)
		Showmsg ( '站點信息請填寫完整', $basename . '&step=3' );
	
	if (! ($marksite = $_checkService->markSite ()))
		Showmsg ( '雲服務驗證失敗，請重試', $basename . '&step=3' );
	
	if (! CloudWind::yunApplyPlatform ( $siteurl, $sitename, $bossname, $bossphone, $marksite )) {
		$marksite = $_checkService->markSite ( false );
		Showmsg ( '申請雲服務失敗，請檢查網絡或重試', $basename . '&step=3' );
	}
	(is_null ( $db_yun_model )) && $_checkService->setYunMode ( array () );
	$step = 5;
} else {
	$yundescribe = $_checkService->getYunDescribe ();
}
include PrintEot ( 'yunbasic' );

